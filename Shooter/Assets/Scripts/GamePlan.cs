﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class GamePlan : MonoBehaviour 
{
	static GamePlan instance__;

	static public GamePlan instance { get { return instance__;}}

	public Camera mainCamera;

	// Use this for initialization
	void Start () 
	{
		instance__ = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnDrawGizmos()
	{
		if (mainCamera == null )
			return;

		Rect pixelRect = mainCamera.pixelRect;// = new Rect((Screen.width - w) / 2, (Screen.height - h) / 2, w, h);
	//	Debug.Log(pixelRect);

		float distance = transform.localPosition.z;

		if (pixelRect.width == 0 || pixelRect.height == 0)
		{
			Debug.LogWarning("Invalid Pixel rect size, Please Play once the scene...");
			return;
		}


		Gizmos.color = Color.yellow;

		Vector3 p1 = mainCamera.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMin, distance));
		Vector3 p2 = mainCamera.ScreenToWorldPoint(new Vector3(pixelRect.xMax, pixelRect.yMin, distance));
		Vector3 p3 = mainCamera.ScreenToWorldPoint(new Vector3(pixelRect.xMax, pixelRect.yMax, distance));
		Vector3 p4 = mainCamera.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMax, distance));

		Gizmos.DrawLine(p1, p2);
		Gizmos.DrawLine(p2, p3);
		Gizmos.DrawLine(p3, p4);
		Gizmos.DrawLine(p4, p1);

	}


}
