﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



[ExecuteInEditMode]
public class Game : MonoBehaviour 
{
	static public Game instance;

	[System.Serializable]
	public class Prefabs
	{
		public GameObject[] explosions;

		public Points pointsAnim;
	}
	public Prefabs prefabs;

	[System.Serializable]
	public class MainGameObjects
	{
		public MainGUI gui;
		public Scroller Decors;
	}
	public MainGameObjects mainGameObjects;
	
	JantilShooter currentJantil;
	public static JantilShooter jantil { get { return instance.currentJantil; } }


	int NbLifes_ = 0;
	public int NbLifes
	{
		get { return NbLifes_; }
		set 
		{
			NbLifes_ = value;
			if (mainGameObjects.gui != null)
				mainGameObjects.gui.gameGUI.setNbLifes(value);
		}
	}


	int points_ = 0;
	public int points
	{
		get { return points_; }
		set
		{
			points_ = value;
			if (mainGameObjects.gui != null)
				mainGameObjects.gui.gameGUI.setPoints(value);
		}
	}

	int currentLevel_;
	public int currentLevel
	{
		get { return currentLevel_; }
		set
		{
			currentLevel_ = value;
		}
	}


	int NextLevelStep_;
	public int NextLevelStep
	{
		get { return NextLevelStep_; }
		set
		{
			NextLevelStep_ = value;
		}
	}


	[System.Serializable]
	public class GameParams
	{
		public int StartLifes = 8;

		[Space(10)]
		public int startLevel = 0;

		[Message]
		public string SelectedLevelMsg;

		[Space(10)]
		public bool autoStart = false;

		public int autoStartHero = 0;

		[Message]
		public string SelectedHeroMsg;

		

		public float speedDecorsIddle = 20;

		public bool noGameOver = false;
		public bool debugGUI = false;

		public FramePositon debugPos = new FramePositon(FramePositon.Anchor.BottomLeft, new Rect(10, 10, 170, 230));
	}

	public void OnValidate()
	{
		autoListLevels();
		if (levels == null) return;
		if (levels.Length== 0) return;

		if (gameParams.startLevel < 0)
			gameParams.startLevel = 0;
		else if (gameParams.startLevel >= levels.Length)
			gameParams.startLevel = levels.Length - 1;

		gameParams.SelectedLevelMsg = "Level : " + levels[gameParams.startLevel].name;



		if (gameParams.autoStartHero < 0)
			gameParams.autoStartHero = 0;
		else if (gameParams.autoStartHero >= jantils.Length)
			gameParams.autoStartHero = jantils.Length - 1;


		gameParams.SelectedHeroMsg = "Hero : " + jantils[gameParams.autoStartHero].name;




	}

	public GameParams gameParams;
	
	GameLevel[] levels;
	void autoListLevels()
	{
		levels = gameObject.GetComponentsInChildren<GameLevel>();
	}

	[Array]
	public JantilShooter[] jantils;


	[Button("autoListHeroes")]
	public bool m_autoListHeroes;
	void autoListHeroes()
	{
		jantils = gameObject.GetComponentsInChildren<JantilShooter>(true);
	}

	
	public Transform startPosition;

	

	// Use this for initialization
	void Start () 
	{
		instance = this;
		autoListLevels();
		if (Application.isPlaying)
		{
			stopGame();
			mainGameObjects.gui.gameObject.SetActive(true);

			if (gameParams.autoStart)
			{
				startGame_(gameParams.autoStartHero);
			}
		}
	}

	public void stopGame()
	{
		mainGameObjects.Decors.Speed = gameParams.speedDecorsIddle;

		for (int i = 0; i < jantils.Length; i++)
			jantils[i].gameObject.SetActive(false);

		for (int i = 0; i < levels.Length; i++)
			levels[i].stop(true);
	}

	public static void startGame(int selectedHero)
	{
		instance.startGame_(selectedHero);
	}

	public static bool startGame(string heroName)
	{
		int selected = instance.findJantil(heroName);
		if (selected == -1) return false;

		startGame(selected);
		return true;
	}

	public int findJantil(string name)
	{
		if (jantils == null || jantils.Length == 0)
			return -1;

		for (int i = 0; i < jantils.Length; i++)
			if (jantils[i].name == name)
				return i;

		return -1;
	}

	void startGame_(int selectedJantil)
	{ 
		currentJantil = jantils[selectedJantil];
		currentJantil.gameObject.SetActive(true);
		currentJantil.transform.position = startPosition.position;

		points = 0;
		NbLifes = gameParams.StartLifes;
		NextLevelStep = 0;
		currentLevel = -1;

		mainGameObjects.gui.gotoGame();
		changeLevel(gameParams.startLevel);

	}

	void changeLevel(int indexLevel)
	{
		if (indexLevel < 0) return;

		if (currentLevel == indexLevel) return;


		if (indexLevel >= levels.Length)
		{
			Debug.LogError("wrong level " + indexLevel);
			return;
		}

		int prevLevel = currentLevel;
		if (prevLevel >= 0)
		{
			GameLevel curLevel = levels[prevLevel];
			curLevel.stop(false);
		}
		
		currentLevel = indexLevel;

		GameLevel level = levels[indexLevel];

		mainGameObjects.Decors.Speed = level.speedDecors;
		NextLevelStep += level.NextLevelPoints;
		mainGameObjects.gui.gameGUI.setNewLevel(indexLevel, level.name);
		level.start();
	}

	public void createExplosion(Vector3 pos, int addPts)       
	{
		
		if (prefabs.explosions.Length != 0)
		{
			int indexPrefab = Random.Range (0, prefabs.explosions.Length);
			if (prefabs.explosions[indexPrefab] != null)
			{
				GameObject theExplosion = (GameObject)GameObject.Instantiate(prefabs.explosions[indexPrefab]);
				theExplosion.transform.parent = transform;
				theExplosion.transform.position = pos;
			}
		}

		addPoint(pos, addPts);
	}

	public void explodeMe(GameObject source, int addPts)
	{
		createExplosion(source.transform.position, addPts);
		
		Destroy(source);
	}

	public static void addPoint(Vector3 pos, int add)
	{
		instance.addPoint__(pos, add);
	}

	void addPoint__(Vector3 pos, int add)
	{
		if (add == 0) return;

		if (prefabs.pointsAnim != null)
		{
			GameObject theGo = (GameObject)GameObject.Instantiate(prefabs.pointsAnim.gameObject);
			theGo.transform.position = pos;
			theGo.SetActive(true);
			Points pt = theGo.GetComponent<Points>();
			pt.points = add;
			pt.show();
		}

		points += add;

		if (points >= NextLevelStep)
		{
			int nextLevel = currentLevel + 1;
			if (nextLevel >= levels.Length)
			{
				Debug.LogWarning("Max level reached");
			}
			else
				changeLevel(nextLevel);
		}

		
		
	}

	public static void addLife(Vector3 pos, int add)
	{
		instance.addLife_(pos, add);
	}

	void addLife_(Vector3 pos, int add)
	{
		NbLifes += add;
	}

	public void touched()
	{
		NbLifes--;

		if (!gameParams.noGameOver && NbLifes == 0)
		{
			stopGame();
			mainGameObjects.gui.gotoGameOver(points, currentLevel + 1);
		}
	}

	
	void OnGUI()
	{
		if (!gameParams.debugGUI) return;

		Rect pos =  gameParams.debugPos.MainRc;
		GUI.Box(pos, "Debug GUI");
		pos = pos.reduce(gameParams.debugPos.padding);
		GUILayout.BeginArea(pos);

		GUILayout.Label("Level Index " + currentLevel);
		if (levels.Length > 0 && currentLevel < levels.Length)
		{
			GameLevel level = levels[currentLevel];
			GUILayout.Label("Name : " + level.name);
		}

		GUILayout.Label("Next Level Step " + NextLevelStep);

		if (GUILayout.Button("Prev Level"))
		{
			changeLevel(currentLevel - 1);
		}

		if (GUILayout.Button("next Level"))
		{
			changeLevel(currentLevel + 1);
		}

		GUILayout.Label("Lifes " + NbLifes);


		if (GUILayout.Button("Add Life"))
		{
			NbLifes++;
		}

		gameParams.noGameOver = GUILayout.Toggle(gameParams.noGameOver, "No Game Over");
		
		//GUILayout.Label("Level Index " + gameData.NbLifes);

		GUILayout.EndArea();
	}

}
