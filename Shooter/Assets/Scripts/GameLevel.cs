﻿using UnityEngine;
using System.Collections;

public class GameLevel : MonoBehaviour
{
	ChoixMechant[] spawners;
	void autoListSpawners()
	{
		spawners = gameObject.GetComponentsInChildren<ChoixMechant>();
	}

	public float speedDecors = 200;

	public int NextLevelPoints = 1000;

	public void stop(bool gameOver)
	{
		if (spawners == null) return;
		foreach (ChoixMechant sp in spawners)
		{
			sp.stopNow(gameOver);
			if (gameOver)
				sp.gameObject.SetActive(false);
		}
	}

	public void start()
	{
		foreach (ChoixMechant sp in spawners)
		{
			sp.startNow();
			sp.gameObject.SetActive(true);
		}
	}

	public void Start()
	{
		autoListSpawners();
	}
}
