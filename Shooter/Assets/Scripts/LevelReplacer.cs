﻿using UnityEngine;
using System.Collections;

public class LevelReplacer : MonoBehaviour 
{
	public Mechant findOnly = null;

	[System.Serializable]
	public class PrefabReplacement
	{
		public Mechant leMechant;

		[Array]
		public Mechant[] randomReplacers;

		[Array]
		public Spawner[] sources;
	}

	public PrefabReplacement[] remplacement;

	[Button("clear")]
	public bool m_clear;

	void clear()
	{
		remplacement = new PrefabReplacement[0];
	}

	[Button("listPrefabs")]
	public bool m_listPrefabs;

	void listPrefabs()
	{

		Spawner[] spawners = transform.GetComponentsInChildren<Spawner>();

		foreach (Spawner spawner in spawners)
		{
			foreach (Mechant m in spawner.typeMechant)
			{
				if (findOnly != null)
				{
					if (findOnly != m) continue;
				}

				addMechant(spawner, m);
			}
		}
	}

	void addSourceSpawner(PrefabReplacement r, Spawner fromSpawner)
	{
		if (r.sources == null)
			r.sources = new Spawner[0];

		for (int i = 0; i < r.sources.Length; i++)
		{
			if (r.sources[i] == fromSpawner)
			{
				return;
			}
		}

		System.Array.Resize<Spawner>(ref r.sources, r.sources.Length + 1);

		r.sources[r.sources.Length - 1] = fromSpawner;
	}


	void addMechant(Spawner fromSpawner, Mechant m)
	{
		for (int i = 0; i< remplacement.Length; i++)
		{
			if (remplacement[i].leMechant == m)
			{
				addSourceSpawner(remplacement[i], fromSpawner);
				return;
			}
		}

		System.Array.Resize<PrefabReplacement>(ref remplacement, remplacement.Length + 1);

		PrefabReplacement r = new PrefabReplacement();
		r.leMechant = m;
		remplacement[remplacement.Length - 1] = r;
	}

	[Button("ApplyReplacement")]
	public bool m_ApplyReplacement;

	void ApplyReplacement()
	{
		foreach (PrefabReplacement r in remplacement)
		{
			ApplyReplacement(r);
		}
	}

	void ApplyReplacement(PrefabReplacement r)
	{
		if (r.randomReplacers == null || r.randomReplacers.Length == 0)
			return;
		Spawner[] spawners = transform.GetComponentsInChildren<Spawner>();

		foreach (Spawner spawner in spawners)
		{
			for (int i = 0; i < spawner.typeMechant.Length; i++)
			{
				if (spawner.typeMechant[i] == r.leMechant)
				{
					int indexreplacer = Random.Range(0, r.randomReplacers.Length);

					spawner.typeMechant[i] = r.randomReplacers[indexreplacer];

				}
			}
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
