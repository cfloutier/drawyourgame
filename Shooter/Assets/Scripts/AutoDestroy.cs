﻿using UnityEngine;
using System.Collections;

public class AutoDestroy : MonoBehaviour {
	
	public float afterTime = 3;   
	
	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds(afterTime);
		Destroy (gameObject);
	}
}
