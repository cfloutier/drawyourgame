﻿using UnityEngine;
using System.Collections;

public class CameraAspectRatio : MonoBehaviour 
{
//	static CameraAspectRatio instance = null;
	//public static CameraAspectRatio get { get { return instance; } }
	public delegate void onScreenChanged(Rect newPixelrect);
	public static event onScreenChanged onScreenChanged_event;


	public float ratioX = 3;
	public float ratioY = 2;


	float lastScreenW = -1;
	float lastScreenH = -1;

	[Button("updateCam")]
	public bool applyNow;

	public void FixedUpdate()
	{
		updateCam();
	}

	void updateCam()
	{
		if (lastScreenW == Screen.width && lastScreenH == Screen.height)
			return;

		lastScreenW = Screen.width;
		lastScreenH = Screen.height;

		Camera cam = GetComponent<Camera>();

		float w = Screen.height * ratioX / ratioY;
		float h = Screen.height;

		if (w > Screen.width)
		{
			h = Screen.width * ratioY / ratioX;
			w = Screen.width;
		}

		Rect rc = new Rect((Screen.width - w) / 2, (Screen.height - h) / 2, w, h);

		cam.pixelRect = rc;

		if (onScreenChanged_event != null)
			onScreenChanged_event(rc);
	}

	void OnValidate()
	{
		if (!Application.isPlaying)
			updateCam();
	}

}
