﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CanvasGroupFader : MonoBehaviour
{
	CanvasGroup currentGroup;

	CanvasGroup fadeTo;
	public float durationFade;

	public void startFade(CanvasGroup fadeTo)
	{
		if (currentGroup == null)
		{
			currentGroup = fadeTo;
			currentGroup.gameObject.SetActive(true);
			currentGroup.alpha = 1;
		}
		else
		{
			currentGroup.alpha = 1;
			currentGroup.gameObject.SetActive(true);
			fadeTo.alpha = 0;
			fadeTo.gameObject.SetActive(true);

			startTime = Time.time;
			this.fadeTo = fadeTo;
		}
	}

	float startTime = -1;
	void Update()
	{
		if (startTime == -1)
			return;

		float progress = 1;
		
		if (durationFade != 0)
			progress = (Time.time - startTime) / durationFade;

		if (progress >= 1)
		{
			fadeTo.alpha = 1;
			currentGroup.alpha = 1;
			currentGroup.gameObject.SetActive(false);
			currentGroup = fadeTo;
			startTime = -1;
		}
		else
		{
			currentGroup.alpha = 1 - progress;
			fadeTo.alpha = progress;

		}
	}

}


public class MainGUI : CanvasGroupFader 
{
	public CanvasGroup SelectPlayer;
	public CanvasGroup GameGUI;
	public CanvasGroup GameOver;

	GameGUI GameGUI_;
	public GameGUI gameGUI 
	{ 
		get 
		{ 
			if (GameGUI_ == null)
				GameGUI_ = GameGUI.GetComponent<GameGUI>();

			return GameGUI_; 
		} 
	}

	public enum Mode
	{
		SelectPlayer,
		Game,
		GameOver
	}

	public Mode mode;

	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			CanvasGroup grp = transform.GetChild(i).GetComponent<CanvasGroup>();
			if (grp != null)
				grp.gameObject.SetActive(false);
		}

		setMode(mode);
	}


	public void gotoSelectPlayer() { setMode(Mode.SelectPlayer); }
	
	public void gotoGame() { setMode(Mode.Game); }
	public void gotoGameOver(int pts, int maxLevel) 
	{
		GameOver.GetComponent<GameOverGUI>().setData(pts, maxLevel);
		setMode(Mode.GameOver); 
	}
	

	void setMode(Mode mode)
	{
		this.mode = mode;
		switch (mode)
		{
			case Mode.SelectPlayer:
				startFade(SelectPlayer);
				break;

			case Mode.Game:
				startFade(GameGUI);
				break;

			case Mode.GameOver:
				startFade(GameOver);
				break;

			default:
				break;
		}
	}
}
