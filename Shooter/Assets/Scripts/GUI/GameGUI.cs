﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameGUI : MonoBehaviour 
{
	public Text pointsTxt;
	public Text lifeTxt;
	public Text levelNumber;
	public Text levelName;
	public Animator levelAnim;
	public BarreDeVie barreDeVies;

	public void setPoints(int pts)
	{
		if (pts == 0)
			pointsTxt.text = "0 Points";
		else
			pointsTxt.text = pts + " Points";
	}

	public void setNbLifes(int lifes)
	{
		if (lifeTxt != null)
		{
			if (lifes == 0)
				lifeTxt.text = "Argggggg";
			else
				lifeTxt.text = "Vies : " + lifes + "x";
		}


		if (barreDeVies != null)
		{
//			Debug.Log("lifes" + lifes);
			barreDeVies.set(lifes);
		}
	}

	public void setNewLevel(int Level, string name)
	{
		levelNumber.text = "Niveau " + (Level + 1);
		levelName.text = name;
		StartCoroutine(setLevel());
	}

	IEnumerator setLevel()
	{
		levelAnim.SetBool("Show", true);
		yield return new WaitForEndOfFrame();
		levelAnim.SetBool("Show", false);
	}
}
