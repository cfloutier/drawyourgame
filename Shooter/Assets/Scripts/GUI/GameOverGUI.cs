﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverGUI : MonoBehaviour 
{
	public Text points;
	public Text maxLevel;
	
	public void setData(int pts, int MaxLevel)
	{
		points.text = pts + " Points !!";
		maxLevel.text = "Niveau : " + MaxLevel;
	}
}
