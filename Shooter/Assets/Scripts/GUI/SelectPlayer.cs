﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[ExecuteInEditMode]
public class SelectPlayer : MonoBehaviour
{
	
	public int IndexPlayer = 0;
	public string playerName;

	public GameObject objPlayer;

	[Button ("++++", "add")]
	public bool m_add;

	void add() { IndexPlayer++; OnValidate(); }

	[Button("----", "rem")]
	public bool m_rem;

	void rem() { IndexPlayer--; OnValidate(); }

#if UNITY_EDITOR
	void OnValidate()
	{
		Game game = FindObjectOfType<Game>();
		if (game == null)
		{
			return;
		}

		if (game.jantils.Length == 0)
		{
			return;
		}

		if (IndexPlayer < 0)
			IndexPlayer = 0;
		else if (IndexPlayer >= game.jantils.Length)
		{
			IndexPlayer = game.jantils.Length -1;
		}

		playerName = game.jantils[IndexPlayer].name;

		objPlayer = game.jantils[IndexPlayer].gameObject;
		gameObject.name = playerName;
	}
#endif

	void PlayerSelected(int index)
	{
		IndexPlayer = index;
		Game.startGame(index);
	}

	public void autoSelectPlayer()
	{
		if (string.IsNullOrEmpty(playerName))
			PlayerSelected(IndexPlayer);
		else
		{
			 if (!Game.startGame(playerName))
				 PlayerSelected(IndexPlayer);
		}
	}

}
