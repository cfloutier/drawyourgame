﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BarreDeVie : MonoBehaviour {


	public GameObject[] coeurs;

	[Button("listCoeurs")]
	public bool m_listCoeurs;

	public Text moreLifes;

	public int nbVie = 8;

	void listCoeurs()
	{
		coeurs = new GameObject[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			coeurs[i] = transform.GetChild(i).gameObject;
		}
	}

	// Use this for initialization
	void Start()
	{
		
	}

	void OnValidate()
	{
		setNbVie();
	}


	public void set(int nbVie)
	{
		this.nbVie = nbVie;
		setNbVie();
	}
	
	// Update is called once per frame
	
	void setNbVie () 
	{
		//Debug.Log("nb vies " + nbVie);
		for (int i = 0; i < coeurs.Length; i++)
		{
			coeurs[i].SetActive(i < nbVie);
		}

		if (nbVie > coeurs.Length)
		{
			moreLifes.gameObject.SetActive(true);
			moreLifes.text = "+"+ (nbVie - coeurs.Length);
		}
		else
			moreLifes.gameObject.SetActive(false);
	}
}
