﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	public void loadLevel(int index)
	{
		Application.LoadLevel(index);
	}

	public void ExitApp()
	{
		Application.Quit();
	}
}
