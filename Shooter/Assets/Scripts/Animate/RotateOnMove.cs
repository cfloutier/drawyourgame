﻿using UnityEngine;
using System.Collections;

public class RotateOnMove : MonoBehaviour {

	public Vector3 rotationHoriz = Vector3.zero;
	public Vector3 rotationVert = Vector3.zero;


	public Vector3 speed;
	// Use this for initialization
	void Start () 
	{
		lastPos = transform.position;
	}

	Vector3 lastPos;
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 deltaPos = transform.position - lastPos;
		Vector3 InstantSpeed = deltaPos / Time.deltaTime;
		speed = Vector3.Lerp(speed, InstantSpeed, 0.7f);
		lastPos = transform.position;



		transform.localEulerAngles = (rotationHoriz * speed.x  +rotationVert * speed.y)*0.1f;
	}
}
