﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour 
{
	public Vector3 angleSpeed;

	public bool random = false;
	void Start()
	{
		if (random)
		angleSpeed = Vector3.Lerp(angleSpeed, -angleSpeed, Random.Range(0, 1f));
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.Rotate(angleSpeed * Time.deltaTime);
	}
}
