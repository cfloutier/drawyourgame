﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CustomMenu  
{




	[MenuItem("GameObject/DYG - Create Direct Parent", false, 10)]
	static void CreateCustomGameObject(MenuCommand menuCommand)
	{
		GameObject currentGo = menuCommand.context as GameObject;
		if (currentGo == null)
		{
			Debug.LogError("No selected object");
			return;
		}

		Undo.RecordObject(currentGo, "Create parent");

		// Create a custom game object
		GameObject go = new GameObject(currentGo.name);

		GameObjectUtility.SetParentAndAlign(go, currentGo);


		go.transform.parent = currentGo.transform.parent;
		currentGo.transform.parent = go.transform;

		if (currentGo.GetComponent<SpriteRenderer>() != null)
			currentGo.name = "Sprite";

		Selection.activeObject = go;
	}

	[MenuItem("GameObject/DYG - Transfert Scale to childrens", false, 10)]
	static void TransfertScale(MenuCommand menuCommand)
	{
		GameObject currentGo = menuCommand.context as GameObject;
		if (currentGo == null)
		{
			Debug.LogError("No selected object");
			return;
		}

		Undo.RecordObject(currentGo, "Transfert Scale");


		// Create a custom game object
		GameObject temp = new GameObject("temp");

		while (currentGo.transform.childCount > 0)
			currentGo.transform.GetChild(0).parent = temp.transform;

		currentGo.transform.localScale = Vector3.one;

		while (temp.transform.childCount > 0)
			temp.transform.GetChild(0).parent = currentGo.transform;

		GameObject.DestroyImmediate(temp);
	}
}
