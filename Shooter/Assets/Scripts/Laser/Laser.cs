﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour 
{
	// good or bad
	public bool good = true;
	

	public Vector3 speed;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.localPosition += speed * Time.deltaTime;

		if (!GetComponent<Renderer>().isVisible)
		{
			Destroy(gameObject);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
//		Debug.Log("collision with " + other.name);
		JantilShooter jantil = other.transform.findComponentInParentHierarchy<JantilShooter>();
		if (jantil != null)
		{
			if (!good)
			{
				jantil.hit(transform.position);
				Destroy(gameObject);
			}
			else
				return;
		}

		if (good && jantil != null)
		{
			// nothing to do
			return;
		}

		Mechant bug = other.transform.findComponentInParentHierarchy<Mechant>();
		if (good && bug != null)
		{
			bug.hit(transform.position, other);
			Destroy(gameObject);
			return;
		}
	}
}
