﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
// the base class for all mechant spawner

public class Spawner : MonoBehaviour 
{
	[Array]
	public Spawnable[] typeMechant;

	public Vector3 minPos;
	public Vector3 maxPos;
#if UNITY_EDITOR
	[Button("recordMinPos")]
	public bool m_recordMinPos;
	void recordMinPos() { minPos = transform.position; SceneView.RepaintAll(); }

	[Button("recordMaxPos")]
	public bool m_recordMaxPos;
	void recordMaxPos() { maxPos = transform.position; SceneView.RepaintAll(); }
#endif

	public delegate void onEnded(Spawner spawner);

	public bool autoSpawnOnStart = false;

	public virtual void Start()
	{
		if (autoSpawnOnStart)
		{
			startSending(null);
		}
	}

	protected void setStartPos()
	{
		transform.position = Vector3.Lerp(minPos, maxPos, Random.Range(0,1f));
	}

	protected Spawnable spawnRandomAt(Vector3 startPos, Quaternion startRot)
	{
		return spawnRandomAt(typeMechant, startPos, startRot);
	}

	protected Spawnable spawnRandomAt(Spawnable[] tableauSpawnables, Vector3 startPos, Quaternion startRot)
	{
		int typeBug = Random.Range(0, tableauSpawnables.Length);
		GameObject newBug = (GameObject)GameObject.Instantiate(tableauSpawnables[typeBug].gameObject, startPos, startRot);
		newBug.transform.parent = transform;
		newBug.SetActive(true);
		return newBug.GetComponent<Spawnable>();
	}

	public void startStop(bool start)
	{
		if (start)
			startSending(null);
		else
			stopSending(false);
	}

	public virtual void startSending(onEnded ended)
	{
		if (ended != null)
			ended(this);

		Debug.Log("Not implemented");
	}

	public virtual void stopSending(bool destroyEnemies)
	{
		if (destroyEnemies)
			transform.cleanChildren();
	}

#if UNITY_EDITOR
	public virtual void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.cyan;

		Vector3 Pa = minPos;
		Vector3 Pb = minPos; Pb.x = maxPos.x;
		Vector3 Pc = minPos; Pc.y= maxPos.y;
		Vector3 Pd = maxPos;

		Gizmos.DrawLine(Pa, Pb);
		Gizmos.DrawLine(Pa, Pc);
		Gizmos.DrawLine(Pb, Pd);
		Gizmos.DrawLine(Pc, Pd);

		Gizmos.color = Color.red;
		Gizmos.DrawLine(Pa, Pd);
	}
#endif
	
}
