﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class LignePiece : Spawner 
{
	public Cadeau[] pieces;

	[Message("Temps entre deux pièces", true)]
	public float deltaTimePieces = 0.1f;

	[Message("taille de la zonne sans piece", true)]
	public float sizeEmptyZone = 0.1f;

	[Message("Temps entre deux obstacles", true)]
	public float deltaTimeObstacle = 0.1f;

	[Message("Temps aleatoire entre deux obstacles", true)]
	public float randomTimeObstacle = 0.1f;

	onEnded ended;

	[Message("La durée de la vague -1 pour infinit", true)]
	public float duration = 60;

	[Message("vitesse verticale de deplacement de la ligne", true)]
	public float speedLine = 1;
	public iTween.EaseType easeLine;

	[Message("vitesse verticale de defilement", true)]
	public GotoPoint.SpeedFactor globalSpeed ;

	GameObject posPiece;
	float nextTimePiece = -1;
	float nextTimeObstacle = -1;

	float endTime = -1;
	onEnded onended;
	public override void startSending(onEnded ended)
	{
		if (duration > 0)
			endTime = Time.time + duration;

		onended = ended;

		this.ended = ended;
		gameObject.SetActive(true);

		transform.cleanChildren();
		posPiece = new GameObject();
		posPiece.name = "Pos Pieces";
		posPiece.transform.parent = transform;
		posPiece.transform.setIdentity();

		nextMove();
		setNextTimePiece();
		setNextTimeObstacle();
	}

	public override void stopSending(bool destroyEnemies)
	{
		base.stopSending(destroyEnemies);

		nextTimePiece = -1;
		nextTimeObstacle = -1;
	}

	void setNextTimePiece()
	{
		nextTimePiece = Time.time + deltaTimePieces;
	}

	void setNextTimeObstacle()
	{
		float delta = deltaTimeObstacle + Random.Range(0, randomTimeObstacle);
	//	Debug.Log("setNextTimeObstacle " + delta);
		nextTimeObstacle = Time.time + delta;
	}

	[HideInInspector]
	public Vector3 pa, pb, pc;
	void spawnOneObstacle()
	{
		float range = minPos.y - maxPos.y;
		if (range < 0) range = -range;
		
		float minPosEmpty = posPiece.transform.position.y - sizeEmptyZone;
		float maxPosEmpty = posPiece.transform.position.y + sizeEmptyZone;

		if (minPosEmpty < minPos.y)
			minPosEmpty = minPos.y;

		if (maxPosEmpty > maxPos.y)
			maxPosEmpty = maxPos.y;

		float realSizeEmpty = maxPosEmpty - minPosEmpty;

		range = range - realSizeEmpty;
		float posInRange = Random.Range(0, range) + minPos.y;
		if (posInRange > minPosEmpty && posInRange < maxPosEmpty)
		{
			posInRange += realSizeEmpty;
		}

		pa = minPos; pa.y = minPosEmpty;
		pb = minPos; pb.y = maxPosEmpty;

		pc = minPos;
		pc.y = posInRange;

		Mechant badguy = (Mechant)spawnRandomAt(pc, transform.rotation);

		GotoPoint gotoPoint = badguy.gameObject.AddComponent<GotoPoint>();
		//gotoPoint.destinationPoint = Game.instance.jantil.transform.position;

		gotoPoint.destinationPoint = pc + Vector3.left * 100;
		gotoPoint.orient = false;
		gotoPoint.speedFactor = globalSpeed;
		
		gotoPoint.startMove(true);

		setNextTimeObstacle();
	}

	void spawnPiece()
	{
		Vector3 pos = posPiece.transform.position;
		Cadeau laPiece = (Cadeau) spawnRandomAt(pieces, pos, transform.rotation);

		GotoPoint gotoPoint = laPiece.gameObject.AddComponent<GotoPoint>();
		//gotoPoint.destinationPoint = Game.instance.jantil.transform.position;

		gotoPoint.destinationPoint = pos + Vector3.left * 100;
		gotoPoint.orient = false;
		gotoPoint.speedFactor = globalSpeed;

		gotoPoint.startMove(true);

		setNextTimePiece();
	}

	void nextMove()
	{
		Vector3 pos = Vector3.Lerp(minPos, maxPos, Random.Range(0, 1f));
		float distance = (pos - posPiece.transform.position).magnitude;
		float time = distance / speedLine;
		iTween.MoveTo(posPiece, iTween.Hash(
			"position", pos,
			"time", time,
			"easetype", easeLine,
			"oncomplete", "nextMove",
			"oncompletetarget", gameObject	));
	}

	void Update()
	{
		if (endTime > 0 && Time.time > endTime)
		{
			stopSending(false);
			if (onended != null)
				onended(this);

			return;
		}
		if (nextTimePiece == -1)
			return;

		if (Time.time > nextTimeObstacle)
		{
			spawnOneObstacle();
		}
		if (Time.time > nextTimePiece)
		{
			spawnPiece();
		}
	}


	public override void OnDrawGizmosSelected()
	{
		base.OnDrawGizmosSelected();

		if (posPiece == null) return;

		Gizmos.color = Color.red;
		Vector3 worldPos = posPiece.transform.position;
		Gizmos.DrawWireSphere(worldPos, sizeEmptyZone);

		Gizmos.DrawSphere(pa, 1);
		Gizmos.color = Color.green;
		Gizmos.DrawSphere(pb, 1);
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(pc, 1);
	}

}
