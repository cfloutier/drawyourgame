﻿using UnityEngine;
using System.Collections;

public class VagueDeMechants : Spawner 
{
	[Message("NB Missile to Launch, -1 for infinite", true)]
	public int Nb = 5;

	[Message("Time between 2 spawns", true)]
	public float deltaTime = 1;

	public float randomTime = 1;

	[Message("Speed and acceleration of The Méchant Missil", true)]
	public GotoPoint.SpeedFactor speedFactor;

	public bool orientMechant = true;

	public enum ModeTir
	{
		ToutDroit,
		DirectionJantil
	}

	public ModeTir modeDeTir;

	onEnded ended;

	int remaining = 0;
	int destroyed = 0;

	public override void startSending(onEnded ended)
	{
		this.ended = ended;
		gameObject.SetActive(true);

		remaining = Nb;
		destroyed = 0;
		setNextTime();
	}

	public override void stopSending(bool destroyEnemies)
	{
		base.stopSending(destroyEnemies);
		nextTime = -1;
	}

	public void onMechantEnded(Mechant mechant, bool destroyedByPlayer)
	{
		destroyed++;
		
		if (Nb != -1 && destroyed >= Nb)
		{
			if (ended != null)
				ended(this);

			gameObject.SetActive(false);
		}
	}

	void setNextTime()
	{
		nextTime = Time.time + deltaTime + Random.Range(0, randomTime);
	}

	void spawnOne()
	{
		Vector3 pos = Vector3.Lerp(minPos, maxPos, Random.Range(0, 1f));
		Mechant badguy = (Mechant)spawnRandomAt(pos, transform.rotation);

		GotoPoint gotoPoint = badguy.gameObject.AddComponent<GotoPoint>();
		//gotoPoint.destinationPoint = Game.instance.jantil.transform.position;

		switch (modeDeTir)
		{
			case ModeTir.ToutDroit:
				gotoPoint.destinationPoint = pos + Vector3.left * 100;
				break;
			case ModeTir.DirectionJantil:
				gotoPoint.destinationPoint = Game.jantil.transform.position;
				break;
		}

		gotoPoint.orient = orientMechant;


		gotoPoint.speedFactor = speedFactor;
		
		gotoPoint.startMove(true);

		badguy.onMechantEndedListener = onMechantEnded;

		if (Nb != -1)
			remaining--;

		
		setNextTime();
	}

	float nextTime = -1;

	void Update()
	{
		if (Nb != -1 && remaining <= 0)
			nextTime = -1;

		if (nextTime == -1)
			return;

		if (Time.time > nextTime)
		{

			spawnOne();
		}
	}
	
}
