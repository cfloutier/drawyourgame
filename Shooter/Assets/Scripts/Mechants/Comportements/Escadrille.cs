﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Curve))]
public class Escadrille : Spawner
{
	Curve curve;

	public int nb;

	public int BonusPoints = 0;

	int remainingToSpawn = 0;
	int destroyed = 0;
	int NbDestroyedByPlayer = 0;

	public float speed = 1;
	public float distanceBetween = 1;
	float deltaTimeSpawn = 1;

	public bool orient = false;



	public override void Start()
	{
		curve = GetComponent<Curve>();
		base.Start();
	}

	onEnded ended;
	float nextSpawn = -1;
	public override void startSending(onEnded ended)
	{
		this.ended = ended;
		gameObject.SetActive(true);

		setStartPos();

		nextSpawn = Time.time;
		remainingToSpawn = nb;
		destroyed = 0;
		NbDestroyedByPlayer = 0;

		if (speed <= 0)
			speed = 1;

		deltaTimeSpawn = distanceBetween / speed;
	}

	public override void stopSending(bool destroyEnemies)
	{
		base.stopSending(destroyEnemies);
		nextSpawn = -1;
	}

	void spawnOne()
	{
		if (typeMechant.Length == 0)
		{
			Debug.LogError("typeMechant not set");
		}

		Vector3 startPos = Vector3.zero;
		Quaternion startRot = Quaternion.identity;
		curve.getPos(0, ref startPos, ref startRot, true);


		Vector3 pos = startPos;
		Quaternion orientation = Quaternion.identity;
		if (orient)
			orientation = startRot;

		Mechant badguy = (Mechant) spawnRandomAt(pos, orientation);
		
		badguy.onMechantEndedListener = onMechantEnded;
		FollowCurve follow = badguy.gameObject.AddComponent<FollowCurve>();
		follow.curve = curve;
		follow.speed = speed;
		follow.orient = orient;
		follow.startAnim();

		remainingToSpawn--;
	}

	public void onMechantEnded(Mechant mechant, bool destroyedByPlayer)
	{
		destroyed++;
		if (destroyedByPlayer)
			NbDestroyedByPlayer++;

		if (destroyed >= nb)
		{
			if (ended != null)
				ended(this);

			if (NbDestroyedByPlayer >= nb)
				Game.addPoint(mechant.transform.position, BonusPoints);

			gameObject.SetActive(false);
		}
	}

	void Update()
	{
		if (nextSpawn != -1 && Time.time >= nextSpawn )
		{
			if (remainingToSpawn > 0)
			{
				spawnOne();

				if (remainingToSpawn > 0)
					nextSpawn += deltaTimeSpawn;
				else
					nextSpawn = -1;
			}
		}
	}

}
