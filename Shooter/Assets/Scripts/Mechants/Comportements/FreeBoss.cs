﻿using UnityEngine;
using System.Collections;

public class FreeBoss : Spawner 
{
	public GotoPoint.Mode followMode;
	public GotoPoint.SpeedFactor speedFactor;
	public GotoPoint.TweenFactor tweenFactor;
	
	// Use this for initialization
	public override void Start()
	{
		base.Start();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	onEnded ended;
	public override void startSending(onEnded ended)
	{
		this.ended = ended;
		gameObject.SetActive(true);

		spawnOne();
	}

	Vector3 randomDestPoint()
	{
		return new Vector3(
			Random.Range(minPos.x, maxPos.x),
			Random.Range(minPos.y, maxPos.y),
			Random.Range(minPos.z, maxPos.z));
	}

	void spawnOne()
	{
		Mechant badguy = (Mechant)spawnRandomAt(transform.position, transform.rotation);

		GotoPoint gotoPoint = badguy.gameObject.AddComponent<GotoPoint>();
		gotoPoint.destinationPoint = randomDestPoint();

		gotoPoint.mode = followMode;

		gotoPoint.speedFactor = speedFactor;
		gotoPoint.tweenFactor = tweenFactor;
		gotoPoint.startMove(true);
		gotoPoint.onPointReached_listener = onPointReached;
		badguy.onMechantEndedListener = onMechantEnded;
	}

	public void onPointReached(GotoPoint point)
	{
		point.destinationPoint = randomDestPoint();
		point.mode = followMode;
		point.startMove(false);
	}

	public void onMechantEnded(Mechant mechant, bool destroyedByPlayer)
	{
		if (ended != null)
			ended(this);

		gameObject.SetActive(false);
		
	}
}
