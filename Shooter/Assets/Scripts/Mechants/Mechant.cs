﻿using UnityEngine;
using System.Collections;

public class Mechant : Spawnable
{
	public delegate void onMechantEnded(Mechant mechant, bool destroyedByPlayer);
	public onMechantEnded onMechantEndedListener;

	public int NbPointDeVie = 1;

	public int points = 100;

	public bool autodestroyOnNotvisible = true;

	float minX = - 100;

	public enum ModeTir
	{
		ToutDroit,
		Conique,
		DirectionJantil
	}

	// only for conics
	public int NbPerShoot = 1;
	public float angleConique = 45;

	public ModeTir modeDeTir;

	public float speedTir = 30;
	public float periodTir = 1;

	public Transform firePos;
	public Transform altFirePos;

	[Array]
	public Laser[] lasers;
	float nextFire = -1;

	// Use this for initialization
	public virtual void Start () 
	{
		if (autodestroyOnNotvisible)
		{
			Vector3 posInCameraSpace = Camera.main.transform.InverseTransformPoint(transform.position);

			float BorderScreenXPos = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, posInCameraSpace.z)).x;

			minX = BorderScreenXPos - 1;
		}

		if (periodTir > 0)
			nextFire = Time.time + periodTir;
	}

	// Update is called once per frame
	void Update () 
	{
		if (autodestroyOnNotvisible)
		{
			if (transform.position.x < minX)
			{
				if (!GetComponentInChildren<Renderer>().isVisible)
				{
					Destroy(gameObject);
				}
			}
		}


		if (nextFire > 0 && Time.time >= nextFire)
		{
			fire();
		}
	}

	bool destroyedByPlayer = false;
	public void OnDestroy()
	{
		if (onMechantEndedListener != null)
			onMechantEndedListener(this, destroyedByPlayer);
	}

	public virtual void hit(Vector3 pos, Collider hit)
	{
		NbPointDeVie--;

		if (NbPointDeVie > 0)
			Game.instance.createExplosion(pos, 0);
		else
		{
			Game.instance.explodeMe(gameObject, points);
			destroyedByPlayer = true;
		}
	}


	public virtual void fire()
	{
		if (lasers.Length == 0)
			return;

		float dAplha = 0;

		if (modeDeTir != ModeTir.Conique)
			NbPerShoot = 1;

		else if (NbPerShoot > 1)
		{
			dAplha = (2 * angleConique / (NbPerShoot - 1));
		}
		
		for (int indexTir = 0; indexTir < NbPerShoot; indexTir++)
		{
			int indexType = Random.Range(0, lasers.Length);

			Vector3 startPos = transform.position;
			if (altFirePos != null)
			{
				int index = Random.Range(0, 2);
				if (index == 0)
					startPos = firePos.position;
				else
					startPos = altFirePos.position;
			}
			else if (firePos != null)
				startPos = firePos.position;

			GameObject newObj = (GameObject)GameObject.Instantiate(lasers[indexType].gameObject, startPos, lasers[indexType].transform.rotation);

			Laser l = newObj.GetComponent<Laser>();
			l.good = false;

			switch (modeDeTir)
			{
				case ModeTir.ToutDroit:
					l.speed = Vector3.left * speedTir;


					break;
				case ModeTir.DirectionJantil:
					{
						Vector3 posJantil = Game.jantil.transform.position;
						Vector3 dir = (posJantil - newObj.transform.position).normalized;

						float angle = Mathf.Atan2(dir.x, dir.y);
						newObj.transform.localEulerAngles = new Vector3(0, 0, angle * Mathf.Rad2Deg);


						l.speed = dir * speedTir;
					}
					break;
				case ModeTir.Conique:
					{
						float angle = 0;
						if (NbPerShoot > 1)
							angle = -angleConique + dAplha * indexTir;
						
						newObj.transform.localEulerAngles = new Vector3(0, 0, -angle);

				//		Debug.Log("angle = " + angle);
						angle = angle * Mathf.Deg2Rad;


						Vector3 dir = new Vector3(

							-Mathf.Cos(angle), 
							Mathf.Sin(angle), 0);

						l.speed = dir * speedTir;
					}
					break;
			}


		}




		

		if (periodTir > 0)
			nextFire = Time.time + periodTir;
	}
}
