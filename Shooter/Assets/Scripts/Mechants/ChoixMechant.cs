﻿using UnityEngine;
using System.Collections;

public class ChoixMechant : MonoBehaviour 
{
	public enum Mode
	{
		All,
		OneAtATime
	}

	public Mode mode = Mode.OneAtATime;

	Spawner[] spawners;

	void autoFindSpawners()
	{
		spawners = GetComponentsInChildren<Spawner>();
	}

	public bool spawn = true;

	public void stopNow(bool destroyEnememies)
	{
		autoFindSpawners();
		spawn = false;
		foreach (Spawner sp in spawners)
		{
			sp.stopSending(destroyEnememies);
		}
	}

	public void startNow()
	{
		spawn = true;
		if (mode == Mode.OneAtATime)
		{
			nextSpanAt = Time.time;
		}
		else
		{
			foreach (Spawner sp in spawners)
			{
				sp.startSending(null);
			}
		}
	}

	public float breatheBetweenEnemies = 1;

	// Use this for initialization
	void Start () 
	{
		autoFindSpawners();
		if (spawn)
			nextSpanAt = Time.time + breatheBetweenEnemies;
	}

	float nextSpanAt = -1;

	public void onEnded(Spawner spawner)
	{
		if (mode == Mode.OneAtATime)
		{
			if (spawn)
			{
				nextSpanAt = Time.time + breatheBetweenEnemies;
			}
		}
	}

	void nextSpawn()
	{
		if (mode == Mode.OneAtATime)
		{
			int indexMechant = Random.Range(0, spawners.Length);
			if (spawners[indexMechant] != null)
				spawners[indexMechant].startSending(onEnded);
			else
			{
				Debug.LogError("Empty spawner at index " + indexMechant);

				autoFindSpawners();
				if (spawners.Length != 0)
					nextSpawn();


				return;
			}

			nextSpanAt = -1;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mode == Mode.OneAtATime)
		{
			if (nextSpanAt >= 0 && Time.time > nextSpanAt)
				nextSpawn();
		}
	}
}
