﻿using UnityEngine;
using System.Collections;

public class FollowCurve : MonoBehaviour 
{
	public Curve curve;
	public float speed = 0;

	float time = 0;
	float dt = 0;

	public bool orient = false;
	Vector3 addRotation = Vector3.up * 90;

	float distance;
	public void startAnim()
	{
		distance = curve.totalLenght;

		

		time = 0;
	}

	void Update()
	{
		dt = speed / distance;
		time += Time.deltaTime * dt;

		if (time > 1)
		{
			Destroy(gameObject);
		}

		Vector3 pos = Vector3.zero;
		Quaternion rotation = Quaternion.identity;
		curve.getPos(time, ref pos, ref rotation, true);

		if (orient)
		{
			Vector3 euler = rotation.eulerAngles;
			euler.z = 0;

			transform.eulerAngles = euler;
			transform.Rotate(addRotation);
		}

		transform.position = pos;
	}
}
