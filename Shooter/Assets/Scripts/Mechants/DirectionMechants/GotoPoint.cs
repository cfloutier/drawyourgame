﻿using UnityEngine;
using System.Collections;

public class GotoPoint : MonoBehaviour 
{
	public Vector3 destinationPoint;

	[System.Serializable]
	public class SpeedFactor
	{
		public float startSpeed;
		public float acc;
		public float maxSpeed = 50;
		public float frottement = 0.01f;
	}

	[System.Serializable]
	public class TweenFactor
	{
		public iTween.EaseType easeType;
		public float tweenSpeed = 30;
	}

	public SpeedFactor speedFactor;
	public TweenFactor tweenFactor;

	public bool orient = false;
	

	Vector3 direction;
	public Vector3 speedVector;

	public float minDistance =  1f;
	public delegate void onPointReached(GotoPoint point);

	public onPointReached onPointReached_listener;

	public enum Mode
	{
		FollowDirection,
		TeteChercheuse,
		TweenMode
	}

	public Mode mode = Mode.FollowDirection;

	// Use this for initialization
	void Start () 
	{
		startMove(true);
		
	}

	public void startMove(bool first)
	{
		if (mode != Mode.TweenMode)
		{
			if (mode == Mode.FollowDirection || first)
			{
				direction = destinationPoint - transform.position;
				direction.Normalize();
				speedVector = direction * speedFactor.startSpeed;
			}
		}
		else
		{
			float time = 1;
			if (tweenFactor.tweenSpeed > 0)
				time = (destinationPoint - transform.position).magnitude / tweenFactor.tweenSpeed;

			iTween.MoveTo(gameObject, iTween.Hash(
				"position",destinationPoint,
				"easetype",tweenFactor.easeType,
				"islocal",false,
				"time", time,
				"oncomplete", "OnEndTween"
				));
		}
	}

	void OnEndTween()
	{
		if (onPointReached_listener != null)
			onPointReached_listener(this);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mode == Mode.TweenMode) return;

		if (speedFactor.acc > 0)
		{
			switch (mode)
			{
				case Mode.FollowDirection:
					speedVector += direction * speedFactor.acc * Time.deltaTime;
					break;
				case Mode.TeteChercheuse:
					speedVector += (destinationPoint - transform.position).normalized * speedFactor.acc * Time.deltaTime;
					break;
				default:
					break;
			}
		}

		direction = speedVector.normalized;
		speedVector = speedVector - direction * speedFactor.frottement * Time.deltaTime;

		if (speedVector.magnitude > speedFactor.maxSpeed)
			speedVector = speedVector.normalized * speedFactor.maxSpeed;

		Vector3 pos = transform.position;
		pos += speedVector * Time.deltaTime;
		transform.position = pos;


		if (orient)
		{
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			transform.eulerAngles = new Vector3(0, 0, angle + 180 );
		}

		if (onPointReached_listener != null)
		{
			float dist = (destinationPoint - transform.position).magnitude;
			if (dist < minDistance)
				onPointReached_listener(this);
		}
	}


#if UNITY_EDITOR
	public void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(destinationPoint, 1);
	}
#endif
}
