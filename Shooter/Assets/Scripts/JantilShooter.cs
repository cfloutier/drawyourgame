﻿using UnityEngine;
using System.Collections;

public class JantilShooter : MonoBehaviour 
{
	public float sensibility = 1;
	Vector3 minPos;
	Vector3 maxPos;

	public float repeatDuration = 1;
	float nextFireTime = -1;

	public float speedLaser = 10;

	public float timeBeforeShoot = 0f;


	[Array]
	public Laser[] lasers;

	public Transform laserPos;
	public Transform alternateLaserPos;

	public Vector3 rotationHoriz = Vector3.zero;
	public Vector3 rotationVert = Vector3.zero;

	Animator animator;

	// Use this for initialization
	void Start () 
	{
		Camera cam = Camera.main;
		animator = GetComponent<Animator>();

		Rect pixelRect = cam.pixelRect;

		minPos = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMin, transform.position.z));
		maxPos = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMax , pixelRect.yMax, transform.position.z));
	}

	int indexPosLaser = 0;
	void fireNow()
	{
		int indexLaser = Random.Range(0, lasers.Length);

		GameObject newLaser = (GameObject)GameObject.Instantiate(lasers[indexLaser].gameObject);
		newLaser.transform.parent = transform.parent;

		if (alternateLaserPos != null)
		{

			if (indexPosLaser == 0)
				newLaser.transform.position = laserPos.position;
			else
				newLaser.transform.position = alternateLaserPos.position;

			indexPosLaser++;
			indexPosLaser = indexPosLaser % 2;

			//Debug.Log(indexPosLaser);
		}
		else
			newLaser.transform.position = laserPos.position;
		
		
		
		newLaser.GetComponent<Laser>().speed = Vector3.right * speedLaser;
	}

	void fireOnOff(bool on)
	{
		animator.SetBool("fire", on);
		if (on)
		{
			if (nextFireTime == -1)
				nextFireTime = Time.time + timeBeforeShoot;
		}

		if (nextFireTime >0 && Time.time > nextFireTime)
		{
			fireNow();

			if (on)
				nextFireTime += repeatDuration;
			else
				nextFireTime = -1;
		}
	}

	void Move()
	{
		float vert = Input.GetAxis("Vertical");
		float horiz = Input.GetAxis("Horizontal");

		Vector3 pos = transform.localPosition;

		pos += Vector3.right * horiz * sensibility;
		pos += Vector3.up * vert * sensibility;

		if (pos.x < minPos.x)
			pos.x = minPos.x;
		if (pos.x > maxPos.x)
			pos.x = maxPos.x;

		if (pos.y < minPos.y)
			pos.y = minPos.y;
		if (pos.y > maxPos.y)
			pos.y = maxPos.y;

		transform.localPosition = pos;
		transform.localEulerAngles = rotationHoriz * horiz + rotationVert * vert;
	}

	public void OnTriggerEnter(Collider other)
	{
		Cadeau cadeau = other.transform.findComponentInParentHierarchy<Cadeau>();
		if (cadeau != null)
		{
			Game.addLife(other.transform.position, cadeau.bonusLife);
			Game.addPoint(other.transform.position, cadeau.bonusPoints);
			Destroy(cadeau.gameObject);
			return;
		}

		Mechant mechant = other.transform.findComponentInParentHierarchy<Mechant>();

		if (mechant != null)
		{
			Game.instance.touched();
			Game.instance.explodeMe(mechant.gameObject, 0);
		}
	}

	public void hit(Vector3 pos)
	{
		Game.instance.touched();
		Game.instance.createExplosion(pos, 0);
	}

	// Update is called once per frame
	void Update()
	{
		Move();

		fireOnOff(Input.GetButton("Fire1"));
	}
}
