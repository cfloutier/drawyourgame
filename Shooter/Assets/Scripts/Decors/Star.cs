﻿using UnityEngine;
using System.Collections;

public class Star : MonoBehaviour {

	public float minX;
	
	StarField parentField;
	Scroller parentscroll;

	Vector3 baseScale;

	public float moveX = 0;

	Scroller parentScroller;

	// Use this for initialization
	void Start () 
	{
		parentScroller = transform.GetComponentInParent<Scroller>();
		

		baseScale = transform.localScale;
		parentField = transform.GetComponentInParent<StarField>();
		parentscroll = transform.GetComponentInParent<Scroller>();
	}
	
	void Update()
	{
		Vector3 pos = transform.position;

		if (parentScroller != null)
		{
			pos.x += moveX * parentScroller.Speed * Time.deltaTime;
		}



		if (pos.x < minX)
		{
			pos.x = -minX;
		}
		transform.position = pos; 
		transform.localScale = new Vector3(baseScale.x * (1 + 0.001f * parentField.ratioDistortionObjects * parentscroll.Speed), baseScale.y, baseScale.z);
	}
}
