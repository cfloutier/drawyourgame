﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class autoSortLayers : MonoBehaviour {

	[Button("SortDependingOnZ")]
	public bool m_SortLayers;

	public void SortDependingOnZ()
	{
		SpriteRenderer [] renderers = GetComponentsInChildren<SpriteRenderer>();

		foreach (var r in renderers)
		{
			int sortOrder =  (int) -r.transform.position.z * 10;
			r.sortingOrder = sortOrder;
		}
	}

	void Start()
	{
		if (Application.isPlaying)
			SortDependingOnZ();
	}





}
