﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PlanDecors : MonoBehaviour 
{
	public float cloneDistance = 10;

	[Button("ComputeCloneDistance")]
	public bool m_ComputeSize;

	[Button("recenter")]
	public bool m_CenterPivot;

	[Button("CloneAfter", true)]
	public bool m_CloneAfter;

	[Button("CloneMirror")]
	public bool m_CloneMirror;

	[Button("RemoveClone")]
	public bool m_RemoveClone;

	[Button("AddCloneToPlan")]
	public bool m_AddCloneToPlan;

	[Button("RemoveZ")]
	public bool m_RemoveZ;

	[Button("SortLayers")]
	public bool m_SortLayers;

	void SortLayers()
	{
		transform.GetComponentInParent<autoSortLayers>().SortDependingOnZ();
	}

	void ComputeCloneDistance()
	{
		if (transform.childCount == 0) return;
		Bounds b = computeBounds();
		cloneDistance = b.max.x - b.min.x;
	}

	Bounds computeBounds()
	{
		Renderer[] renderers = GetComponentsInChildren<Renderer>();

		int nbChildren = renderers.Length;
		Bounds b = new Bounds();

		for (int i = 0; i < nbChildren; i++)
		{
			Renderer r = renderers[i];
			if (i == 0)
				b = r.bounds;
			else
				b.Encapsulate(r.bounds);
		}

		return b;
	}

	void recenter()
	{
		Bounds b = computeBounds();

		GameObject tempObject = new GameObject();
		tempObject.name = "temp";
		tempObject.transform.setIdentity();

		while (transform.childCount > 0)
		{
			Transform child = transform.GetChild(0);
			child.transform.parent = tempObject.transform;
		}

		transform.position = b.center;

		while (tempObject.transform.childCount > 0)
		{
			Transform child = tempObject.transform.GetChild(0);
			child.transform.parent = transform;
		}

		DestroyImmediate(tempObject);
	}

	public GameObject cloneObject = null;

	void CloneAfter()
	{
		if (cloneObject != null)
			DestroyImmediate(cloneObject);

		cloneObject = (GameObject) GameObject.Instantiate(gameObject);
		cloneObject.transform.parent = transform.parent;
		Vector3 pos = transform.position;
		pos += Vector3.right * cloneDistance;

		cloneObject.transform.position = pos;
	}

	void CloneMirror()
	{
		CloneAfter();

		Vector3 scale = cloneObject.transform.localScale;
		scale.x = -scale.x;
		cloneObject.transform.localScale = scale;
	}

	void AddCloneToPlan()
	{
		if (cloneObject == null) return;


		while (cloneObject.transform.childCount > 0)
		{
			Transform child = cloneObject.transform.GetChild(0);
			child.transform.parent = transform;
		}

		DestroyImmediate(cloneObject);
		cloneObject = null;

		ComputeCloneDistance();
	}

	void RemoveClone()
	{
		if (cloneObject == null) return;

		DestroyImmediate(cloneObject);
		cloneObject = null;

	}

	void RemoveZ()
	{
		for (int i = 0; i < transform.childCount; i++)
		{
			Transform child = transform.GetChild(i);
			Vector3 localPos = child.localPosition;
			localPos.z = 0;
			child.localPosition = localPos;
		}

	}

	public float minXPos;

	public bool AutoClone = false;

	void Start()
	{
		computeMinPos();
		CameraAspectRatio.onScreenChanged_event += onScreenChanged;
		if (Application.isPlaying)
		{

			if (cloneObject == null && AutoClone)
			{
				AutoClone = false;
				CloneAfter();
			}
		}
	}

	void onScreenChanged(Rect newPixelrect)
	{
		computeMinPos();
	}

	void computeMinPos()
	{
		Bounds b = computeBounds();

		Vector3 posInCameraSpace = Camera.main.transform.InverseTransformPoint(transform.position);


		Rect pixelRect = Camera.main.pixelRect;
		float BorderScreenXPos = Camera.main.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMin, posInCameraSpace.z)).x;


		float delatX = b.max.x - transform.position.x;

		minXPos = BorderScreenXPos - delatX;

	}


	void Update()
	{
		if (transform.position.x < minXPos)
		{
			Vector3 pos = transform.position;
			pos.x += cloneDistance*2;
			transform.position = pos;
		}

	}





}
