﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SpriteFog : MonoBehaviour {

	public float minDepth;
	public float maxDepth;
	public Color fadeColor;

	[Button("setFog")]
	public bool m_setFog;
	public void  setFog()
	{
		Material m = new Material(Shader.Find("Sprite/Faded Sprite"));

		SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();

		foreach (var r in renderers)
		{
			float distanceFromCam = r.transform.position.z - Camera.main.transform.position.z;
			

			Color c = fadeColor;
			c.a = Mathf.InverseLerp(minDepth, maxDepth, distanceFromCam);
			r.color = c;
			
			r.sharedMaterial = m;

		//	r.sortingOrder = (int)-r.transform.position.z * 100;
		}
	}

	void OnValidate()
	{
		setFog();
	}

	void Start()
	{
		if (Application.isPlaying)
			setFog();
	}



}
