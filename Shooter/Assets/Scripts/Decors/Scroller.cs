﻿using UnityEngine;
using System.Collections;

public class Scroller : MonoBehaviour 
{
	public static Scroller instance;

	public float Speed = 1;

	GamePlan plan;

	public enum Mode
	{
		MoveDecors,
		MoveGamePlan
	}

	public Mode mode;
	

	// Use this for initialization
	void Start () 
	{

		instance = this;

		plan = GameObject.FindObjectOfType<GamePlan>();
		

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (mode == Mode.MoveDecors)
			transform.position -= new Vector3(Speed * Time.deltaTime, 0, 0);
		else
			plan.transform.position += new Vector3(Speed * Time.deltaTime, 0, 0);
	
	}
}
