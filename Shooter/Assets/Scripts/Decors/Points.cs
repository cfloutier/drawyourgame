﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Points : MonoBehaviour 
{
	public int points = 100;
	public Text txt;
	public Color colorMinPoint;
	public Color colorMaxPoint;
	public int maxPoint;

	public Vector3 minScale;
	public Vector3 maxScale;

	[Button("recordMinScale")]
	public bool m_recordMinScale;
	public void recordMinScale()	{		minScale = transform.localScale;	}
	[Button("recordMaxScale")]
	public bool m_recordMaxScale;
	public void recordMaxScale() { maxScale = transform.localScale; }


	// Use this for initialization
	void Start () 
	{
		//ivisible at start
		//gameObject.SetActive(false);
	}

	[Button("show")]
	public bool m_show;

	public void show()
	{
		txt.text = "" + points;
		float factorPoints = (float)points / maxPoint;

		txt.color = Color.Lerp(colorMinPoint, colorMaxPoint, factorPoints);
		transform.localScale = Vector3.Lerp(minScale, maxScale, factorPoints);
		GetComponent<Animator>().SetBool("Show", true);
	}

	public bool debugNoDestroy = false;

	public void destroy()
	{
		if (!debugNoDestroy)
			Destroy(gameObject);
	}
	
	
}
