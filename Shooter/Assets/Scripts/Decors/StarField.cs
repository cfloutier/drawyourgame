﻿using UnityEngine;
using System.Collections;

public class StarField : MonoBehaviour
{
	[Array]
	public GameObject[] stars;

	public int NbObjects = 100;
	public float nearClip = 10;
	public float farClip = 500;

	public float floor = -100;

	public Vector2 Size = new Vector2(1, 1);

	public float ratioDistortionObjects = 0;

	[Button("buildObjects")]
	public bool m_buildObjects;

	[Button("cleanUp")]
	public bool m_cleanUp;

	void cleanUp()
	{
		transform.cleanChildren();
	}

	void buildObjects()
	{
		transform.cleanChildren();
		Camera cam = Camera.main;

		//		float aspect = cam.aspect;

		if (farClip > cam.farClipPlane)
			farClip = cam.farClipPlane;

		if (nearClip < cam.nearClipPlane)
			nearClip = cam.nearClipPlane;

		Rect pixelrect = cam.pixelRect;

		for (int i = 0; i < NbObjects; i++)
		{
			int indexObj = Random.Range(0, stars.Length);
			GameObject newObject = (GameObject)GameObject.Instantiate(stars[indexObj]);

			newObject.transform.parent = transform;
			newObject.transform.localScale = Vector3.one * Random.Range(Size.x, Size.y);
			float screenX = Random.Range(pixelrect.xMin, pixelrect.xMax);
			float screenY = Random.Range(pixelrect.yMin, pixelrect.yMax);

			float ScreenZ = Random.Range(nearClip, farClip);
			Vector3 point = cam.ScreenToWorldPoint(new Vector3(screenX, screenY, ScreenZ));

			if (point.y < floor) point.y = floor - point.y;

//			Sprite sprite = newObject.GetComponent<SpriteRenderer>().sprite;


	//		Star star = newObject.AddComponent<Star>();

//			float size = (sprite.bounds.max.x * sprite.bounds.min.x) * newObject.transform.localScale.x;

			

			newObject.transform.position = point;
		}

		computeMinX();
	}

	void computeMinX()
	{
		Star[] stars = GetComponentsInChildren<Star>();

		Camera cam = Camera.main;

		Rect pixelRect = cam.pixelRect;

		float midPixelY =  (pixelRect.yMin + pixelRect.yMax) /2;
		float minPixelX =  pixelRect.xMin;

		foreach (Star star in stars)
		{
			Sprite sprite = star.GetComponent<SpriteRenderer>().sprite;
			float size = sprite.bounds.size.x * star.transform.lossyScale.x;

			//Debug.Log("size " + size);
			float ZValue = cam.transform.InverseTransformPoint(star.transform.position).z;
			Vector3 result = cam.ScreenToWorldPoint(new Vector3(minPixelX, midPixelY, ZValue));

			star.minX = result.x - size*2;
		}
	}

	public float widthCollider = 100;

/*	void buildOutCollider()
	{
		GameObject colliderObj = new GameObject();
		colliderObj.transform.parent = transform;
		BoxCollider box = colliderObj.AddComponent<BoxCollider>();
		Camera cam = Camera.main;

		float screenX = 0;
		float screenY = cam.pixelHeight / 2;
		//float ScreenZ = nearClip + farClip / 2;

		Vector3 p1 = cam.ScreenToWorldPoint(new Vector3(screenX, screenY, nearClip));
		Vector3 p2 = cam.ScreenToWorldPoint(new Vector3(screenX, screenY, farClip));
		colliderObj.transform.position = (p1 + p2) / 2;


		float height = cam.ScreenToWorldPoint(new Vector3(0, 0, farClip)).y * 2;
		float depth = (p1 - p2).magnitude;


		box.size = new Vector3(depth, height, widthCollider);
		float aspect = ((float)cam.pixelWidth / cam.pixelHeight);
		Debug.Log(aspect);

		aspect = cam.aspect;
		Debug.Log(aspect);
		aspect = 16f / 9f;

		float vFOVInRads = Camera.main.fieldOfView * Mathf.Deg2Rad;
		float hFOVInRads = 2 * Mathf.Atan(Mathf.Tan(vFOVInRads / 2) * Camera.main.aspect);
		float hFOV = hFOVInRads * Mathf.Rad2Deg;
		Debug.Log(hFOV);

		float dir = 90 - hFOV / 2;
		colliderObj.transform.localEulerAngles = new Vector3(0, dir, 0);

		colliderObj.transform.position -= colliderObj.transform.forward * widthCollider / 2;
	}
*/
	// Use this for initialization
	void Start()
	{
		CameraAspectRatio.onScreenChanged_event += onScreenChanged;
		computeMinX();
	}

	void onScreenChanged(Rect newPixelrect)
	{
		computeMinX();
	}

	// Update is called once per frame
	void Update()
	{

	}


	public void OnDrawGizmosSelected()
	{
		Camera cam = Camera.main;
		if (cam == null)
			return;
		

		Rect pixelRect = cam.pixelRect;


		Vector3 p1A = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMin, nearClip));
		Vector3 p2A = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMax, pixelRect.yMin, nearClip));
		Vector3 p3A = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMax, pixelRect.yMax, nearClip));
		Vector3 p4A = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMax, nearClip));

		Vector3 p1B = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMin, farClip));
		Vector3 p2B = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMax, pixelRect.yMin, farClip));
		Vector3 p3B = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMax, pixelRect.yMax, farClip));
		Vector3 p4B = cam.ScreenToWorldPoint(new Vector3(pixelRect.xMin, pixelRect.yMax, farClip));

		if (p1A.y < floor) p1A.y = floor;
		if (p2A.y < floor) p2A.y = floor;
		if (p3A.y < floor) p3A.y = floor;
		if (p4A.y < floor) p4A.y = floor;
		if (p1B.y < floor) p1B.y = floor;
		if (p2B.y < floor) p2B.y = floor;
		if (p3B.y < floor) p3B.y = floor;
		if (p4B.y < floor) p4B.y = floor;







		Gizmos.color = Color.cyan;

		Gizmos.DrawLine(p1A, p2A);
		Gizmos.DrawLine(p2A, p3A);
		Gizmos.DrawLine(p3A, p4A);
		Gizmos.DrawLine(p4A, p1A);

		Gizmos.DrawLine(p1B, p2B);
		Gizmos.DrawLine(p2B, p3B);
		Gizmos.DrawLine(p3B, p4B);
		Gizmos.DrawLine(p4B, p1B);

		Gizmos.DrawLine(p1A, p1B);
		Gizmos.DrawLine(p2A, p2B);
		Gizmos.DrawLine(p3A, p3B);
		Gizmos.DrawLine(p4A, p4B);
	}
}
