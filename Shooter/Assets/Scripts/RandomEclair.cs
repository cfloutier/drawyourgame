﻿using UnityEngine;
using System.Collections;

public class RandomEclair : MonoBehaviour 
{
	public GameObject[] eclairs;

	int curentIndex = 0;


	float nextFire;
	public Vector2 period = new Vector2(1, 10);
	public float durationShow = 0.05f;

	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < eclairs.Length; i++)
			eclairs[i].SetActive(false);
		nextFire = Time.time + Mathf.Lerp(period.x, period.y, Random.Range(0,1f));
	}

	void fire()
	{
		StartCoroutine(animate());
		nextFire = Time.time + Mathf.Lerp(period.x, period.y, Random.Range(0, 1f));
	}

	IEnumerator animate()
	{
		curentIndex = 0;
		while (curentIndex < eclairs.Length)
		{
			eclairs[curentIndex].SetActive(true);

			yield return new WaitForSeconds(durationShow);
			if (curentIndex >= eclairs.Length)
			{
				break; // vraiment étrange de temps en temps on passe là
			}
			eclairs[curentIndex].SetActive(false);
			curentIndex++;
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (nextFire < 0) return;

		if (Time.time > nextFire)
		{
			fire();
		}
	
	}
}
