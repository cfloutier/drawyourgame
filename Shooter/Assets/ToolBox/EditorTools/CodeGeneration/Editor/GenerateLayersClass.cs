﻿using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;

public static class GenerateLayersTagsClass
{
	public static string findAssetByName(string fileName)
	{
		string[] files = System.IO.Directory.GetFiles(Application.dataPath, fileName, System.IO.SearchOption.AllDirectories);
		foreach (string file in files)
		{
			return "Assets" + file.Substring(Application.dataPath.Length).Replace('\\', '/');
		}

		return "";
	}

	[MenuItem("Tools/CodeGeneration/Generate Layers Class")]
	public static void GenerateLayers()
	{
		string path = findAssetByName("Layers.cs");
		if (path == "")
			path =	"Assets/Layers.cs";

		using (var fileStream = new FileStream(path, FileMode.Create))
		{
			using (var writer = new StreamWriter(fileStream, Encoding.UTF8))
			{
				writer.WriteLine("public static class Layers");
				writer.WriteLine("{");
				for (int i = 0; i < 32; i++)
				{
					string name = UnityEditorInternal.InternalEditorUtility.GetLayerName(i);
					if (!string.IsNullOrEmpty(name))
					{
						if (Regex.IsMatch(name, "^[0-9]."))
						{
							name = "_" + name;
						}
						writer.WriteLine(string.Format("\tpublic const int {0} = {1};", name.Replace(" ", ""), i.ToString()));
					}
				}
				writer.WriteLine("}");
				writer.WriteLine("");
				writer.WriteLine("");

				writer.WriteLine("public static class LayersName");
				writer.WriteLine("{");
				for (int i = 0; i < 32; i++)
				{
					string name = UnityEditorInternal.InternalEditorUtility.GetLayerName(i);
					if (!string.IsNullOrEmpty(name))
					{
						if (Regex.IsMatch(name, "^[0-9]."))
						{
							name = "_" + name;
						}
						writer.WriteLine(string.Format("\tpublic static readonly string {0} = \"{1}\";", name.Replace(" ", ""), name));
					}
				}

				writer.WriteLine("}");
				
			}
		}

		AssetDatabase.Refresh();
	}


	[MenuItem("Tools/CodeGeneration/Generate Tags Class")]
	public static void GenerateTags()
	{
		string path = findAssetByName("Tags.cs");
		if (path == "")
			path = "Assets/Tags.cs";

	
        using (var fileStream = new FileStream(path, FileMode.Create))
        {
            using (var writer = new StreamWriter(fileStream, Encoding.UTF8))
            {
                writer.WriteLine("public static class Tags {");
 
                foreach (var tag in UnityEditorInternal.InternalEditorUtility.tags)
                {
					string name = tag;
					if (Regex.IsMatch(name, "^[0-9]."))
					{
						name = "_" + name;
					}

					writer.WriteLine(string.Format("\tpublic static readonly string {0} = \"{1}\";", name.Replace(" ", ""), tag));
                }
 
                writer.WriteLine("}");
            }
        }

		AssetDatabase.Refresh();
	}
}