// AlmostLogical Software - http://www.almostlogical.com - support@almostlogical.com
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class BonesViewerWindow : EditorWindow
{
    [MenuItem("Tools/Misc/Bones Viewer", false, 100)]
    static void Open()
    {
        // Get existing open window or if none, make a new one:
        BonesViewerWindow window = (BonesViewerWindow)EditorWindow.GetWindow(typeof(BonesViewerWindow));
        window.titleContent = new GUIContent("Bones Viewer 0.1");
        window.Show();
    }

    static BonesPositionsMemory current = null;
    static float sizeCap = 0.1f;
    static SceneView _sceneview;
    static bool m_showNodes = true;
    static bool m_showLines = true;
    static bool m_showNames = false;
	static bool m_autoCenter = true;
    static int depth = 20;

	BonesPositionsMemory[] memoryList;
	
	void findPosers()
	{
		memoryList = (BonesPositionsMemory[])Object.FindObjectsOfType<BonesPositionsMemory>();
	}

	void addPoserOnSelection()
	{
		if (Selection.activeTransform != null)
		{
			BonesPositionsMemory bonesMem = Selection.activeTransform.gameObject.GetComponent<BonesPositionsMemory>();
			if (bonesMem == null)
				bonesMem = Selection.activeTransform.gameObject.AddComponent<BonesPositionsMemory>();

			current = bonesMem;
		}
	}

    void OnEnable()
    {
        current = null;
		findPosers();
        autoRepaintOnSceneChange = true;
		readPrefs();
        SceneView.onSceneGUIDelegate += OnScene;
    }

	void OnDisable()
	{
		writePrefs();
		SceneView.onSceneGUIDelegate -= OnScene;
	}

	void readPrefs()
	{
		sizeCap = EditorPrefs.GetFloat("BonesWindow.sizeCap", sizeCap);
		m_showNodes = EditorPrefs.GetBool("BonesWindow.showBones", m_showNodes);
		m_showLines = EditorPrefs.GetBool("BonesWindow.showLines", m_showLines);
		m_showNames = EditorPrefs.GetBool("BonesWindow.showNames", m_showNames);
		m_autoCenter = EditorPrefs.GetBool("BonesWindow.autoCenter", m_autoCenter);

	}

	void writePrefs()
	{
		EditorPrefs.SetFloat("BonesWindow.sizeCap", sizeCap);
		EditorPrefs.SetBool("BonesWindow.showBones", m_showNodes);
		EditorPrefs.SetBool("BonesWindow.showLines", m_showLines);
		EditorPrefs.SetBool("BonesWindow.showNames", m_showNames);
		EditorPrefs.SetBool("BonesWindow.autoCenter", m_autoCenter);

		
	}

    void OnSelectionChange()
    {
        //SceneView.currentDrawingSceneView.

        if (Selection.activeTransform != null && current != null && SceneView.currentDrawingSceneView != null)
        {
			if (m_autoCenter)
			{
				if (isParent(Selection.activeTransform, current.transform))
					SceneView.currentDrawingSceneView.LookAt(Selection.activeTransform.position);
			}
        }

        Repaint();
    }

    public static bool isParent(Transform tr, Transform possibleParent)
    {
        if (tr == possibleParent || tr.parent == possibleParent)
            return true;

        if (tr.parent == null)
            return false;

        return isParent(tr.parent, possibleParent);
    }

    void OnDestroy()
    {
        SceneView.onSceneGUIDelegate -= OnScene;
    }

    static void ShowPos(Transform tr, int depth)
    {
        if (m_showLines)
            Handles.DrawLine(tr.parent.position, tr.position);

        Handles.DrawCapFunction capfct = Handles.ConeCap;
        float size = HandleUtility.GetHandleSize(tr.position);
        Quaternion delta = Quaternion.identity;
		capfct = Handles.SphereCap;

		Handles.color = Color.white;

		if (Selection.activeTransform == tr)
			Handles.color = Color.cyan;
		
        if (Handles.Button(tr.position, tr.rotation * delta, sizeCap * size, sizeCap * size, capfct))
        {
            Selection.activeTransform = tr;
        }

        if (m_showNames || Selection.activeTransform == tr)
            Handles.Label(tr.position, "     " + tr.name);

        depth--;
        if (depth == 0) return;
        for (int i = 0; i < tr.childCount; i++)
            ShowPos(tr.GetChild(i), depth);
    }

    private static void OnScene(SceneView sceneview)
    {
        _sceneview = sceneview;
        if (m_showNodes)
        {
            if (current != null)
            {
                //Debug.Log("This event opens up so many possibilities.");
                ShowPos(current.transform, depth + 1);
            }
        }
    }

    void showBones(BonesPositionsMemory viewer)
    {
        Selection.activeTransform = viewer.transform;
        current = viewer;
    }

    void repaint()
    {
        if (_sceneview != null)
            _sceneview.Repaint();
    }


    public bool toggle(string name, bool value)
    {
        bool val = GUILayout.Toggle(value, name);
        if (val != value)
        {
            value = val;
            repaint();
        }
        return value;
    }
    public float slider(string name, float value, float min, float max)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(name + " : " + value, GUILayout.Width(300));

        float val = GUILayout.HorizontalSlider(value, min, max, GUILayout.ExpandWidth(true));
        if (val != value)
        {
            value = val;
            repaint();
        }
        GUILayout.EndHorizontal();
        return value;
    }

	void guiNoMemory()
	{
		if (Selection.activeTransform != null)
		{
			if (GUILayout.Button("Add Poser on Selection : " + Selection.activeTransform.name, GUILayout.Height(40)))
			{
				addPoserOnSelection();
				findPosers();
			}
		}
		else
		{
			GUILayout.Space(20);
			GUILayout.Label("Please select root transform and Press 'Add Root Node'");
		}

		if (GUILayout.Button("Find existing pose memory", GUILayout.Height(40)))
		{
			findPosers();
		}
	}

	void guiRootSelection()
	{

		if (current == null)
		{
			GUILayout.Label("Choose an existing Root Node : ");
		}
		else
		{
			if (GUILayout.Button("Stop Bone Viewer", GUILayout.Height(30)))
			{
				current = null;
				SceneView.currentDrawingSceneView.Repaint();
			}
		}

		GUILayout.BeginHorizontal();

		for (int i = 0; i < memoryList.Length; i++)
		{
//			bool isCurrent = current == memoryList[i];
			if (GUILayout.Button(memoryList[i].name, GUILayout.Height(30)))
			{
				current = memoryList[i];
				Selection.activeTransform = current.transform;
				SceneView.currentDrawingSceneView.Repaint();
			}

			if (i % 3 == 2)
			{
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
			}
		}

		GUILayout.EndHorizontal();

		GUILayout.Space(20);

		if (Selection.activeTransform != null)
		{
			if (GUILayout.Button("Add Poser on Selection : " + Selection.activeTransform.name, GUILayout.Height(40)))
			{
				addPoserOnSelection();
				findPosers();
			}
		}
	}

   void guiPoses()
    {
		if (current == null)
			mode = Mode.PosersChooser;

        GUILayout.Label("Poses : ");

        int widthBT = 100;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Start Pose");

        if (GUILayout.Button("Record", GUILayout.Width(widthBT)))
        {
            current.recordStart();
        }

        if (GUILayout.Button("Apply", GUILayout.Width(widthBT)))
        {
            current.applyStartPose();
        }

        GUILayout.Space(widthBT + 4);
        GUILayout.EndHorizontal();

        if (current.poses != null)
        {

            for (int i = 0; i < current.poses.Count; i++)
            {
                BonesPositionsMemory.Pose pose = current.poses[i];

                GUILayout.BeginHorizontal();
                pose.Name = GUILayout.TextField(pose.Name);

                if (GUILayout.Button("Record", GUILayout.Width(widthBT)))
                {
                    current.recordPose(pose);
                }

                if (GUILayout.Button("Apply", GUILayout.Width(widthBT)))
                {
                    current.applyPose(pose);
                }

                if (GUILayout.Button("Delete", GUILayout.Width(widthBT)))
                {
                    current.deletePose(i);
                }

                GUILayout.EndHorizontal();
            }
        }

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("++ Record New +++", GUILayout.Width(widthBT + 50)))
        {
            current.recordNew();
        }

        GUILayout.EndHorizontal();

    }
   void guiNavigation()
   {
	   if (current != null)
	   {
		   Transform tr = Selection.activeTransform;

		   if (tr.parent != null && current.transform != tr)
		   {
			   GUI.color = Color.cyan;
			   GUILayout.Label("Parent :");
			   GUILayout.BeginHorizontal();
			   GUILayout.FlexibleSpace();
			   if (GUILayout.Button(tr.name, GUILayout.Height(30)))
			   {
				   Selection.activeTransform = tr.parent;
			   }
			   GUILayout.FlexibleSpace();
			   GUILayout.EndHorizontal();
		   }

		   if (tr.childCount != 0)
		   {
			   GUI.color = Color.yellow;
			   GUILayout.Label("Children :");
			   //GUI.color = Color.white;
			   GUILayout.BeginHorizontal();
			   GUILayout.FlexibleSpace();
			   for (int i = 0; i < tr.childCount; i++)
			   {
				   Transform tc = tr.GetChild(i);

				   if (GUILayout.Button(tc.name, GUILayout.Height(30)))
				   {
					   Selection.activeTransform = tc;
				   }

				   if (i % 3 == 2)
				   {
					   GUILayout.FlexibleSpace();
					   GUILayout.EndHorizontal();
					   GUILayout.BeginHorizontal();
					   GUILayout.FlexibleSpace();
				   }
			   }
			   GUILayout.FlexibleSpace();
			   GUILayout.EndHorizontal();
		   }

		   GUI.color = Color.white;
	   }

	   GUILayout.Space(15);
   }
   void guiOptions()
    {
        m_showNodes = toggle("Show Nodes", m_showNodes);
        m_showLines = toggle("Show Lines", m_showLines);
        m_showNames = toggle("Show Nodes Names", m_showNames);
		m_autoCenter = toggle("Auto Center Cam", m_autoCenter);




		if (current != null)
			current.maintainsPositions = GUILayout.Toggle(current.maintainsPositions, "Avoid positions moves ");

        sizeCap = slider("Bones Size", sizeCap, 0, 1);
        depth = (int)slider("Depth ", depth, 0, 30);


    }
		enum Mode
	{
		PosersChooser,
		PosesRecorder,
		Navigation,
		Options
	}

	Mode mode = Mode.PosersChooser;

    public void OnGUI()
    {
		GUILayout.Space(10);
		GUILayout.Label("Poser !");
		GUILayout.Space(10);

		if (memoryList.Length == 0)
		{
			guiNoMemory();
			return;
		}

		mode = (Mode) GUILayout.SelectionGrid((int) mode, new string[]
			{
				"Main",
				"Recorder",
				"Navigation",
				"Options"
			}, 4, GUILayout.Height(25));
		GUILayout.Space(20);
		switch (mode)
		{
			case Mode.PosersChooser:
				guiRootSelection();
				break;
			case Mode.PosesRecorder:
				guiPoses();
				break;
			case Mode.Navigation:
				guiNavigation();
				break;
			case Mode.Options:
				guiOptions();
				break;
			default:
				break;
		}
    }

}
