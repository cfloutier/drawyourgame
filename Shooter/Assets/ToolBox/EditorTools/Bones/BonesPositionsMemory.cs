using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class BonesPositionsMemory : MonoBehaviour 
{
    [System.Serializable]
    public class Pose
    {
        public string Name;

        public List<BonePosition> bonePositions;

        public void record(Transform tr)
        {
            bonePositions = new List<BonePosition>();
			BonePosition pos = new BonePosition("", tr);
			pos.trName = "Root";
			bonePositions.Add(pos);

            string baseName = "";
            recordChild(baseName, tr);
        }

        void recordChild(string baseName, Transform tr)
        {  
            for (int i = 0; i < tr.childCount; i++)
            {
                BonePosition pos = new BonePosition(baseName, tr.GetChild(i));
                bonePositions.Add(pos);

                recordChild(pos.trName, tr.GetChild(i));
            }
        }

        public void applyRot(Transform rootTr)
        {
           apply(rootTr , applyRot);
        }


        void applyRot(Transform currentTr, BonePosition pos)
        {
            currentTr.localEulerAngles = pos.rot;
        }

         void applyPos(Transform currentTr, BonePosition pos)
        {
            currentTr.localPosition = pos.pos;
        }

        delegate void  ApplySomething(Transform currentTr, BonePosition pos);
       

        void apply(Transform rootTr, ApplySomething fct )
        {
            for (int i = 0; i < bonePositions.Count; i++)
            {
                BonePosition bone = bonePositions[i];

                Transform tr = null;
				if (bone.trName == "Root")
                    tr = rootTr;
                else
                    tr = rootTr.FindChild(bone.trName);

                if (tr == null)
                {
                    Debug.LogWarning(bone.trName + " not found !!");
                    continue;
                }

                fct(tr, bone);
            }
        }

    }

    [System.Serializable]
    public class BonePosition 
    {
        public string trName;
        public Vector3 pos;
        public Vector3 rot;

        public BonePosition(string baseName, Transform tr)
        {
            trName = baseName == "" ? tr.name : baseName + "/" + tr.name;
            pos = tr.localPosition;
            rot = tr.localEulerAngles;
        }
    }

    public Pose startPose;

    public List<Pose> poses;

	public bool maintainsPositions = true;

    void Start()
    {
        if (startPose == null)
            recordStart();
    }

	public void recordStart()
	{
        startPose = new Pose();
        startPose.record(transform);

        startPose.Name = "Start";
	}

    public void recordPose(Pose pose)
    {
        pose.record(transform);
    }

    public void applyStartPose()
    {
       // UnityEditor.Undo.RecordObject(transform, "Apply Start Pose");
        startPose.applyRot(transform);
    }

    public void applyPose(Pose pose)
    {
        //  UnityEditor.Undo.RecordObject(transform, "Apply Pose " + pose.name);
        pose.applyRot(transform);
    }

    public void recordNew()
    {
        Pose newPose = new Pose();
        newPose.record(transform);

        newPose.Name = "New Pose";

        if (poses == null)
            poses = new List<Pose>();

        poses.Add(newPose);
    }

    public void deletePose(int index)
    {
        poses.RemoveAt(index);
      //  UnityEditor.Undo.RecordObject(transform, "Apply Pose " + pose.name);
    }

	void Update()
	{
        
	}
}
