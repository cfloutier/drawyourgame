﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEditor;

public class CleanUpAssets : EditorWindow
{
	[MenuItem("Tools/List and Clean Up Assets")]
	public static void Init()
	{
		// Get existing open window or if none, make a new one:
		CleanUpAssets window = (CleanUpAssets)EditorWindow.GetWindow(typeof(CleanUpAssets));
		window.Show();
		window.titleContent = new GUIContent( "List and Clean Up Assets");
	}

	void OnEnable()
	{

	}

	void OnDisable()
	{

	}

	void listDependencies()
	{
		UnityEngine.Object[] unfilteredResult = EditorUtility.CollectDependencies(rootObjects.ToArray());

		result.Clear();
		for (int i = 0; i < unfilteredResult.Length; i++)
		{
			string path = AssetDatabase.GetAssetPath(unfilteredResult[i]);
			if (path == "")
				continue;

			if (!path.StartsWith("Assets"))
				continue;

			string guiid = AssetDatabase.AssetPathToGUID(path);

			if (result.IndexOf(guiid) == -1)
				result.Add(guiid);
		}
	}

	void CreateDirectoryRecursive(string fullPath)
	{
		if (!System.IO.Directory.Exists(fullPath))
		{
			string[] dirs = fullPath.Split('/');
			string path = dirs[0];
			for (int i = 1; i < dirs.Length; i++)
			{
				path += "/" + dirs[i];
				//  Debug.Log("path " + path);
				if (!System.IO.Directory.Exists(path))
				{
					System.IO.Directory.CreateDirectory(path);
				}
			}
		}
	}

	void listFilesToCleanUp()
	{
		string[] folders = cleanUpdirectories.ToArray();
		string[] safeFolders = directoryToAvoid.ToArray();

		filesToCleanUp.Clear();
		string[] res = AssetDatabase.FindAssets("", folders);
		List<string> fileToSave = new List<string>();
		for (int i = 0; i < result.Count; i++)
		{
			fileToSave.Add(AssetDatabase.GUIDToAssetPath(result[i]));
		}

		for (int i = 0; i < res.Length; i++)
		{
			string path = AssetDatabase.GUIDToAssetPath(res[i]);

			// avoid scripts
			if (path.EndsWith(".cs") || path.EndsWith(".js"))
				continue;

			string fullPath = Application.dataPath + path.Substring(6);
			// if thsi is a directory
			if (System.IO.Directory.Exists(fullPath))
				continue;

			bool keepThisFile = false;
			for (int j = 0; j < safeFolders.Length; j++)
			{
				if (path.StartsWith(safeFolders[j]))
					keepThisFile = true;
			}

			if (keepThisFile)
				continue;

			// check that the file is not contained in the reference List			
			if (fileToSave.IndexOf(path) == -1)
			{
				if (filesToCleanUp.IndexOf(path) == -1)
					filesToCleanUp.Add(path);
			}
		}

	}

	void doCleanUp()
	{
		for (int i = 0; i < filesToCleanUp.Count; i++)
		{
			string path = filesToCleanUp[i];
			string fullPath = Application.dataPath + path.Substring(6);

			string relDest = destinationDirectory.Substring(6) + path.Substring(6);
			string relDestDir = relDest.Substring(0, relDest.LastIndexOf("/"));

			string fulldestDir = Application.dataPath + relDestDir;

			if (!System.IO.Directory.Exists(fullPath))
			{
				CreateDirectoryRecursive(fulldestDir);

				AssetDatabase.ImportAsset("Assets" + relDestDir);
			}

			string newFileName = "Assets" + relDest;

			string resultError = AssetDatabase.MoveAsset(path, newFileName);
			if (!string.IsNullOrEmpty(resultError))
			{
				Debug.LogError("Error moving file ");
				Debug.LogError("from : " + path);
				Debug.LogError("to : " + newFileName);
				Debug.LogError(resultError);

				return;
			}
			else
				Debug.Log("Moved file to " + newFileName);


			//	Debug.Log("Moved file to " + newFileName);
		}
		filesToCleanUp.Clear();
	}

	void findEmptyDirectories()
	{
		emptyDirs.Clear();

		for (int i = 0; i < cleanUpdirectories.Count; i++)
		{
			findEmptyDirectories(cleanUpdirectories[i]);
		}
	}

	void findEmptyDirectories(string path)
	{
		for (int i = 0; i < directoryToAvoid.Count; i++)
		{
			if (path.StartsWith(directoryToAvoid[i]))
				return;
		}

		if (isEmpty(path))
		{
			emptyDirs.Add(path);
			return;
		}

		string fullPath = Application.dataPath + path.Substring(6);
		if (System.IO.Directory.Exists(fullPath))
		{
			System.IO.DirectoryInfo infos = new System.IO.DirectoryInfo(fullPath);


			System.IO.DirectoryInfo[] subDirs = infos.GetDirectories();
			for (int i = 0; i < subDirs.Length; i++)
			{
				string subPath = path + "/" + subDirs[i].Name;
				findEmptyDirectories(subPath);
			}
		}
	}


	bool isEmpty(string path)
	{
		string fullPath = Application.dataPath + path.Substring(6);
		if (System.IO.Directory.Exists(fullPath))
		{

			System.IO.DirectoryInfo infos = new System.IO.DirectoryInfo(fullPath);

			System.IO.FileInfo[] files = infos.GetFiles();
			for (int i = 0; i < files.Length; i++)
			{
				if (files[i].Extension != ".meta")
				{
					// this is a valid file
					return false;
				}
			}

			System.IO.DirectoryInfo[] subDirs = infos.GetDirectories();
			for (int i = 0; i < subDirs.Length; i++)
			{
				string subPath = path + "/" + subDirs[i].Name;
				if (!isEmpty(subPath))
					return false;
			}
		}
		return true;
	}



	List<UnityEngine.Object> rootObjects = new List<UnityEngine.Object>();
	List<string> result = new List<string>();
	List<string> cleanUpdirectories = new List<string>();
	List<string> directoryToAvoid = new List<string>();
	List<string> filesToCleanUp = new List<string>();
	List<string> emptyDirs = new List<string>();

	string destinationDirectory = "";

	Vector2 scrollPos = Vector2.zero;

	//GUISkin controlSkin;
	void loadSkin()
	{
		//if (skin == null)
		{
			string[] list = AssetDatabase.FindAssets("Skin_AssetsCleanUp");

			if (list == null || list.Length == 0)
			{
				Debug.LogError("Skin_AssetsCleanUp skin not found");
				return;
			}

			skin = (GUISkin)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(list[0]), typeof(GUISkin));
		}

		if (skin != null)
		{
			//GUI.skin = skin;
			Group = skin.GetStyle("General.Group");
		}
	}

	GUIStyle Group;


	void RootObjectsGUI()
	{
		for (int i = 0; i < rootObjects.Count; i++)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(rootObjects[i].name);
			if (GUILayout.Button("Remove", GUILayout.Width(100)))
			{
				rootObjects.RemoveAt(i);
				break;
			}

			GUILayout.EndHorizontal();

			GUILayout.Space(10);
		}

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("Drop here New Object as root\n(From the Assets directory)");

		UnityEngine.Object refObject = EditorGUILayout.ObjectField(null, typeof(UnityEngine.Object), false, GUILayout.Width(100), GUILayout.Height(40));
		GUILayout.EndHorizontal();
		if (refObject != null)
		{
			if (rootObjects.IndexOf(refObject) == -1)
				rootObjects.Add(refObject);
		}
	}

	void DirectoriesGUI(List<String> listing)
	{
		for (int i = 0; i < listing.Count; i++)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(listing[i]);
			if (GUILayout.Button("Remove", GUILayout.Width(100)))
			{
				listing.RemoveAt(i);
				break;
			}

			GUILayout.EndHorizontal();

			GUILayout.Space(10);
		}
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		GUILayout.Label("Drop here\nnew directory");

		UnityEngine.Object refObject = EditorGUILayout.ObjectField(null, typeof(UnityEngine.Object), false, GUILayout.Width(100), GUILayout.Height(40));
		GUILayout.EndHorizontal();
		if (refObject != null)
		{
			string path = AssetDatabase.GetAssetPath(refObject);

			if (path != "" && path.StartsWith("Assets"))
			{
				string fullPath = Application.dataPath + path.Substring(6);
				if (System.IO.Directory.Exists(fullPath))
					listing.Add(path);
				else
					Debug.LogError(path + "is not a directory");

			}
		}
	}

	void emptyDirsGUI()
	{
		for (int i = 0; i < emptyDirs.Count; i++)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(emptyDirs[i]);
			if (GUILayout.Button("Remove from list", GUILayout.Width(150)))
			{
				emptyDirs.RemoveAt(i);
				break;
			}

			GUILayout.EndHorizontal();

			GUILayout.Space(10);
		}

		if (emptyDirs.Count != 0)

		if (GUILayout.Button("Clean Up", GUILayout.Height(40)))
		{
			for (int i = 0; i < emptyDirs.Count; i++)
			{
				AssetDatabase.DeleteAsset(emptyDirs[i]);
			}

			emptyDirs.Clear();
		}

	}

	void destDirGUI()
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label("Drop Destination Dir Here");

		UnityEngine.Object refObject = EditorGUILayout.ObjectField(null, typeof(UnityEngine.Object), false, GUILayout.Height(40));
		GUILayout.EndHorizontal();
		if (refObject != null)
		{
			string path = AssetDatabase.GetAssetPath(refObject);

			if (path != "" && path.StartsWith("Assets"))
				destinationDirectory = path;
		}

		GUILayout.Label("Destination Directory : " + destinationDirectory);
	}

	GUISkin skin = null;

	bool rootObjectsOpened = true;
	bool directoriesOpened = true;
	bool directories2Opened = true;
	bool destDirOpened = true;
	bool emptyDirsOpened = true;
	bool listFilesOpened = true;
	bool listUnusedFiles = true;

	void OnGUI()
	{
		loadSkin();

		scrollPos = GUILayout.BeginScrollView(scrollPos);

		rootObjectsOpened = GUILayout.Toggle(rootObjectsOpened, "- Root Objects - ", Group);
		if (rootObjectsOpened)
		{
			RootObjectsGUI();
		}

		if (rootObjects.Count > 0)
			GUI.enabled = true;
		else
			GUI.enabled = false;


		if (GUILayout.Button("Find references", GUILayout.Height(40)))
		{
			listDependencies();
		}

		GUI.enabled = true;


		listFilesOpened = GUILayout.Toggle(listFilesOpened, "Assets List to keep (" + result.Count + ")", Group);

		if (listFilesOpened)
		{
			for (int i = 0; i < result.Count; i++)
			{
				string path = AssetDatabase.GUIDToAssetPath(result[i]);
				if (GUILayout.Button(path))
				{
					Selection.activeObject = AssetDatabase.LoadAssetAtPath(path, typeof(UnityEngine.Object));
				}
			}
		}

		directoriesOpened = GUILayout.Toggle(directoriesOpened, "Directories to cleanUp : " + cleanUpdirectories.Count, Group);

		if (directoriesOpened)
		{
			DirectoriesGUI(cleanUpdirectories);
		}

		directories2Opened = GUILayout.Toggle(directories2Opened, "Directories to Keep  " + directoryToAvoid.Count, Group);

		if (directories2Opened)
		{
			DirectoriesGUI(directoryToAvoid);
		}

		GUI.enabled = true;

		if (cleanUpdirectories.Count > 0 && result.Count > 0)
			GUI.enabled = true;
		else
			GUI.enabled = false;

		if (GUILayout.Button("List File to cleanUp", GUILayout.Height(40)))
		{
			listFilesToCleanUp();
		}

		listUnusedFiles = GUILayout.Toggle(listUnusedFiles, "Assets List to Move (" + filesToCleanUp.Count + ")", Group);

		if (listUnusedFiles)
		{
			for (int i = 0; i < filesToCleanUp.Count; i++)
			{
				//string path = AssetDatabase.GUIDToAssetPath(filesToCleanUp[i]);
				if (GUILayout.Button(filesToCleanUp[i]))
				{
					Selection.activeObject = AssetDatabase.LoadAssetAtPath(filesToCleanUp[i], typeof(UnityEngine.Object));
				}
			}
		}
		GUI.enabled = true;
		destDirOpened = GUILayout.Toggle(destDirOpened, "Destination Directory", Group);

		if (destDirOpened)
		{
			destDirGUI();
		}

		if (filesToCleanUp.Count > 0 && destinationDirectory != "")
			GUI.enabled = true;
		else
			GUI.enabled = false;

		if (GUILayout.Button("Clean Up", GUILayout.Height(40)))
		{
			doCleanUp();
		}

		GUI.enabled = true;

		if (GUILayout.Button("List Empty Dirs", GUILayout.Height(40)))
		{
			findEmptyDirectories();
		}

		emptyDirsOpened = GUILayout.Toggle(emptyDirsOpened, "Empty Directories : " + emptyDirs.Count, Group);

		if (emptyDirsOpened)
		{
			emptyDirsGUI();
		}

		GUILayout.EndScrollView();
	}
}
