using UnityEngine;
using UnityEditor;
using System.Collections;

public class RenderCubmapToFile : ScriptableWizard
{
	public Transform renderFromPosition;
	public int sizeImage = 512;
	public string DirectoryImages = "skyBox";
	public Cubemap cubemap;

	static Transform lastTransform = null;

    string[] skyBoxImage = {"front", "right", "back", "left", "top", "bottom"};
    Vector3[] skyDirection = {new Vector3(0, 0, 0), new Vector3(0, -90, 0), new Vector3(0, 180, 0), new Vector3(0, 90, 0), new Vector3(-90, 0, 0), new Vector3(90, 0, 0)};
    
    void OnWizardUpdate()
    {
		if (renderFromPosition == null) renderFromPosition = lastTransform;
        helpString = "Select transform to render from";
		isValid = (renderFromPosition != null && sizeImage > 0);
    }
   
    void OnWizardCreate()
    {
		lastTransform = renderFromPosition;
        GameObject go = new GameObject ("SkyboxCamera");
        go.AddComponent(typeof(Camera));
       
        go.GetComponent<Camera>().backgroundColor = Color.black;
        go.GetComponent<Camera>().clearFlags = CameraClearFlags.Skybox;
        go.GetComponent<Camera>().fieldOfView = 90;   
        go.GetComponent<Camera>().aspect = 1.0f;
		go.transform.position = renderFromPosition.position;


		if (renderFromPosition.GetComponent<Renderer>())
		{
			go.transform.position = renderFromPosition.GetComponent<Renderer>().bounds.center;
		}

		go.transform.rotation = Quaternion.identity;


		if (cubemap == null)
		{
			for (int i = 0; i < skyDirection.Length; i++)
			{
				renderSkyImage(i, go);
			}

			
		}
		else
		{
		
			go.GetComponent<Camera>().RenderToCubemap(cubemap);
		}

		DestroyImmediate(go);
       
      
    }
   
    [MenuItem("Tools/Misc/Render Skybox", false, 4)]

    static void RenderSkyBox()
    {
        ScriptableWizard.DisplayWizard ("Render SkyBox", typeof(RenderCubmapToFile), "Render!");
    }
   
    void renderSkyImage(int orientation, GameObject go)
    {

		Debug.Log("renderSkyImage " + orientation);


        go.transform.eulerAngles = skyDirection[orientation];

		RenderTexture rt = new RenderTexture(sizeImage, sizeImage, 24);
        go.GetComponent<Camera>().targetTexture = rt;
		Texture2D screenShot = new Texture2D(sizeImage, sizeImage, TextureFormat.RGB24, false);
        go.GetComponent<Camera>().Render();
        RenderTexture.active = rt;
		screenShot.ReadPixels(new Rect(0, 0, sizeImage, sizeImage), 0, 0);
        RenderTexture.active = null;
        DestroyImmediate (rt);
 
        byte[] bytes = screenShot.EncodeToPNG();

		var directory = "Assets/Skyboxes/" + DirectoryImages;
        if (!System.IO.Directory.Exists(directory))
        System.IO.Directory.CreateDirectory(directory);
		System.IO.File.WriteAllBytes(System.IO.Path.Combine(directory , skyBoxImage[orientation] + ".png"), bytes); 
    }
}
