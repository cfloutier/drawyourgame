﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


public class CameraSnap : EditorWindow
{
	[MenuItem("Tools/SnapShot/Sized SnapShot")]
	static void openWindow()
	{
		// Get existing open window or if none, make a new one:
		CameraSnap window = (CameraSnap)EditorWindow.GetWindow(typeof(CameraSnap));
		window.titleContent = new GUIContent( "Camera Snap" );
		window.Show();
	}

	int width_ = 0;
	int height_ = 0;

	int realWidth = 0;
	int realHeight = 0;
	float multiplier = 1;
	bool transparent = true;

	void OnGUI()
	{
		GUILayout.BeginHorizontal();

		GUILayout.BeginVertical();
		if (GUILayout.Button("Get Game Screen size"))
		{
			Vector2 size = GetMainGameViewSize();
			width_ = (int)size.x;
			height_ = (int)size.y;
		}

		width_ = EditorGUILayout.IntField("Width", width_);
		height_ = EditorGUILayout.IntField("Height", height_);
		multiplier = EditorGUILayout.Slider("x " + multiplier, multiplier, 0.1f, 10f);


		realWidth = ((int)(width_ * multiplier));
		realHeight = ((int)(height_ * multiplier));
		if (realWidth > 8192)
		{
			realWidth = 8192;
			realHeight = (int)(realWidth / width_ * height_);
		}

		if (realHeight > 8192)
		{
			realHeight = 8192;
			realWidth = (int)(realHeight / height_ * width_);
		}


		GUILayout.Label("Real Size is " + realWidth + " x " + realHeight);

		transparent = EditorGUILayout.Toggle("Transparent", transparent);
		GUILayout.EndVertical();

		if (GUILayout.Button("doSnap", GUILayout.Width(150), GUILayout.Height(70)))
		{
			takeASnapshot(realWidth, realHeight, transparent);
		}

		GUILayout.EndHorizontal();

	}

	void OnEnable()
	{
		if (EditorPrefs.HasKey("SnapShot.width"))
		{
			width_ = EditorPrefs.GetInt("SnapShot.width");
			height_ = EditorPrefs.GetInt("SnapShot.height");
			multiplier = EditorPrefs.GetFloat("SnapShot.multiplier");
			transparent = EditorPrefs.GetInt("SnapShot.transparent") == 1;
		}
		else
		{
			width_ = Screen.width;
			height_ = Screen.height;
			multiplier = 1;
			transparent = false;
		}
	}

	void OnDisable()
	{
		EditorPrefs.SetInt("SnapShot.width", width_);
		EditorPrefs.SetInt("SnapShot.height", height_);
		EditorPrefs.SetFloat("SnapShot.multiplier", multiplier);
		EditorPrefs.SetInt("SnapShot.transparent", transparent ? 1 : 0);
	}

	[MenuItem("Tools/SnapShot/Screen Size Transparent SnapShot #% ")]
	static void takeASnapshot()
	{
		takeASnapshot(Screen.width, Screen.height, true);
	}


	static void takeASnapshot(int width, int height, bool transparent)
	{
		takeASnapshot(Camera.main,  Application.dataPath + "/../SnapShot_" + index + ".png",
			width, height, transparent);
	}

	static public void takeASnapshot(Camera cam, string path, int width, int height, bool transparent)
	{
		if (transparent)
		{
			createCams(cam, width, height);

			CameraBlack.GetComponent<Camera>().Render();
			CameraWhite.GetComponent<Camera>().Render();
			CameraColor.GetComponent<Camera>().Render();

			finalFilePath = path;

			OnPostRender();
		}
		else
		{
			//Debug.Log("not transparent");
			renderTextureColor = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);

			cam.targetTexture = renderTextureColor;
			cam.Render();

			RenderTexture.active = renderTextureColor;
			finaltexture = new Texture2D(renderTextureColor.width, renderTextureColor.height, TextureFormat.RGB24, false);
			finaltexture.ReadPixels(new Rect(0, 0, renderTextureColor.width, renderTextureColor.height), 0, 0);

			finaltexture.Apply();

			save(path);

			cam.targetTexture = null;
			RenderTexture.active = null;

			DestroyImmediate(renderTextureColor);
			renderTextureColor = null;
			DestroyImmediate(finaltexture);
			finaltexture = null;
		}

		System.GC.Collect();
		Resources.UnloadUnusedAssets();
	}

	public static Vector2 GetMainGameViewSize()
	{
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
		System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
		return (Vector2)Res;
	}

	static Texture2D blacktexture;
	static Texture2D whitetexture;
	static Texture2D colortexture;
	static Texture2D finaltexture;

	static GameObject CameraWhite;
	static GameObject CameraBlack;
	static GameObject CameraColor;
	static RenderTexture renderTextureBlack;
	static RenderTexture renderTextureWhite;
	static RenderTexture renderTextureColor;

	static void createCams(Camera cam, int width, int height)
	{
		if (CameraWhite == null)
		{
			renderTextureWhite = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);

			CameraWhite = new GameObject("CameraWhite");
			Camera camW = CameraWhite.AddComponent<Camera>();
			camW.transform.parent = cam.transform;
			camW.transform.localPosition = Vector3.zero;
			camW.transform.localRotation = Quaternion.identity;
			camW.transform.localScale = Vector3.one;

			camW.backgroundColor = new Color(1, 1, 1, 0);
			camW.depth = 101;
			camW.fieldOfView = cam.fieldOfView;
			camW.farClipPlane = cam.farClipPlane;
			camW.nearClipPlane = cam.nearClipPlane;
			camW.clearFlags = CameraClearFlags.Color;

			camW.targetTexture = renderTextureWhite;
		}

		if (CameraBlack == null)
		{
			renderTextureBlack = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);

			CameraBlack = new GameObject("CameraBlack");
			Camera camB = CameraBlack.AddComponent<Camera>();
			camB.transform.parent = cam.transform;
			camB.transform.localPosition = Vector3.zero;
			camB.transform.localRotation = Quaternion.identity;
			camB.transform.localScale = Vector3.one;

			camB.backgroundColor = new Color(0, 0, 0, 0);

			camB.depth = 100;
			camB.fieldOfView = cam.fieldOfView;
			camB.farClipPlane = cam.farClipPlane;
			camB.nearClipPlane = cam.nearClipPlane;
			camB.clearFlags = CameraClearFlags.Color;

			camB.targetTexture = renderTextureBlack;
		}

		if (CameraColor == null)
		{
			renderTextureColor = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);

			CameraColor = new GameObject("CameraBlack");
			Camera camC = CameraColor.AddComponent<Camera>();
			camC.transform.parent = cam.transform;
			camC.transform.localPosition = Vector3.zero;
			camC.transform.localRotation = Quaternion.identity;
			camC.transform.localScale = Vector3.one;

			camC.backgroundColor = cam.backgroundColor;

			camC.depth = 103;
			camC.fieldOfView = cam.fieldOfView;
			camC.farClipPlane = cam.farClipPlane;
			camC.nearClipPlane = cam.nearClipPlane;
			camC.clearFlags = CameraClearFlags.Color;

			camC.targetTexture = renderTextureColor;
		}
	}

	static string finalFilePath = "";

	static int index = 0;
	static void OnPostRender()
	{
		RenderTexture.active = renderTextureBlack;
		blacktexture = new Texture2D(
			renderTextureColor.width, renderTextureColor.height,
			TextureFormat.ARGB32, false);

		blacktexture.ReadPixels(new Rect(0, 0, renderTextureColor.width, renderTextureColor.height), 0, 0);
		blacktexture.Apply();

		RenderTexture.active = renderTextureWhite;
		whitetexture = new Texture2D(
			renderTextureColor.width, renderTextureColor.height,
			TextureFormat.ARGB32, false);

		whitetexture.ReadPixels(new Rect(0, 0, renderTextureColor.width, renderTextureColor.height), 0, 0);
		whitetexture.Apply();

		RenderTexture.active = renderTextureColor;
		colortexture = new Texture2D(
			renderTextureColor.width, renderTextureColor.height,
			TextureFormat.ARGB32, false);

		colortexture.ReadPixels(new Rect(0, 0, renderTextureColor.width, renderTextureColor.height), 0, 0);
		colortexture.Apply();

		finaltexture = new Texture2D(renderTextureColor.width, renderTextureColor.height, TextureFormat.ARGB32, false);
		for (int x = 0; x < colortexture.width; x++)
			for (int y = 0; y < colortexture.height; y++)
			{

				Color c = colortexture.GetPixel(x, y);

				Color w = whitetexture.GetPixel(x, y);
				Color b = blacktexture.GetPixel(x, y);

				finaltexture.SetPixel(x, y, new Color(c.r, c.g, c.b,
					1 - (w.r - b.r)));
			}

		finaltexture.Apply();

		RenderTexture.active = null;

		save(finalFilePath);

		DestroyImmediate(CameraBlack);
		DestroyImmediate(CameraColor);
		DestroyImmediate(CameraWhite);

		DestroyImmediate(blacktexture);
		DestroyImmediate(whitetexture);
		DestroyImmediate(colortexture);
		DestroyImmediate(finaltexture);

		DestroyImmediate(renderTextureBlack);
		DestroyImmediate(renderTextureWhite);
		DestroyImmediate(renderTextureColor);

		blacktexture = whitetexture = colortexture = finaltexture = null;
		renderTextureWhite = renderTextureBlack = renderTextureColor = null;

		CameraBlack = CameraColor = CameraWhite = null;
	}

	static void save(string path)
	{
		byte[] octets = finaltexture.EncodeToPNG();
		
		Debug.Log("saved to " + path);
		System.IO.File.WriteAllBytes(path, octets);
		index++;
	}
}
