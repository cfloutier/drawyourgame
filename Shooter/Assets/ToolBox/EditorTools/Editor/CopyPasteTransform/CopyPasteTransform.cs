using UnityEditor;
using UnityEngine;

using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Reflection;
using System.Xml;

public class CopyPasteTransform : EditorWindow
{
	enum PasteDirOption
	{
		All,
		OnlyX,
		OnlyY,
		OnlyZ
	} ;

	static bool recursive = false;
	static bool local = false;

	static bool pastePos = true;
	static PasteDirOption posDir = PasteDirOption.All;
	static bool pasteScale = true;
	static bool pasteRot = true;

	// Add menu named "My Window" to the Window menu
	[MenuItem("Tools/Copy Transforms/Show Options", false, 0)]
	static void Init()
	{
		// Get existing open window or if none, make a new one:
		CopyPasteTransform window = (CopyPasteTransform)EditorWindow.GetWindow(typeof(CopyPasteTransform));
		window.Show();
	}

	void OnEnable()
	{
		//Debug.Log("OnEnable");
		recursive = EditorPrefs.GetBool("CopyPasteTransform.recursive", false);
		local = EditorPrefs.GetBool("CopyPasteTransform.local", false);
		pastePos = EditorPrefs.GetBool("CopyPasteTransform.pastePos", true);
		posDir = (PasteDirOption)EditorPrefs.GetInt("CopyPasteTransform.posDir", (int)PasteDirOption.All);
		pasteScale = EditorPrefs.GetBool("CopyPasteTransform.pasteScale", true);
		pasteRot = EditorPrefs.GetBool("CopyPasteTransform.pasteRot", true);
	}

	void OnDisable()
	{
		//Debug.Log("OnDisable");
		EditorPrefs.SetBool("CopyPasteTransform.recursive", recursive);
		EditorPrefs.SetBool("CopyPasteTransform.local", local);
		EditorPrefs.SetBool("CopyPasteTransform.pastePos", pastePos);
		EditorPrefs.SetInt("CopyPasteTransform.posDir", (int) posDir);
		EditorPrefs.SetBool("CopyPasteTransform.pasteScale", pasteScale);
		EditorPrefs.SetBool("CopyPasteTransform.pasteRot", pasteRot);
	}

	void OnGUI()
	{
		GUILayout.Label("Copy Paste Tranform Options", EditorStyles.boldLabel);
		//myString = EditorGUILayout.TextField("Text Field", myString);

		recursive = EditorGUILayout.Toggle("Recursive", recursive);
		local = EditorGUILayout.Toggle("Local Paste ", local);

		pastePos = EditorGUILayout.Toggle("paste Position", pastePos);
		posDir = (PasteDirOption) EditorGUILayout.EnumPopup("Position Filter ", (System.Enum) posDir);

		
		pasteRot = EditorGUILayout.Toggle("paste Rotation", pasteRot);
		pasteScale = EditorGUILayout.Toggle("paste Scale", pasteScale);
	}

	[MenuItem("Tools/Copy Transforms/Set Identity #%i", false, 50)]
    public static void setIdentity()
    {
        Debug.Log("SetIdentity");
        setIdentity(Selection.activeTransform);
    }

	[MenuItem("Tools/Copy Transforms/copy transforms #%c", false, 50)]
	public static void copyTransforms()
	{
		Debug.Log("copyTransforms");
        string path = Application.dataPath + "\\..\\Transforms.xml";
		exportTransform(path);
	}

	[MenuItem("Tools/Copy Transforms/paste transforms #%v", false, 50)]
	public static void pasteTransforms()
	{
		Debug.Log("pasteTransforms");
        string path = Application.dataPath + "\\..\\Transforms.xml";
		importTransforms(path);
	}

	
	[MenuItem("Tools/Copy Transforms/Export transforms to XML", false, 100)]
	public static void exportTransformsToXML()
	{
		Debug.Log("exportTransformsToXML");

		string path = EditorUtility.SaveFilePanel("Export to XML", "", "TransformStructure", "xml");

		exportTransform(path);
	}



	[MenuItem("Tools/Copy Transforms/Import transforms from XML", false, 100)]
	public static void importTransformsFromXML()
	{
		Debug.Log("importTransformsFromXML");

		string path = EditorUtility.OpenFilePanel("Import from XML", "", "xml");

		importTransforms(path);
	}




	public static void exportTransform(string path)
	{
		if (path.Length != 0)
		{
			// Build the resource file from the m_active selection.
			Transform tr = Selection.activeTransform;

			XmlDocument xmlDoc = new XmlDocument();

			ExportTr(xmlDoc, null, tr);

            

			// Save the XML content into a XmlDocument
            xmlDoc.Save(path);
		}
	}

	static void ExportTr(XmlDocument xmlDoc, XmlNode parentEl, Transform tr)
	{
		XmlElement newEL = xmlDoc.CreateElement("Transform");

		newEL.SetAttribute("name", tr.gameObject.name);
		newEL.SetAttribute("localScale", formatVector(tr.localScale));
		newEL.SetAttribute("lossyScale", formatVector(tr.lossyScale));
		newEL.SetAttribute("eulerAngles", formatVector(tr.eulerAngles));
		newEL.SetAttribute("localEulerAngles", formatVector(tr.localEulerAngles));
		newEL.SetAttribute("position", formatVector(tr.position));
		newEL.SetAttribute("localPosition", formatVector(tr.localPosition));

		if (recursive)
		{
			for (int i = 0; i < tr.childCount; i++)
			{
				Transform child = tr.GetChild(i);
				ExportTr(xmlDoc, newEL, child);
			}
		}

		if (parentEl != null)
			parentEl.AppendChild(newEL);
		else
			xmlDoc.AppendChild(newEL);
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////
	public static void importTransforms(string path)
	{
		if (path.Length != 0)
		{
			// Build the resource file from the m_active selection.

			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(path);

			XmlElement root = xmlDoc.DocumentElement;

			if (root == null || root.Name != "Transform" || !root.HasAttribute("name"))
			{
				// error invalid file
				Debug.LogError("error : invalid xml structure");
				Debug.LogError(root.Name);
				return;
			}

			if (Selection.activeTransform == null)
			{
				Debug.LogError("please select a root node");
			}

			if (Selection.transforms.Length != 1)
			{
                Undo.RecordObjects(Selection.transforms, "paste Transforms ");


				//Undo.RegisterUndo(Selection.transforms, "paste Transforms ");
				foreach (Transform tr in Selection.transforms)
				{
					ImportTr(root, tr, false);
				}
			}
			else
			{
                Undo.RecordObject(Selection.activeTransform, "paste Transforms ");

				//Undo.RegisterUndo(Selection.activeTransform, "paste Transforms ");
				ImportTr(root, Selection.activeTransform, true);
			}
		}
	}

	static void ImportTr(XmlElement el, Transform tr, bool setChilds)
	{
		/*Vector3 tmpVec = tr.position;
		tmpVec.y = parseVector3(el.GetAttribute("position")).y;
		
		tr.position = tmpVec;*/

		if (pastePos)
		{
			if (local)
			{
				if (posDir == PasteDirOption.All)
					tr.localPosition = parseVector3(el.GetAttribute("localPosition"));
				else
				{
					Vector3 cur = tr.localPosition;
					if (posDir == PasteDirOption.OnlyX)
						cur.x = parseVector3(el.GetAttribute("localPosition")).x;
					else if (posDir == PasteDirOption.OnlyY)
						cur.y = parseVector3(el.GetAttribute("localPosition")).y;
					else if (posDir == PasteDirOption.OnlyY)
						cur.z = parseVector3(el.GetAttribute("localPosition")).z;

					tr.localPosition = cur;
				}		
			}
			else
			{
				if (posDir == PasteDirOption.All)
					tr.position = parseVector3(el.GetAttribute("position"));
				else
				{
					Vector3 cur = tr.position;
					if (posDir == PasteDirOption.OnlyX)
						cur.x = parseVector3(el.GetAttribute("position")).x;
					else if (posDir == PasteDirOption.OnlyY)
						cur.y = parseVector3(el.GetAttribute("position")).y;
					else if (posDir == PasteDirOption.OnlyY)
						cur.z = parseVector3(el.GetAttribute("position")).z;

					tr.position = cur;
				}		

				/*if (OnlyY)
				{
					Vector3 cur = tr.position;
					cur.y = parseVector3(el.GetAttribute("position")).y;
					tr.position = cur;
				}
				else*/
					
			}
		}
		if (pasteScale)
		{
			tr.localScale = parseVector3(el.GetAttribute("localScale"));
		}
		if (pasteRot)
		{
			if (local)
				tr.localEulerAngles = parseVector3(el.GetAttribute("localEulerAngles"));
			else
				tr.eulerAngles = parseVector3(el.GetAttribute("eulerAngles"));
		}
		

		if (setChilds && recursive)
		{
			// now find childs
			int index = 0;
			XmlNodeList list = el.ChildNodes;
			foreach (XmlElement node in list)
			{
				if (tr.childCount > index)
				{
					Transform childTr = tr.GetChild(index);
					if (childTr != null)
						ImportTr(node, childTr, true);

					index++;
				}
			}
		}
	}

	static string formatVector(Vector3 vec)
	{
		string str = vec.x + ":" + vec.y + ":" + vec.z;
		return str;
	}

	static Vector3 parseVector3(string str)
	{
		string[] vals = str.Split(':');
		if (vals.Length != 3)
		{
			return Vector3.zero;
		}

		Vector3 vec = new Vector3();
		vec.x = (float)Double.Parse(vals[0]);
		vec.y = (float)Double.Parse(vals[1]);
		vec.z = (float)Double.Parse(vals[2]);

		return vec;
	}

    static void setIdentity(Transform tr)
    {
        Undo.RecordObject(tr, "setIdentity");
		//Undo.RegisterUndo(tr, "setIdentity");

        tr.localPosition = Vector3.zero;
        tr.localEulerAngles = Vector3.zero;
        tr.localScale = Vector3.one;
    }
}
