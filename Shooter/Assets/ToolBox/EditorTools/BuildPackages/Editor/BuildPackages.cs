﻿using System.IO;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;

public static class BuildPackages
{
	[MenuItem("Tools/Build/Build package from selected Directories")]
	public static void Build()
	{
		UnityEngine.Object[] objArray = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets);

		foreach (UnityEngine.Object obj in objArray)
		{
			string path = AssetDatabase.GetAssetPath(obj);

			if (Directory.Exists(path))
			{
				ExportPackage(path);
			}
		}

		AssetDatabase.Refresh();
	}

	static void ExportPackage(string assetPath)
	{
		string filePath = assetPath + ".unitypackage";
		AssetDatabase.ExportPackage(assetPath, filePath, ExportPackageOptions.Recurse);
	}



}