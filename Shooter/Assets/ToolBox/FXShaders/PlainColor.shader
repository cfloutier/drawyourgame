Shader "FX/Plain Color" {
Properties {
	_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
	
}

Category {
	Tags {"Queue"="Geometry" "IgnoreProjector"="True" "RenderType"="Transparent"}
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha 
	SubShader {
		Material {
			Diffuse [_Color]
		}
		Pass {
			ColorMaterial AmbientAndDiffuse
			Lighting Off
			Cull Off
       
        SetTexture [_Color] {
            constantColor [_Color]
            Combine primary * constant, primary * constant
        }  
		}
	} 
}
}