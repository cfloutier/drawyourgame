Shader "Unlit/Neg Colored" {
Properties {
	_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
}

Category {
	Tags {"Queue"="Geometry" "IgnoreProjector"="True" "RenderType"="Transparent"}
	ZWrite Off
	Blend Off
	SubShader {
		
		Pass 
		{
			Lighting Off
			Cull Off
			ZWrite On
			CGPROGRAM


			/**********************STRUCTS**********************/
			struct a2f_uv0 {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f_uv0 {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			/**********************VERTS**********************/
			v2f_uv0 vert_uv0(a2f_uv0 v) {
				v2f_uv0 o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord.xy;
				o.color = v.color;
				return o;
			}

			float computeColor(float c, float t)
			{
				/* 
				two linear interpolations 
				if (texture is lesser than middle grey)
					then interpolate between black and command color
				if (texture is greater)
					tehn interpolate between color and white
				
				
				*/



				if (t <= 0.5)
				{
					return c*t*2;
				}
				else
				{
					
			/*		
				original formula, perhaps better 

			
					return c+(1-c)*(t*2-1); */

					return 2*(c+t-t*c)-1;



				}


			
			}

			float4 Incrustation (float4 c, float4 t) 
			{ 

			return float4(
			
				computeColor(c.r, 1-t.r),
				computeColor(c.g, 1-t.g),
				computeColor(c.b, 1-t.b),
				c.a);
			}

			#pragma vertex vert_uv0
			#pragma fragment frag 

			sampler2D _MainTex;
			float4 _Color;

			float4 frag( v2f_uv0 i ) : COLOR {
				float4 a = tex2D(_MainTex, i.uv);
				return Incrustation(_Color , a);
			}

			ENDCG
	 
			
		}
	} 
}
}