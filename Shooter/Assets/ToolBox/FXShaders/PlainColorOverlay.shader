Shader "FX/Plain Color Overlay" {
Properties {
	_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
	
}

Category {
	Tags {"Queue"="Transparent+10" "IgnoreProjector"="True" "RenderType"="Transparent"}
	ZWrite Off
	ZTest Always
	Blend SrcAlpha OneMinusSrcAlpha 
	SubShader {
		Material {
			Diffuse [_Color]
		}
		Pass {
			ColorMaterial AmbientAndDiffuse
			Lighting Off
			Cull Off
       
        SetTexture [_Color] {
            constantColor [_Color]
            Combine primary * constant DOUBLE, primary * constant
        }  
		}
	} 
}
}