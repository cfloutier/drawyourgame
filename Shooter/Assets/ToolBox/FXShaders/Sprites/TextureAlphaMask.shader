Shader "Sprite/Alpha Masked Texture"
{
   Properties
   {
      _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _Mask ("Culling Mask", 2D) = "white" {}
   }

   SubShader
   {
      Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
      ZWrite Off

      Blend SrcAlpha OneMinusSrcAlpha
      Cull Off
      
      
      Pass
      {
         SetTexture [_Mask] {combine texture}
         
         
         
         SetTexture [_MainTex] {
            Combine texture, texture * previous
        }
        SetTexture [_MainTex] {
            constantColor [_Color]
            Combine previous * constant DOUBLE, previous * constant
        }  
         
         
      }
   }
} 