Shader "Sprite/Colored Picto" {
Properties {
	_Color ("Main Color", Color) = (0.5,0.5,0.5,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
}

Category {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha 
	SubShader {
		
		Pass 
		{
			Lighting Off Fog { Mode Off }
			Cull Off

			CGPROGRAM

			#include "UnityCG.cginc"

			/**********************STRUCTS**********************/
			struct a2f_uv0 {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f_uv0 {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
		uniform float4 _MainTex_ST; // Needed for TRANSFORM_TEX(v.texcoord, _MainTex)
			/**********************VERTS**********************/
			v2f_uv0 vert_uv0(a2f_uv0 v) {
				v2f_uv0 o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex); 
				/* * _MainTex.xy + _MainTex.zw;*/
				o.color = v.color;
				return o;
			}

			float4 Incrustation (float4 a, float4 b, float4 c) 
			{ 
				float4 result =  float4(
					a.r*b.r+c.r*2-1,
					a.g*b.g+c.g*2-1,
					a.b*b.b+c.b*2-1,
					a.a*b.a*c.a
					);

				return result; 
			}

			#pragma vertex vert_uv0
			#pragma fragment frag 

			
			float4 _Color;

			float4 frag( v2f_uv0 i ) : COLOR {
				float4 a = tex2D(_MainTex, i.uv);
				return Incrustation(_Color ,i.color, a);
			}

			ENDCG
	 
			
		}
	} 
}
}