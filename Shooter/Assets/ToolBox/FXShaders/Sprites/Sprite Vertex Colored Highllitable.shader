Shader "Sprite/Vertex Colored Hightlit"
{
      Properties
      {
            _Color ("Main Color", Color) = (1,1,1,1)
            _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
            _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
      }
 
      SubShader
      {
            Tags {"Queue"="Transparent" "RenderType"="TransparentCutout"}
            LOD 200
            Lighting Off
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            Pass
            {    
                  CGPROGRAM
                  #pragma vertex vert
                  #pragma fragment frag
                  #include "UnityCG.cginc"
 
                  struct appdata_t
                  {
                        float4 vertex : POSITION;
                        float2 texcoord : TEXCOORD0;
                  };
 
                  struct v2f
                  {
                        float4 vertex : POSITION;
                        float2 texcoord : TEXCOORD0;
                  };
 
                  sampler2D _MainTex;
                  uniform float4 _MainTex_ST;
                  uniform fixed4 _Color;
 
                  v2f vert (appdata_t v)
                  {
                        v2f o;
                        o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                        o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
                        return o;
                  }
 
                  fixed4 frag (v2f i) : COLOR
                  {
                        fixed4 col = tex2D(_MainTex, i.texcoord);
                        col = (col*2*_Color);
                        return col;
                  }
                  ENDCG
            }
      }
 
      Fallback "Transparent/Cutout/VertexLit"
}