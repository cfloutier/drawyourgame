Shader "Sprite/Masked Cutout Texture"
{
   Properties
   {
      _Color ("Main Color", Color) = (0.5,0.5,0.5,1)
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _Mask ("Culling Mask", 2D) = "white" {}
       _Cutoff ("Alpha cutoff", Range (0,1)) = 0.1
   }
   SubShader
   {
      Tags {"Queue"="Transparent"}
      Lighting Off
		ZWrite On
		ZTest Less 
      Blend SrcAlpha OneMinusSrcAlpha
      Cull Off
      Alphatest Greater [_CutOff]
      
      Pass
      {
         SetTexture [_Mask] {combine texture}
         
         
         
         SetTexture [_MainTex] {
            Combine texture * previous, texture * previous
        }
        SetTexture [_MainTex] {
            constantColor [_Color]
            Combine previous * constant DOUBLE, previous * constant
        }  
         
         
      }
   }
} 