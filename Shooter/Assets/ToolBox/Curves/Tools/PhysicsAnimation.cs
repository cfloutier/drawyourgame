using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PhysicsAnimation : MonoBehaviour 
{
	// the curve to anim on
	public Curve curve;
	// the speed to set
	public float speed;
	// a delat postion to place the camera depending on the curve
	public Vector3 deltaPos = Vector3.up;

	// current postion (0-1)
	[Range (0,1f)]
	public float curPos = 0;

	// total curve's size
	float curveSize = 1;
	
	public float puissanceMoteur = 10;
	public float puissanceMoteurSansManette = 10;
	public float frottementRails = 10;
	public float brakes = 100;
	public float brakeMinSpeed;
	public float gravity = 10;

	public UnityEngine.UI.Text speedMetter;
	
	void Start()
	{
		if (curve == null) return;
		// gets the size
		curveSize = curve.totalLenght;	
	}

	void Update()
	{

		if (!Application.isPlaying) return;


		if (curve == null || curveSize == 0) 
			return;


		// convert speed to position on curve
		float ratioSpeed = speed / curveSize;

		// adds delta pos to current
		curPos += ratioSpeed * Time.deltaTime;


	//	speed += Input.GetAxis("Vertical") * sensibilty * Time.deltaTime;
		speed -= speed * frottementRails / 100 * Time.deltaTime;

		if (addGravity)
		{
			Vector3 gravityDirection = Vector3.down * gravity;
			gravityDirection = transform.InverseTransformDirection(gravityDirection);
			speed += gravityDirection.z * Time.deltaTime;

			
		}


		if (useManette)
		{
			speed += Input.GetAxis("Vertical") * puissanceMoteur * Time.deltaTime;
			if (Input.GetButton("Brakes"))
			{
				speed -= speed * brakes / 100 * Time.deltaTime;

				if (Mathf.Abs(speed) < brakeMinSpeed)
					speed = 0;
			}
		}
		else if (addOnScreenControls)
		{
			if (brake)
			{
				speed -= speed * brakes / 100 * Time.deltaTime;

				if (Mathf.Abs(speed) < brakeMinSpeed)
					speed = 0;
			}

			if (acc)
			{
				speed += puissanceMoteur * Time.deltaTime;
			}
		}
		else
		{
			speed += puissanceMoteurSansManette * Time.deltaTime;
		}

		speed = Mathf.Max(speed, minSpeed);
		speed = Mathf.Min(speed, maxSpeed);

		setPOS();
	}

	bool brake = false;
	bool acc = false;


	public bool useManette = true;
	public bool addOnScreenControls = false;
	public bool addGravity = true;

	void setPOS()
	{
		Quaternion rot = Quaternion.identity;
		Vector3 pos = Vector3.zero; ;
		
		// check that we're still on the range 0-1
		while (curPos > 1)
			curPos -= 1;
		while (curPos < 0)
			curPos += 1;

		// get values from the curve
		curve.getPos(curPos, ref pos, ref rot, true);

		// apply it to current transform
		transform.rotation = rot;
		transform.position = pos + rot * deltaPos;

		if (speedMetter != null)
		{
			speedMetter.text = "" + (speed * 3600 / 1000).ToString("0.0") + " Km/h";
		}
	}

	public float minSpeed = -100;
	public float maxSpeed = 100;

	public float sizeButtons = 400;

	void OnValidate()
	{
		if (!Application.isPlaying)
		{
			setPOS();
		}

	}

	void OnGUI()
	{
		if (!addOnScreenControls) return;

		brake = GUI.RepeatButton(new Rect(0, Screen.height - sizeButtons, sizeButtons, sizeButtons), "Freiner");
		

		acc = GUI.RepeatButton(new Rect(Screen.width - sizeButtons, Screen.height - sizeButtons, sizeButtons, sizeButtons), "Acc�l�rer");
		

	}
}
