using UnityEngine;
using System.Collections;

public class SpeedCurveAnim : MonoBehaviour 
{
	// the curve to anim on
	public Curve curve;
	// the speed to set
	public float speed;
	// a delat postion to place the camera depending on the curve
	public Vector3 deltaPos = Vector3.up;

	// current postion (0-1)
	float curPos = 0;
	// total curve's size
	float curveSize = 1;
	
	public bool showGUI = true;

	
	void Start()
	{
		if (curve == null) return;
		// gets the size
		curveSize = curve.totalLenght;	
	}

	void Update()
	{
		if (curve == null || curveSize == 0) 
			return;

		Quaternion rot = Quaternion.identity;
		Vector3 pos = Vector3.zero; ;

		// convert speed to position on curve
		float ratioSpeed = speed / curveSize;

		// adds delta pos to current
		curPos += ratioSpeed * Time.deltaTime;
		// check that we're still on the range 0-1
		while (curPos > 1)
			curPos -= 1;
		while (curPos < 0)
			curPos += 1;

		// get values from the curve
		curve.getPos(curPos, ref pos, ref rot, true);

		// apply it to current transform
		transform.rotation = rot;
		transform.position = pos + rot * deltaPos;
	}


	void OnGUI()
	{
		if (showGUI)
		{
			GUI.Box(new Rect(0, 0, Screen.width , 60), "");
			GUI.Label(new Rect(10, 10, 200, 100), "Speed : " + (speed * 3600 / 1000) + " Km/h");
			speed = GUI.HorizontalSlider(new Rect(10, 40, Screen.width - 20, 30), speed, -2000, 2000);
		}
	}
}
