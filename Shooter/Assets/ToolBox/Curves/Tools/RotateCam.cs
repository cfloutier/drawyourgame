using UnityEngine;
using System.Collections;

public class RotateCam : MonoBehaviour 
{
	public float Verticalzone = 0;


	public Vector2 minMaxY = new Vector2(-120, 120);
	public Vector2 minMaxX = new Vector2(-80, 80);
	public Vector2 sensibilty = new Vector2(50, 50);


	public bool mouseDownNeeded = true;

	Vector3 lastMousePos;
	Vector3 wantedDirection;

	public float smooth = 1;
	void Start()
	{
		wantedDirection = transform.localEulerAngles;
	}

	bool dragin = false;

	// Update is called once per frame
	void Update () 
	{
		if (Verticalzone <= 0 || (Screen.height - Input.mousePosition.y) > Verticalzone)
		{
			if ((!mouseDownNeeded && !dragin) || Input.GetMouseButtonDown(0))
			{
				lastMousePos = Input.mousePosition;
				dragin = true;

			}
		}
	

		if (dragin)
		{
			if (!mouseDownNeeded || Input.GetMouseButton(0))
			{
				Vector3 deltaMouse = lastMousePos - Input.mousePosition;
				lastMousePos = Input.mousePosition;
				//Debug.Log(Input.GetAxis("Vertical"));
				wantedDirection += new Vector3(deltaMouse.y * sensibilty.y * 0.01f, -deltaMouse.x * sensibilty.x * 0.01f, 0);

				wantedDirection.x = ClampAngle(wantedDirection.x, minMaxX.x, minMaxX.y);
				wantedDirection.y = ClampAngle(wantedDirection.y, minMaxY.x, minMaxY.y);

			}
			else
			{
				dragin = false;
			}

		}


	/*	Vector3 euleur = transform.localEulerAngles;
		euleur = Vector3.Lerp(euleur, wantedDirection, Time.deltaTime * smooth);*/

		transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(wantedDirection), Time.deltaTime * smooth);
		//transform.localRotation = Quaternion.Euler(wantedDirection);

	//	transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(wantedDirection), Time.deltaTime * smooth);

		
		
	}

	float ClampAngle(float angle, float min, float max)
	{
		if (angle < -180)
			angle += 360;

		if (angle > 180)
			angle -= 360;

		return Mathf.Clamp(angle, min, max);
	}
}
