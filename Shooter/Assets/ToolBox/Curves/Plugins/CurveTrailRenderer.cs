﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[ExecuteInEditMode]
public class CurveTrailRenderer : MonoBehaviour
{
	public Material mat;
	public Vector2 size;

	public Vector3 speed;

	public float duration = 10;
	public float orientation = 90;

	public bool showOnEdit = false;


	[SerializeField]
	GameObject curveObj;
	[SerializeField]
	Curve curve;
	[SerializeField]
	CurveRenderer curveRenderer;

	List<float> times = new List<float>();
	List<CurveControlPoint> points = new List<CurveControlPoint>();

	// Use this for initialization
	void Start () 
	{
		cleanUp();
		
	}

	void cleanUp()
	{
		if (curveObj != null)
		{
			if (Application.isPlaying)
				Destroy(curveObj);
			else
				DestroyImmediate(curveObj);

			curveObj = null;

			times.Clear();
			points.Clear();
		}
	}


	public void OnEnable()
	{
		if (curveObj == null)
		{
			createObject();
			applyParams();
		}
	}



	public void OnDisable()
	{
		cleanUp();
	}

	public void Awake()
	{
		cleanUp();
	}

	public void OnDestroy()
	{
		cleanUp();
	}



	void createObject()
	{
		if (!Application.isPlaying && !showOnEdit)
		{
			cleanUp();
			return;
		}
		if (curveObj != null) return;

		curveObj = new GameObject();
		curveObj.name = "Trail Renderer";
		curve = curveObj.AddComponent<Curve>();
		curveRenderer = curveObj.AddComponent<CurveRenderer>();
	}

	void applyParams()
	{
		if (curveObj == null) return;

		if (curveRenderer.data == null)
			curveRenderer.data = new CurveRendererData();

		curveRenderer.data.curMode = CurveRendererData.Mode.FlatLine;
		curveRenderer.data.material = mat;
		curveRenderer.data.maxWidth = size.y;
		curveRenderer.data.minWidth = size.x;

		curve.interpolation_mode = Curve.InterpolationMode.PointsDivision;
		curve.nbPtsPerCurve = 1;
		curve.curve_type = Curve.CurveType.CatmullRom;
	}

	void OnValidate()
	{
		applyParams();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!Application.isPlaying && !showOnEdit)
		{
			cleanUp();
			return;
		}
		if (curveObj == null) createObject();

		applyParams();
		cleanUpPoints();
		updateSpeed();
		addPoint();

		curve.controlPoints = points.ToArray();
		curve.rebuildLine();
	}

	void updateSpeed()
	{
		float deltaTime = Time.deltaTime;
		if (!Application.isPlaying)
			deltaTime = 0.1f;

		for (int i = 0; i < points.Count; i++)
		{
			Vector3 pos = points[i].main;
			pos += speed * deltaTime;
			points[i].main = pos;
		}
	}

	void addPoint()
	{
		CurveControlPoint pt = new CurveControlPoint();
		pt.main = transform.position;
		pt.CurveUpOrientation = orientation;
		points.Add(pt);
		times.Add(Time.time);
	}

	void cleanUpPoints()
	{
		if (Application.isPlaying)
		{
			float minTime = Time.time - duration;
			while (times.Count > 0)
			{
				if (times[0] < minTime)
				{
					times.RemoveAt(0);
					points.RemoveAt(0);
				}
				else
					break;
			}
		}
		else
		{
			while (times.Count > (int )(15*duration))
			{
				times.RemoveAt(0);
				points.RemoveAt(0);
			}
		}
	}
}
