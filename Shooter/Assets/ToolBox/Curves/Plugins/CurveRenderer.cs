using UnityEngine;
using System.Collections;

[System.Serializable]

public class CurveRendererData
{
	public enum Mode
	{
		LineRenderer,
		FlatLine,
		Steps,
	}

	public CurveRendererData()
	{
		widthCurve = new AnimationCurve();
		widthCurve = AnimationCurve.Linear(0, 0, 1, 1);
	}

	public Mode curMode;

	public Material material = null;

	public UnityEngine.Rendering.ShadowCastingMode castShadows = UnityEngine.Rendering.ShadowCastingMode.On;
	public bool receiveShadows = false;

	public float minWidth = 1;
	public float maxWidth = 1;

	public bool doubleSided;

	public AnimationCurve widthCurve = new AnimationCurve();

	public float AdaptTilingOnDistance = 0;

	public Color[] colors = new Color[] { Color.gray, Color.gray };

	CurveRendererData lastValues = null;

	public bool checkModeChanged()
	{
		return lastValues == null ||
			curMode != lastValues.curMode;
	}

	public bool checkChanged()
	{
		bool changed = false;

		changed = lastValues == null ||
			curMode != lastValues.curMode ||
			minWidth != lastValues.minWidth ||
			maxWidth != lastValues.maxWidth ||
			material != lastValues.material ||
			doubleSided != lastValues.doubleSided ||
			lastValues.colors.Length != colors.Length ||
			lastValues.widthCurve.length != widthCurve.length ||
			AdaptTilingOnDistance != lastValues.AdaptTilingOnDistance;
		
		if (!changed)
		{
			// compare colors
			for (int i = 0 ; i < colors.Length; i++)
			{
				if (lastValues.colors[i] != colors[i])
				{
					changed = true;
					break;
				}
			}
		}

		if (!changed)
		{
			// compare curves
			for (int i = 0; i < widthCurve.length; i++)
			{
				Keyframe lastKey = lastValues.widthCurve.keys[i];
				Keyframe curKey = widthCurve.keys[i];

				if (lastKey.inTangent != curKey.inTangent ||
					lastKey.outTangent != curKey.outTangent ||
					lastKey.tangentMode != curKey.tangentMode ||
					lastKey.time != curKey.time ||
					lastKey.value != curKey.value
					
					)
				{
					changed = true;
					break;
				}
			}
		}

		if (changed)
		{
			if (minWidth < 0)
				minWidth = 0;

			if (maxWidth < 0)
				maxWidth = 0;

			lastValues = new CurveRendererData();
			lastValues.curMode = curMode;
			lastValues.minWidth = minWidth;
			lastValues.maxWidth = maxWidth;
			lastValues.material = material;
			lastValues.doubleSided = doubleSided;
			lastValues.widthCurve = new AnimationCurve(widthCurve.keys);
			lastValues.colors = (Color[]) colors.Clone();

			return true;
		}
		return false;

	}

	public Color getColor(float ratio)
	{
		if (colors.Length == 0)
		{
			return Color.white;
		}
		else if (colors.Length == 1)
		{
			return colors[0];
		}

		ratio = Mathf.Clamp(ratio, 0, 1);
		if (ratio == 1)
			return colors[colors.Length - 1];

		int indexColor = (int)Mathf.Floor(ratio * (colors.Length - 1));
		float ratioColor = ratio * (colors.Length - 1) - indexColor;


		return Color.Lerp(colors[indexColor], colors[indexColor + 1], ratioColor);
	}

	public float getWidth(float ratio)
	{
		return Mathf.Clamp(minWidth + widthCurve.Evaluate(ratio) * (maxWidth - minWidth), 0, float.MaxValue);
	}
}

[ExecuteInEditMode]
[AddComponentMenu("Curves/Curve Renderer")]
public class CurveRenderer : MonoBehaviour 
{
	public CurveRendererData data;

	LineRenderer m_lineRenderer = null;
	MeshFilter m_meshFilter = null;
	MeshRenderer m_meshRenderer = null;

	public Curve m_curve = null;

	void getCurve()
	{
		if (m_curve == null)
		{
			m_curve = gameObject.GetComponent<Curve>();
			if (m_curve == null)
			{
				m_curve = gameObject.AddComponent<Curve>();
			}
		}
	}

	int versionIndex = -1;

#region Flat Line Creation
	Vector3[]  buildFlatLineVerticles()
	{
		int len = m_curve.resultPolyline.Length;
		int size = len * 2;
		if (data.doubleSided)
			size = size * 2;

		Vector3 [] array = new Vector3[size];
		Vector3 right = Vector3.zero;
				
		for (int i = 0 ; i < len ; i++)
		{
			right = m_curve.orientations[i] * Vector3.right;
			right.Normalize();

			float ratio = (float)i / len;
			float width = data.getWidth(ratio);

			array[i * 2] = m_curve.resultPolyline[i] - right * width;
			array[i * 2 + 1] = m_curve.resultPolyline[i] + right * width; 
		}

		if (data.doubleSided)
		{
			int startIndex = len*2;
			// just copy pos for second side,
			// normal and triangles will change
			for (int i = 0; i < len; i++)
			{
				array[startIndex + i * 2] = array[i * 2];
				array[startIndex + i * 2 + 1] = array[i * 2 + 1];
			}
		}

		return array;
	}

	Vector3[] buildFlatLineNormals()
	{
		int len = m_curve.resultPolyline.Length;
		int size = len * 2;
		if (data.doubleSided)
			size = size * 2;

		Vector3[] array = new Vector3[size];

		for (int i = 0; i < len; i++)
		{
			array[i * 2] = array[i * 2 + 1] = m_curve.orientations[i] * Vector3.up;
		}

		if (data.doubleSided)
		{
			int startIndex = len * 2;
			// invert normal
			for (int i = 0; i < len; i++)
			{
				array[startIndex + i * 2] = array[startIndex + i * 2 + 1] = -array[i * 2];
			}
		}

		return array;
	}

	Vector2[] buildFlatLineUVs()
	{
		int len = m_curve.resultPolyline.Length;
		int size = len * 2;
		if (data.doubleSided)
			size = size * 2;

		Vector2[] array = new Vector2[size];
		float deltax = 1.0f / len;

		float x = 0;  
		for (int i = 0; i < len; i++)
		{
			array[i * 2] = new Vector2(x, 0);
			array[i * 2 + 1] = new Vector2(x, 1);
			x += deltax;
		}

		if (data.doubleSided)
		{
			int startIndex = len * 2;
			// copy uv
			for (int i = 0; i < len; i++)
			{
				array[startIndex + i * 2] = array[i * 2];
				array[startIndex + i * 2 + 1] = array[i * 2 + 1];
			}
		}

		return array;
	}

	Color[] buildFlatLineColors()
	{
		int len = m_curve.resultPolyline.Length;
		int size = len * 2;
		if (data.doubleSided)
			size = size * 2;

		Color[] array = new Color[size];

		float deltax = 1.0f / len;

		float x = 0;  
		for (int i = 0; i < len; i++)
		{
			float ratio = (float)i / len;

			array[i * 2] = data.getColor(ratio);
			array[i * 2 + 1] = array[i * 2];

			x += deltax;
		}

		if (data.doubleSided)
		{
			int startIndex = len * 2;
			// copy color
			for (int i = 0; i < len; i++)
			{
				array[startIndex + i * 2] = array[startIndex + i * 2 + 1] = array[i * 2];
			}
		}

		return array;
	}

	int[] buildFlatLineTriangles()
	{
		int len = m_curve.resultPolyline.Length;
		int size = (len -1) * 6;
		if (data.doubleSided)
			size = size * 2;

		int[] array = new int[size];
		int curIndex = 0;
		for (int i = 0; i < len - 1; i++)
		{
			int mainPointIndex = i * 2;
			curIndex = i * 6;

			array[curIndex] = mainPointIndex;
			array[curIndex + 1] = mainPointIndex + 2;
			array[curIndex + 2] = mainPointIndex + 1;

			array[curIndex + 3] = mainPointIndex + 1;
			array[curIndex + 4] = mainPointIndex + 2;
			array[curIndex + 5] = mainPointIndex + 3;
		}

		if (data.doubleSided)
		{
			// invert triangle order
			for (int i = 0; i < len - 1; i++)
			{
				int mainPointIndex = len * 2 + i * 2;
				curIndex = i * 6 + (len - 1 )* 6;

				array[curIndex] = mainPointIndex;
				array[curIndex + 1] = mainPointIndex + 1;
				array[curIndex + 2] = mainPointIndex + 2;

				array[curIndex + 3] = mainPointIndex + 1;
				array[curIndex + 4] = mainPointIndex + 3;
				array[curIndex + 5] = mainPointIndex + 2;
			}
		}

		return array;
	}

	void updateFlatLine()
	{
		
		BuildRenderers();
		
		getCurve();
		
		if (m_curve.versionIndex != versionIndex || isDirty)
		{
			versionIndex = m_curve.versionIndex;
			isDirty = false;

			// build lines

			Mesh mesh = new Mesh();
			mesh.name = "FlatLine";

			mesh.vertices = buildFlatLineVerticles();
			mesh.normals = buildFlatLineNormals();
			mesh.uv = buildFlatLineUVs();
			mesh.triangles = buildFlatLineTriangles();
			mesh.colors = buildFlatLineColors();
			
			m_meshFilter.sharedMesh = mesh;
			
			if (data.material != null)
			{
				m_meshRenderer.sharedMaterial = data.material;
				
				if (data.AdaptTilingOnDistance > 0)
				{
					m_meshRenderer.sharedMaterial.SetTextureScale("_MainTex", new Vector2(m_curve.totalLenght / data.AdaptTilingOnDistance, 1));
				}
				return;
			}

			m_meshRenderer.shadowCastingMode = data.castShadows;
			m_meshRenderer.receiveShadows = data.receiveShadows;
		}
	}

#endregion 

	void BuildRenderers()
	{
		if (m_meshFilter == null)
		{
			m_meshFilter = gameObject.GetComponent<MeshFilter>();
			if (m_meshFilter == null)
				m_meshFilter = gameObject.AddComponent<MeshFilter>();
			isDirty = true;
		}

		if (m_meshRenderer == null)
		{
			m_meshRenderer = gameObject.GetComponent<MeshRenderer>();
			if (m_meshRenderer == null)
				m_meshRenderer = gameObject.AddComponent<MeshRenderer>();
			isDirty = true;
		}

		getCurve();
	}

#region Steps Creation
	Vector3[] buildStepsVerticles()
	{
		int len = m_curve.loop ? m_curve.resultPolyline.Length - 1 : m_curve.resultPolyline.Length;
		int size = len * 4;
		if (data.doubleSided)
			size = size * 2;

		Vector3[] array = new Vector3[size];
		
		int i;
		Vector3 forward = Vector3.zero;
		Vector3 right =  Vector3.zero;

		for (i = 0; i < len; i++)
		{
			forward = m_curve.orientations[i] * Vector3.forward;
			right = m_curve.orientations[i] * Vector3.right;
			Vector3 pt = m_curve.resultPolyline[i];

			float ratio = (float)i / m_curve.resultPolyline.Length;
			float width = data.getWidth(ratio);

			array[i * 4 + 0] = pt + right * width / 2 + forward * width / 2;
			array[i * 4 + 1] = pt - right * width / 2 + forward * width / 2;
			array[i * 4 + 2] = pt + right * width / 2 - forward * width / 2;
			array[i * 4 + 3] = pt - right * width / 2 - forward * width / 2;    
		}
		
		if (data.doubleSided)
		{
			int startIndex = len * 4;
			// just copy pos for second side,
			// normal and triangles will change
			for ( i = 0; i < len; i++)
			{
				array[startIndex + i * 4] = array[i * 4];
				array[startIndex + i * 4 + 1] = array[i * 4 + 1];
				array[startIndex + i * 4 + 2] = array[i * 4 + 2];
				array[startIndex + i * 4 + 3] = array[i * 4 + 3];
			}

		}


		return array;
	}

	Vector3[] buildStepsNormals()
	{
		int len = m_curve.loop ? m_curve.resultPolyline.Length - 1 : m_curve.resultPolyline.Length;
		int size = len * 4;
		if (data.doubleSided)
			size = size * 2;

		Vector3[] array = new Vector3[size];

		for (int i = 0; i < len; i++)
		{
			//Debug.Log("i" + i);
			array[i * 4] = array[i * 4 + 1] = array[i * 4 + 2] = array[i * 4 + 3] = m_curve.orientations[i] * Vector3.up;			
		}

		if (data.doubleSided)
		{
			int startIndex = len * 4;
			// invert normal
			for (int i = 0; i < len; i++)
			{
				array[startIndex + i * 4] = array[startIndex + i * 4 + 1] = array[startIndex + i * 4 + 2] = array[startIndex + i * 4 + 3] = -array[i * 4];
			}
		}

		return array;
	}

	Vector2[] buildStepsUVs()
	{
		int len = m_curve.loop ? m_curve.resultPolyline.Length - 1 : m_curve.resultPolyline.Length;
		int size = len * 4;
		if (data.doubleSided)
			size = size * 2;

		Vector2[] array = new Vector2[size];

		for (int i = 0; i < len; i++)
		{
			array[i * 4] = new Vector2(0, 1);
			array[i * 4 + 1] = new Vector2(1, 1);
			array[i * 4 + 2] = new Vector2(0, 0);
			array[i * 4 + 3] = new Vector2(1, 0);
		}

		if (data.doubleSided)
		{
			int startIndex = len * 4;
			for (int i = 0; i < len; i++)
			{
				array[startIndex+ i * 4] = new Vector2(0, 1);
				array[startIndex +i * 4 + 1] = new Vector2(1, 1);
				array[startIndex +i * 4 + 2] = new Vector2(0, 0);
				array[startIndex + i * 4 + 3] = new Vector2(1, 0);
			}
		}


		return array;
	}

	Color[] buildStepsColors()
	{
		int len = m_curve.loop ? m_curve.resultPolyline.Length - 1 : m_curve.resultPolyline.Length;
		int size = len * 4;
		if (data.doubleSided)
			size = size * 2;
		Color[] array = new Color[size];

		float deltax = 1.0f / m_curve.resultPolyline.Length;

		float x = 0;
		for (int i = 0; i < len; i++)
		{
			float ratio = (float)i / m_curve.resultPolyline.Length;
			array[i * 4] = data.getColor(ratio);
			array[i * 4 + 1] = array[i * 4];
			array[i * 4 + 2] = array[i * 4];
			array[i * 4 + 3] = array[i * 4];
			x += deltax;
		}

		if (data.doubleSided)
		{
			int startIndex = len * 4;
			for (int i = 0; i < len; i++)
			{
				array[startIndex + i * 4] = array[i * 4];
				array[startIndex + i * 4 + 1] = array[i * 4];
				array[startIndex + i * 4 + 2] = array[i * 4];
				array[startIndex + i * 4 + 3] = array[i * 4];
			}
		}

		return array;
	}

	int[] buildStepsTriangles()
	{
		int len = m_curve.loop ? m_curve.resultPolyline.Length - 1 : m_curve.resultPolyline.Length;
		int size = len * 6;
		if (data.doubleSided)
			size = size * 2;

		int[] array = new int[size];
		int curIndex = 0;

		for (int i = 0; i < len; i++)
		{
			int mainPointIndex = i * 4;
			curIndex = i * 6;

			array[curIndex] = mainPointIndex + 3;
			array[curIndex + 1] = mainPointIndex + 1;
			array[curIndex + 2] = mainPointIndex ;

			array[curIndex + 3] = mainPointIndex + 2;
			array[curIndex + 4] = mainPointIndex + 3;
			array[curIndex + 5] = mainPointIndex + 0;
		}

		if (data.doubleSided)
		{
			// invert triangle order
			for (int i = 0; i < len ; i++)
			{
				int mainPointIndex = len * 4 + i * 4;
				curIndex = i * 6 + (len ) * 6;

				array[curIndex] = mainPointIndex + 3;
				array[curIndex + 1] = mainPointIndex + 0;
				array[curIndex + 2] = mainPointIndex + 1;

				array[curIndex + 3] = mainPointIndex + 2;
				array[curIndex + 4] = mainPointIndex + 0;
				array[curIndex + 5] = mainPointIndex + 3;
			}
		}

		return array;
	}

	void updateSteps()
	{

		BuildRenderers();
		
		if (m_curve.versionIndex != versionIndex || isDirty)
		{
			versionIndex = m_curve.versionIndex;
			isDirty = false;

			// build lines

			Mesh mesh = new Mesh();
			mesh.name = "FlatLine";

			mesh.vertices = buildStepsVerticles();
			mesh.normals = buildStepsNormals();
			mesh.uv = buildStepsUVs();
			mesh.triangles = buildStepsTriangles();
			mesh.colors = buildStepsColors();

			m_meshFilter.mesh = mesh;

			m_meshRenderer.sharedMaterial = data.material;
		}
	}

#endregion 

	void updateLineRenderer()
	{
		if (m_lineRenderer == null)
		{
			m_lineRenderer = gameObject.GetComponent<LineRenderer>();
			if (m_lineRenderer == null)
			{
				m_lineRenderer = gameObject.AddComponent<LineRenderer>();
				m_lineRenderer.shadowCastingMode = data.castShadows;
				m_lineRenderer.receiveShadows = data.receiveShadows;
			}
		}

		getCurve();

		if (versionIndex != m_curve.versionIndex || isDirty)
		{
			versionIndex = m_curve.versionIndex;
			isDirty = false;

			m_lineRenderer.SetVertexCount(m_curve.resultPolyline.Length);

			for (int i = 0; i < m_curve.resultPolyline.Length; i++)
			{
				m_lineRenderer.SetPosition(i, m_curve.resultPolyline[i]);
			}

			m_lineRenderer.useWorldSpace = false;

			m_lineRenderer.SetWidth(data.minWidth, data.maxWidth);
			m_lineRenderer.material = data.material;


			m_lineRenderer.SetColors(data.getColor(0), data.getColor(1));
		}
	}

	void cleanUp()
	{
		if (m_lineRenderer != null)
		{
			DestroyImmediate(m_lineRenderer);
		}

		if (m_meshFilter != null)
		{
			DestroyImmediate(m_meshFilter);
			m_meshFilter = null;
		}
		if (m_meshRenderer != null)
		{
			DestroyImmediate(m_meshRenderer);
			m_meshRenderer = null;
		}
	}


	bool isDirty = true;

    public void buildLine()
    {
		if (data == null)
			return;

        if (data.checkModeChanged())
        {
            cleanUp();
        }
		
        isDirty = data.checkChanged();

        switch (data.curMode)
        {
            case CurveRendererData.Mode.LineRenderer:
                updateLineRenderer();
                break;
            case CurveRendererData.Mode.FlatLine:
                updateFlatLine();
                break;
            case CurveRendererData.Mode.Steps:
                updateSteps();
                break;

        }
    }

  
	// Update is called once per frame
	void Update () 
	{
        buildLine();
	}
}
