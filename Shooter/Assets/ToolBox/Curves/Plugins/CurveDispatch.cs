

using UnityEngine;

[AddComponentMenu("Curves/Curve Object Dispatcher")]
public class CurveDispatch : MonoBehaviour
{
	public GameObject objectToDispatch;

	[Range(0,100)]
	public float randomPosRange = 1;

	public Vector2 deltaPos = new Vector2(0, 0);

	[Range(0, 1000)]
	public int nb = 100;

	public bool randomPosOnCurve;

	public bool randomOrientation;

	public enum ScaleMode
	{
		None,
		Rescale,
		Random
	}

	public ScaleMode scaleMode = ScaleMode.Rescale;

	public Vector2 Scale = new Vector2(1, 1);
	public Curve alongCurve;

	[Range(0,1f)]
	public float startPos = 0;

	public bool Cut = false;

	[Range(0, 1f)]
	public float endPos = 0;


	public bool drawGizmos = true;

	public void cleanUp()
	{
		while (transform.childCount > 0)
		{
			GameObject.DestroyImmediate(transform.GetChild(0).gameObject);
		}
	}

	void OnValidate()
	{
		buildPositions();
	}

	void OnDrawGizmosSelected()
	{
		if (!drawGizmos) return;
		
		if (positions == null) return;
		Color c;
		if (alongCurve != null)
		{
			c = Color.green;
			c.a = 0.5f;
			Gizmos.color = c;

			Vector3 pos = Vector3.zero;
			Quaternion rot = Quaternion.identity;
			alongCurve.getPos(startPos, ref pos, ref rot);
			pos = alongCurve.transform.TransformPoint(pos);
			Matrix4x4 m = new Matrix4x4();
			m.SetTRS(pos, rot, Vector3.one * Scale.x * 1.2f);

			Gizmos.matrix = m;

			Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(1, 1, 1));

			if (Cut)
			{
				Gizmos.color = Color.red;

				c = Color.red;
				c.a = 0.5f;
				Gizmos.color = c;


				alongCurve.getPos(endPos, ref pos, ref rot);
				pos = alongCurve.transform.TransformPoint(pos);
				m.SetTRS(pos, rot, Vector3.one * Scale.x * 1.2f);
				Gizmos.matrix = m;
				Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(1, 1, 1));
			}
		}

		
		c = Color.blue;
		c.a = 0.5f;
		Gizmos.color = c;


		for (int i = 0; i < positions.Length; i++)
		{
			Matrix4x4 m = new Matrix4x4();
			m.SetTRS(positions[i], orientations[i], scales[i]);

			Gizmos.matrix = m;
			Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(1, 1, 1)); 
		}

		
		
	}

	Vector3[] positions;
	Quaternion[] orientations;
	Vector3[] scales;

	public void buildPositions()
	{
		positions = new Vector3[nb];
		orientations = new Quaternion[nb];
		scales = new Vector3[nb];

		float t = startPos;
		
		float dt = 0;
		if (Cut)
			dt = (endPos - startPos) / (nb - 1);
		else
			dt = 1f / (nb - 1);
		
		for (int i = 0; i < nb; i++)
		{
			//GameObject newObj = (GameObject)GameObject.Instantiate(objectToDispatch);

			if (alongCurve == null)
			{
				positions[i] = transform.position + Random.onUnitSphere * randomPosRange;		
			}
			else
			{
				if (randomPosOnCurve)
					t = Random.value;

				Vector3 pos = Vector3.zero;
				Quaternion rot = Quaternion.identity;
				// get the position on curve
				alongCurve.getPos(t, ref pos, ref rot);

				positions[i] = alongCurve.transform.TransformPoint(pos);
				orientations[i] = alongCurve.transform.rotation * rot;

				t += dt;

				if (t > 1)
					t -= 1;
			}

			positions[i] += Random.insideUnitSphere * randomPosRange;

			Matrix4x4 m = new Matrix4x4();
			m.SetTRS(positions[i], orientations[i], Vector3.one);
			Vector3 up =  m * Vector3.up;
			Vector3 right =  m * Vector3.right;

			positions[i] += right * deltaPos.x + up * deltaPos.y;

			switch (scaleMode)
			{
				case ScaleMode.None:
					scales[i] = Vector3.one;
					break;
				case ScaleMode.Rescale:
					scales[i] = Vector3.one * Scale.x;
					break;
				case ScaleMode.Random:
					scales[i] = Vector3.one * Random.Range(Scale.x, Scale.y);
					break;
			}


	
			if (randomOrientation)
				orientations[i] *= Random.rotation;
		}

	}

	public void build()
	{
		buildPositions();

		
		for (int i = 0; i < nb; i++)
		{
			GameObject newObj = (GameObject)GameObject.Instantiate(objectToDispatch);
			newObj.transform.position = positions[i];
			newObj.transform.rotation = orientations[i];
			if (scaleMode != ScaleMode.None)
				newObj.transform.localScale = scales[i];

			newObj.transform.parent = transform;
		}


	}
		
	
}
