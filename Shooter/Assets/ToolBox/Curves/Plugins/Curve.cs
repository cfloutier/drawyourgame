using UnityEngine;
using System.Collections;

[System.Serializable]

/// <summary>
/// The Curve Control point. This class is used as a contrainer for control points information.
/// You can edit them programmaticcaly without trouble but remember thats changes will occur only when rebuildLine is called on the curve object.
/// 
/// Depending on curve type not all values are needed
/// 
/// The curves controls points are in Curve.controlPoints's array
/// </summary>
public class CurveControlPoint
{
	/// <summary>
	/// The type of curve part (used only in cubic bezier)
	/// This type determine the way curve is set for the previous part of this point
	/// </summary>
	public enum mode
	{
		/// <summary> a simple Line  </summary>
		Line,
		/// <summary> c1 and c2 are symetrics  </summary>
		Symetric,
		/// <summary> c1 and c2 are differents  </summary>
		Broken
	}

	/// <summary> The kind of control point </summary>
	public mode pointMode = mode.Symetric;

	/// <summary> The main control point : for any time of curve, it will go through this point </summary>
	public Vector3 main = Vector3.zero;
	/// <summary> Used for Quad and cubic bezier </summary>
	public Vector3 control1 = Vector3.zero;
	/// <summary> Used for cubic bezier only </summary>
	public Vector3 control2 = Vector3.zero;

	/// <summary> The forward direction of the curve at this point. don't set this value, it is computed.  </summary>
	public Vector3 forward = Vector3.zero;

	/// <summary> The Up Direction of the curve. 0 is absolute up  </summary>
	public float CurveUpOrientation = 0; // 0 for up

	/// <summary>
	/// A simple tool function giving a difference between to direction in radius
	/// </summary>
	/// <param name="v1">first vector</param>
	/// <param name="v2">second vector</param>
	/// <param name="n">the normal</param>
	/// <returns>angle between the too vector depending on normal</returns>
	public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
	{
		return Mathf.Atan2(
			Vector3.Dot(n, Vector3.Cross(v1, v2)),
			Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
	}


}


[AddComponentMenu("Curves/Curve")]
[ExecuteInEditMode]

/// <summary>
/// BezierCurve create a list of points based on bezier curve edition
/// There is no rendering, you should use a BezierLineRenderer class to make it visible
/// 
/// There is 3 modes of edition : 
/// * Line (no curve mode)
/// * auto bezier (just setup points where the curve should cross)
/// * complet bezier (each point is editable)
///  
/// There is also 2 modes of final poly-line creation
/// * Points Division : each segement between points is divided into a constant number of lines. each segment can have differents lenght depending on curve control points
/// * Constant Distance : the curve is separated into segments that will all have the same difstance. It useful for animations
/// 
/// </summary>
public class Curve : MonoBehaviour
{
#region public fields

	/// <summary>
	/// The type of curve.
	/// </summary>
	public enum CurveType
	{
		/// <summary>Simples connected lines</summary>
		Line,
		/// <summary>Catmull rom (crSpline) : The curve is just defined by the list of crossed points. 
		/// It's less precise than Quad and Cubic but really easy to configure by code</summary>
		CatmullRom,
		/// <summary>
		/// Curves parts are defined by only one control point. Quite difficult to have symetric lines....
		/// </summary>
		QuadBezier,
		///  <summary>Defined by 2 control points. It's the kind of curve that we can met usual vector graphics editor</summary>
		CubicBezier
	}

	/// <summary>
	/// The way interpolation is made for rendering and animation
	/// </summary>
	public enum InterpolationMode
	{
		/// <summary>
		/// Each part of the curve is split into a predefined number of segment.
		/// Each segment can't have the same lenght because of differents curves properties.
		/// This kind of interpolation is the best for rendering, by lame for animation, because it can guarante a constant speed
		/// </summary>
		PointsDivision,
		/// <summary>
		/// A more complex curve interpolation.
		/// The curve is firsty split into a large number of segments
		/// Depending on precision we try to find the nearest point to have all segments the same size.
		/// It can't be guaranted that the curvbe will cross main control points.
		/// And this split is much more longuer to produce than points division.
		/// But the curve result is ready for animation or for step rendering.
		/// Do not animate such curve !!!!
		/// </summary>
		ConstantDistance
	}

	[HideInInspector]
	/// <summary>The type of curve (Catmull Rom, Bezier, Lines...)</summary>
	public CurveType curve_type = CurveType.CubicBezier;

	[HideInInspector]
	/// <summary>
	/// The control Points. changing them will not rebuild a polyline until rebuildLine() is called
	/// </summary>
	public CurveControlPoint[] controlPoints = new CurveControlPoint[0];

	[HideInInspector]
	/// <summary>
	/// Loop the whole curve
	/// </summary>
	public bool loop = false;

	[HideInInspector]
	/// <summary>Place each Control point on the ground</summary>
	public bool VerticalRaycast = false;

	[HideInInspector]
	/// <summary>Place each point of the line on the ground</summary>
	public bool VerticalRaycastLine = false;

	[HideInInspector]
	/// <summary>The height over the ground</summary>
	public float raycastHeight = 0.1f;

	/// <summary>
	/// You can use points from another curve using this reference.
	/// Just assign this field in the inspector. Edition of point in this curve will edit the source points. 
	/// </summary>
	public Curve sharePoints = null;

	[HideInInspector]
	/// <summary>Computed : the result position of each poitn of the curve (local)</summary>
	public Vector3[] resultPolyline = new Vector3[0];

	[HideInInspector]
	/// <summary>Computed : the result orientation of each poitn of the curve (local)</summary>
	public Quaternion[] orientations = new Quaternion[0];

	[HideInInspector]
	/// <summary>For Simple Division interpolation only : the number of segment per curve part</summary>
	public int nbPtsPerCurve = 10;

	[HideInInspector]
	/// <summary>Computed : Total lenght of the interpolated curve</summary>
	public float totalLenght = 0;

	[HideInInspector]
	/// <summary>For constant distance interpolation only : a precision ratio (multiplier for the number of points created before segment's lenght step)</summary>
	public int precision = 10;

	[HideInInspector]
	/// <summary>For constant distance interpolation only : the wanted distance between 2 points</summary>
	public float stepDistance = 1;

	[HideInInspector]
	/// <summary>For constant distance interpolation only : a ratio to place the first step position</summary>
	public float firstPointPos = 0;

	[HideInInspector]
	/// <summary>For constant distance interpolation only : The error factor. Try to have a zero value using the editor functions (computed)</summary>
	public float globalErrorFactor = 0;

	[HideInInspector]
	/// <summary>For constant distance interpolation only : The error factor. Try to have a zero value using the editor functions (computed)</summary>
	public float lastSegmentErrorFactor = 0;

	[HideInInspector]
	/// <summary>The interpolation mode (simple division, contant distance...)</summary>
	public InterpolationMode interpolation_mode = InterpolationMode.PointsDivision;



	[HideInInspector]
	/// <summary>The version of the curve result : incermented each time you move a point on the editor or when rebuild line is called</summary>
	public int versionIndex = 0;

#endregion

#region Points positions functions

	void addCubicPoints(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, ref ArrayList list, int nbPts)
	{
		float dt = 1.0f / nbPts;
		float t = dt;
		list.Add(p0);
		for (int i = 1; i < nbPts; i++)
		{
			list.Add(getCubicPos(p0, p1, p2, p3, t));

			t += dt;
		}
	}

	void addCatmullRomPoints(Vector3 a, Vector3 b, Vector3 c, Vector3 d, ref ArrayList list, int nbPts)
	{
		float dt = 1.0f / nbPts;
		float t = dt;
		list.Add(b);
		for (int i = 1; i < nbPts; i++)
		{

			Vector3 pos = .5f * (
			(-a + 3f * b - 3f * c + d) * (t * t * t)
			+ (2f * a - 5f * b + 4f * c - d) * (t * t)
			+ (-a + c) * t
			+ 2f * b);

			list.Add(pos);
			t += dt;
		}
	}

	void addQuadPoints(Vector3 st, Vector3 ct, Vector3 en, ref ArrayList list, int nbPts)
	{
		float dt = 1.0f / nbPts;
		float t = dt;
		list.Add(st);
		for (int i = 1; i < nbPts; i++)
		{
			list.Add(getQuadPos(st, ct, en, t));

			t += dt;
		}
	}

	void addLinePoints(Vector3 p0, Vector3 p1, ref ArrayList list, int nbPts)
	{
		float dt = 1.0f / nbPts;
		float t = dt;
		list.Add(p0);
		for (int i = 1; i < nbPts; i++)
		{
			Vector3 p = (1 - t) * p0 + t * p1;
			list.Add(p);

			t += dt;
		}
	}

	Vector3 getCubicPos(Vector3 st, Vector3 c1, Vector3 c2, Vector3 en, float t)
	{
		float d = 1f - t;
		return d * d * d * st +
				3 * d * d * t * c1 +
				3 * d * t * t * c2 +
				t * t * t * en;
	}

	Vector3 getQuadPos(Vector3 st, Vector3 ct, Vector3 en, float t)
	{
		float d = 1f - t;
		return d * d * st + 2f * d * t * ct + t * t * en;
	}

	void build_Line_PolyLine(ref ArrayList list, int nbPts)
	{
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints != null)
			pts = sharePoints.controlPoints;

		for (int i = 1; i < pts.Length; i++)
		{
			addLinePoints(pts[i - 1].main, pts[i].main, ref list, nbPts);
		}

		if (loop)
		{
			addLinePoints(pts[pts.Length - 1].main, pts[0].main, ref list, nbPts);
		}
		else
		{
			list.Add(pts[pts.Length - 1].main);
		}
	}

	void build_CatmullRom_PolyLine(ref ArrayList list, int nbPts)
	{
		//list.Add(points[0].__main);
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints != null)
			pts = sharePoints.controlPoints;

		if (pts.Length < 2) return;

		// prepare list
		Vector3[] vector3s;

		//populate calculate path;
		vector3s = new Vector3[pts.Length + 2];
		for (int i = 0; i < pts.Length; i++)
			vector3s[i + 1] = pts[i].main;

		if (loop)
		{
			vector3s[0] = pts[pts.Length - 1].main;
			vector3s[vector3s.Length - 1] = pts[0].main;
		}
		else
		{
			//populate start and end control points:
			vector3s[0] = vector3s[1] + (vector3s[1] - vector3s[2]);
			vector3s[vector3s.Length - 1] = vector3s[vector3s.Length - 2] + (vector3s[vector3s.Length - 2] - vector3s[vector3s.Length - 3]);
		}

		for (int i = 0; i < vector3s.Length - 3; i++)
		{
			addCatmullRomPoints(
				vector3s[i],
				vector3s[i + 1],
				vector3s[i + 2],
				vector3s[i + 3],

				ref list, nbPts);
		}

		if (loop)
		{
			addCatmullRomPoints(
				vector3s[vector3s.Length - 3],
				vector3s[vector3s.Length - 2],
				vector3s[1],
				vector3s[2],

				ref list, nbPts);
		}
		else
			list.Add(pts[pts.Length - 1].main);
	}

	void build_CubicBezier_PolyLine(ref ArrayList list, int nbPts)
	{
		//list.Add(points[0].__main);
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints != null)
			pts = sharePoints.controlPoints;

		for (int i = 1; i < pts.Length; i++)
		{
			switch (pts[i].pointMode)
			{
				case CurveControlPoint.mode.Line:
					addLinePoints(pts[i - 1].main, pts[i].main, ref list, nbPts);
					break;
				case CurveControlPoint.mode.Symetric:
				case CurveControlPoint.mode.Broken:
					addCubicPoints(pts[i - 1].main,
						pts[i - 1].control2,
						pts[i].control1,
						pts[i].main,
						ref list,
						nbPts
						);
					break;
			}
		}

		if (loop)
		{
			switch (pts[0].pointMode)
			{
				case CurveControlPoint.mode.Line:
					addLinePoints(pts[pts.Length - 1].main, pts[0].main, ref list, nbPts);
					break;
				case CurveControlPoint.mode.Symetric:
				case CurveControlPoint.mode.Broken:
					addCubicPoints(pts[pts.Length - 1].main,
						pts[pts.Length - 1].control2,
						pts[0].control1,
						pts[0].main,
						ref list,
						nbPts
						);
					break;
			}
		}
		else
			list.Add(pts[pts.Length - 1].main);
	}

	void build_QuadBezier_PolyLine(ref ArrayList list, int nbPts)
	{
		//list.Add(points[0].__main);
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints != null)
			pts = sharePoints.controlPoints;

		for (int i = 1; i < pts.Length; i++)
		{
			switch (pts[i].pointMode)
			{
				case CurveControlPoint.mode.Line:
					addLinePoints(pts[i - 1].main, pts[i].main, ref list, nbPts);
					break;
				case CurveControlPoint.mode.Symetric:
				case CurveControlPoint.mode.Broken:
					addQuadPoints(pts[i - 1].main,
						pts[i].control1,
						pts[i].main,
						ref list,
						nbPts
						);
					break;
			}
		}

		if (loop)
		{
			switch (pts[0].pointMode)
			{
				case CurveControlPoint.mode.Line:
					addLinePoints(pts[pts.Length - 1].main, pts[0].main, ref list, nbPts);
					break;
				case CurveControlPoint.mode.Symetric:
				case CurveControlPoint.mode.Broken:
					addQuadPoints(pts[pts.Length - 1].main,
						pts[0].control1,
						pts[0].main,
						ref list,
						nbPts
						);
					break;
			}
		}
		else
			list.Add(pts[pts.Length - 1].main);
	}


	void verticalRaycastLine(ref ArrayList list)
	{
		for (int i = 0; i < list.Count; i++)
		{
			Vector3 pt = (Vector3)list[i];
			pt.y = 0;

			Vector3 pos = transform.TransformPoint(pt);
			Vector3 startPos = pos + Vector3.up * 10000;
			Ray ray = new Ray(startPos, -Vector3.up);

			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast(ray, out hit, Mathf.Infinity))
			{
				Vector3 deltaPos = transform.InverseTransformPoint(
					hit.point + Vector3.up * raycastHeight);

				pt = deltaPos;
			}

			list[i] = pt;
		}
	}

	void buildPolyLine(ref ArrayList list, int nbPts)
	{
		switch (curve_type)
		{
			case CurveType.Line:
				build_Line_PolyLine(ref list, nbPts);
				break;
			case CurveType.QuadBezier:
				build_QuadBezier_PolyLine(ref list, nbPts);
				break;
			case CurveType.CubicBezier:
				build_CubicBezier_PolyLine(ref list, nbPts);
				break;
			case CurveType.CatmullRom:
				build_CatmullRom_PolyLine(ref list, nbPts);
				break;
		}

		if (loop)
		{
			if (sharePoints)
				list.Add(sharePoints.controlPoints[0].main);
			else
				list.Add(controlPoints[0].main);
		}

		if (VerticalRaycastLine)
		{
			verticalRaycastLine(ref list);
		}

		// Compute forward dir for controls
		if (sharePoints) return;

		Vector3[] polyline = (Vector3[])list.ToArray(typeof(Vector3));
		//Debug.Log("resultPolyline.Length = " + polyline.Length);
		// compute main points orientations
		for (int i = 1; i < controlPoints.Length - 1; i++)
		{
			int indexPts = i * nbPts;
			Vector3 dir = polyline[indexPts + 1] - polyline[indexPts - 1];
			controlPoints[i].forward = transform.InverseTransformDirection(dir.normalized);
		}

		if (loop && resultPolyline.Length > 2)
		{
			Vector3 dir = resultPolyline[1] - polyline[polyline.Length - 2];
			controlPoints[0].forward = transform.InverseTransformDirection(dir.normalized);

			int indexPts = (controlPoints.Length - 1) * nbPts;
			dir = polyline[indexPts + 1] - polyline[indexPts - 1];
			controlPoints[controlPoints.Length - 1].forward = transform.InverseTransformDirection(dir.normalized);
		}
		else
		{
			Vector3 dir = polyline[1] - polyline[0];
			controlPoints[0].forward = transform.InverseTransformDirection(dir.normalized);
			dir = polyline[polyline.Length - 1] - polyline[polyline.Length - 2];
			controlPoints[controlPoints.Length - 1].forward = transform.InverseTransformDirection(dir.normalized);
		}
	}

	Quaternion[] buildOrientation(Vector3[] polyline)
	{
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints)
			pts = sharePoints.controlPoints;

		// build quaternions
		Quaternion[] result = new Quaternion[polyline.Length];
		totalLenght = 0;
		Vector3 dir;
		int nbPerCtrl;

		if (loop)
			nbPerCtrl = polyline.Length / pts.Length;
		else
			nbPerCtrl = polyline.Length / (pts.Length - 1);

		int indexCur = -1;

		Quaternion curQuat = Quaternion.identity;
		Quaternion nextQuat = Quaternion.identity;

		for (int i = 1; i < polyline.Length - 1; i++)
		{
			dir = polyline[i + 1] - polyline[i - 1];
			totalLenght += dir.magnitude;

			int indexControl = i / nbPerCtrl;
			if (indexCur != indexControl)
			{ 
				indexCur = indexControl;

				if (pts[indexControl].forward != Vector3.zero)
					curQuat = Quaternion.LookRotation(pts[indexControl].forward) * Quaternion.Euler(0, 0, pts[indexControl].CurveUpOrientation);
				else
					curQuat = Quaternion.Euler(0, 0, pts[indexControl].CurveUpOrientation);
		
				if (indexControl < pts.Length - 1)
				{
					

					if (pts[indexControl + 1].forward != Vector3.zero)
						nextQuat = Quaternion.LookRotation(pts[indexControl + 1].forward) * Quaternion.Euler(0, 0, pts[indexControl + 1].CurveUpOrientation);
					else
						nextQuat = Quaternion.Euler(0, 0, pts[indexControl + 1].CurveUpOrientation);

				}
				else
				{	
					nextQuat = Quaternion.LookRotation(pts[0].forward) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);

					if (pts[0].forward != Vector3.zero)
						nextQuat = Quaternion.LookRotation(pts[0].forward) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);
					else
						nextQuat = Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);
				}
			}

			float ratio = (float)(i % nbPerCtrl) / nbPerCtrl;
			
			Quaternion quat = Quaternion.Lerp(curQuat, nextQuat, ratio);

			if (dir == Vector3.zero)
				result[i] = Quaternion.identity;
			else
			{
				//	Vector3 upDir = Quaternion.Euler(0, 0,  Angle)*Vector3.up;
				Vector3 upDir = quat * Vector3.up;
				//upDir = Vector3.up;
				if (upDir == Vector3.zero)
					result[i] = Quaternion.LookRotation(dir.normalized, Vector3.up);
				else
					result[i] = Quaternion.LookRotation(dir.normalized, upDir);
			}
		}

		if (loop && polyline.Length > 2)
		{
			//Debug.Log("loop orientation");
			dir = polyline[1] - polyline[polyline.Length - 2];
			if (dir == Vector3.zero)
				result[0] = Quaternion.identity;
			else
				result[0] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);

			//	Debug.Log(dir.normalized);
			result[polyline.Length - 1] = result[0];
		}
		else
		{
			dir = polyline[1] - polyline[0];
			if (dir.normalized.magnitude == 0)
				result[0] = Quaternion.identity;
			else
				result[0] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);

			dir = polyline[polyline.Length - 1] - polyline[polyline.Length - 2];
			if (dir.magnitude == 0)
				result[polyline.Length - 1] = Quaternion.identity;
			else
				result[polyline.Length - 1] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[pts.Length - 1].CurveUpOrientation);
		}


		return result;
	}

	void buildPolyline_ConstantDistance()
	{
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints)
			pts = sharePoints.controlPoints;

		// Use approximation
		// first build a huge range of points (2000 per curve)

		tempList = new ArrayList();
		nbPtsPerPart = precision * 10;
		buildPolyLine(ref tempList, precision * 10);

		Vector3 lastPoint;
		if (loop)
			lastPoint = pts[0].main;
		else
			lastPoint = pts[pts.Length - 1].main;

		//	Debug.Log("lastPoint " + lastPoint);

		ArrayList orientationList = new ArrayList();

		Vector3[] tmp = (Vector3[])tempList.ToArray(typeof(Vector3));
		Quaternion[] quat = buildOrientation(tmp);

		ArrayList list = new ArrayList();
		
		orientationList.Clear();
		list.Clear();

		float firstStep = stepDistance * firstPointPos;
		Vector3 PrevPt = tmp[0];
		int firstIndex = 0;
		// setup first point
		int i;
		float dist = 0;
		for (i = 0; i < tmp.Length; i++)
		{
			dist = (tmp[i] - PrevPt).magnitude;
			if (dist >= firstStep)
			{
				// add first
				list.Add(tmp[i]);
				PrevPt = tmp[i];
				firstIndex = i;
				orientationList.Add(quat[i]);
				break;
			}
		}
		float meanDist = 0;
			
		for (i = firstIndex; i < tmp.Length; i++)
		{
			dist = (tmp[i] - PrevPt).magnitude;
			if (dist >= stepDistance)
			{
				list.Add(tmp[i]);
				meanDist += dist;
				PrevPt = tmp[i];
				orientationList.Add(quat[i]);
			}
		}

		
		meanDist = meanDist/(list.Count - 1);

		globalErrorFactor = (meanDist - stepDistance) / stepDistance;
		
		// at last we remove the last point to set the real last point
		// the alst segment we have a error lenght set in errorFactor
		list.RemoveAt(list.Count - 1);
		list.Add(lastPoint);

		dist = ((Vector3)list[list.Count - 2] - lastPoint).magnitude;
		lastSegmentErrorFactor = (dist - stepDistance) / stepDistance;			

		// build quaternions
		resultPolyline = (Vector3[])list.ToArray(typeof(Vector3));
		orientations = (Quaternion[])orientationList.ToArray(typeof(Quaternion));

		totalLenght = 0;

		for ( i = 0; i < resultPolyline.Length - 1; i++)
		{
			totalLenght += (resultPolyline[i + 1] - resultPolyline[i]).magnitude;
		}
	}

#endregion

#region public functions
	ArrayList tempList;
	int nbPtsPerPart;

	/// <summary>
	///Rebuild the whole interpolated line.
	///
	///Call this every time controls points or curve parameter has changed
	///This could be long expetially if raycast is done and if constant distance is set.
	/// </summary>
	public void rebuildLine()
	{
		//list.Add(points[0].__main);
		CurveControlPoint[] pts = controlPoints;
		if (sharePoints != null)
			pts = sharePoints.controlPoints;

		if (pts.Length < 2)
		{
			if (pts.Length == 0)
			{
				resultPolyline = new Vector3[0];
				orientations = new Quaternion[0];
			}
			else
			{
				resultPolyline = new Vector3[1];
				resultPolyline[0] = pts[0].main;
				orientations = new Quaternion[1];
				orientations[0] = Quaternion.identity;
			}

			versionIndex++;
			return;
		}


		if (interpolation_mode == InterpolationMode.PointsDivision)
		{
			tempList = new ArrayList();
			nbPtsPerPart = nbPtsPerCurve;
			buildPolyLine(ref  tempList, nbPtsPerCurve);

			resultPolyline = (Vector3[])tempList.ToArray(typeof(Vector3));
			orientations = buildOrientation(resultPolyline);
		}
		else if (interpolation_mode == InterpolationMode.ConstantDistance)
		{
			buildPolyline_ConstantDistance();
		}

		versionIndex++;
	}


	
	/// <summary>
	/// Find the nearest position (vector3) to the curve. Warning it can take quite a long time !
	/// </summary>
	/// <param name="pos">the position to check (in global coordinates)</param>
	/// <returns>the nearest pos on curve (in global coordinates)</returns>
	public Vector3 findNearestPos(Vector3 pos)
	{
		pos = transform.InverseTransformPoint(pos);
		float minDist = 1000000000000000;
		int index = -1;
		for (int i = 0; i < resultPolyline.Length; i++)
		{
			float dist = (resultPolyline[i] - pos).sqrMagnitude;
			if (dist < minDist)
			{
				index = i;
				minDist = dist;
			}
		}

		return transform.TransformPoint(resultPolyline[index]);

	}

	/// <summary>
	/// Find the nearest position to the curve. Warning it can take quite a long time !
	/// </summary>
	/// <param name="pos">the position to check</param>
	/// <returns>the nearest time on curve</returns>
	public float findNearestTime(Vector3 pos)
	{
		pos = transform.InverseTransformPoint(pos);
		float minDist = 1000000000000000;
		int index = -1;
		for (int i = 0; i < resultPolyline.Length; i++)
		{
			float dist = (resultPolyline[i] - pos).sqrMagnitude;
			if (dist < minDist)
			{
				index = i;
				minDist = dist;
			}
		}

		return ((float)index) / (resultPolyline.Length - 1);
	}


	/// <summary>
	/// Gets a position depending on a global time.
	/// </summary>
	/// <param name="globalTime">0-1 value</param>
	/// <param name="pos">the returned position</param>
	/// <param name="rot">the returned rotation</param>
	public void getPos(float globalTime, ref Vector3 pos, ref Quaternion rot)
	{
		getPos(globalTime, ref pos, ref rot, false);
	}


	/// <summary>
	/// Gets a postion depending on a global time.
	/// </summary>
	/// <param name="globalTime">0-1 value</param>
	/// <param name="pos">the returned position</param>
	/// <param name="rot">the returned rotation</param>
	/// <param name="world">if true converts the value to world coordinates</param>
	public void getPos(float globalTime, ref Vector3 pos, ref Quaternion rot, bool world)
	{
		if (globalTime <= 0)
		{
			pos = resultPolyline[0];
			rot = orientations[0];

		}
		else if (globalTime >= 1)
		{
			pos = resultPolyline[resultPolyline.Length - 1];
			rot = orientations[orientations.Length - 1];
		}
		else
		{
			int index = (int)((resultPolyline.Length - 1) * globalTime);
			if (index < (resultPolyline.Length - 1))
			{
				float dt = 1.0f / (resultPolyline.Length - 1);
				float startTime = dt * index;
				float localTime = (globalTime - startTime) / dt;

				pos = Vector3.Lerp(resultPolyline[index], resultPolyline[index + 1], localTime);
				rot = Quaternion.Lerp(orientations[index], orientations[index + 1], localTime);
			}
			else
			{
				pos = resultPolyline[resultPolyline.Length - 1];
				rot = orientations[resultPolyline.Length - 1];
			}
		}

		if (world)
		{
			pos = transform.TransformPoint(pos);
			rot = transform.rotation * rot;
		}
	}

	/// <summary>
	/// Used for access to mid points : access to a position between 2 control points on the curve depending on a interpolation ratio (0-1)
	/// </summary>
	/// <param name="indexPoint">the index</param>
	/// <param name="t">the time [0-1]</param>
	/// <returns>a position</returns>
	public Vector3 getPos(int indexPoint, float t)
	{
		if (tempList == null) rebuildLine();
		if (tempList.Count == 0) return Vector3.zero;
		t = Mathf.Clamp(t, 0,1);
		int globalIndex = indexPoint * nbPtsPerPart + (int) (t * nbPtsPerPart);
		//Debug.Log("globalIndex " + globalIndex);
		if (globalIndex >= tempList.Count)
			return (Vector3) tempList[tempList.Count + 1];
		if (globalIndex <= 0)
			return (Vector3) tempList[0];

		return (Vector3)tempList[globalIndex];
	
	}

	/// <summary>
	/// Gets the whole polyline converted to world coordinates
	/// </summary>
	/// <returns>the whole polyline</returns>
	public Vector3[] getWorldPolyLine()
	{
		Vector3[] array = new Vector3[resultPolyline.Length];

		for (int i = 0; i < resultPolyline.Length; i++)
		{
			array[i] = transform.TransformPoint(resultPolyline[i]);
		}


		return array;
	}


	#endregion

#region Mono Behaviour functions
	void Update()
	{
		if (sharePoints != null && sharePoints.versionIndex != versionIndex)
		{
			rebuildLine();
			versionIndex = sharePoints.versionIndex;
		}
	}
#endregion
}
