using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using System.Globalization;

/// <summary>
/// a simple class to handle float values like a distance. it is composed of a float input field (that only accept numbers) and a slider
/// </summary>
class FloatField
{
	public enum SliderMode
	{
		None,
		Relative,
		Absolut
	}

	public SliderMode sliderMode = SliderMode.None;
	public float step = 0;

	public FloatField(string fieldName)
	{
		this.fieldName = fieldName;
		
		this.minMax = new Vector2(float.MinValue, float.MaxValue);
	}

	public FloatField(string fieldName,
		SliderMode sliderMode,
		Vector2 minMax)
	{
		this.fieldName = fieldName;
		this.sliderMode = sliderMode;
		this.minMax = minMax;
	}

	public FloatField(string fieldName,
		Vector2 minMax, float step)
	{
		this.fieldName = fieldName;
		this.sliderMode = SliderMode.Absolut;
		this.minMax = minMax;
		this.step = step;
	}


	public string fieldName;
	
	string strText = "";
	Vector2 minMax;
	float lastValue;

	public bool LayoutField(ref float value)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label(fieldName, GUILayout.ExpandWidth(false));



		bool changed = false;
		if (lastValue != value && strText == "")
		{
			jogval = 1;
			strText = value.ToString();
			jobCenterValue = value;
			lastValue = value;
		}

		string newValueStr = GUILayout.TextField(strText, GUILayout.Width(100));
		if (newValueStr != strText)
		{
			float newValue;
			if (float.TryParse(newValueStr, out newValue))
			{
				strText = newValueStr;
				if (newValue != value)
				{
					lastValue = value = newValue;
					jobCenterValue = lastValue;
					jogval = 1;
					changed = true;
					value = Mathf.Clamp(value, minMax.x, minMax.y);
				}
			}
		}

		switch (sliderMode)
		{
			case SliderMode.None:
				break;
			case SliderMode.Relative:
				if (Jog(ref value))
					return true;
				break;
			case SliderMode.Absolut:
				if (slider(ref value))
					return true;
				break;
		}

		GUILayout.EndHorizontal();
		return changed;
	}


	float jogval = 1;
	float jobCenterValue;

	bool Jog(ref float value)
	{
		float newVal = GUILayout.HorizontalSlider(jogval, 0, 2, GUILayout.ExpandWidth(true));
		if (newVal != jogval)
		{
			jogval = newVal;
			if (jogval < 1)
			{
				value = jobCenterValue * jogval;
				lastValue = value;
			}
			else if (jogval == 1)
			{
				return false;
			}
			else
			{
				value = jobCenterValue * jogval * jogval;
				lastValue = value;
			}

			strText = value.ToString();
			return true;
		}

		if (GUILayout.Button("center", GUILayout.Width(60)))
		{
			jobCenterValue = value;
			jogval = 1;
		}

		return false;
	}

	bool slider(ref float value)
	{
		GUIStyle st = GUI.skin.FindStyle("General.MiniButton");

		
		float last = value;
		if (step != 0)
		{
			if (GUILayout.Button(GUI.skin.FindStyle("Buttons.less").normal.background, st))
			{
				value = value - step;
			}
		}

		value = GUILayout.HorizontalSlider(value, minMax.x, minMax.y);

		if (step != 0)
		{
			if (GUILayout.Button(GUI.skin.FindStyle("Buttons.add").normal.background, st))
			{
				value = value + step;
			}
		}

		if (value != last)
		{
			strText = value.ToString();
			lastValue = value;
			return true;
		}
		return false;
	}
}

[CustomEditor(typeof(Curve))]
/// <summary>
/// Bezier curve line editor directly usable into the scene view 
/// 
/// There is too part where the curve is edited
/// - In the scene view, where points can be added, moved deleted and so on
/// - in the inspector, 
/// </summary>
public class CurveEditor : Editor
{
	#region Editor Variables

	int curPointIndex_ = 0;
	int curPointIndex
	{
		get { return curPointIndex_; }
		set { curPointIndex_ = value; }
	}

	enum EditionMode
	{
		Curve,
		Interpolation,
		EditPoints
	}

	enum PointsMode
	{
		Position,
		Rotation,
	}

	static EditionMode editionMode = EditionMode.Curve;
	static PointsMode pointsMode = PointsMode.Position;


	int layer;
	Curve curve;
	#endregion

	#region main tools

	void OnEnable()
	{
		curve = (Curve)target;
		
		layer = curve.gameObject.layer;
		curve.gameObject.layer = 2; // no raycast
		Default_Handler_Hidden = editionMode == EditionMode.EditPoints;
	}

	void OnDisable()
	{
		if (editionMode == EditionMode.EditPoints)
		{
			Selection.activeTransform = curve.transform;
		}
		else
		{
			Default_Handler_Hidden = false;
			if (curve != null)
				curve.gameObject.layer = layer;
		}
	}

	void registerUndo()
	{
		Undo.RecordObject(curve, "Curve change");
	}

	static bool Default_Handler_Hidden
	{
		get
		{
			Type type = typeof(Tools);
			FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
			return ((bool)field.GetValue(null));
		}
		set
		{
			Type type = typeof(Tools);
			FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
			field.SetValue(null, value);
		}
	}
	#endregion

	#region Gui windows
	GUISkin skin__ = null;
	GUISkin skin
	{
		get
		{
			if (skin__ == null)
			{
				string[] pathes = AssetDatabase.FindAssets("CurvesSkin");

				if (pathes.Length == 0)
					Debug.LogError("Skin not found");
				else
					skin__ = (GUISkin)(AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(pathes[0]), typeof(GUISkin)));
			}

			return skin__;
		}
	}

	void mainGUI()
	{
		GUILayout.BeginVertical();

		EditionMode newMode = (EditionMode)GUILayout.SelectionGrid((int)editionMode, new string[] { 
				"Curve", "Interpolation",
				"Edit Points" }, 3);

		if (newMode != editionMode)
		{
			editionMode = newMode;
			SceneView.RepaintAll();
		}

		switch (editionMode)
		{
			case EditionMode.Curve:
				curveGUI();
				break;
			case EditionMode.Interpolation:
				interpolationGUI();
				break;
			case EditionMode.EditPoints:
				editPointsGUI();
				break;
		}

		GUILayout.EndVertical();
	}

	FloatField raycatHeightField = new FloatField("Raycast Height : ", FloatField.SliderMode.Relative, new Vector2(-float.MaxValue, float.MaxValue));

	void curveGUI()
	{
		CurveControlPoint[] pts = curve.controlPoints;
		if (curve.sharePoints != null)
			pts = curve.sharePoints.controlPoints;

		Curve.CurveType curve_type = curve.curve_type;
		GUILayout.Label("Curve Type", styles.Title);
		curve_type = (Curve.CurveType)GUILayout.SelectionGrid((int)curve_type, new string[] { "Lines", "Catmull Rom", "Quad Bezier", "Cubic Bezier" }, 2);
		if (curve_type != curve.curve_type)
		{
			registerUndo();
			curve.curve_type = curve_type;
			setChanged();
		}

		bool loop = GUILayout.Toggle(curve.loop, "Loop");
		if (loop != curve.loop)
		{
			registerUndo();
			curve.loop = loop;
			setChanged();
		}

		if (pts.Length > 0)
		{
			GUILayout.Label("Pivot", styles.Title);

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Reset Scale-Rotation"))
			{
				resetPivotScaleRot();
			}

			if (GUILayout.Button("Center"))
			{
				centerPivot();
			}

			GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			bool rayCast = GUILayout.Toggle(curve.VerticalRaycast, "RayCast");
			if (rayCast != curve.VerticalRaycast)
			{
				curve.VerticalRaycast = rayCast;
				if (rayCast)
					rayCastAllControls();
			}

			if (rayCast)
			{
				bool rayCastLine = GUILayout.Toggle(curve.VerticalRaycastLine, "Whole Line");
				if (rayCastLine != curve.VerticalRaycastLine)
				{
					curve.VerticalRaycastLine = rayCastLine;
					setChanged();
				}

				GUILayout.EndHorizontal();

				float raycastHeight = curve.raycastHeight;
				if (raycatHeightField.LayoutField(ref raycastHeight))
				{
					registerUndo();
					curve.raycastHeight = raycastHeight;
					setChanged();
					rayCastAllControls();
				}
			}
			else
				GUILayout.EndHorizontal();
		}
	}

	void editPointsGUI()
	{
		drawControlBar();

		if (pointsMode == PointsMode.Position)
		{
			pointsPositionsGUI();
		}
		else
		{
			orientationPointsGUI();
		}
	}

	void selectPrev()
	{
		CurveControlPoint[] pts = getPoints();

		curPointIndex--;
		if (curPointIndex < 0)
			curPointIndex = pts.Length - 1;

		zoomOn(pts[curPointIndex].main);
	}

	void selectNext()
	{
		CurveControlPoint[] pts = getPoints();

		curPointIndex++;
		if (curPointIndex >= pts.Length)
			curPointIndex = 0;

		zoomOn(pts[curPointIndex].main);
	}

	void centerCurrent()
	{
		if (curPointIndex < 0 || curPointIndex >= curve.controlPoints.Length)
		{
			zoomOn(Vector3.zero);
		}
		else
		{
			zoomOn(curve.controlPoints[curPointIndex].main);
		}
	}

	

	void zoomOn(Vector3 pos)
	{
		pos = curve.transform.TransformPoint(pos);
		ArrayList views = UnityEditor.SceneView.sceneViews;
		foreach (SceneView view in views)
		{
			//	Debug.Log(view.camera.transform.rotation);
			view.LookAt(pos, view.camera.transform.rotation);
		}
	}

	void PointModeGUI()
	{
		if (!isPointSelected()) return;

		CurveControlPoint[] pts = getPoints();
		if (curPointIndex != 0 || curve.loop)
		{
//			CurveControlPoint point = pts[curPointIndex];
			if (curve.curve_type == Curve.CurveType.QuadBezier)
			{
				CurveControlPoint.mode mode = pts[curPointIndex].pointMode;
				mode = (CurveControlPoint.mode)GUILayout.SelectionGrid((int)mode,
					new GUIContent[] { 
							new GUIContent("Line", icons.line),
							new GUIContent("Curved", icons.symetric)				
						}, 2);

				if (mode != pts[curPointIndex].pointMode)
				{
					registerUndo();
					pts[curPointIndex].pointMode = mode;
					setChanged();

					if (mode == CurveControlPoint.mode.Symetric)
					{
						pts[curPointIndex].control2 = 2 * pts[curPointIndex].main - pts[curPointIndex].control1;
					}
				}
			}
			else if (curve.curve_type == Curve.CurveType.CubicBezier)
			{
				CurveControlPoint.mode mode = pts[curPointIndex].pointMode;
				mode = (CurveControlPoint.mode)GUILayout.SelectionGrid((int)mode,

					new GUIContent[] { 
							new GUIContent("Line", icons.line),
							new GUIContent("Symetric", icons.symetric),
						new GUIContent("Broken", icons.broken)
						
						}, 3);

				if (mode != pts[curPointIndex].pointMode)
				{
					registerUndo();
					pts[curPointIndex].pointMode = mode;
					setChanged();

					if (mode == CurveControlPoint.mode.Symetric)
					{
						pts[curPointIndex].control2 = 2 * pts[curPointIndex].main - pts[curPointIndex].control1;
					}
				}
			}
		}
	}

	void pointsPositionsGUI()
	{
		CurveControlPoint[] pts = getPoints();

		if (isPointSelected())
		{
			GUILayout.Label("Current Point " + curPointIndex + " / " + pts.Length);

			CurveControlPoint point = pts[curPointIndex];

			Vector3 vec = EditorGUILayout.Vector3Field("main", point.main);
			if (vec != point.main)
			{
				registerUndo();
				point.main = vec;
				setChanged();
			}

			vec = EditorGUILayout.Vector3Field("Control 1", point.control1);
			if (vec != point.main)
			{
				registerUndo();
				point.control1 = vec;
				setChanged();
			}

			vec = EditorGUILayout.Vector3Field("Control 2", point.control2);
			if (vec != point.main)
			{
				registerUndo();
				point.control2 = vec;
				setChanged();
			}


			PointModeGUI();


		}
		else
		{
			if (pts.Length == 0)
			{
				GUILayout.Label("Please insert a point by pressing the a + icon");
			}
			{
				GUILayout.Label("No selection");
			}
		}

		if (pts.Length != 0)
		{
			GUILayout.Label("All Points Operation", styles.Title);
			GUILayout.BeginHorizontal();
			if (GUILayout.Button(new GUIContent("set Flat", icons.flat)))
			{
				ResetAllZ();
			}

			if (GUILayout.Button(new GUIContent("Clear all", icons.delete)))
			{
				clearPts();
			}
			GUILayout.EndHorizontal();
		}
	}

	FloatField orientationField = new FloatField("Orientation : ", new Vector2(-180, 180), 10);

	void orientationPointsGUI()
	{
		CurveControlPoint[] pts = getPoints();

		if (isPointSelected())
		{
			GUILayout.Label("Current Point : " + (curPointIndex+1) + " / " + pts.Length);

			controlDirectionGUI();	

			float orientation = pts[curPointIndex].CurveUpOrientation;
			if (orientationField.LayoutField(ref orientation))
			{
				registerUndo();
				pts[curPointIndex].CurveUpOrientation = orientation;
				setChanged();
			}
		}
		else
		{
			if (pts.Length == 0)
			{
				GUILayout.Label("Use Control Points tab to insert points");
			}
			else
			{
				GUILayout.Label("Clic twice on circles to select a control point");
			}
		}

		GUILayout.Label("All Points Operations", styles.Title);
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("-"))
		{
			changeAllOrientation(-15);
		}

		if (GUILayout.Button("Reset"))
		{
			resetAllOrientations();
		}

		if (GUILayout.Button("+"))
		{
			changeAllOrientation(15);
		}

		GUILayout.EndHorizontal();
	}

	FloatField stepField = new FloatField("segment lenght : ", FloatField.SliderMode.Relative, new Vector2(0, float.MaxValue));

	void interpolationGUI()
	{
		//CurveControlPoint[] pts = getPoints(curve);
		GUILayout.Label("Interpolation Type", styles.Title);
		Curve.InterpolationMode mode = curve.interpolation_mode;
		mode = (Curve.InterpolationMode)GUILayout.SelectionGrid((int)mode, new string[] { "Simple Division", "Const distance" }, 2);
		if (mode != curve.interpolation_mode)
		{
			registerUndo();
			curve.interpolation_mode = mode;
			setChanged();
		}

		if (curve.interpolation_mode == Curve.InterpolationMode.PointsDivision)
		{
			GUILayout.Label("Nb Pts per curve part = " + curve.nbPtsPerCurve);
			int nbPtsPerCurve = (int)GUILayout.HorizontalSlider(curve.nbPtsPerCurve, 1, 200);
			if (nbPtsPerCurve != curve.nbPtsPerCurve)
			{
				registerUndo();
				curve.nbPtsPerCurve = nbPtsPerCurve;
				setChanged();
			}
		}
		else if (curve.interpolation_mode == Curve.InterpolationMode.ConstantDistance)
		{
			GUILayout.Label("Precision = " + curve.precision + " (curve part subdivision factor)");
			int precision = (int)GUILayout.HorizontalSlider(curve.precision, 1, 50);
			if (precision != curve.precision)
			{
				registerUndo();
				curve.precision = precision;
				setChanged();
			}

			float stepDistance = curve.stepDistance;
			if (stepField.LayoutField(ref stepDistance))
			{
				registerUndo();
				curve.stepDistance = stepDistance;
				setChanged();
			}

			GUILayout.Label("First Point Pos = " + curve.firstPointPos);
			float firstPointPos = GUILayout.HorizontalSlider(curve.firstPointPos, 0, 10);
			if (firstPointPos != curve.firstPointPos)
			{
				registerUndo();
				curve.firstPointPos = firstPointPos;
				setChanged();
			}

			GUILayout.Label("Last Segment Error = " + (curve.lastSegmentErrorFactor * 100) + " %");
			GUILayout.Label("Mean Error = " + (curve.globalErrorFactor * 100) + " %");
		}
	}

	CurveControlPoint[] getPoints()
	{
		CurveControlPoint[] pts = curve.controlPoints;
		if (curve.sharePoints != null)
			pts = curve.sharePoints.controlPoints;

		return pts;
	}

	#endregion

	#region Tools Functions

	void ResetAllZ()
	{
		registerUndo();
		CurveControlPoint[] pts = getPoints();
		for (int i = 0; i < pts.Length; i++)
		{
			pts[i].main.y = 0;
			pts[i].control1.y = 0;
			pts[i].control2.y = 0;
		}

		setChanged();
	}

	void resetPivotScaleRot()
	{
		registerUndo();
		Undo.RecordObject(curve.transform, "curve orientation & scale");
		CurveControlPoint[] pts = getPoints();
		// reset all points position according to localScale
		for (int i = 0; i < pts.Length; i++)
		{
			pts[i].main = curve.transform.TransformPoint(pts[i].main) - curve.transform.position;
			pts[i].control1 = curve.transform.TransformPoint(pts[i].control1) - curve.transform.position;
			pts[i].control2 = curve.transform.TransformPoint(pts[i].control2) - curve.transform.position;
		}

		float localScale = curve.transform.localScale.x + curve.transform.localScale.y + curve.transform.localScale.z;
		localScale /= 3;

		CurveRenderer renderer = curve.gameObject.GetComponent<CurveRenderer>();
		if (renderer != null)
		{
			renderer.data.minWidth = renderer.data.minWidth * localScale;
			renderer.data.maxWidth = renderer.data.maxWidth * localScale;
		}

		curve.transform.localScale = Vector3.one;
		curve.transform.rotation = Quaternion.identity;

		setChanged();
	}

	void resetAllOrientations()
	{
		registerUndo();
		CurveControlPoint[] pts = getPoints();
		if (pts.Length == 0) return;

		for (int i = 0; i < pts.Length; i++)
		{
			pts[i].CurveUpOrientation = 0;
		}

		setChanged();
	}

	void changeAllOrientation(float delta)
	{
		registerUndo();
		CurveControlPoint[] pts = getPoints();
		if (pts.Length == 0) return;

		for (int i = 0; i < pts.Length; i++)
		{
			pts[i].CurveUpOrientation += delta;
		}

		setChanged();
	}

	// return delta pos
	// vect in world coord
	Vector3 verticalRaycastPoint(Vector3 vect, float height)
	{
		Vector3 startPos = vect + Vector3.up * 10000;
		Ray ray = new Ray(startPos, -Vector3.up);

		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		{
			return hit.point + Vector3.up * height - vect;
		}
		return Vector3.zero;
	}

	Vector3 cameraRaycastPoint(Vector3 vect, float height)
	{
		Transform cameraTransform = Camera.current.transform;
		Vector3 fwd = (vect - cameraTransform.position - Vector3.up * height).normalized;

		Vector3 startPos = cameraTransform.position;
		Ray ray = new Ray(startPos, fwd);

		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		{
			return hit.point + Vector3.up * height - vect;
		}
		return Vector3.zero;
	}

	void rayCastAllControls()
	{
		registerUndo();
		CurveControlPoint[] pts = getPoints();
		if (pts.Length == 0) return;

		for (int i = 0; i < pts.Length; i++)
		{
			Vector3 deltaPos = verticalRaycastPoint(curve.transform.TransformPoint(pts[i].main), curve.raycastHeight);
			pts[i].main += deltaPos;
			pts[i].control1 += deltaPos;
			pts[i].control2 += deltaPos;
		}

		setChanged();
	}

	void centerPivot()
	{
		registerUndo();
		Undo.RecordObject(curve.transform, "curve position");
		CurveControlPoint[] pts = getPoints();
		if (pts.Length == 0) return;

		Bounds boundingBox = new Bounds(curve.transform.TransformPoint(pts[0].main), Vector3.zero);
		// reset all points position according to localScale
		for (int i = 0; i < pts.Length; i++)
		{
			boundingBox.Encapsulate(curve.transform.TransformPoint(pts[i].main));
			boundingBox.Encapsulate(curve.transform.TransformPoint(pts[i].control1));
			boundingBox.Encapsulate(curve.transform.TransformPoint(pts[i].control2));
		}

		Vector3 newCenter = boundingBox.center;
		Vector3 delta = curve.transform.position - newCenter;
		for (int i = 0; i < pts.Length; i++)
		{
			pts[i].main += delta;
			pts[i].control1 += delta;
			pts[i].control2 += delta;
		}

		curve.transform.position = newCenter;

		setChanged();
	}

	void clearPts()
	{
		registerUndo();
		if (curve.sharePoints != null)
		{
			curve.sharePoints.controlPoints = new CurveControlPoint[0];
		}
		else
			curve.controlPoints = new CurveControlPoint[0];


		curPointIndex = -1;
		setChanged();
	}

	#endregion

	#region Points Edition
	bool isPointSelected()
	{
		CurveControlPoint[] pts = getPoints();
		return curPointIndex >= 0 && curPointIndex < pts.Length;
	}

	void DeleteCurrent()
	{
		if (!isPointSelected())
			return;

		Delete(curPointIndex);
	}

	void Delete(int index)
	{
		registerUndo();
		CurveControlPoint[] pts = getPoints();

		ArrayList list = new ArrayList(pts);
		list.RemoveAt(index);

		if (curve.sharePoints != null)
		{
			curve.sharePoints.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));

		}
		else
			curve.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));

		pts = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));
		setChanged();
	}

	void InsertAfter()
	{
		if (!isPointSelected())
			insert(-1);
		else
			insert(curPointIndex + 1);
	}

	void InsertBefore()
	{
		if (!isPointSelected())
			insert(0);
		else
			insert(curPointIndex);
	}

	void insert(int position)
	{
		registerUndo();
		CurveControlPoint[] pts = curve.controlPoints;
		if (curve.sharePoints != null)
			pts = curve.sharePoints.controlPoints;

		ArrayList list = new ArrayList(pts);

		CurveControlPoint newPt = new CurveControlPoint();


		if (pts.Length == 0)
		{
			newPt.main = Vector3.zero;
			newPt.pointMode = CurveControlPoint.mode.Line;

			newPt.control1 = Vector3.right;
			newPt.control2 = Vector3.left;

			list.Add(newPt);
		}
		else if (pts.Length == 1)
		{
			if (position == 0)
			{
				newPt.main = pts[0].main / 2;
				newPt.control1 = -pts[0].main / 2;
				newPt.control2 = -newPt.control1;
				newPt.pointMode = CurveControlPoint.mode.Line;

				list.Insert(0, newPt);
			}
			else
			{
				newPt.pointMode = CurveControlPoint.mode.Symetric;

				newPt.main = pts[0].main * 2;
				newPt.control1 = newPt.main - pts[0].main / 2;
				newPt.control2 = newPt.main + pts[0].main / 2;

				list.Add(newPt);
			}
		}
		else
		{
			if (position == -1) // last
				position = pts.Length;

			bool getted = false;
			int prev = position - 1;
			int cur = position;

			if (!curve.loop)
			{
				if (position == 0)
				{
					newPt.main = 2 * pts[0].main - pts[1].main;
					newPt.control1 = 0.5f * (newPt.main + pts[0].main);
					newPt.control2 = 2 * newPt.main - newPt.control1;
					newPt.pointMode = pts[0].pointMode;

					list.Insert(0, newPt);
					getted = true;
				}
				else if (position == pts.Length)
				{
					newPt.main = 2 * pts[position - 1].main - pts[position - 2].main;
					newPt.control1 = 0.5f * (newPt.main + pts[position - 1].main);
					newPt.control2 = 2 * newPt.main - newPt.control1;
					newPt.pointMode = pts[position - 1].pointMode;

					list.Add(newPt);
					getted = true;
				}
			}
			else
			{
				if (position == 0 || position == pts.Length)
				{
					position = pts.Length;
					prev = pts.Length - 1;
					cur = 0;
				}
			}


			if (!getted)
			{
				Vector3 H = (pts[prev].control2 + pts[cur].control1) * 0.5f;
				Vector3 L2 = (pts[prev].main + pts[prev].control2) * 0.5f;
				Vector3 R3 = (pts[cur].control1 + pts[cur].main) * 0.5f;
				newPt.pointMode = pts[prev].pointMode;

				newPt.main = curve.getPos(prev, 0.5f);
				newPt.control1 = 0.5f * (H + L2);
				newPt.control2 = 0.5f * (H + R3);

				// between 2 points, use middle
				list.Insert(position, newPt);
			}

			curPointIndex = position;
		}

		if (curve.sharePoints != null)
		{
			curve.sharePoints.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));
		}
		else
			curve.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));

		setChanged();
	}
	#endregion

	#region skinning
	void loadSin()
	{
		GUI.skin = skin;
		styles.load();
		icons.load();
	}

	public class ButtonsIcons
	{
		bool loaded = false;

		public Texture2D next;
		public Texture2D prev;
		public Texture2D center;
		public Texture2D add;
		public Texture2D delete;
		public Texture2D rotate;
		public Texture2D move;
		public Texture2D flat;
		public Texture2D line;
		public Texture2D symetric;
		public Texture2D broken;
		public Texture2D up;
		public Texture2D down;
		public Texture2D left;
		public Texture2D right;

		public void load()
		{
			if (!loaded)
			{
				next = GUI.skin.FindStyle("Buttons.next").normal.background;
				prev = GUI.skin.FindStyle("Buttons.prev").normal.background;
				center = GUI.skin.FindStyle("Buttons.center").normal.background;
				add = GUI.skin.FindStyle("Buttons.add").normal.background;
				delete = GUI.skin.FindStyle("Buttons.delete").normal.background;
				rotate = GUI.skin.FindStyle("Buttons.rotate").normal.background;
				move = GUI.skin.FindStyle("Buttons.move").normal.background;
				flat = GUI.skin.FindStyle("Buttons.flat").normal.background;
				line = GUI.skin.FindStyle("Buttons.line").normal.background;
				symetric = GUI.skin.FindStyle("Buttons.symetric").normal.background;
				broken = GUI.skin.FindStyle("Buttons.broken").normal.background;
				up = GUI.skin.FindStyle("Buttons.up").normal.background;
				down = GUI.skin.FindStyle("Buttons.down").normal.background;
				left = GUI.skin.FindStyle("Buttons.left").normal.background;
				right = GUI.skin.FindStyle("Buttons.right").normal.background;
			}   
		}
	}


	public class MyGuiStyle
	{
		public GUIStyle Title;
		public GUIStyle RoundButton;
		public GUIStyle SmallButton;

		public void load()
		{
			Title = GUI.skin.FindStyle("General.Title");
			RoundButton = GUI.skin.FindStyle("General.RoundButton");
			SmallButton = GUI.skin.FindStyle("General.SmallButton");
		}
	}

	public ButtonsIcons icons = new ButtonsIcons();
	public MyGuiStyle styles = new MyGuiStyle();
	#endregion

	#region Scene View Handles

	void drawControlBar()
	{
		

		PointsMode newPointsMode = (PointsMode)GUILayout.SelectionGrid((int)pointsMode, new GUIContent[]
		{
			new GUIContent("Position", icons.move),
			new GUIContent("Orientation", icons.rotate)
		}, 2);

		if (newPointsMode != pointsMode)
		{
			pointsMode = newPointsMode;
			SceneView.RepaintAll();
		}

		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();

		if(pointsMode == PointsMode.Position)
			if (GUILayout.Button(icons.add, styles.RoundButton))
			{
				InsertBefore();
			}

		if (GUILayout.Button(icons.prev, styles.RoundButton))
		{
			selectPrev();
		}

		if (GUILayout.Button(icons.center, styles.RoundButton))
		{
			centerCurrent();
		}

		if (pointsMode == PointsMode.Position)
			if (GUILayout.Button(icons.delete, styles.RoundButton))
			{
				DeleteCurrent();
			}

		if (GUILayout.Button(icons.next, styles.RoundButton))
		{
			selectNext();
		}

		if (pointsMode == PointsMode.Position)
			if (GUILayout.Button(icons.add, styles.RoundButton))
			{
				InsertAfter();
			}

		GUILayout.FlexibleSpace();

		GUILayout.EndHorizontal();
	}

	void controlDirectionGUI()
	{
		if (!isPointSelected()) return;

		CurveControlPoint Point = getPoints()[curPointIndex];


		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		if (GUILayout.Button(icons.left, styles.SmallButton))
		{
			registerUndo();
			Point.CurveUpOrientation = -90;
			setChanged();
		}
		if (GUILayout.Button(icons.up, styles.SmallButton))
		{
			registerUndo();
			Point.CurveUpOrientation = 0;
			setChanged();
		}
		if (GUILayout.Button(icons.down, styles.SmallButton))
		{
			registerUndo();
			Point.CurveUpOrientation = 180;
			setChanged();
		}
		if (GUILayout.Button(icons.right, styles.SmallButton))
		{
			registerUndo();
			Point.CurveUpOrientation = 90;
			setChanged();
		}

		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}

	void controlsPointsSceneGUI()
	{
		Handles.BeginGUI();

		loadSin();

		var pts = getPoints();
		GUIStyle framest = skin.FindStyle("ToolBar.Frame");

		FramePositon positionFrame = new FramePositon(FramePositon.Anchor.TopLeft, new Rect(framest.margin.left, framest.margin.top, framest.fixedWidth, framest.fixedHeight));
		Rect rc = positionFrame.MainRc;

		string text = "Curves Points Edition : ";
		if (isPointSelected())
		{
			text += "(" + (curPointIndex + 1) + " / " + pts.Length + ")";
		}
		else
			text += "No Selection (" + pts.Length + ")";

		GUI.Box(rc, text, framest);
		rc = rc.applyPadding(framest);
		GUILayout.BeginArea(rc);

		drawControlBar();

		if (pointsMode == PointsMode.Position)
			PointModeGUI();
		else
			controlDirectionGUI();

		GUILayout.EndArea();

		Handles.EndGUI();
	}

	void drawControlPoint(Curve curve, int indexPoint, Vector3 pos)
	{
		Transform tr = curve.transform;

		CurveControlPoint[] pts = getPoints();
		Vector3 posC1 = tr.TransformPoint(pts[indexPoint].control1);
		Vector3 posC2 = tr.TransformPoint(pts[indexPoint].control2);

		//Handles.SphereCap(-1, pos, Quaternion.identity, size);
		Vector3 newMainPos = Handles.PositionHandle(pos, Quaternion.identity);

		Vector3 newPosC1 = posC1;
		Vector3 newPosC2 = posC2;
		bool changed = false;

		Handles.color = Color.magenta;
		if (curve.curve_type == Curve.CurveType.QuadBezier || curve.curve_type == Curve.CurveType.CubicBezier)
		{
			if (pts[indexPoint].pointMode != CurveControlPoint.mode.Line)
			{
				if (curve.loop || indexPoint != 0)
				{
					newPosC1 = Handles.PositionHandle(posC1, Quaternion.identity);
					Handles.Label(posC1, "   C1");
					Handles.DrawLine(posC1, pos);
				}
			}

			if (curve.curve_type == Curve.CurveType.CubicBezier)
			{
				if (curve.loop || indexPoint != pts.Length - 1)
				{
					newPosC2 = Handles.PositionHandle(posC2, Quaternion.identity);
					Handles.Label(posC2, "   C2");
					Handles.DrawLine(posC2, pos);
				}
			}
			else if (indexPoint >= 1)
			{
				Handles.DrawLine(posC1, tr.TransformPoint(pts[indexPoint - 1].main));
			}
			else if (curve.loop)
			{
				Handles.DrawLine(posC1, tr.TransformPoint(pts[pts.Length - 1].main));
			}
		}

		Handles.color = Color.white;

		if (pts[indexPoint].pointMode == CurveControlPoint.mode.Symetric)
		{
			if (posC1 != newPosC1)
			{
				newPosC2 = 2 * pos - posC1;
				changed = true;
			}
			else if (posC2 != newPosC2)
			{
				newPosC1 = 2 * pos - posC2;
				changed = true;
			}
			else if (pos != newMainPos)
			{
				Vector3 delta = newMainPos - pos;

				newPosC2 += delta;
				newPosC1 += delta;

				changed = true;
			}
		}
		else
		{
			if (pos != newMainPos)
			{
				Vector3 delta = newMainPos - pos;
				newPosC2 += delta;
				newPosC1 += delta;

				changed = true;
			}
			else if (posC2 != newPosC2 || posC1 != newPosC1)
				changed = true;
		}

		if (changed)
		{
			registerUndo();

			Vector3 deltaPos = Vector3.zero;
			if (pos != newMainPos && curve.VerticalRaycast)
			{
				deltaPos = cameraRaycastPoint(newMainPos, curve.raycastHeight);
			}

			pts[indexPoint].main = tr.InverseTransformPoint(newMainPos) + deltaPos;
			pts[indexPoint].control1 = tr.InverseTransformPoint(newPosC1) + deltaPos;
			pts[indexPoint].control2 = tr.InverseTransformPoint(newPosC2) + deltaPos;

			setChanged();
		}
	}

	void drawOrientationPoint(Curve curve, int indexPoint, Vector3 pos)
	{
		//		Transform tr = curve.transform;

		CurveControlPoint[] pts = getPoints();
		float size = HandleUtility.GetHandleSize(pos);

		// direction of the control point
		if (pts[indexPoint].forward != Vector3.zero)
		{
			Handles.color = Color.cyan;

			Vector3 forward = curve.transform.TransformDirection(pts[indexPoint].forward);

			//			Quaternion plane = Quaternion.LookRotation(forward);

			Handles.DrawWireDisc(pos, forward, size * 0.5f);

			float orientation = pts[indexPoint].CurveUpOrientation;

			//Vector3 upDir = Quaternion.LookRotation(pts[indexPoint].forward) * Quaternion.Euler(0, 0, pts[indexPoint].CurveUpOrientation) * Vector3.up;

			Vector3 upDir = Quaternion.LookRotation(pts[indexPoint].forward) * Quaternion.Euler(0, 0, orientation) * Vector3.up;
			Quaternion direction = Quaternion.LookRotation(upDir);

			upDir *= size * 0.5f;

			orientation = (Handles.ScaleValueHandle(
				(360 + orientation) / 100,
				pos + upDir,
				direction,
				size, Handles.ConeCap, 0) - 360) * 100;

			if (orientation != pts[indexPoint].CurveUpOrientation)
			{
				registerUndo();
				while (orientation > 180)
					orientation -= 360;
				while (orientation < -180)
					orientation += 360;

				pts[indexPoint].CurveUpOrientation = orientation;
				setChanged();
			}
		}
	}

	int lastHotControl = -1;
	void controlPointsHandles(Curve curve)
	{
		Default_Handler_Hidden = true;
		Transform tr = curve.transform;

		CurveControlPoint[] pts = getPoints();


		int hotControl = GUIUtility.hotControl;
		if (hotControl != lastHotControl && hotControl != 0)
		{
			lastHotControl = hotControl;
		}

		Vector3 posCurrent = Vector3.zero;
		for (int indexPoint = 0; indexPoint < pts.Length; indexPoint++)
		{
			Vector3 pos = tr.TransformPoint(pts[indexPoint].main);

			Handles.Label(pos, "   " + indexPoint);

			float size = HandleUtility.GetHandleSize(pos);

			Handles.ScaleValueHandle(0, pos, Quaternion.identity, size, (controlID, position, rotation, size2) =>
				{
					if (controlID == lastHotControl && curPointIndex != indexPoint)
					{
						curPointIndex = indexPoint;
						EditorUtility.SetDirty(curve);
					}
					Handles.SphereCap(controlID, position, rotation, size2);
				}
				, 0);


			if (curPointIndex == indexPoint)
			{
				posCurrent = pos;
			}


		}

		if (isPointSelected())
		{
			if (pointsMode == PointsMode.Position)
				drawControlPoint(curve, curPointIndex, posCurrent);
			else
				drawOrientationPoint(curve, curPointIndex, posCurrent);
		}
	}

	void setChanged()
	{
		if (curve.sharePoints != null)
			curve.sharePoints.rebuildLine();

		curve.rebuildLine();
		EditorUtility.SetDirty(target);
	}

	#endregion

#region main overloaded functions
	void OnSceneGUI()
	{
		Handles.DrawPolyLine(curve.getWorldPolyLine());
		if (editionMode == EditionMode.EditPoints)
		{
			controlPointsHandles(curve);
			controlsPointsSceneGUI();
		}
		else
		{
			Default_Handler_Hidden = false;
		}
	}

	override public void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		loadSin();

		mainGUI();
	}

#endregion
}