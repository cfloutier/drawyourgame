using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CurveDispatch))]
public class CurveDispatchEditor : Editor
{




	override public void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		CurveDispatch dispatcher = (CurveDispatch)target;
		

		GUILayout.BeginHorizontal();

		if (GUILayout.Button( "Add Object", GUILayout.Height(30)))
		{
			dispatcher.build();
		}

		if (GUILayout.Button("Clean Up", GUILayout.Height(30)))
		{
			dispatcher.cleanUp();
		}

		if (GUILayout.Button("Clean & Add", GUILayout.Height(30)))
		{
			dispatcher.cleanUp();
			dispatcher.build();
			
		}
		GUILayout.EndHorizontal();
	}
}
