﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(GlobalPathAttribute))]
public class GlobalPathDrawer : PropertyDrawer
{
	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		ArrayDrawer.addArrayTools(ref position, property);

		GlobalPathAttribute att = attribute as GlobalPathAttribute;

		Rect buttonRect = new Rect(position.x + position.width - 50, position.y, 50, position.height);
		Rect textFieldRect = new Rect(position.x, position.y, position.width - 50, position.height);

		property.stringValue = EditorGUI.TextField(textFieldRect,label, property.stringValue );
		if (GUI.Button(buttonRect, "..."))
		{
			property.stringValue = AssetPathDrawer.Browse(property.stringValue, att.extension, false);
		}
	}
}