﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
[CustomPropertyDrawer(typeof(MessageAttribute))]
public class MessageDrawer : PropertyDrawer
{
	const float space = 10;

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		MessageAttribute att = attribute as MessageAttribute;

		Rect rc = position;
		rc.y += space;
		rc.height = heighttext;
		GUIStyle style = new GUIStyle(EditorStyles.label);
		style.wordWrap = true;
		Color prevColor = GUI.color;

		GUI.color = att.myColor;
		if (att.getStringValue)
		{
			att.message = property.stringValue;
		}


		GUI.Label(rc, att.message, style);
		GUI.color = prevColor;
		if (att.showProperty)
		{
			position.height -= rc.height;
			position.y += rc.height;

			EditorGUI.PropertyField(position, property, label, true);
		}
	}
	float heighttext;
	
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) 
	{
		MessageAttribute att = (MessageAttribute)attribute;

		//override height to adjust for word wrapping.
		GUIStyle style = new GUIStyle(EditorStyles.label);
		style.wordWrap = true;

		float size = heighttext = style.CalcHeight(new GUIContent(att.message), Screen.width - 50) + space + space;

		if (att.showProperty)
			size += EditorGUI.GetPropertyHeight(property);

		return size;
	}
}