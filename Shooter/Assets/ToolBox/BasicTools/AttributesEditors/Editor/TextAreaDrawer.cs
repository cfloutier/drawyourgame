﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
[CustomPropertyDrawer(typeof(TextAreaAttribute))]
public class TextAreaDrawer : PropertyDrawer
{
	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		float width = position.width;

		ArrayDrawer.addArrayTools(ref position, property);
		position.width = width;

        Rect rcLabel = position;
        rcLabel.height = EditorGUIUtility.singleLineHeight;

        EditorGUI.LabelField(rcLabel, label);
        position.y += EditorGUIUtility.singleLineHeight;
        position.height -= EditorGUIUtility.singleLineHeight;
        property.stringValue = EditorGUI.TextArea(position, property.stringValue);
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) 
	{
		TextAreaAttribute att = (TextAreaAttribute)attribute;

		//override height to adjust for word wrapping.
		GUIStyle style = new GUIStyle(EditorStyles.textField);
		style.wordWrap = true;

		float size = style.CalcHeight(new GUIContent(property.stringValue), Screen.width - 32);
		size = Mathf.Max(EditorGUIUtility.singleLineHeight, size);

		if (att.maxHeight > EditorGUIUtility.singleLineHeight)
		{
			size = Mathf.Min(att.maxHeight, size);
		}
		return size + EditorGUIUtility.singleLineHeight;
	}
}