﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(ListChoicesAttribute))]
public class ListChoicesDrawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUI.GetPropertyHeight(property);
	}

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		ArrayDrawer.addArrayTools(ref position, property);

		// First get the attribute since it contains the range for the slider
       // ListChoicesAttribute att = attribute as ListChoicesAttribute;

		if (useIntList(property))
		{
			int[] intList = getIntList(property);
			string[] stringList = getStringList(property);

			if (property.propertyType == SerializedPropertyType.Integer)
			{
				int currentValue = property.intValue;
				int index = Array.IndexOf<int>(intList, currentValue);
				if (index == -1)
					index = 0;

				index = EditorGUI.Popup(position, label.text, index, stringList);
				property.intValue = intList[index];

			}
			else if (property.propertyType == SerializedPropertyType.String)
			{
				
			}
			else
				Debug.LogError("invalid property type (string and int allowed ");
		}
		else
		{
			string[] list = getStringList(property);

			if (property.propertyType == SerializedPropertyType.Integer)
				property.intValue = EditorGUI.Popup(position, label.text, property.intValue, list);
			else if (property.propertyType == SerializedPropertyType.String)
			{
				string currentValue = property.stringValue;

				int index = Array.IndexOf<string>(list, currentValue);
				if (index == -1)
					index = 0;

				index = EditorGUI.Popup(position, label.text, index, list);
				property.stringValue = list[index];
			}
			else
				Debug.LogError("invalid property type (string and int allowed ");
		}
	}

	bool useIntList(SerializedProperty property)
	{
		ListChoicesAttribute att = attribute as ListChoicesAttribute;
		if (att.useProperty)
		{
			SerializedProperty listProperty = property.serializedObject.FindProperty(att.IListingProvider_FieldName);
			if (listProperty == null)
			{
				Debug.LogError("invalid property name " + att.IListingProvider_FieldName);
				return false;
			}

			if (listProperty.isArray)
			{
				if (listProperty.arraySize == 0)
				{
					Debug.LogError("Empty list : " + att.IListingProvider_FieldName);
					return false;
				}

				SerializedProperty firstValue = listProperty.GetArrayElementAtIndex(0);

				if (firstValue.propertyType == SerializedPropertyType.String)
					return false;
				else if (firstValue.propertyType == SerializedPropertyType.Integer)
					return true;
				else
					Debug.LogError("Invalid list type only 'string' and 'int' are supported : " + att.IListingProvider_FieldName);
			}
			else
				Debug.LogError("Invalid source type : not an array : " + att.IListingProvider_FieldName);
		}
		else
		{
			if (att.listIntValues != null)
			{
				return true;
			}

			return false;
		}

		return false;
	}

	int[] getIntList( SerializedProperty property)
	{
		ListChoicesAttribute att = attribute as ListChoicesAttribute;
		if (att.useProperty)
		{
			SerializedProperty listProperty = property.serializedObject.FindProperty(att.IListingProvider_FieldName);
			if (listProperty == null)
			{
				Debug.LogError("invalid property name " + att.IListingProvider_FieldName);
				return new int[0];
			}

			int[] array = new int[listProperty.arraySize];
			for (int i = 0; i < array.Length; i++)
				array[i] = listProperty.GetArrayElementAtIndex(i).intValue;

			return array;
		}
		else
			return att.listIntValues;
	}

    string [] getStringList( SerializedProperty property)
    {
		ListChoicesAttribute att = attribute as ListChoicesAttribute;
		if (att.useProperty)
		{
			

			SerializedProperty listProperty = property.serializedObject.FindProperty(att.IListingProvider_FieldName);
			if (listProperty == null)
			{
				Debug.LogError("invalid property name " + att.IListingProvider_FieldName);
				return new string[0];
			}

			string[] array = new string[listProperty.arraySize];
			for (int i = 0; i < array.Length; i++)
			{
				SerializedProperty item = listProperty.GetArrayElementAtIndex(i);
				if (item.propertyType == SerializedPropertyType.String)
					array[i] = listProperty.GetArrayElementAtIndex(i).stringValue;
				else
					array[i] = listProperty.GetArrayElementAtIndex(i).intValue.ToString();
			}

			return array;
		}
		return att.listStringValues;
    }
}