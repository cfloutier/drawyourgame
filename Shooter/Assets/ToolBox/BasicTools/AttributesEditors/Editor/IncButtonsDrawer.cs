﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(IncButtonsAttribute))]
public class IncButtonsDrawer : PropertyDrawer
{
	int labelWidth = 30;
	int buttonWidth = 30;


	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		ArrayDrawer.addArrayTools(ref position, property);


		IncButtonsAttribute att = attribute as IncButtonsAttribute;

		
		Rect rcItemPos = position;
		rcItemPos.width = EditorGUIUtility.labelWidth;

		GUI.Label(rcItemPos, label);
		rcItemPos.x += rcItemPos.width;
		rcItemPos.width = buttonWidth;

		if (GUI.Button(rcItemPos, "-"))
			property.intValue = property.intValue - att.increment;

		rcItemPos.x += rcItemPos.width;
		rcItemPos.width = labelWidth;
		EditorGUI.LabelField(rcItemPos, property.intValue.ToString());

		rcItemPos.x += rcItemPos.width;
		rcItemPos.width = labelWidth;

		if (GUI.Button(rcItemPos, "+"))
			property.intValue = property.intValue + att.increment;
	}



}