﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(SliderAttribute))]
public class SliderDrawer : PropertyDrawer
{
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		ArrayDrawer.addArrayTools(ref position, property);

		float labelWidth = 100;

		float indentation = (EditorGUI.indentLevel) * 9;

		position.x += indentation;
		position.width -= indentation;

		SliderAttribute att = attribute as SliderAttribute;

		Rect rclabelRC = position;
		rclabelRC.width = labelWidth;
		GUI.Label(rclabelRC, label);

		Rect SliderRC = new Rect(position.x + labelWidth, position.y, position.width - labelWidth, position.height);
		if (att.intType)  
		{
			property.intValue = (int)EditorGUI.Slider(SliderRC, (float)property.intValue, (float)att.min_i, (float)att.max_i);
		}
		else
		{
			property.floatValue = EditorGUI.Slider(SliderRC, property.floatValue, att.min_f, att.max_f);
		}
	}



}