﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(ColorExtAttribute))]
public class ColorExtDrawer : PropertyDrawer
{
	const float sizeEditField = 70;


	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
//		ArrayDrawer.addArrayTools(ref position, property);

		ColorExtAttribute att = (ColorExtAttribute) attribute;

		Rect rc  = position;
		rc.width -= sizeEditField - 5;
		//property.colorValue = EditorGUI.ColorField(rc, property.colorValue);

		string code = ColorTools.formatColorHtml(property.colorValue);

		if (att.removeLabel)
			property.colorValue = EditorGUI.ColorField(rc, property.colorValue);
		else
			property.colorValue = EditorGUI.ColorField(rc, label, property.colorValue);

		rc.x += rc.width;
		rc.xMax = position.xMax;

		string result = GUI.TextField(rc, code);
		if (result != code)
		{
			property.colorValue = ColorTools.parseColor(result);
		}


	//	EditorGUI.PropertyField(position, property, new GUIContent(property.propertyPath), true);
	}
}