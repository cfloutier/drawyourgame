﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the ArrayDrawer that it is a drawer for properties with the ArrayAttribute.
[CustomPropertyDrawer(typeof(ArrayAttribute))]
public class ArrayDrawer : PropertyDrawer
{
	const float widthBt = 25;

	public static void addArrayTools(ref Rect position, SerializedProperty property)
	{
		string path = property.propertyPath;
		int arrayInd = path.LastIndexOf(".Array");
		bool bIsArray = path[path.Length - 1] == ']';

		if (bIsArray)
		{
			SerializedObject so = property.serializedObject;
			string arrayPath = path.Substring(0, arrayInd);
			SerializedProperty arrayProp = so.FindProperty(arrayPath);

			//Next we need to grab the index from the path string
			int indStart = path.LastIndexOf("[") + 1;
			int indEnd = path.LastIndexOf("]");
			   
			string indString = path.Substring(indStart, indEnd - indStart);

			int myIndex = int.Parse(indString);
			Rect rcButton = position;
			rcButton.height = EditorGUIUtility.singleLineHeight;
			rcButton.x = position.xMax - widthBt * 4;
			rcButton.width = widthBt;

			bool lastEnabled = GUI.enabled;

			if (myIndex == 0)
				GUI.enabled = false;

			if (GUI.Button(rcButton, "^"))
			{
				arrayProp.MoveArrayElement(myIndex, myIndex - 1);
				so.ApplyModifiedProperties();
				
			}

			rcButton.x += widthBt;
			GUI.enabled = lastEnabled;
			if (myIndex >= arrayProp.arraySize - 1)
				GUI.enabled = false;

			if (GUI.Button(rcButton, "v"))
			{
				arrayProp.MoveArrayElement(myIndex, myIndex + 1);
				so.ApplyModifiedProperties();
			}

			GUI.enabled = myIndex != arrayProp.arraySize - 1;

			rcButton.x += widthBt;
			if (GUI.Button(rcButton, "-"))
			{
				
				if (arrayProp.GetArrayElementAtIndex(myIndex).objectReferenceValue != null)
					arrayProp.DeleteArrayElementAtIndex(myIndex);
				arrayProp.DeleteArrayElementAtIndex(myIndex);
				

				//arrayProp.DeleteArrayElementAtIndex(myIndex);
				so.ApplyModifiedProperties();
			}
			GUI.enabled = lastEnabled;
			rcButton.x += widthBt;
			if (GUI.Button(rcButton, "+"))
			{
				arrayProp.InsertArrayElementAtIndex(myIndex);
				so.ApplyModifiedProperties();
			}

			if (!property.isExpanded)
				position.width -= widthBt * 4;
		}
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		addArrayTools(ref position, property);

		EditorGUI.PropertyField(position, property, label, true);
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUI.GetPropertyHeight(property);
	}
}