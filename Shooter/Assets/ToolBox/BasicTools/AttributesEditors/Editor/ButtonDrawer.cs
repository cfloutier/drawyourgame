﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(ButtonAttribute))]
public class EditorButtonDrawer : PropertyDrawer
{
	const float width = 200;
	const float space = 3;
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		ButtonAttribute att = attribute as ButtonAttribute;

		return att.big ? 40 : 25;
	}

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
		
		// First get the attribute since it contains the range for the slider
        ButtonAttribute att = attribute as ButtonAttribute;

		if (!string.IsNullOrEmpty(att.label))
			label.text = att.label;

		position.x = position.width - width;
		position.width = width;
		position.height -= space + space;
		position.y += space;
		GUIStyle style = new GUIStyle(EditorStyles.miniButton);
		style.wordWrap = true;
		if (GUI.Button(position, label, style))
		{
			foreach (MonoBehaviour script in property.serializedObject.targetObjects)
			{
				if (script == null)
				{
					Debug.Log("no script ");
				}
				else if (att.parameter == null)
				{
					script.SendMessage(att.functionName);
				}
				else
				{
					script.SendMessage(att.functionName, att.parameter);
				}
			}
		}
	}
}