﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
[CustomPropertyDrawer(typeof(DebugAttribute))]
public class DebugDrawer : PropertyDrawer
{
	

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		GUI.enabled = false;

		EditorGUI.PropertyField(position, property, label, true);
		GUI.enabled = true;
	}
	
}