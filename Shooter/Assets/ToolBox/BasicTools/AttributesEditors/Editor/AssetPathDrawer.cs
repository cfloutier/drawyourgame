﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(AssetPathAttribute))]
public class AssetPathDrawer : PropertyDrawer
{
	public static string Browse(string curFile, string extension, bool removeAssetDirectory)
	{
		extension = extension.ToLower();
		string directory = "";
		string filePath = curFile.Replace('\\', '/');
		int lastSlash = filePath.LastIndexOf('/');
		if (curFile == "" || lastSlash == -1)
		{
			directory = Application.dataPath;
		}
		else
		{
			filePath = filePath.Substring(0, lastSlash);

			if (filePath.StartsWith("Assets/"))
			{
				directory = Application.dataPath + filePath.Substring(6);
			}
			else
				directory = filePath;
		}



		//directory = "C:/dev/Visualisation3D/ProjetEntreDeux/Assets/Layers_Carte/";
		//Debug.Log("directory " + directory);
		string path = "";
		if (extension == "folder" || extension == "directory"|| extension == "dir")
			path = EditorUtility.OpenFolderPanel("Select Directory", directory, "");
		else
			path = EditorUtility.OpenFilePanel("Select file (*." + extension+")", directory, extension);

		
		if (path == "")
		{
			return curFile;
		}
		//Debug.Log("path " + path);
		
		if (removeAssetDirectory && path.StartsWith(Application.dataPath))
		{
			path = path.Substring(Application.dataPath.Length);
		}

		return path;
	}

	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
//		ArrayDrawer.addArrayTools(ref position, property);

		AssetPathAttribute att = attribute as AssetPathAttribute;

		Rect buttonRect = new Rect(position.x + position.width - 50, position.y, 50, position.height);
		Rect textFieldRect = new Rect(position.x, position.y, position.width - 50, position.height);

		property.stringValue = EditorGUI.TextField(textFieldRect,label, property.stringValue );
		if (GUI.Button(buttonRect, "..."))
		{
			property.stringValue = Browse(property.stringValue, att.extension, true);
		}
	}
}