﻿using UnityEngine;
using System.Collections;

public class AssetPathAttribute : PropertyAttribute
{
	public string extension;

	public AssetPathAttribute(string extension)
	{
		this.extension = extension.ToLower();
	}
}
