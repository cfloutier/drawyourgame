﻿using UnityEngine;
using System.Collections;

public class SliderAttribute : PropertyAttribute
{
	public int min_i;
	public int max_i;

	public float min_f;
	public float max_f;

	public bool intType = true;
	
	public SliderAttribute(int min, int max)
	{
		intType = true;
		this.min_i = min;
		this.max_i = max;
	}

	public SliderAttribute(float min, float max)
	{
		intType = false;
		this.min_f = min;
		this.max_f = max;
	}
}
