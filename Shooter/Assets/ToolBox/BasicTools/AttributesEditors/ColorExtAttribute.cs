﻿using UnityEngine;
using System.Collections;


public class ColorExtAttribute : PropertyAttribute
{
	public bool removeLabel;

	public ColorExtAttribute()
	{
		removeLabel = false;
	}

	public ColorExtAttribute(bool removeLabel)
	{
		this.removeLabel = removeLabel;
	}

}


