﻿using UnityEngine;
using System.Collections;

public class MessageAttribute : PropertyAttribute
{
	public string message;
	public bool showProperty;
	public Color myColor;
	public bool getStringValue = false;

	public MessageAttribute()
	{
		myColor = Color.cyan;
		getStringValue = true;
	}

	public MessageAttribute(string message)
	{
		this.message = message;
		this.myColor = Color.cyan;
		this.showProperty = true;
	}

	public MessageAttribute(string message, string col)
	{
		this.message = message;
		this.myColor = ColorTools.parseColor(col);
		this.showProperty = true;
	}

	public MessageAttribute(string message, bool showProperty)
	{
		this.message = message;
		this.myColor = Color.cyan;
		this.showProperty = showProperty;
	}

	public MessageAttribute(string message, string col, bool showProperty)
	{
		this.message = message;
		this.myColor = ColorTools.parseColor(col);
		this.showProperty = showProperty;
	}
}
