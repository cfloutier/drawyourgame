﻿using UnityEngine;
using System.Collections;

public class TextAreaAttribute : PropertyAttribute
{
	public float maxHeight = -1;
	public TextAreaAttribute()
	{
		this.maxHeight = -1;
	}
	public TextAreaAttribute(float maxHeight)
	{
		this.maxHeight = maxHeight;
	}
}
