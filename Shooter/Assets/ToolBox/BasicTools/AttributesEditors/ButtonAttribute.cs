﻿using UnityEngine;
using System.Collections;

public class ButtonAttribute : PropertyAttribute
{
	public string label;
	public string functionName;
	public bool big = false;
	public object parameter = null;

	public ButtonAttribute(string functionName)
	{
		this.big = false;
		this.label = "";
		this.functionName = functionName;
	}

	public ButtonAttribute(string functionName, bool big)
	{
		this.big = big;
		this.label = "";
		this.functionName = functionName;
		
	}

	public ButtonAttribute(string label, string functionName)
	{
		this.big = false;
		this.label = label;
		this.functionName = functionName;
	}

	public ButtonAttribute(string label, string functionName, bool big)
	{
		this.big = big;
		this.label = label;
		this.functionName = functionName;
	}

	public ButtonAttribute(string functionName, object parameter)
	{
		this.big = false;
		this.label = "";
		this.functionName = functionName;
		this.parameter = parameter;
	}

	public ButtonAttribute(string label, string functionName, object parameter)
	{
		this.big = false;
		this.label = label;
		this.functionName = functionName;
		this.parameter = parameter;
	}

	public ButtonAttribute(string label, string functionName, bool big, object parameter)
	{
		this.big = big;
		this.label = label;
		this.functionName = functionName;
		this.parameter = parameter;
	}
}
