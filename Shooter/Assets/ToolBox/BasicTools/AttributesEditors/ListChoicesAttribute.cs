﻿using UnityEngine;
using System.Collections;

// a combo box where the content is specified by another serializable attribute
/**
 * Ex :
 * 
 * [Serializable]
 * private string[] listing; // need to be filled in the awake of the monobehavior
 * 
 * [ListChoices("listing")]
 * public int choice;
 */
public class ListChoicesAttribute : PropertyAttribute
{
	public int[] listIntValues;
	public string[] listStringValues;
	public bool useProperty;

    public string IListingProvider_FieldName;

    // just set the 
    public ListChoicesAttribute(string IListingProvider_FieldName)
    {
		this.useProperty = true;
        this.IListingProvider_FieldName = IListingProvider_FieldName;
    }

	public ListChoicesAttribute(int[] listValues)
	{
		this.listIntValues = listValues;
		this.useProperty = false;
		listStringValues = new string[listValues.Length];
		for (int i = 0; i < listValues.Length; i++)
		{
			listStringValues[i] = listValues[i].ToString();
		}
	}

	public ListChoicesAttribute(string[] listValues)
	{
		this.listStringValues = listValues;
		this.useProperty = false;
	}
}