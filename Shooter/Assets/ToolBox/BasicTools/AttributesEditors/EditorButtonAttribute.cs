﻿using UnityEngine;
using System.Collections;
using System;

[Obsolete("Please use ButtonAttribute", true)]
public class EditorButtonAttribute : PropertyAttribute
{
	public string label;
	public string functionName;
	public object value;

	public EditorButtonAttribute(string functionName)
	{
		this.label = "";
		this.functionName = functionName;
		this.value = null;
	}

	public EditorButtonAttribute(string label, string functionName)
	{
		this.label = label;
		this.functionName = functionName;
		this.value = null;
	}

	public EditorButtonAttribute(string label, string functionName, object value) 
	{
		this.label = label;
		this.functionName = functionName;
		this.value = value;
	}
}
