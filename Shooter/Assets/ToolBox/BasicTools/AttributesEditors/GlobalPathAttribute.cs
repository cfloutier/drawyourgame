﻿using UnityEngine;
using System.Collections;

public class GlobalPathAttribute : PropertyAttribute
{
	public string extension;

	public GlobalPathAttribute(string extension)
	{
		this.extension = extension.ToLower();
	}
}
