﻿using UnityEngine;
using System.Collections;

public class IncButtonsAttribute : PropertyAttribute
{
	public int increment = 1;

	public IncButtonsAttribute()
	{

		this.increment = 1;

	}

	public IncButtonsAttribute(int increment)
	{
		
		this.increment = increment;
		
	}
}
