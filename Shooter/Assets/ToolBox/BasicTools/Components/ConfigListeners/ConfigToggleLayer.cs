using UnityEngine;
using System.Collections;

public class ConfigToggleLayer : MonoBehaviour {

    public string configParam;
	public string validValue = "1";
   
    public void onChanged(string name)
    {
		bool show = Config.GetParamString(configParam, "") == validValue;
		gameObject.SetActive(show);
    }

	public void Awake()
	{
		onChanged(configParam);
		Config.addListener(configParam, onChanged);
	}

	public void OnDestroy()
	{
		Config.removeListener(configParam, onChanged);
	}
}
