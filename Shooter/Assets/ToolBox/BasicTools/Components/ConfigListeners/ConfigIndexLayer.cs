using UnityEngine;
using System.Collections;

public class ConfigIndexLayer : MonoBehaviour {

    public string configParam;
    public int index = -1;
	public Transform[] layers = new Transform[0];
   
    public void onChanged(string name)
    {
		if (index >= 0 && index < layers.Length)
			layers[index].gameObject.SetActive(false);

		index = Config.GetParamInt(name, index);
		if (index < 0)
			index = 0;
		else if (index >= layers.Length)
			index = layers.Length - 1;

		layers[index].gameObject.SetActive(true);
    }

	public void Awake()
	{
		if (layers.Length == 0)
		{
			layers = new Transform[transform.childCount];
			for (int i = 0; i < layers.Length; i++)
			{
				layers[i] = transform.GetChild(i);
			}
		}

		for (int i = 0; i < layers.Length; i++)
		{

			layers[i].gameObject.SetActive(index == i);
		}

		Config.addListener(configParam, onChanged);
	}

	void onEnable()
	{
		onChanged(configParam);
	}

	public void OnDestroy()
	{
		Config.removeListener(configParam, onChanged);
	}
}
