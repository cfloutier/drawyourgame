using UnityEngine;
using System.Collections;

public class ConfigNameLayer : MonoBehaviour
{
	public string paramLayerName;
	public string defaultLayer;

	public string paramMainShow;


	Transform[] trArray;

	// Use this for initialization
	void Start()
	{
		trArray = new Transform[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			trArray[i] = transform.GetChild(i);
		}

		onChanged("");

		if (defaultLayer != "")
			Config.SetParamString(paramLayerName, defaultLayer);

		Config.addListener(paramLayerName, onChanged);
		Config.addListener(paramMainShow, onChanged);
	}

	// Update is called once per frame
	void onChanged(string name)
	{
		bool show = true;
		if (paramMainShow != "")
			show = Config.GetParamBool(paramMainShow, show);

		string LayerName = Config.GetParamString(paramLayerName, "");

		for (int i = 0; i < trArray.Length; i++)
		{
			Transform tr = trArray[i];

			bool active = show && tr.name == LayerName;
			if (!active && tr.gameObject.activeSelf)
				tr.gameObject.SetActive(active);

		}

		for (int i = 0; i < trArray.Length; i++)
		{
			Transform tr = trArray[i];

			bool active = show && tr.name == LayerName;
			if (active && !tr.gameObject.activeSelf)
				tr.gameObject.SetActive(active);

		}


	}
}
