using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

/// <summary>
/// Config : This class can be used for 2 main purpose
/// * To get datas read from a simple Config.ini file loaded on application startup.
/// useful parser are added to get direct values into string, bool, float and int 
/// * To create an easy way to share data between application parts without the use of direct links
/// 
/// See GetParamInt, SetParamInt for examples. all Get functions use a default value.
/// If no value is found in data, this default Vluae is returned and the value is added to the global hastable. the configuration grows during application living
/// 
/// '''Usage''' : just adds this class to an empty GameObject
/// '''main paths''' : 
/// depending on the target platform differents paths are set in config
/// * '''httproot''' : defines the main root path of the application
/// ** '''Desktop''' it is defined depending on the main Exe file : ''[Exe]/..'' (Exe's parent directory)
/// ** '''Web''' it is defined depending on the main unity3D file : ''[unity3D]/..'' (unity3d's parent directory).
/// The relative path (..) can be set using the '''dataRelativePath''' field in the unity editor
/// ** '''Mobile''' : it is not defined and MUST BE SET in the config file 
/// 
/// * '''dataroot''' : defines where ini, language and other little text files should be found
///	** for Web and Desktop it is ''[httproot]/Data''
///	** for Mobiles apps it is defined in the StreamingAsset directory (Assets/StreamingAssets). This directory is empeded in the mobile app (apk for android) and available on load
///	
/// * '''bundlesroot''' : defines where asset bundles should be found
///	** for '''Web''' and '''Desktop''' it is ''[httproot]/Bundles''
///	** for '''Mobiles''' it is not defined and MUST BE SET in the config file
///	
/// Any of those predefined path can be overloaded in the config file and you can use them using
/// * Config.HttpRoot, Config.DataRoot and Config.BundlesRoot fields 
/// * or using the WebTools.RebuildUrl function : you can use [HttpRoot]/relativePath for instance
/// 
/// '''Config File Loading''' : 
/// 
/// The Configuration file is loaded by default using the following path [dataroot]/config.ini
/// But you can overload this in the unity editor with the field '''mainConfigFile'''. 
/// It accept relative path from the dataroot, macro-formated like [httproot]/configfile.ini or absolut path
/// 
/// '''Includes''' 
/// The config file can include other using the directive #include "newFile.ini"
/// 
/// The path is relative to the current file or you can use absolut path and macros like [httproot]/newPath/newFile
/// Included file can load others but be carreful for loop !!!
/// 
/// '''Modes''': Modes are simple way to change a complete set of config params depending on your platform or on any use
/// 
/// On start up the config class determines using the current platform and target some predefined mode
/// * Editor : when runnign on editor
/// * Mobile for any mobile platform (android or ios...)
/// * Web 
/// * Desktop
/// * OSX
/// * Windows
/// * Linux
/// * Android 
/// * IOS
/// 
/// The modes are combined together. For insatcne you can be in Editor under OSX building for IOS. The tree modes are then set
/// And you can add new modes in the editor or set it at runtime
/// 
/// Parameters are grouped by modes in the ini files. 
/// 
/// When you change mode, all parameters included on this mode change, listeners are then called if any.
///
/// Modes are define in the ini file using [mode]. 
/// 
/// For instance :
/// 
/// <Code>
/// [IOS]
/// param=only for IOS
/// 
/// [Desktop]
/// param=only for desktops applciation
/// 
/// [Editor]
/// param=you're running in the editor
/// 
///</Code>
///
/// '''Listeners''' : any class can listen to config parameter changes using the addListener fucntion. 
/// It is a really simple way to centralize global application data and to avoid direct link between prefabs. 
/// One can just set a config value
/// The other can be warned using listeners. 
/// 
/// Use it when you have a huge project with many differents parts (GUI and Meshes for isntance) 
/// Both can then communicate in an easy way without direct link or use of statics members
/// 
/// '''Ini file syntax''' : it is quite the same as standards ini file 
/// 
/// ex :
/// <Code>ParamName=Value</Code>
/// 
/// The '=' is then forbiden in values and param name. but any other taht are not carriage return are accepted. 
/// 
/// Modes are set using the brackets : []
/// 
/// ex :
/// <Code>
/// 
/// [myMode]
/// ParamName=NewValue
/// 
/// </Code>
/// 
/// You can include other files using this syntax : <Code>#include "mobile.ini"</Code>
/// 
/// It is particuliary useful to handle differents platform
/// 
/// ex :
/// <Code>
/// MainParam=DefaultValue
/// 
/// [mobile]
/// MainParam=Mobile only value
/// // new set for mobiles
/// #include "mobile.ini"
/// 
/// </Code>
/// 
/// '''Web urls'''
/// 
/// For Web Application the main page url is parsed and all value set in get parameter is copied in the config 
/// ex :  <pre>http://myserver/mypath/myApp.unity3D?paramOne=valueOne&amp;paramTwo=valueTwo </pre>
/// 
/// the mode can then be overriden here
/// 
/// ex :  <pre>http://myserver/mypath/myApp.unity3D?mode=mode1 </pre>
/// 
/// Warning, this feature does not work anymore on lastest versions of unity.
/// we can't get the url complete line, unless using internal javascript or php.....
/// 
/// 
/// </summary>
public class Config : MonoBehaviour
{
#region Fields
	// local debug infos
	public bool m_debugTraces = false;
	bool m_ShowDebugPanel = true;

	DebugPanel m_DbgPanel = null;
	static Config instance = null;

	/// <summary>
	///  the full path of the main configuration File. if not set it use the previously described rules (see summary)
	/// </summary>
	public string mainConfigFile = "[dataroot]/config.ini";


	/// <summary>
	///  add some mode to the modes list defined on start up (editor, ios, ....)
	/// </summary>
	public string[] modes;

	/// <summary>
	///the relative path used for hhtp root for all platform except in editor mode
	/// </summary>
	public string httpRootRelativePath = ".";

	/// <summary>
	/// The relative path used for editor only
	/// </summary>
	public string editorRelativePath = "../WWW";


	string validModesDescription;

	static PlatformFamily.Family s_curFamily;

	static Hashtable s_applicationParams = new Hashtable();
    static Hashtable s_paramsListeners = new Hashtable();

	/// <summary>
	/// HttpRoot is a predefined Config param. It can be used to set the root of your web application
	/// This function is a quick accessor avoiding to get it from the hastable
	/// </summary>
	static public string HttpRoot { get { return s_HttpRoot; } }
    static string s_HttpRoot = "";

	/// <summary>
	/// Data Root is a predefined Config param. It can be used to set the root of your data files 
	/// This function is a quick accessor avoiding to get it from the hastable
	/// </summary>
	static public string DataRoot { get { return s_DataRoot; } }
    static string s_DataRoot = "";

	/// <summary>
	/// Data Root is a predefined Config param. It can be used to set the root of your asset bundles files
	/// This function is a quick accessor avoiding to get it from the hastable
	/// </summary>
	static public string BundlesRoot { get { return s_BundlesRoot; } }
	static string s_BundlesRoot = "";

    static InitState m_status = new InitState("Config");


#endregion

#region Main Values Get and Set functions

	/// <summary>
	/// Get the parameter using string value
	/// If the parameter does not exists, it is added to the config list with default value
	/// This is the amin fucntion of the class
	/// </summary>
	public static string GetParamString(string name, string defaultValue )
	{
		if (s_applicationParams.Contains(name))
			return (string) s_applicationParams[name];

        SetParamString(name, defaultValue);
		return defaultValue;
	}

	/// <summary>
	/// Set the parameter using string value
	/// </summary>
    public static void SetParamString(string name, string Value)
    {
        s_applicationParams[name] = Value;
        sendToListeners(name);
    }

	/// <summary>
	/// Get the parameter using boolean value
	/// If the parameter does not exists, it is added to the config list with default value
	/// </summary>
	public static bool GetParamBool(string name, bool defaultValue)
	{
        if (s_applicationParams.Contains(name))
            return (string)s_applicationParams[name] == "1";

        SetParamBool(name, defaultValue);
		return defaultValue;
	}

	/// <summary>
	/// Set the parameter using boolean value
	/// </summary>
    public static void SetParamBool(string name, bool value)
    {
        s_applicationParams[name] = value ? "1" : "0";
        sendToListeners(name);
    }

	/// <summary>
	/// Get the parameter using int value
	/// If the parameter does not exists, it is added to the config list with default value
	/// </summary>
    public static int GetParamInt(string name, int defaultValue)
    {
		if (s_applicationParams.Contains(name))
		{
			int value = 0;
			int.TryParse((string)s_applicationParams[name], out value);	
			return value;
		}

        SetParamInt(name, defaultValue);
        return defaultValue;
    }

	/// <summary>
	/// Set the parameter using int value
	/// </summary>
    public static void SetParamInt(string name, int value)
    {

        s_applicationParams[name] = value.ToString();
        sendToListeners(name);
    }

	/// <summary>
	/// Get the parameter using float value
	/// If the parameter does not exists, it is added to the config list with default value
	/// </summary>
	public static float GetParamFloat(string name, float defaultValue)
	{
		if (s_applicationParams.Contains(name))
			return float.Parse((string)s_applicationParams[name]);

        SetParamFloat(name, defaultValue);
		return defaultValue;
	}

	/// <summary>
	/// Set the parameter using float value
	/// </summary>
    public static void SetParamFloat(string name, float value)
    {
        s_applicationParams[name] = value.ToString();
        sendToListeners(name);
	}

	/// <summary>
	/// Get the parameter using vector3 value
	/// If the parameter does not exists, it is added to the config list with default value
	/// </summary>
	public static Vector3 GetParamVector3(string name, Vector3 defaultValue)
	{
		if (!s_applicationParams.Contains(name))
		{

			SetParamVector3(name, defaultValue);
			return defaultValue;
		}

		string txt = (string)s_applicationParams[name];
		string[] ar = txt.Split(';');

		if (ar.Length < 3)
		{
			SetParamVector3(name, defaultValue);
			return defaultValue;
		}

		Vector3 result = Vector3.zero;
		try
		{
			result.x = float.Parse(ar[0]);
			result.y = float.Parse(ar[1]);
			result.z = float.Parse(ar[2]);
		}
		catch 
		{
			SetParamVector3(name, defaultValue);
			return defaultValue;
		}

		return result;
	}

	public static void SetParamVector3(string name, Vector3 value)
	{
		string text = value.x+";"+value.y+";"+value.z;
		s_applicationParams[name] = text;
        sendToListeners(name);
	}



#endregion


#region Listeners functions

	/// <summary>
	/// This delegate function is used for config param listeners
	/// </summary>
	/// <param name="name">the name of the config param that have changed</param>
    public delegate void onConfigChanged(string name);

	/// <summary>
	/// add a listener for a Config parameter using function delegate
	/// a first call to the fct delegate is done if the param exists
	/// 
	/// </summary>
	/// <param name="name">the parameter name</param>
	/// <param name="fct">the function to call when parameter has changed</param>
    public static void addListener(string name, onConfigChanged fct)
    {
        ArrayList listeners = null;
        if (s_paramsListeners.Contains(name))
            listeners = (ArrayList)s_paramsListeners[name];
        else
        {
            listeners = new ArrayList();
            s_paramsListeners[name] = listeners;
        }

        // ajout unique
        if (listeners.IndexOf(fct) == -1)
        {
            listeners.Add(fct);
			if (s_applicationParams.Contains(name)) // on appelle le callback tout de suite si on a une valeur
			{
				fct(name);
			}
        }
    }

	/// <summary>
	/// Remove the parameter listener using
	/// </summary>
	/// <param name="name">the param nam</param>
	/// <param name="fct">the listener function</param>
    public static void removeListener(string name, onConfigChanged fct)
    {
        if (!s_paramsListeners.Contains(name)) return;
            
        ArrayList listeners = (ArrayList)s_paramsListeners[name];
        listeners.Remove(fct);
    }

    static void sendToListeners(string name)
    {
        if (!s_paramsListeners.Contains(name)) return;

        ArrayList listeners = (ArrayList)s_paramsListeners[name];
        for (int i = 0; i < listeners.Count; i++)
        {
            onConfigChanged fct = (onConfigChanged)listeners[i];
            fct(name);
        }
	}
#endregion Listeners fucntions

#region Load Functions

	////////////////////////////////////////////////////////////////////////////////////////////////////
	void Awake()
	{
		instance = this;

		s_curFamily = PlatformFamily.getFamily();
		PlatformFamily.Platform platform = PlatformFamily.getPlatform();
		if (s_curFamily == PlatformFamily.Family.Unknown)
        {
            Debug.LogError("Unknow Platform : " + Application.platform);
            return;
        }

		List<string> modesList = new List<string>(modes);

		modesList.Add(s_curFamily.ToString());
		modesList.Add(platform.ToString());

		if (m_debugTraces)
		{
			Debug.Log("Family : " + s_curFamily);
			Debug.Log("Platform : " + platform);
		}

		if (Application.isEditor)
		{
			s_HttpRoot = WebTools.solidURl("file://" + Application.dataPath + editorRelativePath);
			s_DataRoot = WebTools.solidURl("file://" + Application.dataPath + "/StreamingAssets");
			s_BundlesRoot = s_HttpRoot + "/Bundles/Web";

			modesList.Add("editor");
		}
		else if (s_curFamily == PlatformFamily.Family.Desktop)
		{
			s_HttpRoot = WebTools.concatUrl("file://" + Application.dataPath.Replace('\\', '/'), httpRootRelativePath);
			s_DataRoot = WebTools.solidURl("file://" + Application.dataPath + "/StreamingAssets");

			s_BundlesRoot = s_HttpRoot + "/Bundles/Web";
		}
		else if (s_curFamily == PlatformFamily.Family.Web)
        {
          //  Application.ExternalCall("GetVersion");

			if (m_debugTraces) Debug.Log("Application.dataPath : " + Application.dataPath);

			s_HttpRoot = WebTools.concatUrl(Application.dataPath, httpRootRelativePath);
			s_DataRoot = WebTools.solidURl(Application.dataPath + "/StreamingAssets");
			s_BundlesRoot = s_HttpRoot + "/Bundles/Web";
		}
		else if ((s_curFamily == PlatformFamily.Family.Mobile) || (s_curFamily == PlatformFamily.Family.Glasses))
        {
            if (platform == PlatformFamily.Platform.IOS)
            {
				s_HttpRoot = "undefined";
				s_DataRoot = "file://" + Application.dataPath + "/Raw";
				s_BundlesRoot = s_HttpRoot + "/Bundles/IOS";
			}
			else if (platform == PlatformFamily.Platform.Android)
            {
				//"jar:file://" + Application.dataPath + "!/assets/test.txt"
				s_HttpRoot = "undefined";
				s_DataRoot = "jar:file://" + Application.dataPath + "!/assets";
				s_BundlesRoot = s_HttpRoot + "/Bundles/Android";
			}
        }

		if (m_debugTraces) Debug.Log("Setting s_HttpRoot :" + s_HttpRoot);
		if (m_debugTraces) Debug.Log("Setting s_DataRoot :" + s_DataRoot);
		if (m_debugTraces) Debug.Log("Setting s_BundlesRoot :" + s_BundlesRoot);

		modes = modesList.ToArray();
		validModesDescription = string.Join(", ", modes);
	}

    ////////////////////////////////////////////////////////////////////////////////////////////////////
	IEnumerator SetDebugInfo()
	{
		while (true)
		{
			if (m_DbgPanel.isVisible())
			{
				m_DbgPanel.ClearMessages();
				m_DbgPanel.AddMessage("------- Application global parameters");
				//m_DbgPanel.AddMessage("Version = " + s_Version, "green");
				m_DbgPanel.AddMessage("current Modes = " + validModesDescription, DebugPanel.TextColor.green);
				m_DbgPanel.AddMessage("HttpRoot = " + s_HttpRoot, DebugPanel.TextColor.green);
				m_DbgPanel.AddMessage("DataRoot = " + s_DataRoot, DebugPanel.TextColor.green);
				m_DbgPanel.AddMessage("BundleRoot = " + s_BundlesRoot, DebugPanel.TextColor.green);

				m_DbgPanel.AddMessage("------- Application custom parameters");

				List<string> list = new List<string>();

				// Sort fish alphabetically, in ascending order (A - Z)
				IDictionaryEnumerator iterator = s_applicationParams.GetEnumerator();
				while (iterator.MoveNext())
				{
					string val = (string)iterator.Value;
					string key = (string)iterator.Key;
					list.Add(key + " = " + val);

				}
				list.Sort();
				foreach (string value in list)
				{
					m_DbgPanel.AddMessage(value);
				}
			}

			yield return new WaitForSeconds(1);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	void Start()
	{
		if (m_ShowDebugPanel)
		{
            m_DbgPanel = new DebugPanel("Config", false, false, false);
			StartCoroutine(SetDebugInfo());
		}

		// read Parameter twice, once for the mode, once for overloaded params
		if (PlatformFamily.getFamily() == PlatformFamily.Family.Web)
			ReadAppParameters();

		if (s_applicationParams.Contains("mode"))
		{
			List<string> modesList = new List<string>(modes);
			modesList.Add((string)s_applicationParams["mode"]);
			modes = modesList.ToArray();
		}

		if (mainConfigFile != "")
			ReadAppConfig(mainConfigFile);
		else
			m_status.End(true);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	void ReadAppParameters()
	{
		if (m_debugTraces) Debug.Log("ReadAppParameters " + Application.absoluteURL);

		int pos = Application.srcValue.IndexOf('?');

		if (pos > 5)
		{
			// parse Application srcValue
			string urlSrc = Application.srcValue.Substring(pos + 1);

			if (urlSrc.Length >= 2)
			{
				if (m_debugTraces) Debug.Log("urlSrc : " + urlSrc);

				// ok to find parameters
				string[] parametersAndValue = urlSrc.Split('&');
				for (int i = 0; i < parametersAndValue.Length; i++)
				{
					int indexEqual = parametersAndValue[i].IndexOf('=');
					if (indexEqual != -1)
					{
						string paramName = parametersAndValue[i].Substring(0, indexEqual);
						string value = parametersAndValue[i].Substring(indexEqual+1);

						SetParam(paramName, value);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	void ReadAppConfig(string filename)
	{
		if (filename == "")
			return;

		validModesDescription = string.Join(", ", modes);

		if (m_debugTraces) Debug.Log("---- ReadAppConfig " + filename);
		string url;

		if (s_curFamily == PlatformFamily.Family.Web && filename.StartsWith("http://"))
			url = WebTools.rebuildUrl(filename + "?ver=" + GetParamString("Version", UnityEngine.Random.Range(0, 100000).ToString()));
		else
			url = WebTools.rebuildUrl(filename);

		if (m_debugTraces) Debug.Log("---- url : " + url);

		m_status.Start();
		StartCoroutine(LoadConfig(url, true));
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	IEnumerator LoadConfig(string url, bool mainFile)
	{
		if (m_debugTraces) Debug.Log("LoadConfig " + url);

		WWW wwwConfig = new WWW(url);
		yield return wwwConfig;

		if (wwwConfig.error != null && wwwConfig.error != "")
		{
			//Debug.LogError("Error on config file : " + url);
			Debug.LogWarning(wwwConfig.error);
			if (mainFile) m_status.End(false);
		}
		else
		{
			string[] lines = ((string)wwwConfig.text).Split(new char[] { '\n', '\r' });

			// no mode at the beginning
			string mode = "";
			bool readLine = true;

			for (int i = 0; i < lines.Length; i++)
			{
				string line = lines[i];

				// remove space and tabs
				line = line.Trim();

				if (line.Length < 3) continue; // nothing to read if less than 3 characters minimum can be a=b or [a]
				//if (m_debugTraces) Debug.Log("line = " + line);
			
				if (line[0] == '[' && line[line.Length - 1] == ']')
				{
					//mode change
					mode = line.Substring(1, line.Length - 2);
					mode = mode.ToLower();

					if (m_debugTraces) Debug.Log("mode change : " + mode);
					if (m_debugTraces) Debug.Log("valids : " + validModesDescription);

					if (mode == "all")
					{
						readLine = true;
						if (m_debugTraces)	Debug.Log("valid Mode [all] ");
					}
					else
					{
						readLine = false;
						foreach (string validMode in modes)
						{
							if (validMode.ToLower() == mode)
							{
								readLine = true;
							}
						}

						if (m_debugTraces)
						{
							if (readLine)
								Debug.Log("valid Mode : " + mode);
							else
								Debug.Log("skip Mode : " + mode);

						}
					}
				}

				if (readLine)
				{
					// skip comments
					if (line[0] == '/' && line[1] == '/') continue;
					
					if (line.StartsWith("#include"))
					{
						int posQuote = line.IndexOf('"');

						string newFilename = line.Substring(posQuote +1, line.Length - posQuote - 2);
						//if (m_debugTraces) Debug.Log("include newFilename : " + newFilename);

						string newUrl = WebTools.buildRelativeUrl(url, newFilename);

						if (m_debugTraces) Debug.Log("include newUrl : " + newUrl);
						if (newUrl == url)
						{
							Debug.LogError("error including myself");
						}
						else
						{			
							yield return  StartCoroutine( LoadConfig(newUrl, false));
						}
					}
					else if (!line.StartsWith("#"))
					{
						// ok to find parameters

						int indexEqual = line.IndexOf('=');
						if (indexEqual != -1)
						{
							string paramName = line.Substring(0, indexEqual);
							string value = line.Substring(indexEqual + 1);

							SetParam(paramName, value);
						}
						

					}
				}
			}

			if (mainFile) m_status.End(true);

			if (m_debugTraces) Debug.Log("End Load " + url);

		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// return true if the parameter is valid
	void SetParam(string name, string value)
	{
		//Debug.Log("SetParam " + name + "=" + value);
		string lower = name.ToLower();
		switch (lower)
		{
			case "dbgpanel":
				DebugPanelMgr.s_isActive = (value == "1");
				SetParamString(name, value);
				return;

			// those values are directly copied
			case "httproot":
				s_HttpRoot = WebTools.rebuildUrl(value);
				if (m_debugTraces) Debug.Log("change s_HttpRoot : " + s_HttpRoot);
				//	SetParamString(name, value);
				return;
			case "dataroot":
				s_DataRoot = WebTools.rebuildUrl(value);
				if (m_debugTraces) Debug.Log("change s_DataRoot : " + s_DataRoot);
				//	SetParamString(name, value);
				return;

			case "bundleroot":
			case "bundlesroot":
				s_BundlesRoot = WebTools.rebuildUrl(value);
				if (m_debugTraces) Debug.Log("change s_BundlesRoot : " + s_BundlesRoot);
				//	SetParamString(name, value);
				return;

			case "mode":
			case "addmode":
				addMode(value);
				if (m_debugTraces) Debug.Log("added new Mode : " + value);
				return;

			case "removemode":
			case "rmmode":
				removeMode(value);
				if (m_debugTraces) Debug.Log("remove Mode : " + value);
				return;

			default:
				SetParamString(name, value);

				if (m_debugTraces) Debug.Log("SetParam " + name + " = " + value);

				// ce switch permet de filtrer �ventuellement des entr�e invalides. 
				// ici j'ai un default donc on accepte tout
				//if (m_debug) Debug.Log("SetParam default " + name + " = " + value);
				return;
		}

		//if (m_debug) Debug.Log("error setting invalid param " + name + " = " + value);

		// si on sort du switch le param�tre est refus�
		//s_applicationParams.Remove(name);
	}


	// not taken in account util reload is called
	public static void addMode(string mode)
	{
		List<string> modes = new List<string>(instance.modes);

		if (mode != "")
			modes.Add(mode);

		instance.modes = modes.ToArray();
	}

	/// <summary>
	///  not taken in account util reload is called
	/// </summary>
	/// <param name="addMode"></param>
	public static void removeMode(string mode)
	{
		List<string> modes = new List<string>(instance.modes);

		if (mode != "")
			modes.Remove(mode);

		instance.modes = modes.ToArray();
	}

	/// <summary>
	///  reload all config files, useful when you have changed a mode
	/// </summary>
	public static void reload()
	{
		instance.ReadAppConfig(instance.mainConfigFile);
	}

#endregion
	////////////////////////////////////////////////////////////////////////////////////////////////////

    void Update()
    {
        // Quit the standalone app with Echap
		if (s_curFamily == PlatformFamily.Family.Desktop)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }


}

////////////////////////////////////////////////////////////////////////////////////////////////////
