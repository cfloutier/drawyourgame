using UnityEngine;
using System.Collections;
//using System.IO;
using System;
using System.Collections.Generic;

// la classe download permet de g�rer tous les t�l�charment que peuvent avoir besoin une sc�ne.
// un controle de la bande passante et du nombre de t�l�chargement en parall�le est g�r� ici.
// Cette classe n'est pas utils�e par le loader


/// <summary>
/// the Download class is a set of static tools used for help downloading files
/// It use WWW calls and maintains a list of Queued downloads saving Bandwidth.
/// It also have a simple cache management
/// 
/// The Download class must be placed on an empty GameObject, only once.
/// 
/// Use Download.Get().Load(...) functions to start a new download
/// 
/// You will be called when the download is done (or on error)  using a delegate fonction
/// The Download Class also use DebugPanelMgr feature to watch in your final app 
/// All waiting download, cached files list can be viewed there
/// </summary>
/// 
public class Download : MonoBehaviour
{
	/// <summary>
	/// Universal interface to help in the creation of Hashtables.  Especially useful for C# users.
	/// </summary>
	/// <param name="args">
	/// A object[] of alternating name value pairs.  For example "time",1,"delay",2...
	/// </param>
	/// <returns>
	/// a new hastable
	/// </returns>
	public static Hashtable Hash(params object[] args)
	{
		Hashtable hashTable = new Hashtable(args.Length / 2);
		if (args.Length % 2 != 0)
		{
			Debug.LogError("[Download] - Hash requires an even number of arguments!");
			return null;
		}
		else
		{
			int i = 0;
			while (i < args.Length - 1)
			{
				hashTable.Add(args[i], args[i + 1]);
				i += 2;
			}
			return hashTable;
		}
	}

	/// <summary>The type of file to retrieve</summary>
	public enum Type
	{
		/// <summary>Ascii text (UTF7)</summary>
		Ascii_Text,
		/// <summary>UTF8 Text</summary>
		UTF8_Text,
		/// <summary>a 2D Texture</summary>
		Texture2D,
		/// <summary>an asset bundle</summary>
        AssetBundle,
        /// <summary>raw data</summary>
        RawData,
        /// <summary>audio</summary>
        Audio
    };

	static bool s_debug = false;
	bool m_showDownloadingState = true;
	DebugPanel m_DbgPanel = null;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Singleton

	static Download s_instance = null;

	/// <summary>
	/// static Instance Accessor
	/// </summary>
	public static Download Get()
	{
		if (s_instance == null)
		{

			Debug.LogError("[Download] - Instance is NULL!!!!!!!!!!!!");
		}

		return s_instance;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Accessor to count of current Queud downloads
	/// </summary>
	public int GetDownloadRequestSize()
	{
		return s_downloadList.Count;
	}

	public float GetDownloadProgress()
	{
		float progress = 1.0f;
		for (int i = s_computeList.Count - 1; i >= 0; i--)
		{
			DownloadInfos downloadInfos = (DownloadInfos)s_computeList[i];

			progress *= downloadInfos.progress;
		}
		return progress;
	}

	float s_freeBandwidth = 1.0f;

	ArrayList s_downloadList = new ArrayList();
	ArrayList s_computeList = new ArrayList();


	Hashtable s_cache = new Hashtable();

	class DownloadInfos
	{
		public DownloadInfos()
		{
			url = "";
			type = Type.Ascii_Text;

			priority = 0.5f;		// 1 : forte, 0 passe derri�re tout nouveau download
			bandwidth = 0.25f;	// 4 fichiers en parallele

			cache = true;		// mise en cache 

			retry = 0;		// nombre de tentatives apr�s echec

			downloadDelegate = null;		// la fonction � appeler quand c'est termin� (obligatoire)


			userParam = null;		// les param�tres utilisateur s'il y en a
		}

		public string url;
		public Type type;

		public float priority;
		public float bandwidth;
		public float progress;
		public bool cache;
		public int retry;

		public DownloadDelegate downloadDelegate;

		public List<DownloadInfos> children = new List<DownloadInfos>();

		public object userParam;

		public class Comparer : IComparer
		{
			int IComparer.Compare(System.Object x, System.Object y)
			{
				DownloadInfos p1 = (DownloadInfos)x;
				DownloadInfos p2 = (DownloadInfos)y;

				if (p1.priority < p2.priority)
				{
					return -1;
				}
				else if (p1.priority > p2.priority)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		}

		public void analyseParameters(Hashtable table)
		{
			if (table == null) return;

			if (table.Contains("priority"))
				priority = (float)table["priority"];
			if (table.Contains("bandwidth"))
				bandwidth = (float)table["bandwidth"];
			if (table.Contains("cache"))
				cache = (bool)table["cache"];
			if (table.Contains("retry"))
				retry = (int)table["retry"];
			if (table.Contains("userParam"))
				userParam = table["userParam"];

		}

		public void callDelegates(object result, float progress, string error)
		{
			downloadDelegate(result, progress, userParam, error);
			foreach (DownloadInfos child in children)
				child.callDelegates(result, progress, error);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	void Awake()
	{
		s_instance = this;

		if (m_showDownloadingState)
		{
            m_DbgPanel = new DebugPanel("Downloads", false, false, false);
			StartCoroutine(SetDebugInfo());
		}
	}

	IEnumerator SetDebugInfo()
	{
		while (true)
		{
			if (m_DbgPanel.isVisible())
			{
				m_DbgPanel.ClearMessages();
				for (int i = s_computeList.Count - 1; i >= 0; i--)
				{
					DownloadInfos downloadInfos = (DownloadInfos)s_computeList[i];

					string infos = downloadInfos.url;
					m_DbgPanel.AddMessage("In Progress", infos, DebugPanel.TextColor.blue);
				}
				for (int i = s_downloadList.Count - 1; i >= 0; i--)
				{
					DownloadInfos downloadInfos = (DownloadInfos)s_downloadList[i];

					string infos = downloadInfos.url;
					m_DbgPanel.AddMessage("Queued", infos, DebugPanel.TextColor.yellow);
				}


				IDictionaryEnumerator iterator = s_cache.GetEnumerator();
				while (iterator.MoveNext())
				{
					string url = (string)iterator.Key;
					string infos = url;
					m_DbgPanel.AddMessage("Cache", infos, DebugPanel.TextColor.green);
				}
			}

			yield return new WaitForSeconds(1);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Delegate to use in Download.Load method
	/// </summary>
	/// <param name="result">The file downloading: cast into (string) or (Texture2D) to get your data. Null is error</param>
	/// <param name="progress">The current download progress sent. check when value is 1 to know if the download has ended, check tehn error string</param>
	/// <param name="userParam">user parameter as passed to the Load function</param>
	/// <param name="error">Null is no error otherwise contain the error message</param>
	/// 
	public delegate void DownloadDelegate(object result, float progress, object userParam, string error);

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Point d'entree

	/// <summary>
	/// cf. Method Load
	/// </summary>
	/// <param name="fileURL">Address of file to download</param>
	/// <param name="useVersion">Add version to url or not</param>
	/// <param name="type">type of file : "string", "stringUTF8", "Texture2D"</param>
	/// <param name="downloadDelegate">Delegate launch at the end of download</param>
	public static void Get(string fileURL, bool useVersion, Type type, DownloadDelegate downloadDelegate)
	{
		Get(fileURL, useVersion, type, downloadDelegate, null);
	}

	/// <summary>
	/// cf. Method Load
	/// </summary>
	/// <param name="fileURL">Address of file to download</param>
	/// <param name="useVersion">Add version to url or not</param>
	/// <param name="type">type of file : "string", "stringUTF8", "Texture2D"</param>
	/// <param name="downloadDelegate">Delegate launch at the end of download</param>
	/// <param name="parameters">Other parameters, use Download.Hash to ease the creation.</param>
	public static void Get(string fileURL, bool useVersion, Type type, DownloadDelegate downloadDelegate, Hashtable parameters)
	{
		if (useVersion && fileURL.Contains("http://"))
		{
			string version = Config.GetParamString("Version", "");
			if (version.Length > 0)
			{
				fileURL += "?ver=" + version;
			}
		}
		Load(fileURL, type, downloadDelegate, parameters);
	}


    /// <summary>
    /// Simple Call without any other param than url, type and delegate (minimum requirement)
    /// </summary>
    /// <param name="fileURL">Address of file to download</param>
    /// <param name="type">type of file : "string", "stringUTF8", "Texture2D"</param>
    /// <param name="downloadDelegate">Delegate launch at the end of download</param>
    public static void Load(string fileURL, Type type, Action<object, float, string> downloadDelegate)
    {
        Load(fileURL, type, ActionDelegate, Download.Hash("userParam", downloadDelegate));
    }

    public static void ActionDelegate(object result, float progress, object userParam, string error)
    {
        Action<object, float, string> downloadDelegate = (Action<object, float, string>)userParam;
        downloadDelegate(result, progress, error);
    }

    /// <summary>
    /// Simple Call without any other param than url, type and delegate (minimum requirement)
    /// </summary>
    /// <param name="fileURL">Address of file to download</param>
    /// <param name="type">type of file : "string", "stringUTF8", "Texture2D"</param>
    /// <param name="downloadDelegate">Delegate launch at the end of download</param>
    public static void Load(string fileURL, Type type, DownloadDelegate downloadDelegate)
    {
        Load(fileURL, type, downloadDelegate, null);
    }

	/// <summary>
	/// The Main function. Parameters are passed using the Download.Hash funtion
	/// 
	/// usage : Download.Get().Load("myUrl", Type.Texture2D, myDelegateFunction, Download.Hash(
	///			"param", value,
	///			...
	///			));
	///			
	/// Here is the complete list of parameters : 
	/// * "priority" (float [0-1]) : 1 is high priority 0 is low. each new download is sorted in the cue depending on this value
	/// * "bandwidth" (float [0-1]) : the part of complete bandwidth alowed for this download (0-1). 1 will make this download unique when called
	/// * "cache" (bool) : should the result object be added to local cache ? Defult is true
	/// that will remains valid. defautl is false
	/// * "reload" (bool) : erase cache if any and call a new download, default is false
	/// * "userParam" : an object sent to the delegate function. default is null
	/// </summary>
	/// <param name="fileURL">Address of file to download</param>
	/// <param name="type">type of file : "string", "stringUTF8", "Texture2D"</param>
	/// <param name="downloadDelegate">Delegate launch at the end of download, shoudl be either a DownloadDelegate or a DownloadDelegateParam if userParam is set</param>
	/// <param name="parameters">Other parameters, use Download.Hash to ease the creation.</param>
	public static void Load(string fileURL, Type type, DownloadDelegate downloadDelegate, Hashtable parameters)
	{
		DownloadInfos downloadInfos = new DownloadInfos();
		downloadInfos.url = fileURL;

		downloadInfos.downloadDelegate = downloadDelegate;
		downloadInfos.type = type;
		downloadInfos.analyseParameters(parameters);


		DownloadInfos parentDownloadInfos = s_instance.FindCurrentDownload(fileURL);
		if (parentDownloadInfos == null)
		{
			s_instance.LoadProcess(downloadInfos);
		}
		else
		{
			parentDownloadInfos.children.Add(downloadInfos);
		}
	}

	DownloadInfos FindCurrentDownload(string fileURL)
	{
		for (int i = 0; i < s_computeList.Count; i++)
		{
			DownloadInfos infos = (DownloadInfos)s_computeList[i];
			if (infos.url == fileURL)
				return infos;
		}

		for (int i = 0; i < s_downloadList.Count; i++)
		{
			DownloadInfos infos = (DownloadInfos)s_downloadList[i];
			if (infos.url == fileURL)
				return infos;
		}

		return null;
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////

	void LoadProcess(DownloadInfos downloadInfos)
	{
		if (s_debug)
		{
			Debug.Log("[Download] - LoadProcess '" + downloadInfos.url + "'");
		}

		if (downloadInfos.url == null || downloadInfos.url == "")
		{

			downloadInfos.callDelegates(null, 1, "Empty url");
			return;
		}

		if (CacheAction(downloadInfos) == false)
		{
			s_downloadList.Add(downloadInfos);

			// Sort by priority order
			s_downloadList.Sort(new DownloadInfos.Comparer());
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	bool CacheAction(DownloadInfos downloadInfos)
	{
		if (downloadInfos.cache)
		{
			object result = s_cache[downloadInfos.url];

			if (result != null)
			{

				downloadInfos.callDelegates(result, 1, null);
				return true;
			}
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	void Update()
	{
		if (s_downloadList.Count > 0)
		{
			DownloadInfos downloadInfos = (DownloadInfos)s_downloadList[s_downloadList.Count - 1];

			if (CacheAction(downloadInfos))
			{
				// Remove this download from the list
				s_downloadList.RemoveAt(s_downloadList.Count - 1);

				return;
			}

			if (s_freeBandwidth - downloadInfos.bandwidth >= 0.0f)
			{
				// Ok, it's time to download it!
				s_computeList.Add(downloadInfos);

				// Remove this download from the list
				s_downloadList.Remove(downloadInfos);

				// Keep updated bandwidth
				s_freeBandwidth -= downloadInfos.bandwidth;

				// Launch process
				StartCoroutine(DownloadCoroutine(downloadInfos));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Coroutine

	IEnumerator DownloadCoroutine(DownloadInfos downloadInfos)
	{
		if (s_debug) Debug.Log("[Download] - Loading " + downloadInfos.type + " request " + downloadInfos.url);

		WWW www = new WWW(downloadInfos.url);

		int attempt = downloadInfos.retry;
		while (attempt >= 0)
		{
			while (!www.isDone)
			{
				yield return new WaitForSeconds(0.1f);

				if (www.progress != 1)
				{
					downloadInfos.progress = www.progress;
					downloadInfos.callDelegates(null, www.progress, null);
				}
				if (www.error != null)
					break;
			}
			if (www.error != null)
			{
				attempt--;
				if (attempt >= 0)
				{
					Debug.LogError("[Download] - Request Error " + www .url + " (" + www.error + "). New try.");
					www = new WWW(downloadInfos.url);
				}
			}
			else
			{
				break;
			}
		}

		if (www.error != null)
		{
			downloadInfos.callDelegates(null, 1, www.error);
		}
		else
		{
			//Debug.Log(www.text);

			object result = null;
			try
			{
				switch (downloadInfos.type)
				{
					case Type.Ascii_Text: result = (object)System.Text.Encoding.UTF7.GetString(www.bytes); break;
                    case Type.UTF8_Text: result = (object)System.Text.Encoding.UTF8.GetString(www.bytes, 3, www.bytes.Length - 3); break;// Skip thr first 3 bytes (i.e. the UTF8 BOM)
                    //case Type.UTF8_Text: result = (object)System.Text.Encoding.UTF8.GetString(www.bytes); break;
                    case Type.Texture2D:
						{
							result = (object)www.texture;
							/*if (www.text[0] == '<')
							{
								Debug.LogError("[Download] - Invalid Url for texture " + www.url);
								result = null;
							}
							else
								result = (object)www.texture;*/

							break;
						}
					case Type.AssetBundle: result = (object)www.assetBundle; break;
                    case Type.RawData: result = (object)www.bytes; break;
                    case Type.Audio: result = (object)www.audioClip; break;
                    default: result = null; break;
				}
			}
			catch (Exception e)
			{
				Debug.LogError("[Download] - Access Error " + e.ToString());
			}

			if (result == null)
			{
				downloadInfos.callDelegates(null, 1, "Error invalid type");
			}
			else
			{
				if (downloadInfos.cache && result != null)
				{
					s_cache[downloadInfos.url] = result;
				}

				downloadInfos.callDelegates(result, 1, null);
			}
		}

		s_computeList.Remove(downloadInfos);
		s_freeBandwidth += downloadInfos.bandwidth;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Check if an url is in Queued List
	/// </summary>
	/// <param name="url">the url name</param>
	static public bool IsLoading(string url)
	{
		for (int i = s_instance.s_downloadList.Count - 1; i >= 0; i--)
		{
			DownloadInfos downloadInfos = (DownloadInfos)s_instance.s_downloadList[i];

			if (downloadInfos.url == url)
			{
				return true;
			}
		}

		return false;
	}



}
