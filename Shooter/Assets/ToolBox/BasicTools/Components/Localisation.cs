using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Text;
using System.Text.RegularExpressions;
using ReadWriteCsv;

using System.Collections.Generic;
using System.Threading;

/// <summary>
/// Helper Class for Localisation use 
/// 
/// Simple use L.tr("mystringCode") to translate 
/// 
/// Warning this method will use the global Hastable and therefore should'nt be used in Update fuicntion or OnGui and so on
/// </summary>
public class L
{
	/// <summary>
	/// translate a code to current language 
	/// </summary>
	/// <param name="code">the string code, this string should be found in the main localisation file</param>
	/// <returns>the translation according to current language. if not found the code is returned</returns>
	static public string tr(string code)
	{
		return Localisation.Get().Translate(code);
	}

	static public string[] trEnum(string prefixCode, System.Type enumType)
	{
		string[] texts = System.Enum.GetNames(enumType);
		for (int i = 0; i < texts.Length; i++)
		{
			texts[i] = L.tr(prefixCode + "." + enumType.Name + "." + texts[i]);
		}

		return texts;
	}
}

/// <summary>
/// This class can be used as a cache for any calls that need text to be translated.
/// 
/// The Usage is quite simple :
/// * Create an enum as a member of the class
/// * and add a variable to the class with the enum passed as template parameter 
/// ex :
/// class myClass
/// {
///		enum TextCodes {
///			Code1,
///			Code2,
///			...
///		}
///		
///		LocalisationCache<TextCodes> L = new LocalisationCache&#60;TextCodes&#62;();
/// }
/// 
/// in any function that need some text to be translated use
/// translationTable.tr(TextCodes.Code1)
/// 
/// to ease the reading, I advice to use short name for translationTable (like the L character)
/// 
/// you will then have : L.tr(TextCodes.Code1);
/// 
/// using enum have many advantages :
/// * when you remove an item, you will see when compiling everywhere it is used.
/// * the Visual c# and MonoDevellop completion will gretly help to choose you text without caring about case sensitivity or any mispelled wrold troubles.
/// 
/// The class automatically reload it's content on language changes
/// </summary>
/// <typeparam name="T">An enum</typeparam>
public class LocalisationCache<T> where T : struct, IConvertible
{
	string[] localTable = null;
	Type m_enumType;
	/// <summary>
	/// Constructor
	/// </summary>
	public LocalisationCache()
	{
		if (!typeof(T).IsEnum)
		{
			Debug.Log("T must be an enumerated type");
		}

		// Init static Localisation cr�ation
		Localisation.Get();
		m_enumType = typeof(T);
		Config.addListener("Language.Loaded", load);
	}
	
	// should be called first with a valid enum
	void load(string name)
	{
		if (!Application.isPlaying) return;
		string[] names = System.Enum.GetNames(m_enumType);
		localTable = new string[names.Length];
		for (int i = 0; i < names.Length; i++)
		{
			localTable[i] = L.tr(names[i]);
		}
		if (delegateFct != null)
			delegateFct();
	}

	public delegate void onCacheReloaded();
	public onCacheReloaded delegateFct = null;


	/// <summary>
	/// The main translation function
	/// </summary>
	/// <param name="code">one of the enum value</param>
	/// <returns>the translated string according to current language</returns>
	public string tr(T code)
	{
		if (localTable != null)
			return localTable[code.ToInt16(null)];
		return "";
	}
}


/// <summary>
/// The main Localisation class : it will help you with multi-language purpose
/// 
/// The Localisation is not a MonoBehavior : it is no use to insatntiate it. The first user will load its content.
/// 
/// The language table is a cvs file located in "[dataroot]/Language.csv" 
/// it is a real coma separated table written in utf8. 
/// It can be edited using 
/// * notepad++ but it hard 
/// * google docs spreadsheet
/// * Open Office Calc. (use coma as separator and unicode utf8 as import parameter)
/// 
/// The Localisation class use the Config class 
/// * to know which language to load (parameter "Language", if not set in Config.ini the default param is FR)
/// * to warn classes that need to know when language has changed and is loaded : add a listener on "Language.Loaded"
/// 
/// The Language file is automatically loaded as soon as Config is ready (using InitManager)
/// 
/// To force the first loading please use the accessor Localisation.Get();
/// 
/// To translate a string use Localisation.Get().Translate(stringcode); 
/// The stringcode is case sensitive
/// 
/// 
/// 
/// </summary>
public class Localisation
{
	// Singleton
	static Localisation instance;

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// The static accessor
	/// </summary>
	/// <returns>main Localisation static class</returns>
	public static Localisation Get()
	{
		if (instance == null)
		{
			instance = new Localisation();
		}

		return instance;
	}

	ArrayList rows = new ArrayList();
	Hashtable mainTable = new Hashtable();
	public bool showWarningMessages = true;
	string language;
	InitState m_status = new InitState("Localisation");

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	
	Localisation()
	{
		// need config to start
		m_status.AddDependency("Config");
		m_status.setCallback(InitDelegate);
	}

	bool firstInitSet = false;

	void InitDelegate(InitState initstate, object param)
	{
		if (!firstInitSet)
		{
			firstInitSet = true;
			LoadDataRootLanguageFile();
		}

		Config.addListener("LocalisationFile", onFileChanged);
	}

	void onFileChanged(string paramName)
	{
		LoadDataRootLanguageFile();
	}

	void LoadDataRootLanguageFile()
	{
		if (Download.Get() != null)
		{
			string LanguageFile = Config.GetParamString("LocalisationFile", "[dataroot]/Language.csv");
			Download.Get(WebTools.rebuildUrl(
				LanguageFile), true,
				Download.Type.UTF8_Text, LoadingDelegate, Download.Hash(
					"priority", 1.0f,
					"retry", 3
				));
		}
		else
			m_status.End(false);
	}

	void LoadingDelegate(object data, float progress, object userParam, string error)
	{
		if (progress != 1) return;

		Config.removeListener("Language", onLanguageChanged);

		//Debug.Log("LoadingDelegate");
		if (error != null)
		{
			Debug.LogError("[Internal] - " + error);
			m_status.End(false);
			return;
		}

		System.IO.MemoryStream ms = new System.IO.MemoryStream(
				System.Text.Encoding.UTF8.GetBytes((string)data));
		
		rows.Clear();

		CsvFileReader reader = new CsvFileReader(ms, ',');
		
		CsvRow row = new CsvRow();

		while (reader.Peek() != -1)
		{
			reader.ReadRow(row);
			rows.Add(row);
			row = new CsvRow();
		}


		// Now at last, add a dependency on language config param
		// set default if not previously set
		Config.GetParamString("Language", "FR");
		Config.addListener("Language", onLanguageChanged);

		m_status.End(true);

		Config.SetParamBool("Language.Loaded", true);
	}

	int findCodeIndex(CsvRow codeRow, string langCode)
	{
		for (int i = 1; i < codeRow.Count; i++)
		{
			if (codeRow[i] == langCode)
			{
				return i;
			}
		}

		return - 1;
	}

	void onLanguageChanged(string name)
	{
		language = Config.GetParamString("Language", language);

		// Check if Lang is in first row codes
		if (rows.Count == 0)
		{
			Debug.LogError("Error empty or incorrect Language.csv file");
			return;
		}

		CsvRow codeRow = (CsvRow)rows[0];
		int index = findCodeIndex(codeRow, language);
		if (index == -1) 
		{
			Debug.LogError("Error wrong language code : "+ language);
			return;
		}

		//Debug.Log("count " + rows.Count);
		mainTable.Clear();
		for (int i = 1; i < rows.Count; i++)
		{
			CsvRow row = (CsvRow)rows[i];
			if (row.Count > 0)
			{
				if (row.Count <= index)
				{
					Debug.LogWarning("Wrong row lenght for code " + row[0] +  " line " + (i+1));
					continue;
				}
				mainTable[row[0]] = row[index];
			}
		}

		Config.SetParamBool("Language.Loaded", true);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Simple access to current language code
	/// </summary>
	/// <returns>FR, UK SP and so on </returns>
	public string GetCurLanguage()
	{
		return language;
	}

    System.Globalization.CultureInfo oldCulture;
    public void SwitchCultureToLanguage()
    {
        oldCulture = Thread.CurrentThread.CurrentCulture;
        if ((language == "FR") && (oldCulture.Name != "fr-FR"))
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR");
        }
        else if ((language == "UK") && (oldCulture.Name != "en-GB"))
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-GB");
        }
        else if ((language == "SP") && (oldCulture.Name != "es-ES"))
        {
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es-ES");
        }
    }

    public void RestoreCulture()
    {
        Thread.CurrentThread.CurrentCulture = oldCulture;
    }

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// The main translate function. It give access to the main hastable
	/// </summary>
	/// <param name="code">the string code (case sensitive)></param>
	/// <returns>the translated string</returns>
	public string Translate(string code)
	{
		if (string.IsNullOrEmpty(code)) return "";
		if (!m_status.isReady()) return "TR not ready - " + code;
		string result = (string)mainTable[code];
		
		if (string.IsNullOrEmpty(result))
		{
			if (showWarningMessages)
				Debug.LogWarning("Localisation : no code found [ " + code + " ]");

			result = "[" + code + "]";
		}
        result = result.Replace('|', '\n');

		return result;
	}

	

	//////////////////////////////////////////////////////////////////
}
