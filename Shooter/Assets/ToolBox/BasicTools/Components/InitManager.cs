﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;

/// <summary>
/// The InitState class is the center class of the Init Manager.
/// 
/// It can be used as a tool in your own classes to prevent other classes to start their init process before when other processes 
/// that depends on it isn't ready. 
/// 
/// It also send notification when a dependency has fallen to error. All classes that depends on it directly or indirectly are notified.
/// 
/// usage :
/// 
/// In your class, add an InitState member 
/// ex :
/// <pre>protected InitState myInitState;</pre>
/// 
/// And build it in Start or Awake functions
/// <pre>myInitState = new InitState("MyProcess");</pre>
/// The name of the state is used to check dependencies. 
/// 
/// * If your process does does not depends on another, just do a '''myInitState.Start()''' and start your init process
/// * If it depends on anothers, adds them by using for each
/// <pre>   myInitState.AddDependencies("OtherProcess");</pre>
/// defines then your delegate function using setCallback(...)
/// 
/// When thoses dependencies will be ready, this fucntion will be called, and you could then start your init process
/// 
/// When It's ready call
/// * '''End(true)''' : if the init process is a success
/// depending states will be called and could begin theiur init process
/// * call ''''End(false)''' : if the init process met an error
/// depending states will be called whith a state set to '''DependencyError'''
/// 
/// At any time, when you met an error you can call End(false)
/// </summary>
public class InitState
{
	static Hashtable s_InitList = new Hashtable();
	/// <summary>
	/// used by the InitManager to get states. don't use it directly
	/// </summary>
	public static Hashtable InitList { get { return s_InitList;}}

	/// <summary>
	/// The delegate function. All process can be warned of error or succes of dependencies using this callback
	/// </summary>
	/// <param name="initstate">the State that has changed</param>
	/// <param name="param">the callback parameters/param>
	public delegate void InitDelegate(InitState initstate, object param);

	/// <summary>
	/// Enum : differents states
	/// </summary>
	public enum ProgressState
	{
		/// <summary>This State has ended its init process with success</summary>
		Ready,
		/// <summary>This State has started its init process</summary>
		Started,
		/// <summary>This State has not been started because dependencies are not ready</summary>
		WaitinForDependencies,
		/// <summary>A depencency has been set to error</summary>
		DependencyError,
		/// <summary>This State has ended its init process with error</summary>
		Error
	}

	/// <summary>
	/// The main indentifier of the State
	/// </summary>
	public string m_identifier;
	/// <summary>
	/// List of dependencies
	/// </summary>
	public ArrayList m_dependencies = new ArrayList();

	/// inverse of dependencies
	ArrayList children = new ArrayList();

	InitDelegate m_InitDelegate;
	object m_InitDelegateParam;
	ProgressState m_progressState = ProgressState.WaitinForDependencies;

	/// <summary>
	/// Direct access to progress state, For debug purpose only. you should use function isReady() instead
	/// </summary>
	/// <returns>the current state</returns>
	[Obsolete("Use getStatus")]
	public ProgressState getProgressState() { return m_progressState; }
	public ProgressState getStatus() { return m_progressState; }

	/// <summary>
	/// Checks if the task is ready
	/// </summary>
	/// <returns>true if ready</returns>
	public bool isReady()
	{
		return m_progressState == ProgressState.Ready;
	}

	/// <summary>
	/// Checks if the task is on error
	/// </summary>
	/// <returns>true if on error</returns>
	public bool isError()
	{
		return (m_progressState == ProgressState.Error) || (m_progressState == ProgressState.DependencyError);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// main Constructor. Please call it only in Start ou Awake
	/// </summary>
	/// <param name="idString">The main identifier. you should use differents name for each of your state</param>
	public InitState(string idString)
	{
		m_identifier = idString;
		s_InitList.Remove(m_identifier);
		s_InitList.Add(m_identifier, this);

		//Debug.Log("InitState constructor " + m_identifier);

		// find any other state where I'm a dependency
		IDictionaryEnumerator iterator = s_InitList.GetEnumerator();
		while (iterator.MoveNext())
		{
			InitState initItem = (InitState)iterator.Value;
			foreach (string dep in initItem.m_dependencies)
			{
				if (dep == idString)
				{
					//Debug.Log("Found children " + dep);
					children.Add(initItem.m_identifier);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// the the delegate fucntion used to warn process to start or when other processes are on error
	/// </summary>
	/// <param name="startedDelegate">the functions</param>
	public void setCallback(InitDelegate startedDelegate)
	{
		setCallback(startedDelegate, null);
	}

	/// <summary>
	/// the the delegate fucntion used to warn process to start or when other processes are on error
	/// </summary>
	/// <param name="startedDelegate">the functions</param>
	/// <param name="param">the functions parameters</param>
	public void setCallback(InitDelegate startedDelegate, object param)
	{
		m_InitDelegate = startedDelegate;
		m_InitDelegateParam = param;
		checkStatus(this);
	}

	/// <summary>
	/// Adds a dependency
	/// </summary>
	/// <param name="dependency">the dependency name</param>
	public void AddDependency(string dependency)
	{
		m_dependencies.Add(dependency);

		InitState dep = (InitState)s_InitList[dependency];
		if (dep != null)
		{
			dep.children.Add(m_identifier);
		}
	}

	/// <summary>
	/// Adds a dependency
	/// </summary>
	/// <param name="idString">The main identifier. you should use differents name for each of your state</param>
	/// <param name="startedDelegate">the functions</param>
	/// <param name="dependencies">the dependencies names</param>
	public static InitState AddInitState(string idString, InitDelegate startedDelegate, params string[] dependencies)
	{
		return AddInitState(idString, startedDelegate, null, dependencies);
	}

	/// <summary>
	/// Adds a dependency
	/// </summary>
	/// <param name="idString">The main identifier. you should use differents name for each of your state</param>
	/// <param name="startedDelegate">the functions</param>
	/// <param name="param">the functions parameters</param>
	/// <param name="dependencies">the dependencies names</param>
	public static InitState AddInitState(string idString, InitDelegate startedDelegate, object param, params string[] dependencies)
	{
		InitState state = new InitState(idString);
		for (int i = 0; i < dependencies.Length; i++)
		{
			state.AddDependency(dependencies[i]);
		}
		state.setCallback(startedDelegate, param);
		InitManager.computeProgress();
		return state;
	}

	/// <summary>
	/// Adds State with a ready status
	/// </summary>
	/// <param name="idString">The main identifier. you should use differents name for each of your state</param>
	public static InitState AddReadyState(string idString)
	{
		
		InitState state = new InitState(idString);
		state.End(true);
		InitManager.computeProgress();
		return state;
	}
	
	/// <summary>
	/// Passe en mode Démarré : à lancer si on n'as aps de dépendance
	/// </summary>
 
	public void Start()
	{
		InitManager.computeProgress();
		m_progressState = ProgressState.Started;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// Appelez cette fonction si vous avez besoin de réinitialiser l'état de votre tache
	/// </summary>
	public void ReInit()
	{
		InitManager.computeProgress();
		m_progressState = ProgressState.WaitinForDependencies;
		checkStatus(this);
	}

	void DependencyError()
	{
		m_progressState = ProgressState.DependencyError;
	}

	/// <summary>
	/// Appelez cette fonction quand la tache de fond est terminée (en erreur ou non)
	/// </summary>
	/// <param name="success"></param>
	public void End(bool success)
	{
	//	Debug.Log("State " + m_identifier + " end " + success);
	//	InitManager.DebugLogAll();



		if (success)
		{
			Config.SetParamString("InitManager.LastSuccess", this.m_identifier);
			InitManager.computeProgress();

			m_progressState = ProgressState.Ready;
		}
		else
		{
			Config.SetParamString("InitManager.LastError", this.m_identifier);
			m_progressState = ProgressState.Error;
		}

		propagation(this);
	}

	static void checkStatus(InitState curState)
	{
		//Debug.Log("checkStatus " + curState.m_identifier);
		bool ready = true;
		// check for each other dependencies.
		foreach (string dep in curState.m_dependencies)
		{
			InitState depItem = ((InitState)(InitState.s_InitList[dep]));
			if ((depItem==null) || !depItem.isReady())
			{
				ready = false;
				break;
			}
		}

		if (ready)
		{
			// Start the current State
			curState.Start();
			if (curState.m_InitDelegate != null)
			{
				curState.m_InitDelegate(curState, curState.m_InitDelegateParam);
			}
		}
	}

	static void propagation(InitState curState)
	{
		
		foreach (string childName in curState.children)
		{
			InitState child = ((InitState)(InitState.s_InitList[childName]));
			if (child == null) continue;

			if (curState.isReady())
			{
				if (child.getStatus() == ProgressState.DependencyError ||
					child.getStatus() == ProgressState.WaitinForDependencies)
				{
					checkStatus(child);
				}
			}
			else if (curState.m_progressState ==  ProgressState.Error ||
				curState.m_progressState ==  ProgressState.DependencyError)
			{
			//	Debug.Log("error on " + child.m_identifier);
				child.m_progressState = ProgressState.DependencyError;
				propagation(child);
				if (child.m_InitDelegate != null)
				{
					child.m_InitDelegate(child, child.m_InitDelegateParam);
				}
			}
			else if (curState.m_progressState == ProgressState.Started || curState.m_progressState == ProgressState.WaitinForDependencies)
			{
				child.m_progressState = ProgressState.WaitinForDependencies;
				propagation(child);
				if (child.m_InitDelegate != null)
				{
					child.m_InitDelegate(child, child.m_InitDelegateParam);
				}
			}
		}
	}


	/// <summary>
	/// Fonction statique d'accès à un State
	/// </summary>
	/// <param name="initStateName">Le Nom de l'état</param>
	/// <returns></returns>
	static public InitState getState(string initStateName)
	{
		IDictionaryEnumerator iterator = InitState.s_InitList.GetEnumerator();

		while (iterator.MoveNext())
		{
			InitState initItem = (InitState)iterator.Value;

			if (initItem.m_identifier == initStateName)
			{
				return initItem;
			}
		}

		return null;
	}
	/// <summary>
	/// static call to isReady
	/// </summary>
	/// <param name="initStateName">state's name</param>
	/// <returns></returns>
	static public bool isReady(string initStateName)
	{
		InitState initItem = getState(initStateName);
		if (initItem != null)
			return initItem.isReady();

		return false;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// conversion to string used by Init manager console
	/// </summary>
	/// <returns></returns>
	override public string ToString()
	{
		if (m_progressState == ProgressState.Ready)
		{
			return "True";
		}
		else
		{
			return "False - m_progressState : " + m_progressState.ToString() ;
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	///  used to compare differents states
	/// </summary>
	public class Comparer : IComparer<InitState>
	{
		/// <summary>
		///  used to compare differents states
		/// </summary>
		public int Compare(InitState x, InitState y)
		{
			return x.m_progressState - y.m_progressState;
		}
	}

}


////////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>
/// The Init Manager isn't used anymore to check states
/// 
/// It is then useless for main purpose.
/// It just use the DebugPanelMgr to show a complete graph of states in the scene
/// </summary>
public class InitManager : MonoBehaviour
{
	DebugPanel m_DbgPanel = null;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    void Awake()
    {
        
        DontDestroyOnLoad(this);

        m_DbgPanel = new DebugPanel("Init Manager", false, false, false);
		StartCoroutine(SetDebugInfo());
    }

	static float globalProgress = 0;

	/// <summary>
	/// Access to global init progress
	/// </summary>
	static public float getProgress()
	{
		return globalProgress;
	}

	/// <summary>
	/// Called when any init state is created or change of status
	/// </summary>
	static public void computeProgress()
	{
		int totalNumber = 0;
		int NbReady = 0;
		IDictionaryEnumerator iterator = InitState.InitList.GetEnumerator();
		while (iterator.MoveNext())
		{
			InitState initItem = (InitState)iterator.Value;
			totalNumber++;
			if (initItem.isReady())
			{
				NbReady++;
			}
		}

		globalProgress = (float)NbReady / totalNumber;


	}
	

	public static void DebugLogAll()
	{
		Debug.LogWarning(" ------------------ Trace Init State --------------");


		IDictionaryEnumerator iterator = InitState.InitList.GetEnumerator();
		List<InitState> states = new List<InitState>();
		while (iterator.MoveNext())
		{
			InitState initItem = (InitState)iterator.Value;
			if (initItem != null)
			{
				states.Add(initItem);
			}
		}

		states.Sort(new InitState.Comparer());

		for (int i = 0; i < states.Count; i++)
		{
			InitState initItem = states[i];
			
			Debug.Log(initItem.m_identifier + " - " + initItem.ToString());
			
			
			int j = 0;
			while (j < initItem.m_dependencies.Count)
			{
				InitState itemDep = ((InitState)(InitState.InitList[initItem.m_dependencies[j]]));
				if (itemDep == null)
				{
					Debug.LogError("\t\t Error wrong dependency + " + initItem.m_dependencies[j]);
				}
				else if (itemDep.getStatus() == InitState.ProgressState.Ready)
				{
					Debug.Log("\t\t" + itemDep.m_identifier + " OK");
				}
				else
				{
					Debug.LogWarning("\t\t" + itemDep.m_identifier + " Not OK");
				}

				j++;
			}
		}	
	}


    ///////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	IEnumerator SetDebugInfo()
	{
		while (true)
		{
			if (m_DbgPanel.isVisible())
			{
				m_DbgPanel.ClearMessages();

				IDictionaryEnumerator iterator = InitState.InitList.GetEnumerator();
				List<InitState> states = new List<InitState>();
				while (iterator.MoveNext())
				{
					InitState initItem = (InitState)iterator.Value;
					if (initItem != null)
					{
						states.Add(initItem);
					}
				}

				states.Sort(new InitState.Comparer());


				for (int i = 0; i < states.Count; i++)
				{
					DebugPanel.TextColor color = DebugPanel.TextColor.white;
					InitState initItem = states[i];
					switch (initItem.getStatus())
					{

						case InitState.ProgressState.Started:
							color = DebugPanel.TextColor.yellow;
							break;
						case InitState.ProgressState.Ready:
							color = DebugPanel.TextColor.green;
							break;
						case InitState.ProgressState.WaitinForDependencies:
							color = DebugPanel.TextColor.blue;
							break;
						case InitState.ProgressState.DependencyError:
						case InitState.ProgressState.Error:
							color = DebugPanel.TextColor.red;
							break;
					}

					m_DbgPanel.AddMessage(initItem.m_identifier + " - " + initItem.ToString(), color);
					
					int j = 0;
					while (j < initItem.m_dependencies.Count)
					{
						InitState itemDep = ((InitState)(InitState.InitList[initItem.m_dependencies[j]]));
						if (itemDep == null)
						{
							m_DbgPanel.AddMessage("\t\t Error wrong dependency + " + initItem.m_dependencies[j], DebugPanel.TextColor.red);
						}
						else if (itemDep.getStatus() == InitState.ProgressState.Ready)
						{
							m_DbgPanel.AddMessage("\t\t" + itemDep.m_identifier + " OK", DebugPanel.TextColor.green);
						}
						else
						{
							m_DbgPanel.AddMessage("\t\t" + itemDep.m_identifier + " Not OK", DebugPanel.TextColor.yellow);
						}

						j++;
					}
				}	
			}

			yield return new WaitForSeconds(1);
		}


	}


	
}