using UnityEngine;
using System.Collections;

public class Accelerometer2Axis : MonoBehaviour
{
	public string XAxisName = "Rotate X";
	public string YAxisName = "Rotate Y";
	public string ZAxisName = "Rotate Z";

	Vector3 center;
	public void Start()
	{


	}

	public void Calibrate()
	{
		center = Input.acceleration;
		curValue = Vector3.zero;
	}

	void OnEnable()
	{
		Calibrate();

	}

	void OnDisable()
	{
		if (xAxis != null) xAxis.setValue = 0;
		if (yAxis != null) yAxis.setValue = 0;
		if (zAxis != null) zAxis.setValue = 0;
	}

	CommandsManager.CommandAxis xAxis = null;
	CommandsManager.CommandAxis yAxis = null;
	CommandsManager.CommandAxis zAxis = null;

	bool ready = false;
	void init()
	{
		if (ready) return;
		ready = true;
		if (xAxis == null && XAxisName != "")
		{
			xAxis = CommandsManager.getAxis(XAxisName);
			if (xAxis == null) ready = false;
		}

		if (yAxis == null && YAxisName != "")
		{
			yAxis = CommandsManager.getAxis(YAxisName);
			if (yAxis == null) ready = false;
		}


		if (zAxis == null && ZAxisName != "")
		{
			zAxis = CommandsManager.getAxis(ZAxisName);
			if (zAxis == null) ready = false;
		}

	
	}

	public Vector3 sensibility = new Vector3(5,1,-5);
	public float smooth = 2;
	Vector3 curValue = Vector3.zero;

	public void Update()
	{
		init();
		Vector3 acc = Input.acceleration;

		Vector3 value = new Vector3(
		 (acc.x - center.x + 1) % 2 - 1,
		 (acc.y - center.y + 1) % 2 - 1,
		 (acc.z - center.z + 1) % 2 - 1);

		curValue = Vector3.Lerp(curValue, value, smooth * Time.deltaTime);
		curValue = Vector3.ClampMagnitude(curValue, 1);

		if (xAxis != null) xAxis.setValue = curValue.x * sensibility.x;
		if (yAxis != null) yAxis.setValue = curValue.y * sensibility.y;
		if (zAxis != null) zAxis.setValue = curValue.z * sensibility.z;
	}

	public bool debugGUI = false;
/*	RectPlacement rcpos = new RectPlacement(FramePositon.Anchor.BottomCenter, new Rect(0, 0, 0, 300));

	public void OnGUI()
	{
		if (debugGUI)
		{
			GUI.skin = GUIAddOn.Skin;

			Rect rc = rcpos.mainRc();
			GUILayout.BeginArea(rc);
			if (GUILayout.Button( "Calibrate", GUILayout.Height(40), GUILayout.Width(200)))
			{
				Calibrate();
			}

			if (xAxis != null)
			{
				GUILayout.Label("sensibility X " + sensibility.x);
				sensibility.x = GUILayout.HorizontalSlider(sensibility.x, -10, 10);
			}

			if (yAxis != null)
			{
				GUILayout.Label("sensibility Y " + sensibility.y);
				sensibility.y = GUILayout.HorizontalSlider(sensibility.y, -10, 10);
			}

			if (zAxis != null)
			{
				GUILayout.Label("sensibility Z " + sensibility.z);
				sensibility.z = GUILayout.HorizontalSlider(sensibility.z, -10, 10);
			}

			GUILayout.Label("smooth " + smooth);
			smooth = GUILayout.HorizontalSlider(smooth, 0, 30);

			GUILayout.Label("curValue " + curValue);

			GUILayout.EndArea();
		}
	}*/


}
