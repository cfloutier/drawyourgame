using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;

public class ExtEditorGUILayout
{
	static public void ArrayToolBar(SerializedProperty parent, int index, SerializedProperty item, bool canBeEmpty)
	{
		//GUILayout.Label(name, styleBold);
		GUI.enabled = index > 0;
		if (GUILayout.Button("Up", GUILayout.MaxWidth(50)))
		{
			parent.MoveArrayElement(index, index - 1);

			parent.GetArrayElementAtIndex(index - 1).isExpanded = true;
			parent.GetArrayElementAtIndex(index).isExpanded = false;
		}

		if (canBeEmpty)
			GUI.enabled = true;
		else
			GUI.enabled = parent.arraySize > 1;

		if (GUILayout.Button("Delete", GUILayout.MaxWidth(50)))
		{
			parent.DeleteArrayElementAtIndex(index);

			if (index < parent.arraySize - 1)
			{
				parent.GetArrayElementAtIndex(index).isExpanded = true;
			}
			else if (index > 0)
			{
				parent.GetArrayElementAtIndex(index - 1).isExpanded = true;
			}
		}
		GUI.enabled = true;
		if (GUILayout.Button("Clone", GUILayout.MaxWidth(50)))
		{
			parent.InsertArrayElementAtIndex(index);
			item.isExpanded = false;
			SerializedProperty newItem = parent.GetArrayElementAtIndex(index + 1);
			SerializedProperty name = newItem.FindPropertyRelative("Name");
			if (name != null)
				name.stringValue += "(Cloned)";


			newItem.isExpanded = true;
		}

		GUI.enabled = index < parent.arraySize - 1; ;
		if (GUILayout.Button("Down", GUILayout.MaxWidth(50)))
		{
			parent.MoveArrayElement(index, index + 1);

			parent.GetArrayElementAtIndex(index + 1).isExpanded = true;
			parent.GetArrayElementAtIndex(index).isExpanded = false;
		}

		GUI.enabled = true;
	}

	static public void PropertyField(SerializedProperty parent, string name)
	{
		EditorGUILayout.PropertyField(parent.FindPropertyRelative(name), true);
	}

	static public void PropertyField(SerializedObject parent, string name)
	{
		EditorGUILayout.PropertyField(parent.FindProperty(name), true);
	}

	public delegate void ItemGUI(SerializedProperty parent, int index, SerializedProperty item);
	public delegate void addNewItem();

	static public void ArrayItemContent(SerializedProperty item)
	{
		EditorGUI.indentLevel++;

		IEnumerator enume = item.GetEnumerator();
		while (enume.MoveNext())
		{
			SerializedProperty Current = (SerializedProperty)enume.Current;
			EditorGUILayout.PropertyField(Current, true, GUILayout.ExpandWidth(true));
		}

		EditorGUI.indentLevel--;
	}

	static public void mainClassContent(SerializedObject ob)
	{
		mainClassContent(ob, 0, 1);
	}


	static public void mainClassContent(SerializedObject obj, int skipItems, int NbPerLine )
	{
		SerializedProperty prop = obj.GetIterator();
		prop.NextVisible(true);
		for (int i = 0 ; i < skipItems ; i++)
			prop.NextVisible(false);

		int indexCol = 0;

		GUILayout.BeginVertical();

		if (NbPerLine != 1)
			GUILayout.BeginHorizontal();

		while (prop.NextVisible(false))
		{
			EditorGUILayout.PropertyField(prop, true, GUILayout.ExpandWidth(true));
			indexCol++;
			if (indexCol >= NbPerLine)
			{
				indexCol = 0;
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal();
			}
		}

		if (NbPerLine != 1)
			GUILayout.EndHorizontal();
		GUILayout.EndVertical();
	}


	static public void ArrayGUI(SerializedProperty array, string ItemName)
	{
		ArrayGUI(array, null, null, ItemName);
	}

	static public void ArrayGUI(
		SerializedProperty array,
		ItemGUI ItemDelegate,
		addNewItem addDelegate,
		string ItemName
		)
	{
		ArrayGUI(array, ItemDelegate, addDelegate, ItemName, false);
	}



	static public void ArrayGUI(
		SerializedProperty array,
		ItemGUI ItemDelegate,
		addNewItem addDelegate,
		string ItemName,
		bool noClass // means override standard property field, useful for no standard type items
		
		)
	{
		if (array.arraySize == 0)
		{
			
			GUI.enabled = true;
			
			GUILayout.Label(ItemName + " : Empty");
			EditorGUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("New " + ItemName, GUILayout.Height(25)))
			{
				if (addDelegate != null)
					addDelegate();
				else
				{
					array.InsertArrayElementAtIndex(0);
				}
			}
			GUILayout.Space(EditorGUI.indentLevel * 20);
			EditorGUILayout.EndHorizontal();
	
		
		}
		else
		{
			GUILayout.Label(ItemName + " : " + array.arraySize);

			if (EditorGUILayout.PropertyField(array))
			{   
				EditorGUI.indentLevel++;

				for (int i = 0; i < array.arraySize; i++)
				{
					SerializedProperty item = array.GetArrayElementAtIndex(i);

					if (noClass || EditorGUILayout.PropertyField(item))
					{
						if (ItemDelegate != null)
						{
							ItemDelegate(array, i, item);
						}
						else
						{

							GUILayout.BeginHorizontal();
							GUILayout.BeginVertical();

							ArrayItemContent(item);
							
							GUILayout.EndVertical();

						//	GUILayout.FlexibleSpace();

							GUILayout.BeginVertical();
							ArrayToolBar(array, i, item, true);
							GUILayout.EndVertical();


							GUILayout.EndHorizontal();
							//Debug.Log("CountInProperty " + item.CountInProperty());
							//Debug.Log("arraySize " + item.arraySize);
						}
					}
				}
				EditorGUI.indentLevel--;


				EditorGUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				GUI.enabled = true;

				if (GUILayout.Button("New " + ItemName, GUILayout.Height(25)))
				{
					if (addDelegate != null)
						addDelegate();
					else
					{
						array.InsertArrayElementAtIndex(array.arraySize);
					}
				}

				GUILayout.Space(EditorGUI.indentLevel*20);

				EditorGUILayout.EndHorizontal();
			}
		}
	}



}