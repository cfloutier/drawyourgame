using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;


class KeyCodeConverter
{
	static string[] stringsCodes = null;
	static KeyCode[] codes = null;

	public static KeyCode fromString(string str)
	{
		if (stringsCodes == null)
		{
			stringsCodes = Enum.GetNames(typeof(KeyCode));
			for (int i = 0; i < stringsCodes.Length; i++)
			{
				stringsCodes[i] = stringsCodes[i].ToLower();

			}

			codes = (KeyCode[]) Enum.GetValues(typeof(KeyCode));
		}
		str = str.ToLower().Trim();
		int index = Array.FindIndex<string>(stringsCodes, s => s.Equals(str));
		if (index != -1)
		{
			return codes[index];
		}


		return KeyCode.None;
	}

	public static string toString(KeyCode code )
	{
		return Enum.GetName(typeof(KeyCode), code);
	}

	public static string toString(int code)
	{
		return Enum.GetName(typeof(KeyCode), (KeyCode) code);
	}
}

[CustomEditor(typeof(CommandsManager))]
public class CommandsManagerEditor : Editor
{
	string waitingKeyCode = "";
	string manualCode = "";
	bool setManual = false;

	GUIStyle keyCodeStyle = null;

	void KeyRecorder(string name, string globalCode, SerializedProperty key)
	{
		GUI.enabled = true;

		if (keyCodeStyle == null)
		{
			keyCodeStyle = new GUIStyle();

			keyCodeStyle.normal.textColor = GUI.skin.label.normal.textColor;
			keyCodeStyle.fontSize = 14;
		}

		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();

		KeyCode code = (KeyCode)key.intValue;
		GUILayout.Label(name + " : "+ KeyCodeConverter.toString(code), keyCodeStyle);
		GUILayout.FlexibleSpace();

		GUILayout.EndHorizontal();

		if (globalCode == waitingKeyCode)
		{
			
			if (setManual)
			{
				GUILayout.BeginHorizontal();	
				GUILayout.Label("code : ");

				manualCode = GUILayout.TextField(manualCode);

				KeyCode parsedCode = KeyCodeConverter.fromString(manualCode);

				GUILayout.Label("Parsed code : " + parsedCode, keyCodeStyle);
				GUILayout.EndHorizontal();

				GUI.enabled =  parsedCode != KeyCode.None;
				
				GUILayout.BeginHorizontal();

				GUILayout.FlexibleSpace();
				if (GUILayout.Button("Apply", GUILayout.Width(70), GUILayout.Height(25)))
				{
					waitingKeyCode = "";

					key.intValue = (int)parsedCode;
				}
				GUI.enabled = true;
				if (GUILayout.Button("Cancel", GUILayout.Width(70), GUILayout.Height(25)))
				{
					waitingKeyCode = "";
				}
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			else
			{

				GUILayout.BeginHorizontal();

				GUILayout.FlexibleSpace();


				// record mode

				GUILayout.Label("Press any Key to record");
				Event dirEvent = Event.current;
				if (dirEvent.isKey)
				{
					if (dirEvent.keyCode != KeyCode.None)
					{
						key.intValue = (int)dirEvent.keyCode;
						waitingKeyCode = "";
					}
				}

				if (GUILayout.Button("Cancel", GUILayout.Width(70), GUILayout.Height(15)))
				{
					waitingKeyCode = "";
				}

				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();

			}	
		}
		else
		{
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Record", GUILayout.Width(70), GUILayout.Height(25)))
			{
				GUI.FocusControl("");
				setManual = false;
				waitingKeyCode = globalCode;
			}


			if (GUILayout.Button("Set Code", GUILayout.Width(70), GUILayout.Height(25)))
			{
				waitingKeyCode = globalCode;
				setManual = true;
				manualCode = KeyCodeConverter.toString(code);
			}

			GUI.enabled =  code != KeyCode.None;
			if (GUILayout.Button("Unset", GUILayout.Width(70), GUILayout.Height(25)))
			{
				waitingKeyCode = "";
				key.intValue = (int)KeyCode.None;
			}


			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();

		}

		GUILayout.EndVertical();
		
		GUI.enabled = true;
	}

	void CommandKeyGUI(SerializedProperty parent, int index, SerializedProperty item)
	{

		EditorGUI.indentLevel++;

		GUILayout.BeginHorizontal();

		
		KeyRecorder("Key " + index, "CommandButton:" + cmdIndex + ":" + index, item);
		if (GUILayout.Button("Delete Key " + index, GUILayout.Width(100), GUILayout.Height(50)))
		{
			parent.DeleteArrayElementAtIndex(index);
			return;
		}
		
		GUILayout.EndHorizontal();
		EditorGUI.indentLevel--;
	}

	void AxisMappingGUI(SerializedProperty parent, int index, SerializedProperty item)
	{
		EditorGUI.indentLevel++;

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();
		GUILayout.Label("Standard Input");

		EditorGUILayout.PropertyField(item.FindPropertyRelative("axisName"), false);
		EditorGUILayout.PropertyField(item.FindPropertyRelative("positive"), false);
		EditorGUILayout.PropertyField(item.FindPropertyRelative("threshold"), false);

		GUILayout.EndVertical();

		if (GUILayout.Button("Delete Axis", GUILayout.Width(100), GUILayout.Height(40)))
		{
			parent.DeleteArrayElementAtIndex(index);
			return;
		}
		GUILayout.EndHorizontal();
		EditorGUI.indentLevel--;
	}

	int cmdIndex;
	void CommandButtonGUI(SerializedProperty parent, int index, SerializedProperty item)
	{
		cmdIndex = index;
		EditorGUI.indentLevel++;

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		SerializedProperty name = item.FindPropertyRelative("Name");

		EditorGUILayout.PropertyField(name, false);
		if (GUILayout.Button("Delete", GUILayout.Width(150), GUILayout.Height(25)))
		{
			parent.DeleteArrayElementAtIndex(index);
			return;
		}
		GUILayout.EndHorizontal();
		SerializedProperty keys = item.FindPropertyRelative("keys");

		ExtEditorGUILayout.ArrayGUI(keys, CommandKeyGUI, null, "Commands Key", true);

		SerializedProperty axisMapping = item.FindPropertyRelative("axisMapping");

		ExtEditorGUILayout.ArrayGUI(axisMapping, AxisMappingGUI, null, "Axis Mapping", true);


	
		/*key = item.FindPropertyRelative("altKey");
		KeyRecorder("Alt Key", "CommandButtonAlt" + index, key);*/
	
		EditorGUI.indentLevel--;

		GUILayout.EndVertical();

		GUILayout.BeginVertical();
		
		//ExtGUILayout.ArrayToolBar(parent, index, item, true);
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
	}

	void StdInputAxisGUI(SerializedProperty parent, int index, SerializedProperty item)
	{
		EditorGUI.indentLevel++;
		GUILayout.BeginHorizontal();

		GUILayout.Label("Standard Input Axis Name (" + index + ") : ");
		item.stringValue = EditorGUILayout.TextField(item.stringValue);
		
	//	EditorGUILayout.TextField(item);
		if (GUILayout.Button("Delete", GUILayout.Width(150), GUILayout.Height(25)))
		{
			parent.DeleteArrayElementAtIndex(index);
			return;
		}
		GUILayout.EndHorizontal();
		EditorGUI.indentLevel--;
	}

	int indexAxis;

	void AxisKeysGUI(SerializedProperty parent, int index, SerializedProperty item)
	{
		EditorGUI.indentLevel++;
		GUILayout.BeginHorizontal();


		GUILayout.BeginHorizontal();
		SerializedProperty key = item.FindPropertyRelative("positiveKey");
		KeyRecorder("Positive Key", "Axis Positive Key " + index + ":" + indexAxis, key);

		key = item.FindPropertyRelative("negativeKey");
		KeyRecorder("Negative Key", "Axis Negative Key " + index+ ":"+ indexAxis, key);
		GUILayout.EndHorizontal();


		
	//	EditorGUILayout.TextField(item);
		if (GUILayout.Button("Delete", GUILayout.Width(150), GUILayout.Height(25)))
		{
			parent.DeleteArrayElementAtIndex(index);
			return;
		}
		GUILayout.EndHorizontal();
		EditorGUI.indentLevel--;
	}
	

	void AxisGUI(SerializedProperty parent, int index, SerializedProperty item)
	{
		indexAxis = index;
		EditorGUI.indentLevel++;

		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

		GUILayout.BeginHorizontal();
		SerializedProperty name = item.FindPropertyRelative("Name");

		EditorGUILayout.PropertyField(name, false);
		if (GUILayout.Button("Delete", GUILayout.Width(150), GUILayout.Height(25)))
		{
			parent.DeleteArrayElementAtIndex(index);
			return;
		}
		GUILayout.EndHorizontal();

		
		//ExtEditorGUILayout.PropertyField(item, "StandardInputAxis");

		SerializedProperty StandardInputAxis = item.FindPropertyRelative("StandardInputAxis");
		//GUILayout.Label("Use standard Input values, joystick axis for instance");
		ExtEditorGUILayout.ArrayGUI(StandardInputAxis, StdInputAxisGUI, null, "Standard Input Axis", true);

		GUILayout.Space(10);
		SerializedProperty keys = item.FindPropertyRelative("keys");
		ExtEditorGUILayout.ArrayGUI(keys, AxisKeysGUI, null, "Axis Keys", true);

		GUILayout.Space(10);

	
		GUILayout.BeginHorizontal();
		ExtEditorGUILayout.PropertyField(item, "gravity");
		ExtEditorGUILayout.PropertyField(item, "sensibility");
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		ExtEditorGUILayout.PropertyField(item, "snap");
		ExtEditorGUILayout.PropertyField(item, "deadZone");
		GUILayout.EndHorizontal();
		




		EditorGUI.indentLevel--;

		GUILayout.EndVertical();

		GUILayout.BeginVertical();

		//ExtGUILayout.ArrayToolBar(parent, index, item, true);
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
	}

	void OnEnable()
	{
		// Setup the SerializedProperties
		m_commandButtons = serializedObject.FindProperty("m_commandButtons");
		m_commandAxis = serializedObject.FindProperty("m_commandAxis");
		targetCM = (CommandsManager) target;
	}



	SerializedProperty m_commandButtons;
	SerializedProperty m_commandAxis;
	CommandsManager targetCM;


	override public void OnInspectorGUI()
	{
		serializedObject.Update();

		GUILayout.Space(10);

		targetCM.addCommandPanel = GUILayout.Toggle(targetCM.addCommandPanel, "add Command Panel");

		ExtEditorGUILayout.ArrayGUI(m_commandButtons, CommandButtonGUI, null, "Commands Buttons");
		GUILayout.Space(10);
		ExtEditorGUILayout.ArrayGUI(m_commandAxis, AxisGUI, null, "Axis");
		GUILayout.Space(10);

		serializedObject.ApplyModifiedProperties();
	}
}
