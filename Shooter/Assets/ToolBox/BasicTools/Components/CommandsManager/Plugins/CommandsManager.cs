using UnityEngine;
using System.Collections;


/// <summary>
/// This class is used as a substitute to the standard Input Manager from Unity.
/// The main 2 lacks where
/// - no way to set inputs values (with on screen joystick for instance)
/// - no way to change the input buttons live, for instance to change between differents types of keyboards : qwerty or azerty....)
/// 
/// It mainly use the main input manager to compute axis but input buttons can be overloaded here
/// </summary>
[ExecuteInEditMode] 
public class CommandsManager : MonoBehaviour 
{
	static CommandsManager instance = null;
	static public CommandsManager Get() 
	{
		if( instance == null )
		{
			GameObject obj = GameObject.Find ("CommandsManager");
			
			if (obj == null)
				return null;
			
			instance = obj.GetComponent<CommandsManager>();
			
		}
		return instance; 
	}

	// called if registered on a Command Button
	public delegate void onCommandButton(string commandName, bool pressed);
	
	[System.Serializable]
	public class CommandButton
	{
		public string Name;

		public KeyCode[] keys;

		[System.Serializable]
		public class AxisMapping
		{
			public string axisName;
			public bool positive;
			public float threshold = 0.5f;
		}

		public AxisMapping[] axisMapping;
		

		[HideInInspector]
		public bool pressed = false;
		[HideInInspector]
		public bool keepPressed = false;

		[HideInInspector]
		bool lastValue = false;

		public event onCommandButton onButton;

		void Raise()
		{
			if (onButton != null)
				onButton(Name, pressed);
		}

		public void compute()
		{
			if (onButton == null) return;
			// no listener, no checks

			if (lastValue)
			{
				bool keyPressed = false;
				for (int i = 0; i < keys.Length; i++)
				{
					keyPressed = keyPressed || Input.GetKey(keys[i]);
				}

				bool axispressed = false;
				for (int i = 0; i < axisMapping.Length; i++)
				{
					AxisMapping mapping = axisMapping[i];
					if (mapping.positive)
						axispressed = axispressed || Input.GetAxis(mapping.axisName) > mapping.threshold;
					else
						axispressed = axispressed || Input.GetAxis(mapping.axisName) < -mapping.threshold;
				}


				// was pressed
				if (!axispressed && !keyPressed && !keepPressed)
				{
					
					// released
					pressed = false;
					Raise();
				}

				
			}
			else
			{

				for (int i = 0; i < keys.Length && !pressed; i++)
				{
					if (Input.GetKeyDown(keys[i]))
						pressed = true;
				}

				for (int i = 0; i < axisMapping.Length && !pressed; i++)
				{
					AxisMapping mapping = axisMapping[i];

					if (mapping.positive)
						pressed = pressed || Input.GetAxis(mapping.axisName) > mapping.threshold;
					else
					{
						pressed = pressed || Input.GetAxis(mapping.axisName) < -mapping.threshold;
					}
				}

				// was not pressed
				if (pressed)
				{
					CommandsManager.lastActionTime = Time.time;
					pressed = true;
					Raise();
				}
			}

			lastValue = pressed;
		}
	}

	[System.Serializable]
	public class CommandAxis
	{
		// name should be the exactly same as in the input manager
		// if this axis is not found it will throw errors in the update fct
		public string Name;

		public string[] StandardInputAxis;


		[System.Serializable]
		public class AxisKeys
		{
			public KeyCode positiveKey;
			public KeyCode negativeKey;
		}

		public AxisKeys[] keys;

		public float gravity = 3;
		public float sensibility = 3;
		/// <summary>
		/// if value is inverted (positive to negative or negative to positive, first jump to zero before smoothing it)
		/// </summary>
		public bool snap = true;

		public float deadZone = 0.05f;

		[HideInInspector]
		public float smoothValue;

		[HideInInspector]
		public float setValue;

		[HideInInspector]
		public float value;

		void DebugLog(string txt)
		{
			if (Name == "Vertical") Debug.Log(txt);
		}

		public void compute()
		{

			
			
			for (int i = 0; i < StandardInputAxis.Length; i++)
			{
				float computedValue = Input.GetAxis(StandardInputAxis[i]);
				if (computedValue != 0)
				{
					value = computedValue;
					return;
				}
			}

			bool positivePressed = false;
			bool negativePressed = false;
			for (int i = 0; i < keys.Length; i++)
			{
				AxisKeys key = keys[i];
				positivePressed = positivePressed || Input.GetKey(key.positiveKey);
				negativePressed = negativePressed || Input.GetKey(key.negativeKey);
			}
			
			if (setValue != 0)
			{
				CommandsManager.lastActionTime = Time.time;
				value = setValue;
			}
			else if (smoothValue == 1 || positivePressed)
			{
				CommandsManager.lastActionTime = Time.time;
				// positive
				if (value < 0 && snap)
					value = 0;

				value = Mathf.Lerp(value, 1, sensibility * Time.deltaTime);
			}
			else if (smoothValue == -1 || negativePressed)
			{
				CommandsManager.lastActionTime = Time.time;

				// negative
				if (value > 0 && snap)
					value = 0;

				value = Mathf.Lerp(value, -1, sensibility * Time.deltaTime);
			}
			else
			{
				// back to center
				value = Mathf.Lerp(value, 0, gravity * Time.deltaTime);
				if (value < deadZone && value > -deadZone)
					value = 0;
			}

			// reset smoothValue
			smoothValue = 0;
		}
	}

	public CommandButton[] m_commandButtons;
	public CommandAxis[] m_commandAxis;

	Hashtable commandHashtable;
	Hashtable axisHashtable;

	DebugPanel m_DbgPanel = null;
	public bool addCommandPanel = false;
	void Awake()
	{
		instance = this;
		
		if (!Application.isPlaying ) 
			return;
		
		commandHashtable = new Hashtable();
		foreach (CommandButton cmd in m_commandButtons)
		{
			commandHashtable[cmd.Name] = cmd;
		}

		axisHashtable = new Hashtable();
		foreach (CommandAxis axis in m_commandAxis)
		{
			axisHashtable[axis.Name] = axis;
		}

		Config.addListener("CommandsManager.active", OnActive);

		if (addCommandPanel)
            m_DbgPanel = new DebugPanel("Commands Manager", false, false, false);

		StartCoroutine(SetDebugInfo());
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	IEnumerator SetDebugInfo()
	{
		if (m_DbgPanel == null)
			yield break;
		while (true)
		{
			if (m_DbgPanel.isVisible())
			{
				m_DbgPanel.ClearMessages();
				m_DbgPanel.AddMessage("Buttons : ");
				foreach (CommandButton bt in m_commandButtons)
				{
					m_DbgPanel.AddMessage("* " + bt.Name + " : " + bt.pressed);
				}

				m_DbgPanel.AddMessage("Axis : ");
				foreach (CommandAxis axis in m_commandAxis)
				{
					m_DbgPanel.AddMessage("* " + axis.Name + " : " + axis.value);
				}

			}

			yield return new WaitForFixedUpdate();
		}

	}

	void OnDestroy()
	{
		Config.removeListener("CommandsManager.active", OnActive);
	}

	void OnActive(string name)
	{

		this.enabled = Config.GetParamBool("name", true);
	}

	public static bool addCommandListener(string name, onCommandButton fct)
	{
		if (!checkInstance()) return false;
		if (!instance.commandHashtable.ContainsKey(name))
		{
			Debug.LogError("No Command " + name);
			return false;
		}

		CommandButton cmd = (CommandButton)instance.commandHashtable[name];
		cmd.onButton += fct;
		return true;
	}

	public static bool removeCommandListener(string name, onCommandButton fct)
	{
		if (!checkInstance()) return false;
		if (!instance.commandHashtable.ContainsKey(name))
		{
			Debug.LogError("Command Not found : " + name);
			return false;
		}

		CommandButton cmd = (CommandButton)instance.commandHashtable[name];
		cmd.onButton -= fct;
		return true;
	}


	public static void setCommand(string name, bool pressed)
	{
		if (!checkInstance()) return;
		if (!instance.commandHashtable.ContainsKey(name))
		{
			Debug.LogError("Command Not found : " + name);
			return;
		}

		CommandButton cmd = (CommandButton)instance.commandHashtable[name];
		cmd.pressed = pressed;

		cmd.keepPressed = pressed;
	}

	// like key down and then key Up
	public static void setCommandOnce(string name)
	{
		if (!checkInstance()) return;
		if (!instance.commandHashtable.ContainsKey(name))
		{
			Debug.LogError("Command Not found : " + name);
			return;
		}

		CommandButton cmd = (CommandButton)instance.commandHashtable[name];
		cmd.pressed = true;
		cmd.keepPressed = false;
	}

	static bool checkInstance()
	{
		if (instance == null)
		{
			Debug.LogError("Please add a CommandsManager to your scene");
			return false;
		}

		return true;
	}

	public static float getAxisValue(string name)
	{
		if (!checkInstance()) return 0;
		if (!instance.axisHashtable.ContainsKey(name))
		{
			Debug.LogError("Axis Not found : " + name);
			return 0;
		}

		CommandAxis axis = (CommandAxis)instance.axisHashtable[name];
		return axis.value;
	}

	public static CommandAxis getAxis(string name)
	{
		if (!checkInstance()) return null;
		if (!instance.axisHashtable.ContainsKey(name))
		{
			Debug.LogError("Axis Not found : " + name);
			return null;
		}

		return (CommandAxis)instance.axisHashtable[name];
	}

	public static bool setAxisValue(string name, float value)
	{
		if (!checkInstance()) return false;
		if (!instance.axisHashtable.ContainsKey(name))
		{
			Debug.LogError("Axis Not found : " + name);
			return false;
		}

		CommandAxis axis = (CommandAxis)instance.axisHashtable[name];
		axis.setValue = value;
		return true;
	}

	// used by on screen command button
	// same effect as key pressed
	// ----- Important : must be called each frame : repeat buttons -----
	public static bool setSmoothAxis(string name, bool positive)
	{
		if (!checkInstance()) return false;
		if (!instance.axisHashtable.ContainsKey(name))
		{
			Debug.LogError("Axis Not found : " + name);
			return false;
		}

		CommandAxis axis = (CommandAxis)instance.axisHashtable[name];
		if (positive)
			axis.smoothValue = 1;
		else
			axis.smoothValue = -1;

		return true;
	}

	static public float lastActionTime = -1;
	
	// Update is called once per frame
	void Update () 
	{
		if (!Application.isPlaying ) 
			return;
		
		foreach (CommandButton cmd in m_commandButtons)
		{
			cmd.compute();
		}

		foreach (CommandAxis axis in m_commandAxis)
		{
			axis.compute();
		}
	}
}
