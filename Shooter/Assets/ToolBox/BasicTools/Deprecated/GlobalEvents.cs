using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Obsolete("not maintained anymore", false)]
public class GlobalEvents : MonoBehaviour
{
	/// <summary>
	/// This delegate function is used for event listeners
	/// </summary>
	/// <param name="name">the name of the event that have changed</param>
	public delegate void onGlobalEvent(string name, int value);

	/// <summary>
	/// Internal global event definition
	/// </summary>
	class GlobalEvent
	{
		public int val;
		public string str; 
		public event onGlobalEvent evt;

		public GlobalEvent(int v)
		{
			val = v;
		}

		public GlobalEvent(string s)
		{
			str = s;
		}


		public void Call(string name)
		{
			if (evt != null) evt(name, val);
		}
	}

	/// <summary>
	/// Debug informations manager
	/// </summary>
	DebugPanel m_DbgPanel = null;

	/// <summary>
	/// Events list
	/// </summary>
	static Hashtable s_Events = new Hashtable();

	////////////////////////////////////////////////////////////////////////////////////////////////////

	void Start()
	{
        m_DbgPanel = new DebugPanel("GlobalEvents", false, false, false);
		StartCoroutine(SetDebugInfo());
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	IEnumerator SetDebugInfo()
	{
		while (true)
		{
			if (m_DbgPanel.isVisible())
			{
				m_DbgPanel.ClearMessages();
				m_DbgPanel.AddMessage("------- Application global events");

				// Sort fish alphabetically, in ascending order (A - Z)
				List<string> list = new List<string>();
				IDictionaryEnumerator iterator = s_Events.GetEnumerator();
				while (iterator.MoveNext())
				{
					GlobalEvent val = (GlobalEvent)iterator.Value;
					string key = (string)iterator.Key;
					list.Add(key + " = " + val.val);

				}
				list.Sort();
				foreach (string value in list)
				{
					m_DbgPanel.AddMessage(value);
				}
			}

			yield return new WaitForSeconds(1);
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Get the event using integer value
	/// If the event does not exists, it is added to the event list with default value
	/// </summary>
	public static int GetEvent(string name, int defaultValue)
	{
		if (s_Events.Contains(name))
			return ((GlobalEvent)s_Events[name]).val;

		SetEvent(name, defaultValue);
		return defaultValue;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Set the event using integer value
	/// </summary>
	public static void SetEvent(string name, int value)
	{
		if (s_Events.Contains(name))
		{
			((GlobalEvent)s_Events[name]).val = value;
			sendToListeners(name);
		}
		else
		{
			GlobalEvent e = new GlobalEvent(value);
			s_Events[name] = e;
		}
	}

	/// <summary>
	/// Set the event using integer value
	/// </summary>
	public static void SetEvent(string name, string str)
	{
		if (s_Events.Contains(name))
		{
			((GlobalEvent)s_Events[name]).str = str;
			sendToListeners(name);
		}
		else
		{
			GlobalEvent e = new GlobalEvent(str);
			s_Events[name] = e;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// add a listener for an event using function delegate
	/// a first call to the fct delegate is done if the event exists
	/// </summary>
	/// <param name="name">the event name</param>
	/// <param name="fct">the function to call when event has changed</param>
	public static void addListener(string name, onGlobalEvent fct)
	{
		if (s_Events.Contains(name))
		{
			((GlobalEvent)s_Events[name]).evt += fct;
			fct(name, ((GlobalEvent)s_Events[name]).val);
		}
		else
		{
			GlobalEvent e = new GlobalEvent(0);
			e.evt += fct;
			s_Events[name] = e;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Remove the event listener using
	/// </summary>
	/// <param name="name">the event name</param>
	/// <param name="fct">the listener function</param>
	public static void removeListener(string name, onGlobalEvent fct)
	{
		if (!s_Events.Contains(name)) return;

		((GlobalEvent)s_Events[name]).evt -= fct;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	/// <summary>
	/// Call the event listener
	/// </summary>
	/// <param name="name">the event name</param>
	static void sendToListeners(string name)
	{
		if (!s_Events.Contains(name)) return;

		((GlobalEvent)s_Events[name]).Call(name);
	}
}
