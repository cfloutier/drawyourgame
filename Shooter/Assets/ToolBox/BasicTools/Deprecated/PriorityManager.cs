using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// La classe PriorityManager permet de gerer de facon automatique les exclusions entre differents parties de codes. 
/// Le principe est de d�clarer des priorityState dans les differentes classes qui en auraient besoin (possible d'en d�clarer plusieurs par classes),
/// d'indiquer quel est son niveau de priorit� et quel doit �tre son comportement avec les autres PriorityStates qui seraient actif en meme temps.
/// 
/// Exemples d'initialisation:
///      PriorityState lockGUI;
///      lockGUI = new PriorityState("MainGui", 5, 
///				PriorityState.SameLevelAlreadyHere.WAIT , 
///				PriorityState.HigherLevelAlreadyHere.NOTHING, 
///				PriorityState.HigherLevelComming.KILL);
///      
/// Puis quand on a besoin de savoir si on peut avoir le m_focus ou pas :
/// 
///    void OnGUI()
///    {
///        if (GUI.Button(new Rect(m_butonPos, 0, 150, 50), m_name))
///        {
///            lockGUI.TryTakeFocus();
///        }
///
///        if(lockGUI.CanBeUse())
///        {
///            GUI.Box(new Rect(m_id * 20 + 100, m_id * 20 + 100, 500, 500), "Une boite");
///            if (GUI.Button(new Rect(m_id * 20 + 550, m_id * 20 + 100, 50, 50), "X"))
///            {
///                lockGUI.LeaveFocus();
///            }
///        }
///    }
/// 
/// La fonction TryTakeFocus() essai de faire en sorte que le PriorityState ai le focus si possible, 
/// l'utilisateur n'a pas de controle direct la dessus.
/// 
/// Il doit ensuite tester � l'aide de la fonction lockGUI.CanBeUse() 
/// si le manager l'a effectivement autoris� ou pas � s'executer.
/// 
/// Quand on sort du bloc de code necessitant une gestion des priorit� 
/// bien penser a faire un lockGUI.LeaveFocus(); pour permettre a d'autre PriorityState de prendre la main
/// 
/// Il y a egalement un mecanisme de lock permettant d'explicitement 
/// bloquer une PriorityState pour empecher son execution. 
/// Il faut simplement utiliser les fonction Lock( ) et UnLock( ) avec en parametre un identifiant 
/// pour connaitre celui qui a verrouiller le PriorityState
/// 
/// </summary>
[Obsolete("not maintained anymore", false)]
public class PriorityState
{

	[System.Serializable]
	public enum SameLevelAlreadyHere 
		// Si un Prioirity State avec la meme priorit� que le notre est d�j� lanc� alors : 
	{
		NOTHING,    // - on ne fais rien
		WAIT,       // - on attend que les priorityState avec le meme niveau de priorit� 
				    //   soient t�rmin�s avant de debloquer automatiquement le notre
		LAUNCH      // - l'execution commence aussitot peu importe les autre priorityState de meme priorit�
	}
	[System.Serializable]
	public enum HigherLevelAlreadyHere  
		// Si un Prioirity State avec une priorit� sup�rieures � le notre est d�j� lanc� alors :
	{
		NOTHING,    // - on ne fais rien 
		WAIT,       // - on attend que les priorityState avec le niveau de priorit� sup�rieures 
					//   soient t�rmin�s avant de debloquer automatiquement le notre
	}

	[System.Serializable]
	public enum HigherLevelComing // Si un Prioirity State avec une priorit� sup�rieures � le notre se lance alors que le notre est deja actif :
	{
		HIDE,       // - On cache le notre en laissant la place a celui qui a une plus forte priorit�
		STAY,       // - On le laisse tranquille comme si de rien n'etait
		KILL        // - On le kill, il ne reapparaittera pas quand le priorityState de 
					//   niv superieur sera termin�s
	}

	[System.Serializable]
	public enum Layer // Categorie sur lequel s'applique les priorit�. un priorityState peux appartenir a plusieurs layers
	{
		GENERAL,    // Layer global, pas de
		CONTROL,
		GUI,
		INTERACTION
	}

	public string m_name;
	public int m_priority;

	public SameLevelAlreadyHere m_sameLevelAlreadyHere;
	public HigherLevelAlreadyHere m_higherLevelAlreadyHere;
	public HigherLevelComing m_higherLevelComming;
	public bool m_canBlockOthers = true;

	public bool m_active;
	ArrayList m_lockerList = new ArrayList();
	public BitArray m_Layer = new BitArray(8, false);

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Various constructors
	public PriorityState(string name)
	{
		m_name = name;
		m_Layer[(byte)Layer.GENERAL] = true;
		
		m_priority = 5;
		
		m_sameLevelAlreadyHere = SameLevelAlreadyHere.NOTHING;
		m_higherLevelAlreadyHere = HigherLevelAlreadyHere.NOTHING;
		m_higherLevelComming = HigherLevelComing.HIDE;
		
		PriorityManager.AddPriorityState(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public PriorityState(string name, int priority, PriorityState.Layer layer)
	{
		m_name = name;
		m_Layer[(byte)layer] = true;
		m_priority = priority;
		m_sameLevelAlreadyHere = SameLevelAlreadyHere.NOTHING;
		m_higherLevelAlreadyHere = HigherLevelAlreadyHere.NOTHING;
		m_higherLevelComming = HigherLevelComing.HIDE;
		PriorityManager.AddPriorityState(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public PriorityState(string name, int priority, 
		PriorityState.Layer layer, 
		SameLevelAlreadyHere sameLevelAlreadyHere, 
		HigherLevelAlreadyHere higherLevelAlreadyHere, 
		HigherLevelComing higherLevelComming)
	{
		m_name = name;
		m_Layer[(byte)layer] = true;
		m_priority = priority;
		m_sameLevelAlreadyHere = sameLevelAlreadyHere;
		m_higherLevelAlreadyHere = higherLevelAlreadyHere;
		m_higherLevelComming = higherLevelComming;
		PriorityManager.AddPriorityState(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public PriorityState(string name, int priority, PriorityState.Layer layer, SameLevelAlreadyHere sameLevelAlreadyHere, HigherLevelAlreadyHere higherLevelAlreadyHere, HigherLevelComing higherLevelComming, bool canBlockOthers)
	{
		m_name = name;
		m_Layer[(byte)layer] = true;
		m_priority = priority;
		m_sameLevelAlreadyHere = sameLevelAlreadyHere;
		m_higherLevelAlreadyHere = higherLevelAlreadyHere;
		m_higherLevelComming = higherLevelComming;
		m_canBlockOthers = canBlockOthers;
		PriorityManager.AddPriorityState(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	~PriorityState()
	{
		PriorityManager.RemovePriorityState(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Add the priorityState to a new Layer
	public void AddLayer(Layer layer)
	{
		m_Layer[(byte)layer] = true;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	// Add the priorityState to a new Layer
	public void RemoveLayer(Layer layer)
	{
		m_Layer[(byte)layer] = false;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void TryTakeFocus()
	{
		PriorityManager.Get().AddFocus(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void LeaveFocus()
	{
		m_active = false;
		PriorityManager.Get().RemoveFocus(this);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public bool CanBeUse()
	{
		return (m_active && m_lockerList.Count == 0);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void Lock(string lockerName)
	{
		if (!m_lockerList.Contains(lockerName))
		{
			m_lockerList.Add(lockerName);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void Unlock(string lockerName)
	{
		m_lockerList.Remove(lockerName);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void ResetLocker()
	{
		m_lockerList.Clear();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public bool IsLocked()
	{
		return (m_lockerList.Count > 0);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public bool IsLockedBy(string lockerName)
	{
		if (m_lockerList.Contains(lockerName))
		{
			return true;
		}
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
[Obsolete("not maintained anymore", false)]
public class PriorityManager : MonoBehaviour
{
	static PriorityManager s_instance = null;
	static bool s_debug = false;
	DebugPanel m_DbgPanel = null;

	static ArrayList s_allPriorityState = new ArrayList();
	ArrayList m_curFocusRequest = new ArrayList();
	ArrayList m_curActivePriorizers = new ArrayList();

	ArrayList m_deleteList = new ArrayList();

	int[] m_curMaxPriorityLevel = new int[8];

	// Updating only when necessary
	static bool s_needUpdate = false;

	////////////////////////////////////////////////////////////////////////////////////////////////////

	void Awake()
	{
		if (s_instance == null)
		{
			s_instance = this;
		}
		else
		{
			if (s_debug) Debug.LogError("Cannot create more than 2 instance of PriorityManager");
		}

        m_DbgPanel = new DebugPanel("PriorityManager", false, false, false);

		StartCoroutine(SetDebugInfo());
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public IEnumerator SetDebugInfo()
	{
		while (true)
		{
			m_DbgPanel.ClearMessages();

		/*	m_DbgPanel.AddMessage("--- All referenced Priority State --- ");

			for (int i = 0; i < s_allPriorityState.Count; i++)
			{
				PriorityState curPriorityState = ((PriorityState)(s_allPriorityState[i]));
				string infos = curPriorityState.m_name + " - " + curPriorityState.m_priority + " - Layer : ";
				for (int j = 0; j < 8; j++)
				{
					if (curPriorityState.m_Layer[j])
					{
						infos += "[" + j + "]";
					}
				}

				m_DbgPanel.AddMessage("   " + infos);
			}*/

			m_DbgPanel.AddMessage("--- Active Priority State --- ");

			for (int i = 0; i < m_curActivePriorizers.Count; i++)
			{
				PriorityState curPriorityState = ((PriorityState)(m_curActivePriorizers[i]));
				string infos = curPriorityState.m_name + " - " + curPriorityState.m_priority + " - " + (curPriorityState.m_canBlockOthers ? "" : "Not Blocking ") + "- Layer : ";


				for (int j = 0; j < 8; j++)
				{
					if (curPriorityState.m_Layer[j])
					{
						infos += "[" + j + "]";
					}
				}

				m_DbgPanel.AddMessage(infos + (curPriorityState.m_active ? " - Active" : " - Paused"), curPriorityState.m_active ? DebugPanel.TextColor.green : DebugPanel.TextColor.yellow);
			}

			for (int i = 0; i < m_curFocusRequest.Count; i++)
			{
				PriorityState curPriorityState = ((PriorityState)(m_curFocusRequest[i]));
				string infos = curPriorityState.m_name + " - " + curPriorityState.m_priority + " - Layer : ";
				for (int j = 0; j < 8; j++)
				{
					if (curPriorityState.m_Layer[j])
					{
						infos += "[" + j + "]";
					}
				}
				m_DbgPanel.AddMessage(infos + " - Not active & Waiting", DebugPanel.TextColor.blue);
			}

			yield return new WaitForSeconds(0.5f);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public static PriorityManager Get()
	{
		if (s_instance == null)
		{
			s_instance = FindObjectOfType(typeof(PriorityManager)) as PriorityManager;
			if (s_instance == null)
			{
				GameObject PrioManager = new GameObject();
				PrioManager.AddComponent<PriorityManager>();
				PrioManager.name = "Priority Mgr";
				s_instance = FindObjectOfType(typeof(PriorityManager)) as PriorityManager;
			}
		}

		return s_instance;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	void CheckPriority()
	{
		s_needUpdate = true;
		while (s_needUpdate)
		{
			UpdateMaxPriority();
			s_needUpdate = false;
			// Parse all new FocusRequest then :
			//  - Add him to m_active process if priority > current max Priority or m_waitIfNotPossibleNow flag is set
			//  - Reject him if priority is too low 
			// Then clear the request list

			if (s_debug) Debug.LogWarning("REQUEST COUNT : " + m_curFocusRequest.Count);

			for (int i = 0; i < m_curFocusRequest.Count; i++)
			{
				PriorityState curPriorityState = ((PriorityState)(m_curFocusRequest[i]));
				int maxPrio = GetMaxPriority(curPriorityState.m_Layer);
				bool removeFromQueue = true;

				if (s_debug) Debug.LogWarning(" TREATING : " + curPriorityState.m_name + " request");
				if (s_debug) Debug.LogWarning(" maxPrio : " + maxPrio);

				if (!m_curActivePriorizers.Contains(curPriorityState))
				{
					if (curPriorityState.m_priority > maxPrio)
					{
						if (s_debug) Debug.Log("Add Active List :" + curPriorityState.m_name);
						m_curActivePriorizers.Add(curPriorityState);
						s_needUpdate = true;
						if (curPriorityState.m_canBlockOthers)
						{
							SetMaxPriority(curPriorityState.m_Layer, curPriorityState.m_priority);
						}
					}
					else
					{
						if (curPriorityState.m_priority == maxPrio)
						{
							if (curPriorityState.m_sameLevelAlreadyHere == PriorityState.SameLevelAlreadyHere.LAUNCH)
							{
								s_needUpdate = true;
								if (s_debug) Debug.Log("Add Active List :" + curPriorityState.m_name);
								m_curActivePriorizers.Add(curPriorityState);
							}
							else if (curPriorityState.m_sameLevelAlreadyHere == PriorityState.SameLevelAlreadyHere.WAIT)
							{
								removeFromQueue = false;
							}
						}
						else if (curPriorityState.m_higherLevelAlreadyHere == PriorityState.HigherLevelAlreadyHere.WAIT)
						{
							removeFromQueue = false;
						}
					}
				}

				if (removeFromQueue)
				{
					if (s_debug) Debug.Log("remove de la request list :" + curPriorityState.m_name);
					m_deleteList.Add(curPriorityState);
					//m_curFocusRequest.Remove(curPriorityState);
				}
			}

			// Delete all treated request
			for (int i = 0; i < m_deleteList.Count; i++)
			{
				m_curFocusRequest.Remove(m_deleteList[i]);
			}
			m_deleteList.Clear();

			//UpdateMaxPriority();
			

			// Parse all m_active priorityState to know if we need to pause or continue them :
			for (int i = 0; i < m_curActivePriorizers.Count; i++)
			{
				PriorityState curPriorityState = ((PriorityState)(m_curActivePriorizers[i]));
				int maxPrio = GetMaxPriority(curPriorityState.m_Layer);
				if (s_debug) Debug.Log("---- TREATING :" + curPriorityState.m_name);
				if (s_debug) Debug.Log(" maxPrio : " + maxPrio);

				if (curPriorityState.m_priority >= maxPrio)
				{
					if (s_debug) Debug.Log("ACTIVE:" + curPriorityState.m_name);
					curPriorityState.m_active = true;
				}
				else
				{
					if (s_debug) Debug.Log("UN-ACTIVE:" + curPriorityState.m_name);
					curPriorityState.m_active = false;
					if (curPriorityState.m_higherLevelComming == PriorityState.HigherLevelComing.KILL)
					{
						if (s_debug) Debug.Log("Remove Active List :" + curPriorityState.m_name);
						m_deleteList.Add(curPriorityState);
						//m_curActivePriorizers.Remove(curPriorityState);
						s_needUpdate = true;
					}
				}
			}

			// Delete all treated m_active state
			for (int i = 0; i < m_deleteList.Count; i++)
			{
				m_curActivePriorizers.Remove(m_deleteList[i]);
			}
		}
	}
		
	////////////////////////////////////////////////////////////////////////////////////////////////////

	static public void AddPriorityState(PriorityState priorityState)
	{
		s_allPriorityState.Add(priorityState);
		if (s_instance != null)
			s_instance.CheckPriority();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	static public void RemovePriorityState(PriorityState priorityState)
	{
		s_allPriorityState.Remove(priorityState);
		s_instance.m_curActivePriorizers.Remove(priorityState);
		s_instance.m_curFocusRequest.Remove(priorityState);
		s_instance.CheckPriority();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public bool CanUse(PriorityState priorityState)
	{
		return m_curActivePriorizers.Contains(priorityState);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void AddFocus(PriorityState priorityState)
	{
		if (!m_curFocusRequest.Contains(priorityState) && !m_curActivePriorizers.Contains(priorityState))
		{
			if (s_debug) Debug.Log("Add To Request List :" + priorityState.m_name);
			m_curFocusRequest.Add(priorityState);
			CheckPriority();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public int GetMaxPriority(BitArray layer)
	{
		int maxPrio = 0;
		for (int i = 0; i < 8; i++)
		{
			if (layer[i] && m_curMaxPriorityLevel[i] > maxPrio)
			{
				maxPrio = m_curMaxPriorityLevel[i];
			}
		}

		return maxPrio;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void SetMaxPriority(BitArray layer, int priority)
	{
		for (int i = 0; i < 8; i++)
		{
			if (layer[i] && priority > m_curMaxPriorityLevel[i])
			{
				m_curMaxPriorityLevel[i] = priority;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void UpdateMaxPriority()
	{
		int maxPriority = -1;

		for (int j = 0; j < 8; j++)
		{
			maxPriority = -1;
			for (int i = 0; i < m_curActivePriorizers.Count; i++)
			{
				PriorityState curPriorityState = ((PriorityState)(m_curActivePriorizers[i]));

				if (curPriorityState.m_Layer[j] && curPriorityState.m_canBlockOthers && curPriorityState.m_priority > maxPriority)
				{
					maxPriority = curPriorityState.m_priority;
				}
			}
			m_curMaxPriorityLevel[j] = maxPriority;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////

	public void RemoveFocus(PriorityState priorityState)
	{
		if (m_curFocusRequest.Contains(priorityState) || m_curActivePriorizers.Contains(priorityState))
		{
			if (s_debug) Debug.Log("Remove Active et Request List :" + priorityState.m_name);
			if (s_debug) Debug.Log("UN-ACTIVE:" + priorityState.m_name);
			
			m_curFocusRequest.Remove(priorityState);
			m_curActivePriorizers.Remove(priorityState);
			UpdateMaxPriority();
			CheckPriority();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
}