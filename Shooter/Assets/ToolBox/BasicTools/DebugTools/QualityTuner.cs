using UnityEngine;
using System.Collections;

public class QualityTuner : MonoBehaviour
{
	public QualityTunerPanel panel;
	void Awake()
	{
		panel = new QualityTunerPanel();
	}
}

public class QualityTunerPanel : DebugPanel
{
	public QualityTunerPanel()
		: base("Quality Tuner", false, false, false)
	{

	}

	string[] TextureQuality = 
	{
		"Full Res", "Halph Res", "Quarter Res", "Eighth Res"
	};

	string[] AnisotropicTextures = 
	{
		"Disable", "Enable", "Force Enable"
	};

	string[] AntiAliasing = 
	{
		"Disable", "x2", "x4", "x8"
	};

	/*string[] ShadowProj = 
	{
		"CloseFit" , "StableFit"
	};*/

	string[] ShadowCascade = 
	{
		"No Cascade" , "Two Cascades",  "Four Cascades"
	};

	string[] BlendWeights = 
	{
		"One Bone" , "Two Bones",  "Four Bones"
	};

	string[] VSync = 
	{
		"No Sync" , "Every Blank",  "Every Second Blank"
	};
	

	int curShadowCascade()
	{
		int value = QualitySettings.shadowCascades;
		if (value == 0)
			return 0;
		else if (value == 2)
			return 1;
		else if (value == 4)
			return 2;
		else
			return 0;
	}

	void setShadowCascade(int index)
	{
		if (index == 0)
			QualitySettings.shadowCascades = 0;
		else if (index == 1)
			QualitySettings.shadowCascades = 2;
		else if (index == 2)
			QualitySettings.shadowCascades = 4;
		else
			QualitySettings.antiAliasing = 0;
	}

	int curAntialisingIndex()
	{
		int value = QualitySettings.antiAliasing;
		if (value == 0)
			return 0;
		else if (value == 2)
			return 1;
		else if (value == 4)
			return 2;
		else if (value == 8)
			return 3;

		return 0;
	}

	void setAntialiasing(int index)
	{
		if (index == 0)
			QualitySettings.antiAliasing = 0;
		else if (index == 1)
			QualitySettings.antiAliasing = 2;
		else if (index == 2)
			QualitySettings.antiAliasing = 4;
		else if (index == 3)
			QualitySettings.antiAliasing = 8;
		else 
			QualitySettings.antiAliasing = 0;
	}




	GUILayoutOption labelWidth = GUILayout.Width(200);

	public override void OnDrawUI(Vector2 groupSize)
	{
		Rect rcBox = new Rect(0,0, groupSize.x, groupSize.y );

		GUI.Box(rcBox, "Quality Tuner");
		GUILayout.BeginArea(rcBox);
		GUILayout.Space(20);
		int level = GUILayout.SelectionGrid(QualitySettings.GetQualityLevel(), QualitySettings.names, QualitySettings.names.Length, GUILayout.MinHeight(30));
		if (level != QualitySettings.GetQualityLevel())
		{
			QualitySettings.SetQualityLevel(level);
		}

		GUILayout.Space(20);
		GUILayout.Label("Rendering");
		GUILayout.Space(10);

		GUILayout.BeginHorizontal();
		GUILayout.Label("Pixel Light Count : " + QualitySettings.pixelLightCount, labelWidth);
		QualitySettings.pixelLightCount = (int) GUILayout.HorizontalSlider(QualitySettings.pixelLightCount, 0, 10);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		int leveltexture = (int) Mathf.Pow(2, QualitySettings.masterTextureLimit);
		GUILayout.Label("Texture Quality : 1/" + leveltexture, labelWidth);


		level = GUILayout.SelectionGrid(QualitySettings.masterTextureLimit, TextureQuality, TextureQuality.Length);
		if (level != QualitySettings.masterTextureLimit)
		{
			QualitySettings.masterTextureLimit = level;
		}



		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Anisotropic Textures :", labelWidth);
		level = GUILayout.SelectionGrid((int) QualitySettings.anisotropicFiltering, AnisotropicTextures, AnisotropicTextures.Length);
		if (level != (int) QualitySettings.anisotropicFiltering)
		{
			QualitySettings.anisotropicFiltering = (AnisotropicFiltering) level;
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Anti Aliasing : ", labelWidth);
		level = GUILayout.SelectionGrid((int)curAntialisingIndex(), AntiAliasing, AntiAliasing.Length);
		if (level != curAntialisingIndex())
		{
			setAntialiasing(level);
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Soft Vegetation : ", labelWidth);
		QualitySettings.softVegetation = GUILayout.Toggle(QualitySettings.softVegetation, "");

		GUILayout.EndHorizontal();

		GUILayout.Space(20);

		GUILayout.Label("Shadows");
		GUILayout.Space(10);

		GUILayout.BeginHorizontal();
		GUILayout.Label("Shadows Distance : ", labelWidth);
		QualitySettings.shadowDistance = GUILayout.HorizontalSlider(QualitySettings.shadowDistance, 0, 500);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Shadows Cascades : ", labelWidth);
		level = GUILayout.SelectionGrid(curShadowCascade(), ShadowCascade, ShadowCascade.Length);
		if (level != curShadowCascade())
		{
			setShadowCascade(level);
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Shadows Proj : ", labelWidth);
		level = GUILayout.SelectionGrid((int) QualitySettings.shadowProjection, AnisotropicTextures, AnisotropicTextures.Length);
		if (level != (int) QualitySettings.shadowProjection)
		{
			QualitySettings.shadowProjection = (ShadowProjection) level;
		}
		GUILayout.EndHorizontal();
		GUILayout.Space(20);
		GUILayout.Label("Others");
		GUILayout.Space(10);

		GUILayout.BeginHorizontal();
		GUILayout.Label("Blend Weight : ", labelWidth);
		level = GUILayout.SelectionGrid((int) QualitySettings.blendWeights, BlendWeights, BlendWeights.Length);
		if (level != (int) QualitySettings.blendWeights)
		{
			QualitySettings.blendWeights = (BlendWeights) level;
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("VSync: ", labelWidth);
		QualitySettings.vSyncCount = GUILayout.SelectionGrid(QualitySettings.vSyncCount, VSync, VSync.Length);
		
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Lod Bias : x", labelWidth);
		QualitySettings.lodBias = GUILayout.HorizontalSlider(QualitySettings.lodBias, 0, 4);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Maximum LODLevel : ", labelWidth);
		QualitySettings.maximumLODLevel = (int) GUILayout.HorizontalSlider(QualitySettings.lodBias, 0, 4);
		GUILayout.EndHorizontal();



/*
	
		


		
		
		
		GUILayout.Label("Lod Bias" );
		GUILayout.Label("Maximum LODLevel" );
		

	*/
		

		GUILayout.EndArea();

	}
}

