using UnityEngine;
using System.Collections.Generic;


public class DebugCurve : MonoBehaviour
{
	LineRenderer line;
	List<float> values = new List<float>();
	List<float> times = new List<float>();
	public TextMesh minValueMesh;
	public TextMesh maxValueMesh;
	public float smooth = 0;


	public float maxDuration = 3;

	void Start()
	{
		line = gameObject.GetComponentInChildren<LineRenderer>();

	}

	public void addValue(float value)
	{
		if (smooth > 1) smooth = 1;
		if (smooth < 0) smooth = 0;
		if (values.Count != 0 && smooth != 0)
		{
			float lastValue = values[values.Count - 1];
			value = Mathf.Lerp(lastValue, value, 1 - smooth);
		}


		values.Add(value);
		times.Add(Time.time);

		float duration = times[times.Count - 1] - times[0];
		while (duration > maxDuration)
		{
			times.RemoveAt(0);
			values.RemoveAt(0);
			duration = times[times.Count - 1] - times[0];
		}

		float min = values[0];
		float max = values[0];
		for (int i = 0; i < values.Count; i++)
		{
			float val = values[i];
			if (min > val) min = val;
			if (max < val) max = val;
		}

		float range = (max - min);
		if (range == 0) range = 1;
		float minTime = times[0];
		line.SetVertexCount(values.Count);
		for (int i = 0; i < values.Count; i++)
		{
			Vector3 pos = new Vector3(
				(times[i] - minTime) * 10f / maxDuration - 5,
				(values[i] - min) * 10f / (range) - 5,

				0);

			line.SetPosition(i, pos);
		}

		minValueMesh.text = min.ToString();
		maxValueMesh.text = max.ToString();

	}

	void Update()
	{
		//	addValue(UnityEngine.Random.Range(-1000f,1000f));
	}




}
