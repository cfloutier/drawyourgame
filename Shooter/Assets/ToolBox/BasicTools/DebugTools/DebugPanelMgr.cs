/*==== DebugPanel.cs =============================================================*
 * a generic class used to show internal information
 * usage : 
 *		create a DebugPanel in your class
 *		add it to the manager using DebugPanelManager.Add();
 *		and add line to the line list
 *		
 * It will add a panel and a button to recall it when closed
 * =========================================================================*/
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// <summary>
/// The Debug Panel class
/// 
/// Create your own Debug Panel by 
/// Defining a member class DebugPanel in your class
/// Build it using the constructor in Start or Awake function
/// Add lines with AddMessage fucntions
/// You can use categories to filter messages (radio buttons are then added to the panel top)
/// you can use differents colors among TextColor enum
/// 
/// It can be a good choice to use a coroutine to refresh apnel content every x second ex :
/// 
/// in Start
/// panel2 = new DebugPanel("Log Messages", true, true);
/// StartCoroutine(setDebugInfos1());
/// 
/// IEnumerator setDebugInfos1()
///	{
///		while (true)
///		{
///			panel1.ClearMessages();
///			panel1.AddMessage("This console is updated every second", DebugPanel.TextColor.red);
///			yield return new WaitForSeconds(1);
///		}
///	}
/// 
/// 
/// </summary>
public class DebugPanel
{
	/// <summary>
	/// a list of predefined colors
	/// </summary>
	public enum TextColor
	{
		/// <summary>
		/// Color.white
		/// </summary>
		white = 0,
		/// <summary>
		/// Color.yellow
		/// </summary>
		yellow = 1,
		/// <summary>
		/// Color.red
		/// </summary>
		red = 2,
		/// <summary>
		/// Color.green
		/// </summary>
		green = 3,
		/// <summary>
		/// Color.cyan
		/// </summary>
		blue = 4,
		/// <summary>
		/// Color.grey
		/// </summary>
		grey = 5
	}

	/// <summary>
	/// The Console Message class, used by console panels
	/// </summary>
	public class ConsoleMessage
	{
		/// <summary>
		/// the Message core
		/// </summary>
		public string m_text;
		/// <summary>
		/// Message colors (see the enum)
		/// </summary>
		public TextColor m_color;
		/// <summary>
		/// Category of the message
		/// </summary>
		public string m_category;
		
	}


	int m_maxMessages = 150;
	string m_name;
	/// <summary>
	///  panel name (for main DebugPanelMgr class)
	/// </summary>
	public string name { get { return m_name; } set { m_name = value; } }

	bool m_invertOrder;
	/// <summary>
	///  invert order ? (for main DebugPanelMgr class)
	/// </summary>
	public bool invertOrder { get { return m_invertOrder; } }

	bool m_addClearButton;
	/// <summary>
	///  clear button ? (for main DebugPanelMgr class)
	/// </summary>
	public bool addClearButton { get { return m_addClearButton; } }

	ArrayList m_categories = new ArrayList();
	/// <summary>
	///  categories list (for main DebugPanelMgr class)
	/// </summary>
	public ArrayList categories { get { return m_categories; } }

	ArrayList m_activeCategories = new ArrayList();
	/// <summary>
	/// actives categories list (for main DebugPanelMgr class)
	/// </summary>
	public ArrayList activeCategories { get { return m_activeCategories; } }

	ArrayList m_messages = new ArrayList();
	/// <summary>
	/// accessor to array list used by Manager
	/// </summary>
	/// <returns></returns>
	public ArrayList getList() { return m_messages; }

	public void open() { DebugPanelMgr.setVisible(this); }
	public void close() { DebugPanelMgr.setVisible(null); }

	int m_maxWidthLine = 2000;
	int m_sizeLine = 18;

	public GUIStyle[] m_colors;

	protected virtual void buildColors()
	{
		m_colors = new GUIStyle[6];
		for (int i = 0; i < m_colors.Length; i++)
			m_colors[i] = new GUIStyle();

		m_colors[0].normal.textColor = Color.white;
		m_colors[1].normal.textColor = Color.yellow;
		m_colors[2].normal.textColor = Color.red;
		m_colors[3].normal.textColor = Color.green;
		m_colors[4].normal.textColor = Color.cyan;
		m_colors[5].normal.textColor = Color.grey;
	}

	/// <summary>
	/// The main Constructor
	/// </summary>
	/// <param name="name">panel name</param>
	/// <param name="invertOrder">invert order : last messages will be see first</param>
	/// <param name="addClearButton">add a clear button to cleanup content</param>
    /// <param name="active">open by default</param>
    public DebugPanel(string name, bool invertOrder, bool addClearButton, bool active)
	{
		m_name = name;
		m_invertOrder = invertOrder;
		m_addClearButton = addClearButton;
        DebugPanelMgr.addPanel(this, active);

		buildColors();
	}

	public bool isVisible()
	{
		return DebugPanelMgr.isVisible(this);
	}


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// Adds a mesage to the list
    /// </summary>
    /// <param name="message"></param>
    public void AddMessage(string message)
    {
        AddMessage("Global", message, TextColor.white);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// Adds a mesage to the list with color
    /// </summary>
    /// <param name="message"></param>
    /// <param name="color"></param>
    public void AddMessage(string message, TextColor color)
    {
        string category = "Global";
        if (message.StartsWith("["))
        {
            int index = message.IndexOf(']');
            if (index > 0)
            {
                category = message.Substring(1, index-1);
                message = message.Substring(index + 1);
            }
        }

        AddMessage(category, message, color);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// dds a mesage to the list with category and color
    /// </summary>
    /// <param name="category"></param>
    /// <param name="message"></param>
	/// <param name="color">colors are yellow,red,green,grey </param>
	public void AddMessage(string category, string message, TextColor color)
    {
        string[] lines = message.Split('\n');
        foreach (string line in lines)
        {
            if (line.Length == 0) continue;
            ConsoleMessage mes = new ConsoleMessage();

            mes.m_text = line;
			mes.m_color = color;
           

            mes.m_category = category;
           

            m_messages.Add(mes);
            if (!m_categories.Contains(category))
            {
                m_categories.Add(category);
                m_activeCategories.Add(category);
            }

            if (m_messages.Count == m_maxMessages)
                m_messages.RemoveAt(0);
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// <summary>
    /// Clears the messages from the screen and the lists
    /// </summary>
    public void ClearMessages()
    {
        m_messages.Clear();
    }

	public virtual void OnDrawUI(Vector2 groupSize)
	{
		drawConsole(groupSize);
	}

    public static bool GUIButton(Rect rc, string name)
    {
        return GUIButton(rc, name, GUI.skin.button);
    }

    public static bool GUIButton(Rect rc, string name, GUIStyle style)
    {
        bool res = GUI.Button(rc, name, style);
		if (PlatformFamily.getFamily() == PlatformFamily.Family.Mobile)
		{
			bool mouseOver = (Event.current.type == EventType.Repaint) && rc.Contains(Event.current.mousePosition);
			bool mouseUp = Input.GetMouseButtonUp(0);
			return (res || (mouseUp && mouseOver));
		}
		return res;
    }

    public static bool GUIToggle(Rect rc, bool value, string name)
    {
        bool res = GUI.Toggle(rc, value, name, GUI.skin.toggle);
        bool mouseOver = (Event.current.type == EventType.Repaint) && rc.Contains(Event.current.mousePosition);
        bool mouseUp = Input.GetMouseButtonUp(0);
        if ((res == value) && (mouseUp && mouseOver))
        {
            return !value;
        }
        return res;
    }

    public static bool GUIToggle(Rect rc, bool value, string name, GUIStyle style)
    {
        bool res = GUI.Toggle(rc, value, name, style);
        bool mouseOver = (Event.current.type == EventType.Repaint) && rc.Contains(Event.current.mousePosition);
        bool mouseUp = Input.GetMouseButtonUp(0);
        if ((res == value) && (mouseUp && mouseOver))
        {
            return !value;
        }
        return res;
    }

	public void drawConsole(Vector2 groupSize)
	{
		string reg = "";

		if (activeCategories.Count > 0)
		{
			for (int j = 0; j < activeCategories.Count; j++)
			{
				if (j > 0)
				{
					reg += "|";
				}

				reg += "^" + (string)(activeCategories[j]);
			}
		}
		else
		{
			reg = "None";
		}
		System.Text.RegularExpressions.Regex categorieFilter = new System.Text.RegularExpressions.Regex(reg); ;

		int sizeButtons = DebugPanelMgr.Get().sizeButtonsHeight;
		
		float yPos = 0;

		bool oneCat = categories.Count <= 1;
		if (!oneCat)
		{
			yPos += sizeButtons;
			// Show categories
			for (int j = 0; j < categories.Count; j++)
			{
				bool prevState = activeCategories.Contains(categories[j]);
				bool newState = GUI.Toggle(
					new Rect(j * 100 + 5, 5, 100, sizeButtons), 
					activeCategories.Contains(categories[j]), 
					(string)(categories[j]));


				if (prevState != newState)
				{
					if (newState == false)
					{
						activeCategories.Remove(categories[j]);
					}
					else
					{
						activeCategories.Add(categories[j]);
					}
				}
			}
		}

		int y = 0;

		Rect rcPanel = new Rect(0, yPos, groupSize.x, groupSize.y - yPos);

		GUI.BeginGroup(rcPanel);
		GUI.Box(new Rect(0,0,rcPanel.width, rcPanel.height), name);

		if (DebugPanel.GUIButton(new Rect(groupSize.x - sizeButtons, 0, sizeButtons, sizeButtons), "X"))
		{
			close();
			return;
		}

		if (addClearButton)
		{
            if (DebugPanel.GUIButton(new Rect(groupSize.x - 75 - sizeButtons, 0, 75, sizeButtons), "Clear"))
			{
				ClearMessages();
				return;
			}
		}

        if (DebugPanel.GUIButton(new Rect(groupSize.x - sizeButtons, sizeButtons + 5, sizeButtons, sizeButtons), "Up"))
		{
			DebugPanelMgr.Get().m_scrollPosition.y = 0; ;
		}

        if (DebugPanel.GUIButton(new Rect(groupSize.x - sizeButtons, groupSize.y - sizeButtons, sizeButtons, sizeButtons), "Dn"))
		{
			DebugPanelMgr.Get().m_scrollPosition.y = getList().Count * m_sizeLine;
		}

		

		DebugPanelMgr.Get().m_scrollPosition = GUI.BeginScrollView(new Rect(0, 20, groupSize.x - sizeButtons, groupSize.y - 20), DebugPanelMgr.Get().m_scrollPosition,
			new Rect(0, 0, m_maxWidthLine, getList().Count * m_sizeLine));

		y = 0;
		DebugPanelMgr.Get().m_scrollPosition.y = DebugPanelMgr.Get().m_scrollPosition.y < 0 ? 0 : DebugPanelMgr.Get().m_scrollPosition.y;
		int NbMissed = (int)(DebugPanelMgr.Get().m_scrollPosition.y / m_sizeLine);

		if (invertOrder)
		{
			int i = getList().Count - 1 - NbMissed;
			int beginY = NbMissed * m_sizeLine;

			for (; i >= 0; i--)
			{
				DebugPanel.ConsoleMessage mes = getList()[i] as DebugPanel.ConsoleMessage;
				if (categorieFilter.IsMatch(mes.m_category))
				{
					GUIStyle style = m_colors[(int)mes.m_color];

					if (oneCat)
					{
						GUI.Label(new Rect(10, beginY + y, m_maxWidthLine, 18), mes.m_text, style);
					}
					else
					{
						GUI.Label(new Rect(10, beginY + y, m_maxWidthLine, 18), "[" + mes.m_category + "]: " + mes.m_text, style);
					}
					y += m_sizeLine;
					if (y > groupSize.y)
						break;
				}
			}
		}
		else
		{
			int beginY = NbMissed * m_sizeLine;

			for (int i = NbMissed; i < getList().Count; i++)
			{
				DebugPanel.ConsoleMessage mes = getList()[i] as DebugPanel.ConsoleMessage;
				if (categorieFilter.IsMatch(mes.m_category))
				{
					GUIStyle style = m_colors[(int)mes.m_color];

					if (oneCat)
					{
						GUI.Label(new Rect(10, beginY + y, m_maxWidthLine, 18), mes.m_text, style);
					}
					else
					{
						GUI.Label(new Rect(10, beginY + y, m_maxWidthLine, 18), "[" + mes.m_category + "]: " + mes.m_text, style);
					}

					y += m_sizeLine;
					if (y > groupSize.y)
						break;
				}
			}
		}

		GUI.EndScrollView();
		GUI.EndGroup();
	}

}// End Dbg Class

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// <summary>
/// The Debug Panel Manager
/// 
/// It have to be added to an empty GameObject.
/// Only once per scene
/// 
/// It listens for Ctrl+Shift+W to open for editor, exe and web
/// For Mobiles use the flag showDebugButton to show it on the mobile
/// Don't forget to remove it for final releases
/// 
/// Debug logs are added if redirectDebugConsole is set (default)
/// 
/// You can create your own debug panel by creating a DebugPanel.
/// 
/// </summary>
public class DebugPanelMgr : MonoBehaviour
{
	/// <summary>
	/// static : set this value to true if you want to activate the consoles
	/// </summary>
	public static bool s_isActive = false;

	private double fps;
	private double updateInterval = 4.0f;
	private double lastTime;
	private float frames = 0.0f;    // Frames drawn over the interval
	private double timeNow = 0.0f;

	public bool activeOnStartup = false;
    public bool displayOnStartup = false;

	/// <summary>
	/// show all debug logs
	/// </summary>
    public bool redirectDebugConsole = true;

	/// <summary>
	/// show all debug logs
	/// </summary>
	public bool recordLogsFile = false;

	/// <summary>
	/// adds a GUI Dbg button on the left top of the screen. It just opens the panels 
	/// </summary>
	public bool showDebugButton = false;

	/// <summary>
	/// adds a GUI Quit button on the right top of the screen. It just opens the panels 
	/// </summary>
	public bool showQuitButton = false;

	/// <summary>
	/// adds a GUI Fps label 
	/// </summary>
	public bool showFps = false;

	private static DebugPanelMgr s_instance;
	/// <summary>
	/// static accessor
	/// If no Mgr is foudn, it will add a GameObject named "DebugPanelMgr" in the scene root
	/// </summary>
	static public DebugPanelMgr Get()
	{
		if (s_instance == null)
		{
			s_instance = FindObjectOfType(typeof(DebugPanelMgr)) as DebugPanelMgr;
			if (s_instance == null)
			{
				GameObject console = new GameObject();
				console.AddComponent<DebugPanelMgr>();
				console.name = "Debug Panel Mgr";
				s_instance = FindObjectOfType(typeof(DebugPanelMgr)) as DebugPanelMgr;
			}
		}

		return s_instance;
	}

    DebugPanel m_debugPanel = null;

	StreamWriter outfile;

	void Start()
	{
		lastTime = Time.realtimeSinceStartup;
		s_isActive = activeOnStartup;
	}

    void OnEnable()
    {
        if (redirectDebugConsole)
        {
            if (m_debugPanel == null)
			    m_debugPanel = new DebugPanel("Debug Console", true, true, displayOnStartup);

            Application.logMessageReceived += LogCallback;

			if (recordLogsFile)
			{
				if (Application.isEditor)
				{
					outfile = new StreamWriter(Application.dataPath + @"\..\Log.txt", true);
				}
				else if (PlatformFamily.getFamily() == PlatformFamily.Family.Desktop)
				{
					outfile = new StreamWriter(Application.dataPath + @"\..\Log.txt", true);
					Debug.Log("recording Logs ");
				}
			}
        }
    }

    void OnDisable()
    {
        // Remove callback when object goes out of scope
		Application.logMessageReceived -= LogCallback;
    }

   

    void LogCallback(string condition, string stackTrace, LogType type)
    {
		if (outfile != null)
		{
			DateTime current = DateTime.Now;
			string date = String.Format("{0:g}", current);

			outfile.WriteLine(date + " [" + type + "] " + condition);
			if (type == LogType.Error)
				outfile.WriteLine("[stack] :\n" + stackTrace);

			outfile.Flush();
		}
		else if (recordLogsFile && Application.isWebPlayer)
		{
			string log = " [" + type + "] " + condition;
			if (type == LogType.Error)
				log += "[stack] :\n" + stackTrace;
			Application.ExternalEval("console.log(\"" + log + "\")");
		}

        bool showStack = false;
        switch (type)
        {
            case LogType.Warning:
				m_debugPanel.AddMessage(condition, DebugPanel.TextColor.yellow);
                break;
            case LogType.Log:     
                m_debugPanel.AddMessage(condition);
                break;
            case LogType.Error:
            case LogType.Assert:
            case LogType.Exception:
                showStack = true;
				m_debugPanel.AddMessage(condition, DebugPanel.TextColor.red);
                break;
        }

        if (showStack)
        {
			m_debugPanel.AddMessage("[stack]" + stackTrace, DebugPanel.TextColor.blue);
        }
        
    }

	// the current m_active panel
	DebugPanel m_VisiblePanel = null;

	// the list of actives panels
	private ArrayList m_panelArray = new ArrayList();
 
	/// <summary>
	/// The screen space around panels
	/// </summary>
	public BorderRect m_Spacing = new BorderRect(10, 10, 30, 20);

	/// <summary>
	/// Customisation of the button Width
	/// </summary>
	public int sizeButtonsWidth = 40;

	/// <summary>
	/// Customisation of the button Height
	/// </summary>
	public int sizeButtonsHeight = 40;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	void Awake()
	{
		s_instance = this;
		DontDestroyOnLoad(this);
		
	}


	/// <summary>
	/// the global scroll position of the panel
	/// </summary>
	public Vector2 m_scrollPosition = new Vector2(0, 0);

	

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 
    /// <summary>
    /// adds a panel to the panel list.
    /// A new button will be added to show it
    /// </summary>
    /// <param name="panel">your panel</param>
    /// <param name="active">open by default</param>
    static public void addPanel(DebugPanel panel, bool active)
    {
        Get().m_panelArray.Add(panel);
        if (active)
        {
            Get().m_VisiblePanel = panel;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/// <summary>
	/// set currrent Panel
	/// </summary>
	/// <param name="panel">your panel to set as current</param>
	static public void setVisible(DebugPanel panel)
	{
		s_instance.m_VisiblePanel = panel;
	}

	/// <summary>
	/// Check if the panel is currently visible
	/// </summary>
	/// <param name="panel"></param>
	/// <returns></returns>
	static public bool isVisible(DebugPanel panel)
	{
		return s_isActive && s_instance.m_VisiblePanel == panel;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void Update()
    {
		++frames;
		timeNow = Time.realtimeSinceStartup;
		if (timeNow > lastTime + updateInterval)
		{
			fps = frames / (timeNow - lastTime);
			lastTime = timeNow;
			frames = 0;
		}
		
		if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.LeftControl) && ((Input.GetKeyDown(KeyCode.D)) || (Input.GetKeyDown(KeyCode.W))))
        {
            s_isActive = !s_isActive;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    void OnGUI()
	{
		GUI.depth = -5000;
		if (s_isActive && m_panelArray.Count != 0)
        {
			// show first buttons
            int x = sizeButtonsWidth;

			float widthButtons = (Screen.width - sizeButtonsWidth) / m_panelArray.Count;

			for (int i = 0; i < m_panelArray.Count; i++)
			{
				DebugPanel panel = m_panelArray[i] as DebugPanel;

                if (DebugPanel.GUIButton(new Rect(x, 0, widthButtons, sizeButtonsHeight), panel.name))
				{
					if (m_VisiblePanel == panel)
						m_VisiblePanel = null;
					else 
						m_VisiblePanel = panel;
				}

				x += (int)widthButtons;
			}

            if (DebugPanel.GUIButton(new Rect(0, 0, sizeButtonsWidth, sizeButtonsHeight), "X"))
            {
				
                s_isActive = false;
            }

			if (m_VisiblePanel != null)
			{
				Rect rcGroup = new Rect(m_Spacing.left, m_Spacing.top + sizeButtonsHeight, Screen.width - m_Spacing.horizontal, Screen.height - m_Spacing.vertical - sizeButtonsHeight);

				GUI.BeginGroup(rcGroup);

				m_VisiblePanel.OnDrawUI(new Vector2(rcGroup.width, rcGroup.height));
				GUI.EndGroup();
			}
        }
        else if (showDebugButton)
        {
            if (DebugPanel.GUIButton(new Rect(0, 0, sizeButtonsWidth, sizeButtonsHeight), "Dbg" + (showFps ? " (" + ((int)fps).ToString() + ")" : string.Empty)))
            {
                s_isActive = true;
            }
			if (showQuitButton)
			{
                if (DebugPanel.GUIButton(new Rect(Screen.width - sizeButtonsWidth, 0, sizeButtonsWidth, sizeButtonsHeight), "Quit"))
				{
#if UNITY_EDITOR
					EditorApplication.isPlaying = false;
#else
					Application.Quit();
#endif				
				}
			}
        }
	}

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
