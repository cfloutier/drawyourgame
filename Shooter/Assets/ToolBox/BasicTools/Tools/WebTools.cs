﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Specialized;

// Cette classe contient une série de fonctions utile aux application web

/// <summary>
/// Static Tools Class used for Web applications
/// </summary>
public class WebTools 
{
	static bool s_debug = false;

	// accès à la liste des fichier dans le path donné
	/// <summary>
	/// This function will get access to a directory file list
	/// </summary>
	/// <param name="path">The path of the dir to scan. 
	/// For Exe it is relative to main exe 
	/// For Editor, relative to WWW directory
	/// For Web relative to Config.s_HttpRoot</param>
	/// <param name="dir">Ture to include directories in the listing or not</param>
	/// <param name="delegateMethod">The method to call when list is ready</param>
	/// <param name="userparam">user params send to the delegate function</param>
	public static void GetFileList(string path, bool dir, Download.DownloadDelegate delegateMethod, object userparam)
	{
		if (s_debug) Debug.Log("GetFileList : " + path);

		if (Application.isEditor ||
			PlatformFamily.getFamily() == PlatformFamily.Family.Desktop)
		{
			string result = "";
			path = path.Replace("\\", "/");

			path = Config.HttpRoot.Replace("file://", "") + "/" + path;

			if (!path.EndsWith("/"))
			{
				path = path + "/";
			}

			// pour avoir les noms des fichiers
			string[] files = System.IO.Directory.GetFiles(path);

			if (s_debug) 
                Debug.Log("fromEditor correct path is : " + path);

			int filecount = files.GetUpperBound(0) + 1;
			for (int i = 0; i < filecount; i++)
			{
				string fileName = files[i].Replace(path, "");

				result +=  fileName + '\n';
			}
			if (dir)
			{
				// pour avoir les noms des sous-répertoires
				string[] directories = System.IO.Directory.GetDirectories(path);
				int dircount = directories.GetUpperBound(0) + 1;
				for (int i = 0; i < dircount; i++)
				{
					string dirName = directories[i].Replace(path, "");

					result += dirName + '\n';
				}
			}

			if (s_debug) 
				Debug.Log("Detected files : \n" + result);

			delegateMethod(result, 1, userparam, null);
		}
		else
        {
            // il faut qu'il y ait dans le path Http Root un php qui fonctionne avec le script listfiles.php

			string str = Config.HttpRoot + "/scripts/listFiles.php?path=" + path;
            //Debug.Log("getFileList :"+str);
            //str = "http://localhost/Orange/php/listFiles.php?path=MaisonOrange";
            //Debug.Log("getFileList :" + str);

            Download.Load(str, Download.Type.Ascii_Text, delegateMethod, 
				Download.Hash(
				"priority", 1.0f,
				"bandwidth", 1.0f,
				"cache", false, 
				"userParam", userparam,
				"retry", 3));
        }
	}

	public static string buildRelativeUrl(string mainUrl, string relativeUrl)
	{
		string newUrl = getUrlMacros(relativeUrl);
		if (newUrl.StartsWith("file://") || newUrl.StartsWith("http://"))
		{
			return newUrl;
		}

		// build relative path 
		string path = mainUrl;
		path = path.Replace('\\', '/');

		

		int lastIndexOfSlash = path.LastIndexOf('/');
		
		if (lastIndexOfSlash == -1)
		{
			// invalid base path
			Debug.LogError("invalid base path " + mainUrl);
			return newUrl;
		}

		path = path.Substring(0, lastIndexOfSlash);
		
		
		return concatUrl(path, relativeUrl);
	}

	/// <summary>
	/// Remove .. and . from an url
	/// </summary>
	/// <param name="url">url with </param>
	/// <returns></returns>
	public static string solidURl(string url)
	{
		url = url.Replace('\\', '/');
		string[] list = url.Split('/');
		ArrayList array = new ArrayList();
		for (int i = 0; i < list.Length; i++)
		{
			if (list[i] == ".")
			{
				continue;
			}
			else if (list[i] == ".." && array.Count > 0)
			{
				array.RemoveAt(array.Count-1);
			}
			else array.Add(list[i]);
		}

		string res = "";
		for (int i = 0; i < array.Count - 1; i++)
		{
			res += (array[i] as string) + "/";
		}
		res += (array[array.Count - 1] as string);
		//Debug.Log("concatUrl res:" + res);
		return res;
	}

	/// <summary>
	/// Rebuild an url removing ., .. useless / ...
	/// </summary>
	/// <param name="basepath">the base path ex http://myserver/dir</param>
	/// <param name="relativePath">relative path using ../ etc</param>
	/// <returns>a valid url</returns>
	public static string concatUrl(string basepath, string relativePath)
	{
		//Debug.Log("concatUrl basepath:" + basepath);
		//Debug.Log("concatUrl relativePath:" + relativePath);

		relativePath = relativePath.Replace('\\', '/');
		string[] listBase = basepath.Split('/');
		ArrayList array = new ArrayList();
		
		string[] listRel = relativePath.Split('/');
		for (int i = 0; i < listBase.Length; i++)
		{
			//if (listBase[i].Length > 0)
			array.Add(listBase[i]);
		}

		for (int i = 0; i < listRel.Length; i++)
		{
			string str = listRel[i];
			if (str == "..")
			{
				array.RemoveAt(array.Count - 1);
			}
			else if (str != "." && str.Length > 0)
			{
				array.Add(listRel[i]);
			}
		}

		string res = "";
		for (int i = 0; i < array.Count -1; i++)
		{
			res += (array[i] as string) + "/";
		}
		res += (array[array.Count - 1] as string);
		//Debug.Log("concatUrl res:" + res);
		return res;
	}

	static string PlatformDirectory()
	{
		switch (PlatformFamily.getFamily())
		{
            case PlatformFamily.Family.Glasses:
            case PlatformFamily.Family.Mobile:
                if (PlatformFamily.getPlatform() == PlatformFamily.Platform.Android)
					return "Android";
				else if (PlatformFamily.getPlatform() == PlatformFamily.Platform.OSX)		
					return "OSX";
				break;
			default:
				return "Web";
		}

		return "Invalid";
	}

	static string getUrlMacros(string url)
	{
		//Debug.Log("url " + url);
		string realURL = url;
		realURL = realURL.Replace("[platform]", PlatformDirectory());
		realURL = realURL.Replace("[bundles]", Config.BundlesRoot);
		realURL = realURL.Replace("[dataroot]", Config.DataRoot);
		realURL = realURL.Replace("[httproot]", Config.HttpRoot);
		realURL = realURL.Replace("[apppath]", Application.dataPath.Replace('\\','/'));

		return realURL;
	}


	/// <summary>
	///  Rebuild a file or url path using [httproot] or [dataroot] tags
	///  * [httproot] is replaced by Config.HttpRoot
	///  * [dataroot] is replaced by Config.DataRoot
	///  * [bundles] is replaced by Config.BundlesRoot
	///  * [platform] is replaced by Platform directory (Android, IOS, Web, ...)
	///  * [apppath] is replaced by ApplicationDataPathy defined by UNITY
	///  
	/// Use Http root to concat url if needed
	/// </summary>
	/// <param name="url"></param>
	/// <returns></returns>
	static public string rebuildUrl(string url)
	{
		//Debug.Log("url " + url);

		string realURL = getUrlMacros(url);
		realURL = solidURl(realURL);
		if (!realURL.StartsWith("http://") && !realURL.StartsWith("file://") && !realURL.StartsWith("jar:"))
			realURL = WebTools.concatUrl(Config.HttpRoot, realURL);
	
		return realURL;
	}
		
	/// <summary>
	///  extract from url the GET parmeters
	///  
	/// Use Http root to concat url if needed
	/// </summary>
	/// <param name="url"></param>
	/// <returns></returns>
	public static Hashtable decodeParameters(string url)
	{
		Debug.Log("decodeParameters " + url);

		Hashtable hash = new Hashtable();
		string[] temp = url.Split('?');
		if (temp.Length < 2)
			return hash;

		Debug.Log("ok ? found");

		temp = temp[1].Split('&');

		for (int i = 0; i < temp.Length; i++)
		{
			string paramValue = temp[i];

			Debug.Log("paramValue" + paramValue);

			int indexEqual = paramValue.IndexOf('=');
			
			if (indexEqual == -1)
			{
				hash[indexEqual] = "";
			}
			else 
			{
				string param = WWW.UnEscapeURL(paramValue.Substring(0, indexEqual));
				string value =  WWW.UnEscapeURL( paramValue.Substring(indexEqual+1));
				Debug.Log("param " + param);
				Debug.Log("value " + value);


				hash[param] = value;
			}
		}

		return hash;
	}
}
