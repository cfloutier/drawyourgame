﻿using UnityEngine;
using System.Collections;

public class MaterialOnPlatform : MonoBehaviour
{
	[System.Serializable]
	public class MaterialAdaptation
	{
		public PlatformFamily.Family family;
		public PlatformFamily.Platform platform;
		public Material material;
	}

	public MaterialAdaptation[] adaptation;

	// Use this for initialization
	void Start()
	{
		if (GetComponent<Renderer>() == null)
			return;

		for (int i = 0; i < adaptation.Length; i++)
		{
			MaterialAdaptation adapt = adaptation[i];
			if (adapt.family == PlatformFamily.Family.Unknown ||
				adapt.family == PlatformFamily.getFamily())
			{
				if (adapt.platform == PlatformFamily.Platform.Unknown ||
					adapt.platform == PlatformFamily.getPlatform())
				{
					GetComponent<Renderer>().material = adapt.material;
				}
			}
		}
	}
}
