using UnityEngine;
using System.Collections;

public class DestroyOnPlatform : MonoBehaviour 
{
	public PlatformFamily.Family[] families;
	// if true this game object is kept for platforms specifieds, else it is destroyed for thoses platforms
	public bool KeepForPlatform = true;

	// Use this for initialization
	void Start()
	{
		bool platformFamilyFound = PlatformFamily.isInFamilies(families);
		if (KeepForPlatform && !platformFamilyFound)
		{
			//Debug.Log("Destroy " + gameObject.name);

			Destroy(gameObject);
		}
		else if (!KeepForPlatform && platformFamilyFound)
			Destroy(gameObject);
	}
}
