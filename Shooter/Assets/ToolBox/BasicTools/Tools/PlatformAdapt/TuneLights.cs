using UnityEngine;
using System.Collections;


/// <summary>
/// Adds this to any light in your scene, its intrensity ca	n be set on startup
/// </summary>
public class TuneLights : MonoBehaviour 
{
	public float setIntensityOnPlay = 1;

	// Use this for initialization
	void Start () 
	{
		Light light = GetComponent<Light>();
		light.intensity = setIntensityOnPlay;
	}
}
