using UnityEngine;
using System.Collections;



/// <summary>
/// This class is used to adapt your code depending on the current target platform
/// 
/// It is easier to use this kind of test rather than Precompiled directive because you can be sure that your code compile for all platforms
/// 
/// When using the editor the platform is kept depending on build target. 
/// To know if you are in the editor simply use Application.isEditor 
/// 
/// </summary>
public class PlatformFamily
{
	public enum Family
	{
		Unknown,
		Web,
		Desktop,
		Mobile,
		Console,
		Flash,
        Glasses
	}

	public enum Platform
	{
		Unknown,
		OSX,
		Windows,
		Linux,
		Android,
		IOS,
		PS3,
		XBOX360,
		WII
	}

	static Family curFamily = Family.Unknown;
	static Platform curPlatform = Platform.Unknown;

	static public Family getFamily()
	{
		if (curFamily == Family.Unknown)
			computePlatform();

		return curFamily;
	}

	static public Platform getPlatform()
	{
		if (curPlatform == Platform.Unknown)
			computePlatform();

		return curPlatform;
	}

	static bool isEditor() { return Application.isEditor; }
	
	static void computePlatform()
	{
		switch (Application.platform)
		{
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.WindowsEditor:

#if (UNITY_ANDROID)

				curFamily = Family.Mobile;
				curPlatform = Platform.Android;

#endif

#if (UNITY_IPHONE)
			curFamily = Family.Mobile;
			curPlatform = Platform.IOS;
#endif

#if (UNITY_PS3)
			curFamily = Family.Console;
			curPlatform = Platform.PS3;	
#endif	
		
#if (UNITY_XBOX360)
			curFamily = Family.Console;
			curPlatform = Platform.XBOX360;	
#endif	

#if (UNITY_XBOX360)
			curFamily = Family.Console;
			curPlatform = Platform.WII;	
#endif	
#if (UNITY_FLASH)
			curFamily = Family.Flash;
			curPlatform = Platform.Desktop;	
#endif
#if (UNITY_WEBPLAYER) 
			curFamily = Family.Web;
			curPlatform = Application.platform == RuntimePlatform.OSXEditor ? Platform.OSX : Platform.Windows;
#endif
#if (UNITY_STANDALONE_WIN) 
			curFamily = Family.Desktop;
			curPlatform = Platform.Windows;	
#endif
#if (UNITY_STANDALONE_OSX) 
			curFamily = Family.Desktop;
			curPlatform = Platform.OSX;	
#endif
				break;
			case RuntimePlatform.OSXWebPlayer:
				curFamily = Family.Web;
				curPlatform = Platform.OSX;
				break;
			case RuntimePlatform.WindowsWebPlayer:
				curFamily = Family.Web;
				curPlatform = Platform.Windows;
				break;
			case RuntimePlatform.LinuxPlayer:
				curFamily = Family.Web;
				curPlatform = Platform.Linux;
				break;

			case RuntimePlatform.OSXPlayer:
			case RuntimePlatform.OSXDashboardPlayer:
				curFamily = Family.Desktop;
				curPlatform = Platform.OSX;
				break;

			case RuntimePlatform.WindowsPlayer:
				curFamily = Family.Desktop;
				curPlatform = Platform.Windows;
				break;
			case RuntimePlatform.Android:
				curFamily = Family.Mobile;
				curPlatform = Platform.Android;
				break;

			case RuntimePlatform.IPhonePlayer:
				curFamily = Family.Mobile;
				curPlatform = Platform.IOS;
				break;

	//		case RuntimePlatform.NaCl: // google native client


			case RuntimePlatform.PS3:
				curFamily = Family.Console;
				curPlatform = Platform.PS3;
				break;

			case RuntimePlatform.XBOX360:
				curFamily = Family.Console;
				curPlatform = Platform.XBOX360;
				break;
		}

        if (SystemInfo.deviceModel.Contains("EPSON"))
        {
            curFamily = Family.Glasses;
            curPlatform = Platform.Android;
        }
	}

	static public bool isInFamilies(Family[] families)
	{
		PlatformFamily.Family curFamily = PlatformFamily.getFamily();

		bool platformFamilyFound = false;
		foreach (PlatformFamily.Family f in families)
			if (f == curFamily)
				platformFamilyFound = true;

		return platformFamilyFound;
	}
}
