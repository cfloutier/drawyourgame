using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if (UNITY_EDITOR)
using UnityEditor;
#endif

/// <summary>
/// a set of misc static tools
/// </summary>
public class MiscTools
{
	


	public static void applicationExit()
	{
		// Editor mode = stop
		if (Application.isEditor)
		{
#if (UNITY_EDITOR)
			EditorApplication.isPlaying = false;
#endif
		}
		// Web mode = Call login web page
		else if (PlatformFamily.getFamily() == PlatformFamily.Family.Web)
		{
			Application.ExternalEval("window.history.back();");
		}
		// Other mode = quit
		else
		{
			Application.Quit();
		}
	}
	
}
