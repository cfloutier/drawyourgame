using UnityEngine;
using System.Collections;

//[System.Serializable]

/// <summary>
/// base class for all interpolation
/// compute index in values and ratio in between indexes
/// call then the computeValues Virtual function
/// </summary>
public class Interpolator
{
	/// <summary>
	/// The keys : one for each value. They must be ordered and set between 0 and 1
	/// </summary>
    public float [] keys;
	/// <summary>
	/// The current Index
	/// </summary>
    protected int keyindex;

	/// <summary>
	/// the main fucntion to override, see ScalarInterpolator as example
	/// </summary>
	/// <param name="firstkeyindex">the first value index</param>
	/// <param name="inBetWeenFraction">the fraction in between first value and next one</param>
    protected virtual void computeNewValue(int firstkeyindex, float inBetWeenFraction)
    {
        // to be overloaded
        return;
    }
    

    /// <summary>
	/// compute local fraction value and first index
    /// </summary>
    /// <param name="globalFraction">the global fraction value</param>
	protected void computeValue(float globalFraction)
    {
        int keySize = keys.Length;

        if (globalFraction <= keys[0]) 
	    {
            computeNewValue(0, 0);
            keyindex = 0;
        }

	    // check for a fraction AFTER the last key
        else if (globalFraction >= keys[keySize - 1]) 
	    {
            computeNewValue(keySize - 2, 1);
            keyindex = 0; // next time we will continue at the begining
	    }
	    else
	    {
		    // try to find the right indice : key[index] <= fraction < key[index+1]
            while ((globalFraction < keys[keyindex])
                || (globalFraction >= keys[keyindex + 1])) 
		    {
			    keyindex++;
			    if (keyindex >= (keySize - 1)) 
			    {
				    keyindex = 0; // we have to loop
			    }
		    }
    		
		    // check if we have reached the last fraction
		    if (keyindex < (keySize - 1))
		    {
                float key1 = keys[keyindex];
                float key2 = keys[keyindex + 1];
			    float inBetweenFraction = key2 - key1;
			    if (inBetweenFraction != 0)
			    {
                    inBetweenFraction = (globalFraction - key1) / inBetweenFraction;
                    computeNewValue(keyindex, inBetweenFraction);
			    }
			    else
			    {
                    computeNewValue(keyindex, 0);
			    }
		    }
		    else
		    {
                computeNewValue(keyindex - 2, 1);
		    }
	    }
    }
}
