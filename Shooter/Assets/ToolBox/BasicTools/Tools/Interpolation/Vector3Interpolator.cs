using UnityEngine;
using System.Collections;

//[System.Serializable]


/// <summary>
/// Vector3Interpolator is a simple implementation for linear interpolator unsing Vector3 values
/// each value corresponds to a keys's value. The 2 arrays should therefore have the same size
/// 
/// </summary>
public class Vector3Interpolator : Interpolator
{
	/// <summary>
	/// The list of values 
	/// </summary>
	public Vector3[] values;
	/// <summary>
	/// Current value as computed by computeNewValue
	/// </summary>
	public Vector3 CurValue;

	/// <summary>
	/// The main function : return the valeu depending on globalFraction [0-1]
	/// </summary>
	/// <param name="globalFraction">[0-1]</param>
	/// <returns>the value</returns>
    public Vector3 getResult(float globalFraction)
    {
        computeValue(globalFraction);
        return CurValue;
    }

	/// <summary>
	/// Overriden from Interpolator
	/// should complete a new value depending on local fraction 
	/// </summary>
	/// <param name="firstkeyindex">the firstIndex</param>
	/// <param name="inBetweenFraction">the local fraction</param>
    protected override void computeNewValue(int firstkeyindex, float inBetweenFraction)
    {
        CurValue = values[firstkeyindex] + inBetweenFraction *
                    (values[firstkeyindex + 1] - values[firstkeyindex]);
    }
}
