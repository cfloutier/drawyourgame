﻿using UnityEngine;
using System.Collections;

//[System.Serializable]

/// <summary>
/// ScalarInterpolator is a simple implementation for linear interpolator unsing float values
/// each value corresponds to a keys's value. The 2 arrays should therefore have the same size
/// 
/// </summary>
public class ColorInterpolator : Interpolator
{
	/// <summary>
	/// The list of values 
	/// </summary>
	public Color[] values;

	/// <summary>
	/// Current value as computed by computeNewValue
	/// </summary>
	public Color CurValue;

	/// <summary>
	/// The main function : return the valeu depending on globalFraction [0-1]
	/// </summary>
	/// <param name="globalFraction">[0-1]</param>
	/// <returns>the value</returns>
	public Color getResult(float globalFraction)
	{
		computeValue(globalFraction);
		return CurValue;
	}

	/// <summary>
	/// Overriden from Interpolator
	/// should complete a new value depending on local fraction 
	/// </summary>
	/// <param name="firstkeyindex">the firstIndex</param>
	/// <param name="inBetweenFraction">the local fraction</param>
	protected override void computeNewValue(int firstkeyindex, float inBetweenFraction)
	{
		CurValue = Color.Lerp(values[firstkeyindex], values[firstkeyindex + 1], inBetweenFraction);
	}
}
