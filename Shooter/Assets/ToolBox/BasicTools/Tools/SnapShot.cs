﻿using UnityEngine;
using System.Collections;

public class SnapShot : MonoBehaviour
{
	static RenderTexture renderTextureColor;

	public static Texture2D takeASnap(Camera cam, int width, int height)
	{
	
		//Debug.Log("not transparent");
			renderTextureColor = new RenderTexture(width, height, 32, RenderTextureFormat.ARGB32);

			cam.targetTexture = renderTextureColor;
			cam.Render();

			RenderTexture.active = renderTextureColor;
			Texture2D finaltexture = new Texture2D(renderTextureColor.width, renderTextureColor.height, TextureFormat.RGB24, false);
			finaltexture.ReadPixels(new Rect(0, 0, renderTextureColor.width, renderTextureColor.height), 0, 0);

			finaltexture.Apply();

			cam.targetTexture = null;
			RenderTexture.active = null;

			//DestroyImmediate(renderTextureColor);
			renderTextureColor = null;

			return finaltexture;
		
	}

#if UNITY_STANDALONE
	static public void save(Texture2D texture, string path)
	{
		byte[] octets = texture.EncodeToPNG();

		Debug.Log("saved to " + path);
		System.IO.File.WriteAllBytes(path, octets);
	}
#endif
	
}
