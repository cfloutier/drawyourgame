using UnityEngine;
using System.Collections;


/// <summary>
/// A simple tool to merge a GameObject on runtime
/// </summary>
public class MergeChilds : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		merge(transform);
	}

	/// <summary>
	/// Called on startup, merge all SkinnedMeshRenderer 
	/// </summary>
	/// <param name="tr"></param>
	static public void merge(Transform tr)
	{
		SkinnedMeshRenderer curRenderer = tr.GetComponent<SkinnedMeshRenderer>();
		if (curRenderer != null)
		{
			GameObject.DestroyImmediate(curRenderer);
		}


		Material material = null;

		// if not specified, go find meshes

		// find all the mesh filters
		Component[] comps = tr.GetComponentsInChildren(typeof(MeshFilter));
		MeshFilter[] meshFilters = new MeshFilter[comps.Length];

		int mfi = 0;
		foreach (Component comp in comps)
			meshFilters[mfi++] = (MeshFilter)comp;


		// figure out array sizes
		int vertCount = 0;
		int normCount = 0;
		int triCount = 0;
		int uvCount = 0;

		foreach (MeshFilter mf in meshFilters)
		{
			vertCount += mf.sharedMesh.vertices.Length;
			normCount += mf.sharedMesh.normals.Length;
			triCount += mf.sharedMesh.triangles.Length;
			uvCount += mf.sharedMesh.uv.Length;
			if (material == null)
				material = mf.gameObject.GetComponent<Renderer>().sharedMaterial;
		}

		// allocate arrays
		Vector3[] verts = new Vector3[vertCount];
		Vector3[] norms = new Vector3[normCount];
		Transform[] aBones = new Transform[meshFilters.Length];
		Matrix4x4[] bindPoses = new Matrix4x4[meshFilters.Length];
		BoneWeight[] weights = new BoneWeight[vertCount];
		int[] tris = new int[triCount];
		Vector2[] uvs = new Vector2[uvCount];

		int vertOffset = 0;
		int normOffset = 0;
		int triOffset = 0;
		int uvOffset = 0;
		int meshOffset = 0;

		// merge the meshes and set up bones
		foreach (MeshFilter mf in meshFilters)
		{
			foreach (int i in mf.sharedMesh.triangles)
				tris[triOffset++] = i + vertOffset;

			aBones[meshOffset] = mf.transform;
			bindPoses[meshOffset] = Matrix4x4.identity;

			foreach (Vector3 v in mf.sharedMesh.vertices)
			{
				weights[vertOffset].weight0 = 1.0f;
				weights[vertOffset].boneIndex0 = meshOffset;
				verts[vertOffset++] = v;
			}

			foreach (Vector3 n in mf.sharedMesh.normals)
				norms[normOffset++] = n;

			foreach (Vector2 uv in mf.sharedMesh.uv)
				uvs[uvOffset++] = uv;

			meshOffset++;

			MeshRenderer mr =
			  mf.gameObject.GetComponent(typeof(MeshRenderer))
			  as MeshRenderer;

			if (mr)
				mr.enabled = false;
		}

		// hook up the mesh
		Mesh me = new Mesh();
		me.name = tr.gameObject.name;
		me.vertices = verts;
		me.normals = norms;
		me.boneWeights = weights;
		me.uv = uvs;
		me.triangles = tris;
		me.bindposes = bindPoses;



		// hook up the mesh renderer        
		SkinnedMeshRenderer smr =
		  tr.gameObject.AddComponent(typeof(SkinnedMeshRenderer))
		  as SkinnedMeshRenderer;

		smr.sharedMesh = me;
		smr.bones = aBones;
		tr.GetComponent<Renderer>().material = material;




	}
	// Update is called once per frame
	void Update () {
	
	}
}
