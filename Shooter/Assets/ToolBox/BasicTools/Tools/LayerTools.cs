using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// a set of  static tools for object layers
/// </summary>
public class LayerTools  {

	/// <summary>
	/// Convert a list of layers name to a layer mask
	/// </summary>
	/// <param name="names">layers names</param>
	/// <returns>the mask that can be used in raycast for instance</returns>
	static public int namesToLayersMask(params string[] names)
	{
		int mask = 0;
		foreach (string name in names)
		{
			int layer = LayerMask.NameToLayer(name);
			mask |= 1 << layer;
		}

		return mask;
	}

	/// <summary>
	/// Convert a layer mask to a list of string
	/// </summary>
	static public string[] LayersMaskToStrings(int mask)
	{
		List<string> result = new List<string>();
		for (int i = 0; i < 32; i++)
		{
			if ((1 << i & mask) != 0)
			{
				string name = LayerMask.LayerToName(i);
				if (name != "")
					result.Add(name);
			}

		}

		return result.ToArray();
	}

	/// <summary>
	/// Convert a layer mask to single string useful for debug purpose
	/// </summary>
	static public string LayersMaskToString(int mask)
	{
		if (mask == -1)
			return "all";
		else if (mask == 0)
			return "none";

		string result = "";
		string[] array = LayersMaskToStrings(mask);
		for (int i = 0; i < array.Length; i++)
		{
			result += array[i];
			if (i < array.Length - 1)
				result += ", ";
		}

		return result;
	}


}
