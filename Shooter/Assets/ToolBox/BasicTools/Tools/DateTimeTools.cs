﻿using System;
using System.Globalization;

public class DateTimeTools {

	public static bool TryParseSqlDate(string txt, out DateTime dateTime)
	{
		return DateTime.TryParseExact(txt, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture,
						  DateTimeStyles.None, out dateTime);
	}

	public static string formatSql(DateTime dateTime)
	{
		return dateTime.ToString("yyyy-MM-dd HH:mm:ss");
	}

}
