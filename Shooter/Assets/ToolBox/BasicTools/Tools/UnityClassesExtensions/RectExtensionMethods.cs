using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public static class UnityRectExtensions
{

	/// <summary>
	/// Test if this rect intersects with another rect.
	/// </summary>
	/// <param name="rect">
	/// A <see cref="Rect"/>
	/// </param>
	/// <param name="other">
	/// A <see cref="Rect"/>
	/// </param>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public static bool Intersects(this Rect rect, Rect other)
	{
		if (rect.xMax < other.xMin)
			return false;
		if (rect.xMin > other.xMax)
			return false;
		if (rect.yMax < other.yMin)
			return false;
		if (rect.yMin > other.yMax)
			return false;
		return true;
	}

	/// <summary>
	/// Test if this rect intersects with any rects in the list.
	/// </summary>
	/// <param name="rect">
	/// A <see cref="Rect"/>
	/// </param>
	/// <param name="rects">
	/// A <see cref="IEnumerable(Rect)"/> The list of rects to check against.
	/// </param>
	/// <returns>
	/// A <see cref="System.Boolean"/>
	/// </returns>
	public static bool Intersects(this Rect rect, IEnumerable<Rect> rects)
	{
		foreach (var i in rects)
		{
			if (rect.Intersects(i))
				return true;
		}
		return false;
	}

	/// <summary>
	///  apply Padding using a single float (reduce the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect reduce(this Rect rect, float padding)
	{
		rect.width -= 2 * padding;
		rect.x += padding;
		rect.y += padding;
		rect.height -= 2 * padding;

		return rect;
	}


	/// <summary>
	/// apply Padding using GUIStyle.padding (reduce the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect applyPadding(this Rect rc, GUIStyle style)
	{
		return rc.reduce(style.padding); ;
	}

	/// <summary>
	/// apply Padding using GUIStyle.padding (reduce the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect applyOverflow(this Rect rc, GUIStyle style)
	{
		return rc.enlarge(style.overflow); ;
	}


	/// <summary>
	///  apply Padding using RectOffset (reduce the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect reduce(this Rect rc, RectOffset padding)
	{
		rc.x += padding.left;
		rc.y += padding.top;
		rc.width -= padding.horizontal;
		rc.height -= padding.vertical;

		return rc;
	}


	/// <summary>
	/// apply Padding using BorderRect (reduce the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect reduce(this Rect rc, BorderRect padding)
	{
		rc.x += padding.left;
		rc.y += padding.top;
		rc.width -= padding.horizontal;
		rc.height -= padding.vertical;


		return rc;
	}

	/// <summary>
	/// apply Overflow using a single float (enlarge the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect enlarge(this Rect rect, float overflow)
	{
		rect.x -= overflow;
		rect.y -= overflow;
		rect.width += 2 * overflow;
		rect.height += 2 * overflow;

		return rect;
	}


	/// <summary>
	/// apply Overflow using GUIStyle.overflow (enlarge the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect enlarge(this Rect rc, GUIStyle style)
	{
		return rc.enlarge(style.overflow); ;
	}


	/// <summary>
	/// apply Overflow using RectOffset (enlarge the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect enlarge(this Rect rc, RectOffset overflow)
	{
		rc.x -= overflow.left;
		rc.y -= overflow.top;
		rc.width += overflow.horizontal;
		rc.height += overflow.vertical;

		return rc;
	}


	/// <summary>
	/// apply Overflow using BorderRect (enlarge the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect enlarge(this Rect rc, BorderRect overflow)
	{
		rc.x -= overflow.left;
		rc.y -= overflow.top;

		rc.width += overflow.horizontal;
		rc.height += overflow.vertical;


		return rc;
	}

	/// <summary>
	/// apply offset using Vector2 (move the rect)
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect offset(this Rect rc, Vector2 offset)
	{
		rc.x += offset.x;
		rc.y += offset.y;

		return rc;
	}


	/// <summary>
	/// set x and y to zero
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Rect removeOffset(this Rect rc)
	{
		rc.x += 0;
		rc.y += 0;

		return rc;
	}


	/// <summary>
	//. get the top left corner
	/// </summary>
	/// <returns>the result padded rect</returns>
	public static Vector2 topleft(this Rect rc)
	{
		return new Vector2(rc.x, rc.y);
	}

	/// <summary>
	/// return the rect outside the group
	/// </summary>
	/// <param name="?"></param>
	/// <returns></returns>
	public static Rect ungroup(this Rect rc, Rect group)
	{
		return rc.offset(group.topleft());
	}






}
