﻿using System;
using System.Text;
using UnityEngine;
using System.Security.Cryptography;

/// <summary>
/// Useful tools for string formating
/// </summary>
public class StringTools
{
	public static string ReplaceInvalidFileCharacters(string source, char replaceBy)
	{
		char[] invalids = System.IO.Path.GetInvalidFileNameChars();
		for (int i = 0; i < invalids.Length; i++)
		{
			source = source.Replace(invalids[i], replaceBy);
		}

		return source;
	}

	public static string RemoveInvalidFileCharacters(string source)
	{
		char[] invalids = System.IO.Path.GetInvalidFileNameChars();
		for (int i = 0; i < invalids.Length; i++)
		{
			source = source.Replace(""+invalids[i], "");
		}

		return source;
	}


	/// <summary>
	/// Format a string for file sizes 
	/// ex 1.3 Gb, 8.6 Kb  628 b etc ....
	/// </summary>
	public static string FormatFileSize(long sizeOctet)
	{
		string str;
		if (sizeOctet >> 30 > 0)// Gb
		{
			long Go = sizeOctet >> 30;
			long Mo = (sizeOctet >> 20) & 0x3FF;
			str = Go + "." + Mo + " Gb";
		}
		else if (sizeOctet >> 20 > 0)// Mb
		{
			long Mo = sizeOctet >> 20;
			long Ko = (sizeOctet >> 10) & 0x3FF;

			str = Mo + "." + Ko + " Mb";
		}
		else if (sizeOctet >> 10 > 0)// kb
		{
			long Ko = sizeOctet >> 10;
			long o = sizeOctet & 0x3FF;

			str = Ko + "." + o + " Kb";
		}
		else
		{
			str = sizeOctet + " b";
		}

		return str;
	}


	/// <summary>
	/// format for time delays : ex for chronometers
	/// </summary>
	/// <param name="seconds"></param>
	/// <returns></returns>
	public static string FormatTimeDelay(float seconds)
	{
		int secondsI = (int) seconds;
		

		int min = secondsI/60;
		secondsI = secondsI % 60;

		int hours = min/60;
		min = min%60;
		
		string str;
		if (hours != 0)
            str = String.Format("{0:00}", hours) + ":" + String.Format("{0:00}", min) + " : " + String.Format("{0:00}", secondsI);
		else if (min != 0)
            str = String.Format("{0:00}", min) + " : " + String.Format("{0:00}", secondsI);
		else
            str = String.Format("{0:00}", min) + " : " + String.Format("{0:00}", secondsI);

		return str;
	}


	/// <summary>
	/// encode non url http special chars ex : spaces -> %20
	/// </summary>
	public static string HTMLEncodeSpecialChars(string text)
	{
		System.Text.StringBuilder sb = new System.Text.StringBuilder();
		foreach (char c in text)
		{
			if (c == ' ')
				sb.Append("%20");
			else if (c > 127) // special chars
				sb.Append(String.Format("&#{0};", (int)c));
			else
				sb.Append(c);
		}

		return sb.ToString();
	}

	/// <summary>
	/// Hash an input string and return the hash as a 32 character hexadecimal string.
	/// </summary>
	public static string GetMd5Hash(string input)
	{
		// Create a new instance of the MD5CryptoServiceProvider object.
		MD5 md5Hasher = MD5.Create();

		// Convert the input string to a byte array and compute the hash.
		byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

		// Create a new Stringbuilder to collect the bytes
		// and create a string.
		StringBuilder sBuilder = new StringBuilder();

		// Loop through each byte of the hashed data 
		// and format each one as a hexadecimal string.
		for (int i = 0; i < data.Length; i++)
		{
			sBuilder.Append(data[i].ToString("x2"));
		}

		// Return the hexadecimal string.
		return sBuilder.ToString();
	}

	// progress is [0-1]
	public static string formatPercent(float progress)
	{
		return String.Format("{0:0} %", progress*100);
	}
}