using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// The FramePosition class is a class helping to position 2D frames on the screen.
/// It was formely defined by the 2 classes RectPlacment and FramePlacement.
/// 
/// The dependency to Gesture has been removed 
/// 
/// The usage is quite simple. you define a rect by it's anchor position to the Screen rect or relatively to a parent rect
/// 
/// Defines it's positon using constructors are by altering the public fields : reference and posSize
/// You can tehn get the result Rect using mainRect or relativeRect
/// 
/// If you need padding you can then apply this padding using the applyPadding fonction 
/// 
/// you can check if mouse is over the rect using iMousesOver property
/// 
/// </summary>
[System.Serializable]
public class FramePositon
{	
#region(Main Fields) 

	public enum Anchor
	{
		TopLeft,
		TopCenter,
		TopRight,
		MiddleLeft,
		MiddleCenter,
		MiddleRight,
		BottomLeft,
		BottomCenter,
		BottomRight,
	}

	/// <summary>
	/// The anchor position : Where the Rect is computed from
	/// </summary>
	public Anchor anchor= Anchor.TopLeft;

	/// <summary>
	/// The position relatively to the Anchor Point
	/// </summary>
	public Rect position = new Rect(0, 0, 320, 240);

	/// <summary>
	/// The inner padding. Used for Padding function only
	/// </summary>
	public BorderRect padding = new BorderRect(5,5,20,5);

	/// <summary>
	/// you can use this parameter to change all rect sizes
	/// </summary>
	public static float globalSizeMultiplier = 1;

#endregion

	protected Rect result;

	/// <summary>
	/// The Main constructor
	/// </summary>
	/// <param name="anchor">The anchor position : Where the Rect is computed from</param>
	/// <param name="position">The position relatively to the Anchor Point</param>
	public FramePositon(Anchor anchor, Rect position)
	{
		this.position = position;
		this.anchor = anchor;
	}

	public override string ToString()
	{

		return anchor.ToString() + " - " + position.ToString();
	}

	public FramePositon()
	{
		position = new Rect(0, 0, 100, 100);
		anchor = Anchor.TopLeft;
	}

	/// <summary>
	/// The Main constructor
	/// </summary>
	/// <param name="anchor">The anchor position : Where the Rect is computed from</param>
	/// <param name="position">The position relatively to the Anchor Point</param>
	/// <param name="padding">The inner padding. Used for Padding function only</param>
	public FramePositon(Anchor anchor, Rect relativePosition, BorderRect padding)
	{
		this.position = relativePosition;
		this.anchor = anchor;
		this.padding = padding;
	}

#region (Main access fonctions)

	/// <summary>
	/// Compute the resulting main Rect using Screen as parent relative rect
	/// 
	/// This call should be done in the OnGUI fonction of your class,
	/// it is compued only once even if OnGui is called several time per frame
	/// 
	/// </summary>
	public Rect MainRc
	{
		get
		{
			return RelativeRect(new Rect(0, 0, Screen.width, Screen.height));
		}
	}

	/// <summary>
	/// Return the Main Ract but in camera relative pos (0-1)
	/// </summary>
	public Rect Viewport
	{	get
		{
			Rect rc = MainRc;
			return new Rect(

				rc.x / Screen.width,
				(Screen.height - (rc.y + rc.height)) / Screen.height,
				rc.width / Screen.width,
				rc.height / Screen.height);
		}
	}

	/// <summary>
	/// The Main function
	/// Computes the result rect relativemy to a parent rect (inside it's parent)
	/// </summary>
	/// <param name="mainRc">the parent mainRect</param>
	/// <returns>the result Rect (absolute on the screen)</returns>
	public Rect RelativeRect(Rect mainRc)
	{
		if (Event.current != null && 
			Event.current.type != EventType.Repaint && 
			Event.current.type != EventType.Layout && 
			Event.current.type != EventType.Used)
		{
			// only computed once per frame during the good onGUI
			return result;
		}

		float posX;
		float posY;
		float width;
		float height;

		posX = position.x * globalSizeMultiplier;
		posY = position.y * globalSizeMultiplier;
		width = position.width * globalSizeMultiplier;
		height = position.height * globalSizeMultiplier;

		if (width <= 0)
		{
			width = mainRc.width + width;

		}
		if (height <= 0)
		{
			height = mainRc.height + height;
		}

		switch (anchor)
		{
			case Anchor.TopLeft:
				result = new Rect(posX + mainRc.x, posY + mainRc.y, width, height);
				break;
			case Anchor.TopCenter:
				result = new Rect((mainRc.width - width) / 2 + posX + mainRc.x, posY + mainRc.y, width, height);
				break;
			case Anchor.TopRight:
				result = new Rect(mainRc.width - width - posX + mainRc.x, posY + mainRc.y, width, height);
				break;
			case Anchor.MiddleLeft:
				result = new Rect(posX + mainRc.x, (mainRc.height - height) / 2 + posY + mainRc.y, width, height);
				break;
			case Anchor.MiddleCenter:
				result = new Rect((mainRc.width - width) / 2 + posX + mainRc.x, (mainRc.height - height) / 2 + posY + mainRc.y, width, height);
				break;
			case Anchor.MiddleRight:
				result = new Rect(mainRc.width - width - posX + mainRc.x, (mainRc.height - height) / 2 + posY + mainRc.y, width, height);
				break;
			case Anchor.BottomLeft:
				result = new Rect(posX + mainRc.x, mainRc.height - height - posY + mainRc.y, width, height);
				break;
			case Anchor.BottomCenter:
				result = new Rect((mainRc.width - width) / 2 + posX + mainRc.x, mainRc.height - height - posY + mainRc.y, width, height);
				break;
			case Anchor.BottomRight:
				result = new Rect(mainRc.width - width - posX + mainRc.x, mainRc.height - height - posY + mainRc.y, width, height);
				break;
		}

		return result;
	}


	/// <summary>
	/// Just return the last computed rect (quick access but relativeRect or mainRc should be could before at least once)
	/// </summary>
	/// <returns></returns>
	public Rect LastMainRc
	{
		get { return result; }
	}

#endregion


#region (Padding fonctions)

	/// <summary>
	/// compute the inner Rect without the need of a pre computed rect
	/// </summary>
	/// <returns></returns>
	public Rect innerRect()
	{
		return MainRc.reduce(padding);
	}

	/// <summary>
	/// Sets the padding from a GuiStyle
	/// </summary>
	/// <param name="style"></param>
	public void setPadding(GUIStyle style)
	{
		padding = new BorderRect(style.padding);
	}

	/// <summary>
	/// Apply the padding from a pre compued rect
	/// 
	/// </summary>
	/// <example>
	/// Rect rc = myPosition.mainRC;
	/// 
	/// GUI.Box(rc , "my frame");
	/// 
	/// rc = myPosition.applyPadding(Rect rc)
	/// 
	/// GUILayout.BeginArea(rc);
	/// // layout stuff here
	/// GUILayout.EndArea();
	/// 
	/// </example>
	/// <param name="rc">the pre-computed rect</param>
	/// <returns></returns>
	public Rect applyPadding(Rect rc)
	{
		return rc.reduce(padding);
	}

	/// <summary>
	/// Apply the padding on the last computed rect
	/// And retunr a rect with 0.0 ofsset
	/// </summary>
	///
	/// <returns>local padded rect</returns>
	public Rect localPadded()
	{
		Rect rc = applyPadding(result);
		rc.x -= result.x;
		rc.y -= result.y;
		return rc;
	}
	
	/// <summary>
	/// Static version of apply Padding. It can be usefull when used outside this class
	/// </summary>
	/// <param name="padding">a padiing value used for left, right, top and bottom</param>
	/// <param name="rc">the main unpadded rect</param>
	/// <returns>the result padded rect</returns>
	[Obsolete("Use Rect.reduce instead", false)]
	public static Rect applyPadding(float padding, Rect rc)
	{
		return rc.reduce(padding);
	}

	/// <summary>
	/// Static version of apply Padding using GUIStyle. It can be usefull when used outside this class
	/// </summary>
	/// <param name="style">the style where to get padding</param>
	/// <param name="rc">the main unpadded rect</param>
	/// <returns>the result padded rect</returns>
	[Obsolete("Use Rect.reduce instead", false)]
	public static Rect applyPadding(Rect rc, GUIStyle style)
	{
		return applyPadding(style.padding, rc); ;
	}

	/// <summary>
	/// Static version of apply Padding using GUIStyle. It can be usefull when used outside this class
	/// </summary>
	/// <param name="style">the style where to get padding</param>
	/// <param name="rc">the main unpadded rect</param>
	/// <returns>the result padded rect</returns>
	[Obsolete("Use Rect.reduce instead", false)]
	public static Rect applyPadding(GUIStyle style, Rect rc)
	{
		return applyPadding(style.padding, rc); ;
	}


	/// <summary>
	/// Static version of apply Padding using RectOffset. It can be usefull when used outside this class
	/// </summary>
	/// <param name="padding">the apdding to apply</param>
	/// <param name="rc">the main unpadded rect</param>
	/// <returns>the result padded rect</returns>
	[Obsolete("Use Rect.reduce instead", false)]
	public static Rect applyPadding(RectOffset padding, Rect rc)
	{
		rc.x += padding.left;
		rc.y += padding.top;
		rc.width -= padding.horizontal;
		rc.height -= padding.vertical;

		return rc;
	}

	/// <summary>
	/// Static version of apply Padding using RectOffset. It can be usefull when used outside this class
	/// </summary>
	/// <param name="padding">the apdding to apply</param>
	/// <param name="rc">the main unpadded rect</param>
	/// <returns>the result padded rect</returns>
	[Obsolete("Use Rect.reduce instead", false)]
	public static Rect applyPadding(Rect rc, RectOffset padding)
	{
		return applyPadding(padding, rc);
	}

	/// <summary>
	/// Static version of apply Padding using BorderRect. It can be usefull when used outside this class
	/// </summary>
	/// <param name="padding">the apdding to apply</param>
	/// <param name="rc">the main unpadded rect</param>
	/// <returns>the result padded rect</returns>
	[Obsolete("Use Rect.reduce instead", false)]
	public static Rect applyPadding(BorderRect padding, Rect rc)
	{
		rc.width -= padding.horizontal;
		rc.x += padding.left;
		rc.y += padding.top;
		rc.height -= padding.vertical;

		return rc;
	}
#endregion



#region (Misc Tools)

	/// <summary>
	/// Checks if the mouse cursor is over my rect
	/// </summary>
	public bool isMouseOver
	{
		get
		{
			Vector2 pos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
			bool mouseOver = false;
			// check mouse position
			if (result.Contains(pos))
			{
				mouseOver = true;
			}
			else
			{
				Rect screenRc = new Rect(0, 0, Screen.width, Screen.height);
				if (screenRc.Contains(pos))
				{
					// no check when mouse is out of screen
					mouseOver = false;
				}
			}
			return mouseOver;
		}
	}

#endregion


#region(Inner sub fonctions)

	/// <summary>
	/// change the position of the Rect depending on it's anchor 
	/// </summary>
	/// <param name="deltaPos">the difference position</param>
	public void move(Vector2 deltaPos)
	{
		int posX, posY;
		getPosXY(anchor, out posX, out posY);

		if (posX == 2)
			position.x -= deltaPos.x;
		else
			position.x += deltaPos.x;

		if (posY == 2)
			position.y -= deltaPos.y;
		else
			position.y += deltaPos.y;
	}

	/// <summary>
	/// change the size of the frame depending on a reference position. usefull for resizing windows using corner handles
	/// </summary>
	/// <param name="handlerPos">the position of the handler</param>
	/// <param name="deltaSize">the altered size</param>
	public void alterSize(Anchor handlerPos, Vector2 deltaSize)
	{
		int posFrameX, posFrameY, posHandlerX, posHandlerY;
		getPosXY(anchor, out posFrameX, out posFrameY);
		getPosXY(handlerPos, out posHandlerX, out posHandlerY);

		alterSizeX(posFrameX, posHandlerX, deltaSize.x);
		alterSizeY(posFrameY, posHandlerY, deltaSize.y);
	}

	void alterSizeX(int posFrame, int posHandler, float delta)
	{
		switch (posFrame)
		{
			case 0:
				switch (posHandler)
				{
					case 0:
						position.x += delta;
						position.width -= delta;
						break;
					case 2:
						position.width += delta;
						break;
				}
				break;
			case 1:
				switch (posHandler)
				{
					case 0:
						position.x += delta / 2;
						position.width -= delta;
						break;
					case 2:
						position.x += delta / 2;
						position.width += delta;
						break;
				}
				break;
			case 2:
				switch (posHandler)
				{
					case 0:
						position.width -= delta;
						break;
					case 2:
						position.x -= delta;
						position.width += delta;
						break;
				}
				break;
		}

	}
	void alterSizeY(int posFrame, int posHandler, float delta)
	{
		switch (posFrame)
		{
			case 0:
				switch (posHandler)
				{
					case 0:
						position.y += delta;
						position.height -= delta;
						break;
					case 2:
						position.height += delta;
						break;
				}
				break;
			case 1:
				switch (posHandler)
				{
					case 0:
						position.y += delta / 2;
						position.height -= delta;
						break;
					case 2:
						position.y += delta / 2;
						position.height += delta;
						break;
				}
				break;
			case 2:
				switch (posHandler)
				{
					case 0:

						position.height -= delta;
						break;
					case 2:
						position.y -= delta;
						position.height += delta;
						break;
				}
				break;
		}
	}
	void getPosXY(Anchor handlerPos, out int x, out int y)
	{
		x = y = 0;
		switch (handlerPos)
		{
			case Anchor.TopLeft:
				x = 0;
				y = 0;
				break;
			case Anchor.TopCenter:
				x = 1;
				y = 0;
				break;
			case Anchor.TopRight:
				x = 2;
				y = 0;
				break;

			case Anchor.MiddleLeft:
				x = 0;
				y = 1;
				break;
			case Anchor.MiddleCenter:
				x = 1;
				y = 1;
				break;
			case Anchor.MiddleRight:
				x = 2;
				y = 1;
				break;

			case Anchor.BottomLeft:
				x = 0;
				y = 2;
				break;
			case Anchor.BottomCenter:
				x = 1;
				y = 2;
				break;
			case Anchor.BottomRight:
				x = 2;
				y = 2;
				break;
		}
	}
#endregion


	
}