using UnityEngine;
using System.Collections;


// exactly the same as RectOffset but without the troubles in Unity 4
[System.Serializable]
public class BorderRect  
{
	public float left = 0;
	public float right = 0;
	public float top = 0;
	public float bottom = 0;

	public BorderRect()
	{

	
	}

	public BorderRect(RectOffset rc)
	{
		this.left = rc.left;
		this.top = rc.top;
		this.right = rc.right;
		this.bottom = rc.bottom;
	}

	public BorderRect(float left, float right, float top, float bottom)
	{
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;

	}


	public float vertical { get {return top + bottom;} }
	public float horizontal { get { return left + right; } }

}
