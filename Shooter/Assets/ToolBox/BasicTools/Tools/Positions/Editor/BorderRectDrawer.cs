﻿using UnityEngine;
using System.Collections;
using UnityEditor;

// The property drawer class should be placed in an editor script, inside a folder called Editor.
// Tell the RangeDrawer that it is a drawer for properties with the RangeAttribute.
[CustomPropertyDrawer(typeof(BorderRect))]
public class BorderRectDrawer : PropertyDrawer
{
	float fieldWidth = 130;
	float labelWidth = 70;
	float fieldDeltaSpace = 50;
	int NbLine = 3;
	
	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		//base.OnGUI(position, property, label);

		


		Rect contentPosition = EditorGUI.PrefixLabel(position, label);

		if (NbLine == 4)
		{
			contentPosition.y += EditorGUIUtility.singleLineHeight;
			contentPosition.x -= fieldWidth;
		}

		EditorGUIUtility.labelWidth = labelWidth;

//		EditorGUIUtility.fieldWidth = 100;



		Rect rcField = contentPosition;
		rcField.height = EditorGUIUtility.singleLineHeight;

		rcField.x += fieldDeltaSpace;
		rcField.width = fieldWidth;
		

		EditorGUI.PropertyField(rcField, property.FindPropertyRelative("top"));

		rcField = contentPosition;
		rcField.y += EditorGUIUtility.singleLineHeight;
		rcField.height = EditorGUIUtility.singleLineHeight;

		rcField.width = fieldWidth;

		
		EditorGUI.PropertyField(rcField, property.FindPropertyRelative("left"));

		rcField.x += rcField.width;
		EditorGUI.PropertyField(rcField, property.FindPropertyRelative("right"));


		rcField = contentPosition;

		rcField.height = EditorGUIUtility.singleLineHeight;
		rcField.y += EditorGUIUtility.singleLineHeight*2;
		rcField.x += fieldDeltaSpace;
		rcField.width = fieldWidth;

		EditorGUI.PropertyField(rcField, property.FindPropertyRelative("bottom"));
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		if (Screen.width > 400)
			NbLine = 3;
		else
			NbLine = 4;

		return EditorGUIUtility.singleLineHeight * NbLine;
	}
}