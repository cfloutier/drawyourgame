using UnityEngine;
using System.Collections;
using System.Xml;

public class XmlParsing  {

	public static float parseAttributeFloat(XmlElement el, string name, float defaultValue)
	{
		float value;
		if (!float.TryParse(el.GetAttribute(name), out value))
			value = defaultValue;

		return value;
	}

	public static bool parseAttributeBool(XmlElement el, string name, bool defaultValue)
	{
		bool value;
		if (!bool.TryParse(el.GetAttribute(name), out value))
			value = defaultValue;

		return value;
	}

	public static int parseAttributeInt(XmlElement el, string name, int defaultValue)
	{
		int value;
		if (!int.TryParse(el.GetAttribute(name), out value))
			value = defaultValue;

		return value;
	}

	public static Color parseAttributeColor(XmlElement el, string name)
	{
		return ColorTools.parseColor(el.GetAttribute(name));
	}



}
