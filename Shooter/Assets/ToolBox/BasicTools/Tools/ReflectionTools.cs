﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using System.Reflection;

public class ReflectionTools
{
	public static void CopyComponents(Component source, Component dest)
	{
		foreach (System.Reflection.FieldInfo field in dest.GetType().GetFields())
		{
			try
			{
				//Debug.Log(field.GetValue(script));
				field.SetValue(dest, field.GetValue(source));
			}
			catch (System.FieldAccessException ex)
			{
				Debug.LogWarning("[Reflection] error pasting component field" + field.Name);
				Debug.LogWarning("[Reflection] " + ex);
			}
		}
	}

   
    public static object GetValue(object source, string name)
    {
        if (source == null)
            return null;
        var type = source.GetType();
        var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        if (f == null)
        {
            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p == null)
                return null;
            return p.GetValue(source, null);
        }


        return f.GetValue(source);
    }
}
