﻿using UnityEngine;
using System.Collections;

public class RenderCueSetter : MonoBehaviour 
{
	public enum Layer
	{
		Background = 1000,
		Geometry = 2000,
		Transparent = 3000,
		Overlay = 4000
	} ;
	public Layer layer = Layer.Transparent;
	public Material mat;
	public int renderCue;

	void OnValidate()
	{
		mat.renderQueue = (int)layer + renderCue;
	}
}
