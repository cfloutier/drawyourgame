using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public class BorderLine
{
	public float left = 0;
	public float right = 0;
}


// a rectangle split into 9 parts by its border. Usefull for frames, windows and so on
public class SlicedLine : MonoBehaviour
{
#region Internal_Arrays
    private int[] triangles = 
	{
		1,0,2,
     	1,2,3,
		
		3,4,5,
		3,2,4,

		5,6,7,
		5,4,6
    };
   
#endregion

	public BorderLine uvPos;
	public BorderLine borderSizes;

    public Vector2 size = Vector2.one;
	public Color color = Color.white;
	public Vector2 pivotOffset = Vector2.zero;


	public bool addBoxCollider = false;

#region Mesh Building

    Vector3[] buildVerticles()
    {
		if (size.x < (borderSizes.left + borderSizes.right)) size.x = borderSizes.left + borderSizes.right;
		
        Vector3[] myvertexes =  new Vector3[]
	    {  
			new Vector3(-pivotOffset.x, -pivotOffset.y + size.y, 0),
			new Vector3(-pivotOffset.x, -pivotOffset.y , 0),

			new Vector3(-pivotOffset.x + borderSizes.left, -pivotOffset.y + size.y , 0),
			new Vector3(-pivotOffset.x + borderSizes.left, -pivotOffset.y, 0.0f ),

			new Vector3(-pivotOffset.x+size.x-borderSizes.right, -pivotOffset.y + size.y, 0.0f),
			new Vector3(-pivotOffset.x+size.x-borderSizes.right, -pivotOffset.y, 0),

			new Vector3(-pivotOffset.x+size.x, -pivotOffset.y + size.y, 0.0f),
			new Vector3(-pivotOffset.x+size.x, -pivotOffset.y , 0)
		};


        return myvertexes;
    }


    Vector2[] buildUV()
    {
		BorderLine realUv;

		if (GetComponent<Renderer>() == null || GetComponent<Renderer>().sharedMaterial == null || GetComponent<Renderer>().sharedMaterial.mainTexture == null)
		{
			realUv = new BorderLine();
		}
		else
		{
			Texture2D texture = (Texture2D)GetComponent<Renderer>().sharedMaterial.mainTexture;
			if (texture.width == 0 || texture.height == 0)
			{
				realUv = new BorderLine();
			}
			else
			{
				realUv = new BorderLine();
				realUv.left = uvPos.left / texture.width;
				realUv.right = uvPos.right / texture.width;
			}
		}



		Vector2[] uvs = new Vector2[]
	    {  
			new Vector2(0.0f, 1),
			new Vector2(0.0f, 0),

			new Vector2(realUv.left, 1),
			new Vector2(realUv.left, 0),

			new Vector2(1-realUv.right, 1),
			new Vector2(1-realUv.right, 0),

			new Vector2(1, 1),
			new Vector2(1, 0)
		};

		return uvs;
    }
	Color[] buildColors()
	{
		return new Color[]
	    {
	        color,
	        color,
	        color,
	        color,

            color,
	        color,
	        color,
	        color
        };
	}

	Vector3[] buildNormals()
	{
		return new Vector3[]
	    {
	        Vector3.up,
	        Vector3.up,
	        Vector3.up,
	        Vector3.up,
	        Vector3.up,
	        Vector3.up,
	        Vector3.up,
	        Vector3.up
        };
	
	}	

	MeshFilter BuildOrGetMeshFilter()
	{
		MeshFilter filter = (MeshFilter)gameObject.GetComponent(typeof(MeshFilter));

		if (filter == null)
		{
			filter = gameObject.AddComponent<MeshFilter>();
			MeshRenderer renderer = (MeshRenderer)gameObject.GetComponent(typeof(MeshRenderer));
			if (renderer == null)
				renderer = gameObject.AddComponent<MeshRenderer>();
		}

		return filter;
	}

	void BuildMesh(MeshFilter filter)
	{
		Mesh mesh = new Mesh();
		mesh.name = "SlicedLine";

		mesh.vertices = buildVerticles();
		mesh.uv = buildUV();
		mesh.triangles = triangles;
		mesh.colors = buildColors();
		mesh.normals = buildNormals();

		mesh.RecalculateBounds();

		filter.mesh = mesh;
	}


#endregion

	// Apply is needed when parameters are changed from the script
	public void apply()
	{
		MeshFilter filter = BuildOrGetMeshFilter();
	
		BuildMesh(filter);
		BoxCollider collider = gameObject.GetComponent<BoxCollider>();
		if (collider != null)
			DestroyImmediate(collider);

		if (addBoxCollider)
		{
			gameObject.AddComponent<BoxCollider>();
		}
		
	}

	void Start()
	{

	}
	void OnValidate()
	{
		if (enabled)
			apply();
	}
}
