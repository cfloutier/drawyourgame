﻿using UnityEngine;
using System.Collections;


// use only in editor mode
// set the render cue af the materials depending on the order in the list
[ExecuteInEditMode]
public class RenderCueEditor : MonoBehaviour
{
	int baseRenderCue = 3000;

	[System.Serializable]
	public class RenderCueSettings
	{
		public Material[] materials;
	}

	[Array]
	public RenderCueSettings[] settings;

	[Button("Apply", "apply")]
	public bool bt;



	void apply()
	{
		int cue = baseRenderCue;
		for (int i = 0; i < settings.Length; i++)
		{
			if (settings[i] != null)
			{
				foreach (Material mat in settings[i].materials)
				{
					mat.renderQueue = cue;
				}

				cue++;
			}
		}
	}

}
