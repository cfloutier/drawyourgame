﻿using UnityEngine;
using System.Collections;

public class CircleLine : MonoBehaviour
{
	public float widthLine = 1;
	public float radius = 10;
	public int NbNodes = 50;
	public Color color = Color.black;
	public Material mat;



	// Apply is needed when parameters are changed from the script  
	public void apply()
	{
		if (NbNodes < 3)
			NbNodes = 3;

		buildMesh();
		GetComponent<Renderer>().sharedMaterial = mat;
	}

	void Start()
	{

	}
	void OnValidate()
	{
		if (enabled)
			apply();
	}

	MeshFilter BuildOrGetMeshFilter()
	{
		MeshFilter filter = (MeshFilter)gameObject.GetComponent(typeof(MeshFilter));

		if (filter == null)
		{
			filter = gameObject.AddComponent<MeshFilter>();
			MeshRenderer renderer = (MeshRenderer)gameObject.GetComponent(typeof(MeshRenderer));
			if (renderer == null)
				renderer = gameObject.AddComponent<MeshRenderer>();
		}

		return filter;
	}

	Vector3[] buildVerticles()
	{
		Vector3[] myvertexes = new Vector3[(NbNodes + 1) * 2];

		float rad = 0;
		float delatRad = (Mathf.PI * 2) / NbNodes;

		//		Debug.Log("delatRad " + delatRad);
		for (int i = 0; i < NbNodes + 1; i++)
		{
			Vector3 point = new Vector3(Mathf.Cos(rad), 0, Mathf.Sin(rad));

			Vector3 inPoint = point * (radius - widthLine / 2);
			Vector3 outPoint = point * (radius + widthLine / 2);
			myvertexes[i * 2] = inPoint;
			myvertexes[i * 2 + 1] = outPoint;

			rad += delatRad;
		}

		return myvertexes;
	}
	public Vector2[] uvs;
	Vector2[] buildUV()
	{
		uvs = new Vector2[(NbNodes + 1) * 2];

		float u = 0;
		float deltaU = 1f / (NbNodes);

		for (int i = 0; i < NbNodes + 1; i++)
		{
			uvs[i * 2] = new Vector2(u, 1);
			uvs[i * 2 + 1] = new Vector2(u, 0);

			u += deltaU;
		}

		return uvs;
	}


	int[] buildTriangles()
	{
		int[] triangles = new int[6 * NbNodes];

		int index = 0;
		for (int i = 0; i < NbNodes; i++)
		{
			triangles[i * 6] = index;
			triangles[i * 6 + 1] = index + 3;
			triangles[i * 6 + 2] = index + 1;

			triangles[i * 6 + 3] = index;
			triangles[i * 6 + 4] = index + 2;
			triangles[i * 6 + 5] = index + 3;

			index += 2;
		}

		return triangles;
	}

	Color[] buildColors()
	{
		Color[] colors = new Color[(NbNodes + 1) * 2];
		for (int i = 0; i < colors.Length; i++)
			colors[i] = color;

		return colors;
	}

	Vector3[] buildNormals()
	{
		Vector3[] normals = new Vector3[(NbNodes + 1) * 2];
		for (int i = 0; i < normals.Length; i++)
			normals[i] = Vector3.up;

		return normals;
	}

	void buildMesh()
	{
		MeshFilter filter = BuildOrGetMeshFilter();

		Mesh mesh = new Mesh();
		mesh.name = "CircleLine";

		mesh.vertices = buildVerticles();
		mesh.uv = buildUV();
		mesh.triangles = buildTriangles();
		mesh.colors = buildColors();
		mesh.normals = buildNormals();

		mesh.RecalculateBounds();

		filter.mesh = mesh;
	}

}
