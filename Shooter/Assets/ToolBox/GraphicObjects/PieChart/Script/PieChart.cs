﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class PieChart : MonoBehaviour
{
	[System.Serializable]
	public class PieChartValue
	{
		// percent value (0-100)
		public float percent;
		public float value;
		public string name;
		public Color color;
	}

	[Array]
	public PieChartValue[] values;

	// precision in degree
	public float precision = 1;

	public float radius = 1;
	public float explosion = 0;
	public float height = 1;

	public bool buildBottom = false;
	public Material mat;

	public bool colorPerVertex = false;

	void Start()
	{

	}
	void OnValidate()
	{
		if (enabled)
			apply();
	}

	[Button("Random Colors", "randomColors")]
	public bool bt0;

	[Button("Random Values", "randomValues")]
	public bool bt1;

	[Button("Set Names", "setNames")]
	public bool bt4;

	[Button("Fill Last", "fillLast")]
	public bool bt2;

	[Button("Build Slices", "buildSlices")]
	public bool bt3;

	public void randomColors()
	{
		Color[] colors = ColorTools.getRandomColorArray(values.Length);
		for (int i = 0; i < values.Length; i++)
		{
			values[i].color = colors[i];
		}

		apply();
	}

	public void randomValues()
	{
		float baseValue = 100f / values.Length;

		for (int i = 0; i < values.Length - 1; i++)
		{
			float value = baseValue + Random.Range(-baseValue / 2, baseValue / 2);
			values[i].percent = value;
		}

		fillLast();
	}

	public void setNames()
	{
		for (int i = 0; i < values.Length - 1; i++)
		{

			values[i].name = "Item # " + (i + 1).ToString("00");
		}
	}

	public void fillLast()
	{
		float last = 100f;
		for (int i = 0; i < values.Length - 1; i++)
		{
			last -= values[i].percent;
		}

		values[values.Length - 1].percent = last;

		apply();
	}

	string getSliceName(int index)
	{
		return "Slice #" + index.ToString("00");
	}

	public PieChartSlice getSlice(int index)
	{
		string name = getSliceName(index);
		Transform tr = transform.FindChild(name);
		if (tr == null)
			return null;

		return tr.GetComponent<PieChartSlice>();
	}

	public void buildSlices()
	{
		transform.cleanChildren();
		for (int i = 0; i < values.Length; i++)
		{
			GameObject newGo = new GameObject();
			newGo.name = getSliceName(i);
			newGo.transform.parent = transform;
			newGo.transform.localScale = Vector3.one;
			newGo.transform.localPosition = Vector3.zero;
			newGo.transform.localRotation = Quaternion.identity;
			newGo.AddComponent<PieChartSlice>();
		}

		apply();
	}

	public void apply()
	{
		float angle = 0;

		if (explosion < 0)
			explosion = 0;

		if (radius < 0)
			radius = 0;

		if (height < 0)
			height = 0;

		for (int i = 0; i < values.Length; i++)
		{
			PieChartSlice slice = getSlice(i);
			if (slice == null)
			{
				Debug.Log("slice not build yet, please use build Slice fct");
			}

			float endAngle = angle + values[i].percent * 3.6f;

			float midAngle = (angle + endAngle) / 2;


			slice.transform.localPosition = new Vector3(
				Mathf.Cos(Mathf.Deg2Rad * midAngle), 0,

				Mathf.Sin(Mathf.Deg2Rad * midAngle)

				) * explosion;

			slice.value = new PieChartSlice.PieSliceValue();

			slice.value.startAngle = angle;
			slice.value.endAngle = endAngle;

			slice.precision = precision;
			slice.radius = radius;
			slice.mat = mat;
			slice.colorPerVertex = colorPerVertex;
			slice.height = height;

			angle = endAngle;
			slice.value.color = values[i].color;

			slice.apply();
		}
	}

}
