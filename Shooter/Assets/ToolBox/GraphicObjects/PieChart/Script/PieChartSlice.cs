﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PieChartSlice : MonoBehaviour
{
	[System.Serializable]
	public class PieSliceValue
	{
		// in degree
		public float startAngle;
		public float endAngle;
		public Color color;
	}

	public PieSliceValue value;

	// precision in degree
	public float precision = 1;
	public float radius = 1;
	public bool buildBottom = false;
	public Material mat;
	public bool colorPerVertex = true;

	public float height = 1;

	void Start()
	{

	}
	void OnValidate()
	{
		if (enabled)
			apply();
	}

	public void apply()
	{
		if (precision < 1f)
			precision = 1f;

		if (height < 0)
			height = 0;

		MeshFilter filter = BuildOrGetMeshFilter();
		BuildMesh(filter);
	}

	void BuildMesh(MeshFilter filter)
	{
		Mesh mesh = new Mesh();
		mesh.name = "PieSlice";

		vertices = new List<Vector3>();
		triangles = new List<int>();
		colors = new List<Color>();
		uv = new List<Vector2>();
		normals = new List<Vector3>();

		float[] angles = outLineAngles();

		buildTopVertex(angles);
		if (height > 0)
		{
			if (buildBottom)
				buildBottomVertex(angles);
			buildSideVertex(angles);
		}

		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.colors = colors.ToArray();
		mesh.normals = normals.ToArray();

		mesh.RecalculateBounds();

		filter.mesh = mesh;
	}

	List<Vector3> vertices;
	List<int> triangles;
	List<Color> colors;
	List<Vector2> uv;
	List<Vector3> normals;

	float[] outLineAngles()
	{
		List<float> angles = new List<float>();
		float angle = value.startAngle;
		while (angle < value.endAngle)
		{
			if (angle > value.endAngle - precision / 2)
			{
				// skip last if too close from end point
				break;
			}

			angles.Add(angle);
			angle += precision;
		}

		angles.Add(value.endAngle);

		return angles.ToArray();

	}

	void buildTopVertex(float[] angles)
	{
		int startIndex = vertices.Count;

		vertices.Add(Vector3.up * height);
		colors.Add(value.color);
		uv.Add(new Vector2(0, 1));
		normals.Add(Vector3.up);

		for (int i = 0; i < angles.Length; i++)
		{
			float angle = angles[i];

			Vector3 point = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle) * radius, height, Mathf.Sin(Mathf.Deg2Rad * angle) * radius);
			vertices.Add(point);
			colors.Add(value.color);
			uv.Add(new Vector2(0, 1));
			normals.Add(Vector3.up);

		}

		int nbParts = angles.Length - 1;


		for (int i = 0; i < nbParts; i++)
		{
			triangles.Add(startIndex);
			triangles.Add(startIndex + i + 2);
			triangles.Add(startIndex + i + 1);
		}
	}

	void buildBottomVertex(float[] angles)
	{
		int startIndex = vertices.Count;


		vertices.Add(Vector3.zero);
		colors.Add(value.color);
		uv.Add(Vector2.zero);
		normals.Add(-Vector3.up);

		for (int i = 0; i < angles.Length; i++)
		{
			float angle = angles[i];

			Vector3 point = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle) * radius, 0, Mathf.Sin(Mathf.Deg2Rad * angle) * radius);
			vertices.Add(point);
			colors.Add(value.color);
			uv.Add(Vector2.zero);
			normals.Add(-Vector3.up);

		}

		int nbParts = angles.Length - 1;


		for (int i = 0; i < nbParts; i++)
		{
			triangles.Add(startIndex);
			triangles.Add(startIndex + i + 1);
			triangles.Add(startIndex + i + 2);
		}
	}


	void buildSideVertex(float[] angles)
	{

		int startIndex = vertices.Count;

		// start with out side
		Vector3 point1, point2;
		float angle;

		for (int i = 0; i < angles.Length; i++)
		{
			angle = angles[i];

			point1 = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), 0, Mathf.Sin(Mathf.Deg2Rad * angle));

			// normal is simpler toc ompute without raius involved
			normals.Add(point1);
			normals.Add(point1);

			point1 = point1 * radius;
			point2 = point1;
			point2.y = height;

			vertices.Add(point1);
			vertices.Add(point2);

			colors.Add(value.color);
			colors.Add(value.color);

			uv.Add(Vector2.zero);

			uv.Add(new Vector2(0, 1));
		}

		nbParts = angles.Length - 1;

		for (int i = 0; i < nbParts; i++)
		{
			int first = i * 2;
			triangles.Add(startIndex + first);
			triangles.Add(startIndex + first + 1);
			triangles.Add(startIndex + first + 3);
			triangles.Add(startIndex + first);
			triangles.Add(startIndex + first + 3);
			triangles.Add(startIndex + first + 2);
		}

		// inner faces 1
		startIndex = vertices.Count;

		angle = value.startAngle - 90;
		Vector3 normal = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), 0, Mathf.Sin(Mathf.Deg2Rad * angle));
		normals.Add(normal);
		normals.Add(normal);
		normals.Add(normal);
		normals.Add(normal);

		colors.Add(value.color);
		colors.Add(value.color);
		colors.Add(value.color);
		colors.Add(value.color);

		uv.Add(Vector2.zero);
		uv.Add(new Vector2(0, 1));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));

		angle = value.startAngle;
		point1 = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), 0, Mathf.Sin(Mathf.Deg2Rad * angle)) * radius;

		vertices.Add(Vector3.zero);
		vertices.Add(Vector3.up * height);
		vertices.Add(point1);
		point2 = point1;
		point2.y = height;
		vertices.Add(point2);

		triangles.Add(startIndex);
		triangles.Add(startIndex + 1);
		triangles.Add(startIndex + 3);
		triangles.Add(startIndex);
		triangles.Add(startIndex + 3);
		triangles.Add(startIndex + 2);

		// second inner face
		startIndex = vertices.Count;

		angle = value.endAngle + 90;
		normal = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), 0, Mathf.Sin(Mathf.Deg2Rad * angle));
		normals.Add(normal);
		normals.Add(normal);
		normals.Add(normal);
		normals.Add(normal);

		colors.Add(value.color);
		colors.Add(value.color);
		colors.Add(value.color);
		colors.Add(value.color);

		uv.Add(Vector2.zero);
		uv.Add(new Vector2(0, 1));
		uv.Add(new Vector2(1, 0));
		uv.Add(new Vector2(1, 1));

		angle = value.endAngle;
		point1 = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle), 0, Mathf.Sin(Mathf.Deg2Rad * angle)) * radius;

		vertices.Add(point1);
		point2 = point1;
		point2.y = height;
		vertices.Add(point2);

		vertices.Add(Vector3.zero);
		vertices.Add(Vector3.up * height);

		triangles.Add(startIndex);
		triangles.Add(startIndex + 1);
		triangles.Add(startIndex + 3);
		triangles.Add(startIndex);
		triangles.Add(startIndex + 3);
		triangles.Add(startIndex + 2);
	}

	public int nbParts;

	MeshFilter BuildOrGetMeshFilter()
	{
		MeshFilter filter = (MeshFilter)gameObject.GetComponent(typeof(MeshFilter));
		MeshRenderer renderer = (MeshRenderer)gameObject.GetComponent(typeof(MeshRenderer));

		if (filter == null)
		{
			filter = gameObject.AddComponent<MeshFilter>();
		}

		if (renderer == null)
			renderer = gameObject.AddComponent<MeshRenderer>();

		if (colorPerVertex)
			renderer.sharedMaterial = mat;
		else
		{
			Material newMat = (Material)GameObject.Instantiate(mat);
			newMat.color = value.color;
			renderer.sharedMaterial = newMat;
		}

		return filter;
	}
}
