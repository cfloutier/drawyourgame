using UnityEngine;
using System.Collections;

public class SimpleTurnTable : MonoBehaviour 
{
	[System.Serializable]
	public class Parameters
	{
		// nos param�tres de sensibilit� 
		public float mouseSensibility = 1;
		public float wheelSensibility = 2;

		public float amortissementRotation = 2;
		public float lissageZoom = 1;

		// les bornes de la distance  
		public Vector2 minMaxDistance = new Vector2(1, 10);

		// les bornes de la rotation verticale
		public Vector2 minMaxY = new Vector2(-80, 80);
	}
	public Parameters parameters;


	public Transform centerPoint;

	// la distance actuelle
	float distance;
	// la distance voulue (destination de notre animation)
	float wantedDistance;

	
	// la vitesse angulaire
	Vector2 speed = Vector2.zero;
	
	bool mouseDown = false;
	Vector3 lastMousePos = Vector3.zero;
	//float clicTime;
	// Update is called once per frame
	void Update () 
	{
		// r�cup�ration des mouvement de la souris
		if (Input.GetMouseButton(0))
		{
		//	Debug.Log("Input.mousePosition.y " + Input.mousePosition.y);
			// le bouton de la souris est press�
			if (!mouseDown )
			{
				mouseDown = true;
				// on enregistre la position de la souris quand on commence le drag
				lastMousePos = Input.mousePosition;

			}
			else
			{
				Vector2 deltaMouse = Input.mousePosition - lastMousePos;
				lastMousePos = Input.mousePosition;

				// la vitesse est corrig�e selon les action de la souris
				speed.x += deltaMouse.x * parameters.mouseSensibility;
				speed.y -= deltaMouse.y * parameters.mouseSensibility;
			}
		}
		else
			mouseDown = false;

		/////////////////////////// calcul de la distance de la camera ///////////////////////////

		// on r�cup�re les actions sur la molette de la souris
		float wheel = Input.GetAxis("Mouse ScrollWheel");
		// on modifie alors la distance d�sir�e
		wantedDistance += wheel * parameters.wheelSensibility;
			
		// limitation de la distance  
		wantedDistance = Mathf.Clamp(wantedDistance, 
			parameters.minMaxDistance.x, 
			parameters.minMaxDistance.y);

		// enfin calcul liss� de la distance effective
		distance = Mathf.Lerp(distance, wantedDistance, parameters.lissageZoom * Time.deltaTime);

		/////////////////////////// calcul de la position de la camera ///////////////////////////
		// la vitesse est corrig� dans la focntion OnMouseDrag

		// on r�cup�re l'oirentation actuelle de la camera
		Vector3 euler = transform.rotation.eulerAngles;
		// on al corrige avec notre vitesse
		euler.x += speed.y * Time.deltaTime;
		euler.y += speed.x * Time.deltaTime;

		// on borne l'inclinaison en X
		euler.x = ClampAngle(euler.x, parameters.minMaxY.x, parameters.minMaxY.y);

		// on recalcule la position en prenant en compte la distance actuelle 
		// pour cela on calcule la position en multipliant la rotation de la camera par un vecteur arri�re d�pendant de la distance
		Quaternion rotation = Quaternion.Euler(euler);
		Vector3 position = rotation * new Vector3(0F, 0F, -distance) + centerPoint.position;

		// amortissement de la vitesse
		speed = Vector2.Lerp(speed, Vector2.zero, parameters.amortissementRotation * Time.deltaTime);

		// affectation de LayerMask position et de LayerMask rotation calcul�e plus haut
		transform.rotation = rotation;
        transform.position = position;
	}

	// fonction permettant de borner sans erreur des valeur angulaire
	// 
	float ClampAngle(float angle, float min, float max)
	{
		if (angle < -180)
			angle += 360;
		if (angle > 180)
			angle -= 360;

		return Mathf.Clamp(angle, min, max);
	}

	void OnEnable()
	{
		speed = Vector2.zero;
		if (centerPoint != null)
		{
			// on initialise la distance sur la valeur de d�part.
			distance = (centerPoint.position - transform.position).magnitude;
		}

		speed = Vector2.zero;
		wantedDistance = distance;
		distance = wantedDistance;
	}




}
