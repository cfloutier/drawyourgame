using UnityEngine;
using System.Collections;
/*
 Fly Mode using only mouse and keyborad inputs.
 * Can't be configured (hardcoded) but does not need any other manager
 */

public class SimpleFly : MonoBehaviour 
{
	public float mainSpeed = 2; //regular speed
	public float shiftMultiplier  = 2; //multiplied by how long shift is held.  Basically running
	
	public float camSens  = 0.25f; //How sensitive it with mouse

	bool mouseGetted = false;
	private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
	

	void Update  () 
	{
		if (Input.GetMouseButton(0))
		{
			if (mouseGetted)
			{
				lastMouse = Input.mousePosition - lastMouse;
				lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
				lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
				transform.eulerAngles = lastMouse;
				lastMouse = Input.mousePosition;
			}
			else
			{
				mouseGetted = true;
				lastMouse = Input.mousePosition;
			}
		}
		else
			mouseGetted = false;
		//Mouse & camera angle done.  

		//Keyboard commands
		float f  = 0.0f;
		Vector3 p = GetBaseInput(); 
		if (Input .GetKey  (KeyCode.LeftShift))
		{
			p = p * mainSpeed * shiftMultiplier; 
		}
		else
		{
			
			p = p * mainSpeed;
		}
	    
		p = p * Time .deltaTime ;
		if (Input .GetKey (KeyCode.Space ))
		{ 
			//If player wants to move on X and Z axis only 
			f = transform.position.y; 
			transform.Translate (p);
			Vector3 pos = transform.position;
			pos.y = f; 
			transform.position =pos;
		}
		else
		{
			transform.Translate ( p); 
		}
	}


	private Vector3 GetBaseInput()  
	{ 
		//returns the basic values, if it's 0 than it's not active.
		Vector3 p_Velocity = Vector3.zero;
		if (Input .GetKey  (KeyCode.Z)){
			p_Velocity += Vector3.forward;
		}
		if (Input .GetKey  (KeyCode.S)){
			p_Velocity -= Vector3.forward;
		}
		if (Input .GetKey  (KeyCode.Q)){
			p_Velocity += Vector3.left;
		}
		if (Input .GetKey  (KeyCode.D)){
			p_Velocity += Vector3.right;
		}
		if (Input.GetKey(KeyCode.PageUp))
		{
			p_Velocity += Vector3.up;
		}
		if (Input.GetKey(KeyCode.PageDown))
		{
			p_Velocity += Vector3.down;
		}
		return p_Velocity;
	}
}
